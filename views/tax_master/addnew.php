<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add Tax</h3>
	</div>
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=tax_master&task=show"><i class="fa fa-list" aria-hidden="true"></i> Tax List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Tax</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Tax</li>
          <?php } ?>
          </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);  
                header( "refresh:1;url=index.php?control=tax_master&task=show" );     
	   }?>              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
                
                <div class="col-md-3">
                <div class="form-group" style="margin-top:5%;">         
                GST Type :
                </div></div>
                <div class="col-md-6"><div class="form-group">
                  <label class="radio-inline"> <input type="radio" id="watch-me" value="GST" <?php echo ($result['tax']=="GST") ?  "checked" : "" ;  ?> name="gst_type" required="required"> GST</label>
                  <label class="radio-inline"> <input type="radio" value="IGST" <?php echo ($result['tax']=="IGST") ?  "checked" : "" ;  ?> name="gst_type" required="required"> IGST</label>
                </div></div>
                
			<div class="clearfix"></div>
                <div class="col-md-3">
                <div class="form-group" style="margin-top:5%;"> 
                GST Tax % :
                </div></div>
                <div class="col-md-6"><div class="form-group">
                <input class="form-control" type="text" name="gst" id="gst" pattern="[-+]?[0-9]*[.,]?[0-9]+" value="<?php echo $result['gst']; ?>"  maxlength="2" required />
                <span id="msggst" class="msg"></span>
                </div></div>
			   <div class="clearfix"></div>
               <?php if($result['sgst']=='') { ?>
				<div id="show-me" style="display: none;">
                <?php } else { ?>
                <div id="show-me">
                <?php } ?>
                <div class="col-md-3">
                <div class="form-group" style="margin-top:5%;"> 
                SGST :
                </div></div>
                <div class="col-md-6"><div class="form-group">
                <input class="form-control" type="text" name="sgst" id="sgst" value="<?php echo $result['sgst']; ?>" maxlength="2"/>
                <span id="msgsgst" class="msg"></span>
                </div></div>
			    <div class="clearfix"></div>

                <div class="col-md-3">
                <div class="form-group" style="margin-top:5%;"> 
                CGST :
                </div></div>
                <div class="col-md-6"><div class="form-group">
                <input class="form-control" type="text" name="cgst" id="cgst" value="<?php echo $result['cgst']; ?>" required maxlength="2"/>
                <span id="msgcgst" class="msg"></span>
                </div></div>
                <?php if($result['sgst']!='') { ?>
                </div>
                <?php } else { ?>
                </div>
                <?php } ?>
                <div class="clearfix"></div>
                
       
					    
					<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>
					                   

					<input type="hidden" name="control" value="tax_master"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('name').value == '') { 
			document.getElementById('msgname').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgname').innerHTML = "";
		}
		
		

			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>
<script>

$("#gst").keyup( function(){
     var gsts = $("#gst").val();	
    var value = gsts/2;
        if (!isNaN(value) && value !== Infinity) {
           // $("#sgstax").text(value);		  
		    $("input#sgst").val(value);				  
		    $("input#cgst").val(value);
        }
});


$(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'watch-me') {
            $('#show-me').show();           
       }

       else {
            $('#show-me').hide();   
       }
   });
});
</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   