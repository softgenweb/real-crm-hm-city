<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">             
                <div class="box-header">
<h3 class="box-title">View Interest</h3>
<?php foreach($results as $result) { }  ?>
<a href="index.php?control=tax_master&task=addnew_interest" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Interest</a>
 <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
         
Total Interest : <?php echo $no_of_row; ?>
</p>
<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_interest.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a>

                </div><!-- /.box-header -->
                <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Interest List</li>
          </ol>
          
                <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
          
                <div class="box-body">
                 <div>
                 <div class="divoverflow">
                
   <form name="link_feature" action="index.php" method="post" >
                  <table id="example1-1" class="table table-bordered table-striped">
                   <thead>
					<tr>
                        <th class="width_srno" width="15"><div align="left">S.No.</div></th>						
                        <th class="width_normal"><div align="left">Interest (%)</div></th>
                        <th class="width_normal"><div align="left">Action</div></th>
					  </tr>
				</thead>
                 <tbody>
				<?php
					if($results) {
						$countno = ($page-1)*$tpages;
						$i=0;
						foreach($results as $result){ 
						$i++;
						$countno++;
		
						($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad"
					?>
	
						<tr>
							<td class="<?php echo $class; ?>"><div align="left"><?php echo $countno; ?></div></td>
				
							<td class="<?php echo $class; ?>"><div align="left"><?php echo $result['name'].' %' ; ?></div></td>

				<td align="left">
<a href="index.php?control=tax_master&task=addnew_interest&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
<?php
            if($result['status']==1){  ?>
            <a href="index.php?control=tax_master&task=interest_status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
            <?php } else { ?>
            <a href="index.php?control=tax_master&task=interest_status&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:pointer;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
            <?php } ?>
<!--<a href="index.php?control=regional_hierarchy&task=delete&id=<?php echo $result['id']; ?>" title="Delete" onclick="return confirm('Are you sure want to Delete?')"><img src="images/del.png" alt="Delete" title="Delete" /></a>
&nbsp; &nbsp;
<a onclick="return confirm('Are you sure you want to Change Password ?')" href="index.php?control=regional_hierarchy&task=changepassword&id=<?php echo $result['id']; ?>"><img src="images/reload.png" height="15" width="15" alt="Reset" title="Reset" /></a>-->
						  </td>
                        
                     </tr>
						<?php }  }else{?>
                        <?php } ?>
                    </tbody>
                    
                  </table>
                  </form>
                  </div>
                </div><!-- table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            
            <!--================ Second Table ================-->
        
 
          </div><!-- /.row -->
  
<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   