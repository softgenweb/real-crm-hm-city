<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add Late Payment</h3>
	</div>
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=tax_master&task=show"><i class="fa fa-list" aria-hidden="true"></i> Late Payment List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Late Payment</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Late Payment</li>
          <?php } ?>
          </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);  
                header( "refresh:1;url=index.php?control=tax_master&task=show_late_payment" );     
	   }?>              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               
		
                <div class="col-md-3">
                <div class="form-group" style="margin-top:5%;"> 
                Month(in no.):
                </div></div>
                <div class="col-md-6"><div class="form-group">
                <input class="form-control" type="text" name="month" id="month" pattern="[-+]?[0-9]*[.,]?[0-9]+" value="<?php echo $result['month']; ?>"  maxlength="2" required />
                <span id="msggst" class="msg"></span>
                </div></div>
        <div class="clearfix"></div>
              <div class="col-md-3">
                <div class="form-group" style="margin-top:5%;">         
                Type :
                </div></div>
                <div class="col-md-6"><div class="form-group">
                  <label class="radio-inline"> <input type="radio" value="amount" <?php echo ($result['type']=="amount") ?  "checked" : "" ;  ?> name="type" required="required"> Amount</label>
                  <label class="radio-inline"> <input type="radio" value="percent" <?php echo ($result['type']=="percent") ?  "checked" : "" ;  ?> name="type" required="required"> Percent</label>
                </div></div>
			  <div class="clearfix"></div>
         
          <div class="col-md-3">
                <div class="form-group"  id="amount" style="margin-top:5%;"> 
                Amount:
                </div>
                <div class="form-group" id="percent" style="margin-top:5%;"> 
                Percent:
                </div></div>
                <div class="col-md-6" id="input_box"><div class="form-group">
                <input class="form-control" type="text" name="amount" id="amount" pattern="[-+]?[0-9]*[.,]?[0-9]+" value="<?php echo $result['amount']; ?>"  maxlength="8" required />
                <span id="msggst" class="msg"></span>
                </div></div>
              
        <div class="clearfix"></div>
       
      <!--   <div class="col-md-3">
            <div class="form-group" id="percent" style="margin-top:5%;"> 
                Percent:
                </div></div>
                <div class="col-md-6"><div class="form-group">
                <input class="form-control" type="text" name="percent" id="percent" pattern="[-+]?[0-9]*[.,]?[0-9]+" value="<?php echo $result['percent']; ?>"  maxlength="2" required />
                <span id="msggst" class="msg"></span>
                </div></div> -->
					     <div class="clearfix"></div>
            


					<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>
					                   

					<input type="hidden" name="control" value="tax_master"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save_late_payment"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('name').value == '') { 
			document.getElementById('msgname').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgname').innerHTML = "";
		}
		
		

			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>
<script>

$("#gst").keyup( function(){
     var gsts = $("#gst").val();	
    var value = gsts/2;
        if (!isNaN(value) && value !== Infinity) {
           // $("#sgstax").text(value);		  
		    $("input#sgst").val(value);				  
		    $("input#cgst").val(value);
        }
});


$(document).ready(function() {
  $('#amount').hide();
  $('#percent').hide();  
  $('#input_box').hide();  
  $('input[type="radio"]').click(function(){
    if($(this).val() == 'amount') {
        $('#amount').show(); 
        $('#input_box').show(); 
        $('#percent').hide();           
    }else if($(this).val() == 'percent'){
        $('#amount').hide();  
        $('#input_box').show();  
        $('#percent').show();  
       }
   });
});

/*=============*/
$(document).ready(function() {
    if($('input[type="radio"]').val() == 'amount') {
        $('#amount').show(); 
        $('#input_box').show(); 
        $('#percent').hide();           
    }else if($('input[type="radio"]').val() == 'percent'){
        $('#amount').hide();  
        $('#input_box').show();  
        $('#percent').show();  
       }
});
</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   