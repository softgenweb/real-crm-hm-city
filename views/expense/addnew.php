<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add New Expense</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=expense&task=show"><i class="fa fa-list" aria-hidden="true"></i> Expense List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Expense</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Expense</li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
            
			              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">

					
							<div class="col-md-4">
                           <div class="form-group center_text">
								<label>Expense Name</label>
							</div>	
                            </div>
							<div class="col-md-5"><div class="form-group">
								<input type="text" value="<?php echo $result['expense_name']; ?>" id="expense_name" name="expense_name" class="form-control"  required="">
								<span class="msgValid" id="msgcountry_name"></span>
							</div>
						</div>					
                           <div class="clearfix"></div>
                           
                           
                           
					<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>                   

					<input type="hidden" name="control" value="expense"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">
<?php if($result['rate'] !=''){ ?>
    window.load = rate_box();
<?php } ?>


function rate_box(){
if(document.getElementById("expense_name").value == 'Electricity') {

    document.getElementById("expense").classList.remove('col-md-4');
    document.getElementById("expense").classList.add('col-md-2');
    document.getElementById("expense_rate").style.display = 'inline-block';
    document.getElementById("exp_rate").required = true;
    // document.getElementById("expense_rate").getAttribute("required");

    }else{
    document.getElementById("expense").classList.remove('col-md-2');
    document.getElementById("expense").classList.add('col-md-4');
    document.getElementById("expense_rate").style.display = 'none';
    document.getElementById("exp_rate").required = false;
    }
}

/****************************************Form Validation********************************************/

function validation()
{	
var chk=1;

if(document.getElementById("expense_name").value == '') {
chk = 0;
document.getElementById('expense_name').focus();
document.getElementById('expense_name').style.borderColor="red";
}
else {
document.getElementById('expense_name').style.border="1px solid #ccc";
}
	
if(chk){
  // $('#form').submit();
 //alert("EXPENSE ADDED SUCCESSFULLY..!!");
return true;
}else{
return false;	
}	
	
}

</script>
<script>

	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});

   
function goBack() {
    window.history.back();
}
</script>








