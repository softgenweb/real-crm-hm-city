<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Expense Master</h3>
            <?php foreach($results as $result) { }  ?>
            <a href="index.php?control=expense&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Expense</a>
            <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Expense : <?php echo $no_of_row; ?>
            </p>
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_expense.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a>--> 
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Expense List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th class="width_srno" width="15">
                              <div align="left">S.No.</div>
                           </th>
                           <th class="width_normal">
                              <div align="left">Expense Name</div>
                           </th>
                           <th class="width_normal">
                              <div align="left">Action</div>
                           </th>
                        </tr>
                     </thead>
                     <tfoot style="display:none;">
                        <tr>
                           <th class="width_srno" width="15">
                              <div align="left">S.No.</div>
                           </th>
                           <th class="width_normal">
                              <div align="left">Expense Name</div>
                           </th>
                           <th class="width_normal">
                              <div align="left">Action</div>
                           </th>
                        </tr>
                     </tfoot>
                     <tbody>
                        <?php
                           if($results) {
                            $countno = ($page-1)*$tpages;
                            $i=0;
                            foreach($results as $result){ 
                            $i++;
                            $countno++;
                           $sel=mysql_fetch_array(mysql_query("select * from project where id ='".$result['project_id']."'"));
                           
                           $uquery =mysql_fetch_array(mysql_query("select * from day_book where expense_id='".$result['id']."' and status!=2  ORDER BY `id` ASC"));
                           /*       $state=mysql_fetch_array(mysql_query("select * from state where id ='".$result['state_id']."'"));
                           $city=mysql_fetch_array(mysql_query("select * from city where id ='".$result['city_id']."'"));*/
                            ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad"
                           ?>
                        <tr>
                           <td class="<?php echo $class; ?>">
                              <div align="left"><?php echo $countno; ?></div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left">
                                 <?php if($uquery['id']!='') { ?>
                                 <a href="index.php?control=expense_detail&exptype=expense&task=show&exid=<?php echo $result['id']; ?>" title="Show Expense Detail"> <?php echo $result['expense_name'] ; ?></a>
                                 <?php } else { ?>
                                 <a title="No Expense Detail" style="cursor:pointer; color:blue"><?php echo $result['expense_name'] ; ?></a>
                                 <?php } ?>
                              </div>
                           </td>
                           <!--<td class="<?php echo $class; ?>"><div align="left"><?php echo $result['expense_name'];?></div></td>-->
                           <td class="<?php echo $class; ?>">
                              <div align="left">            
                                 <a href="index.php?control=expense&task=addnew&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                                 <?php
                                    if($result['status']==1){  ?>
                                 <a href="index.php?control=expense&task=status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                                 <?php } else { ?>
                                 <a href="index.php?control=expense&task=status&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                                 <?php } ?>
                              </div>
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   
     $(document).ready(function() {
              const table = $('#example1-1').DataTable({
                  stateSave: true
              });
   
              // Setup - add a text input to each footer cell
              $('#example1-1 tfoot th').each( function (index,value) {
                  var title = $(this).text();
                  $(this).html( '<input type="text" placeholder="Search '+title+'" value="'+table.column(index).search()+'"/>' );
              } );
   
   
              // Apply the search
              table.columns().every( function () {
                  var that = this;
   
                  $( 'input', this.footer() ).on( 'keyup change', function () {
                      if ( that.search() !== this.value ) {
                          that
                              .search( this.value )
                              .draw();
                      }
                  } );
              });
          });
   
   function expense_edit(id)
   
   {
   
   //alert(id);
   
   document.getElementById("edit").value = Number(id);
   
   document.getElementById("tpages").value = Number(document.getElementById("filter").value);
   
   document.getElementById("task").value = "addnew";
   
   document.getElementById("id").value = id;
   
   
   //document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
   
   /*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
   
   document.filterForm.submit(); 
   
    
   
   }
   
   
   
   function expense_status(id,status)
   { 
    document.getElementById("id").value = Number(id);
    document.getElementById("tpages").value = Number(document.getElementById("filter").value);
    document.getElementById("task").value = "status";
    document.getElementById("id").value = id;
    document.getElementById("status").value = status;
    document.filterForm.submit(); 
   }
   
   
   
   
   
   function expense_delete(id)
   
   {
   
   document.getElementById("del").value = Number(id);
   
   document.getElementById("tpages").value = Number(document.getElementById("filter").value);
   
   document.getElementById("task").value = "delete";
   
   document.getElementById("id").value = id;
   
   
   
   
   
   if(confirm('Are you sure you want to delete ?')) {
   
    document.filterForm.submit(); 
   
   }
   
   else
   
   {
   
   //alert("No");
   
   }
   
   }
   
   
</script>

