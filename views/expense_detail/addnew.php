    <div class="col-md-12" style="margin-top:4%;">
    	<br>
    <div class="row">
			<div class="col-lg-12">
			   <ol class="breadcrumb">
            <br>
				<li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=bill_account&task=addnew">Account Master</a></li>
                <li class="active">Add New Account</li>
			</ol>
			</div>
		</div><!--/.row-->
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                    <u><h3>Add New Account</h3></u>
                    </div>
                    <br>
                    
                <form name="account" action="" method="post" enctype="multipart/form-data">
                <?php foreach($results as $result) { }  ?>
                

		<div class="col-md-2"><div class="form-group" style="margin-top:5%">
		Account Holder Name :
		</div></div>
		<div class="col-md-4"><div class="form-group">
		<input class="form-control" type="text" name="acc_holder_name" id="acc_holder_name" value="<?php echo $result['acc_holder_name']; ?>" placeholder="Enter Account Holder Name">
		<span id="msgcompany_name" class="msg"></span>
		</div></div>

    
		<div class="col-md-2"><div class="form-group" style="margin-top:5%;">
		Account Number :
		</div></div>
		<div class="col-md-4"><div class="form-group">
		<input class="form-control" type="text" name="account_no" id="account_no" value="<?php echo $result['account_no']; ?>" placeholder="Enter Account Number">
		<span id="msgaccount_number" class="msg"></span>
		</div></div>

		<div class="col-md-2"><div class="form-group" style="margin-top:5%">
		IFSC Code  :
		</div></div>
		<div class="col-md-4"><div class="form-group">
		<input class="form-control" type="text" name="ifsc_code" id="ifsc_code" value="<?php echo $result['ifsc_code']; ?>" placeholder="Enter IFSC Code ">
		<span id="msgifsc_code" class="msg"></span>
		</div></div>


		<div class="col-md-2"><div class="form-group" style="margin-top:5%">
		Bank Name :
		</div></div>
		<div class="col-md-4"><div class="form-group">
		<input class="form-control" type="text" name="bank_name" id="bank_name" value="<?php echo $result['bank_name']; ?>" placeholder="Enter Bank Name">
		<span id="msgbank_name" class="msg"></span>
		</div></div>

 
		<div class="col-md-2"><div class="form-group" style="margin-top:5%">
		Branch Name :
		</div></div>
		<div class="col-md-4"><div class="form-group">
		<input class="form-control" type="text" name="branch_name" id="branch_name" value="<?php echo $result['branch_name']; ?>" placeholder="Enter Branch Name ">
		<span id="msgbranch_name" class="msg"></span>
		</div></div>

 
		<div class="col-md-12" align="left">
		<br>

		<?php if($results[0]['id']) { ?>
		<button type="submit" class="btn btn-primary">Update</button>
		<?php } else { ?>
		<button type="submit" class="btn btn-primary">Submit</button>
		<?php } ?>
		<button type="reset" class="btn btn-default">Reset</button></div> 
                                
		<input type="hidden" name="control" value="bill_account"/>
		<input type="hidden" name="edit" value="1"/>
		<input type="hidden" name="task" value="save"/>
		<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

		<input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
		<input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                 
		</form>

		</div>
		</div>			
		</div>
    
    
<script type="application/javascript">

		function masteraddcbank_valid(){
		var chk=1;
		if(document.getElementById('account_number').value == ''){
		document.getElementById('msgaccount_number').innerHTML = "*Required Field";
		document.getElementById("account_number").focus();
		document.getElementById('account_number').style.borderColor = "red";
		chk=0;	
		}
		else if(document.getElementById("account_number").value!="") {
		
		document.getElementById("msgaccount_number").innerHTML="";
		document.getElementById('account_number').style.borderColor = "green";
		}
		else{
		document.getElementById('msgaccount_number').innerHTML = "";
		}
		
		if(document.getElementById('company_name').value == ''){
		document.getElementById('msgcompany_name').innerHTML = "*Required Field";
		document.getElementById("company_name").focus();
		document.getElementById('company_name').style.borderColor = "red";
		chk=0;	
		}
		else if(document.getElementById("company_name").value!="") {
		
		document.getElementById("msgcompany_name").innerHTML="";
		document.getElementById('company_name').style.borderColor = "green";
		}
		else{
		document.getElementById('msgcompany_name').innerHTML = "";
		}
		if(document.getElementById('bank_name').value == ''){
		document.getElementById('msgbank_name').innerHTML = "*Required Field";
		document.getElementById("bank_name").focus();
		document.getElementById('bank_name').style.borderColor = "red";
		chk=0;	
		}
		else if(document.getElementById("bank_name").value!="") {
		
		document.getElementById("msgbank_name").innerHTML="";
		document.getElementById('bank_name').style.borderColor = "green";
		}
		else{
		document.getElementById('msgbank_name').innerHTML = "";
		}
		if(document.getElementById('branch_name').value == ''){
		document.getElementById('msgbranch_name').innerHTML = "*Required Field";
		document.getElementById("branch_name").focus();
		document.getElementById('branch_name').style.borderColor = "red";
		chk=0;	
		}
		else if(document.getElementById("branch_name").value!="") {
		
		document.getElementById("msgbranch_name").innerHTML="";
		document.getElementById('branch_name').style.borderColor = "green";
		}
		else{
		document.getElementById('msgbranch_name').innerHTML = "";
		}
		if(document.getElementById('ifsc_code').value == ''){
		document.getElementById('msgifsc_code').innerHTML = "*Required Field";
		document.getElementById("ifsc_code").focus();
		document.getElementById('ifsc_code').style.borderColor = "red";
		chk=0;	
		}
		else if(document.getElementById("ifsc_code").value!="") {
		
		document.getElementById("msgifsc_code").innerHTML="";
		document.getElementById('ifsc_code').style.borderColor = "green";
		}
		else{
		document.getElementById('msgifsc_code').innerHTML = "";
		}
		
		
		
		if(chk){
		return true;
		}else{
		return false;
		}
		
		}

</script>
