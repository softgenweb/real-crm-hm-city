<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Expense Ledger</h3>
            <?php foreach($results as $result) { }  ?>
            <!--<a href="index.php?control=daybook&task=cr_expense_addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add-New Cr Expense</a> 
               <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
                       
               Total Dealer : <?php echo $no_of_row; ?>
               </p>-->   
            <!-- <a href="javascript:void(0);" onclick="window.open('script/table_csv.php?table=dealer');"  ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a>               
               <a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_dealer.php');"  ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a>  -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=expense&task=show"> Expense Master</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Expense Ledger</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
                     <div class="row col-md-12">
                        <div class="col-md-12 col-sm-9 col-xs-12">
                           <!--<div class="col-md-2">
                              <div class="form-group" style="margin-top:5%;">
                              Expense Name:
                              </div></div>
                              <div class="col-md-3"><div class="form-group">                    
                              <select class="form-control search" name="expense_id" id="expense_id" style="width:200px !important;">
                              <option value="">-Select Expense-</option>
                              <?php 
                                 $sq="select * from expense where status=1";
                                 $que=  mysql_query($sq);
                                 while($fet=  mysql_fetch_array($que)){
                                 ?>
                              <option value="<?php echo $fet['id']; ?>" <?php if($_REQUEST['expense_id']==$fet['id']) {?> selected="selected" <?php } ?>><?php echo $fet['expense_name']; ?></option>
                              <?php } ?>
                              </select>
                              <span id="msgcountry_id" style="color:red;"></span>
                              </div></div>-->
                           <!--  <div class="clearfix"></div>
                              <div class="col-md-2">
                               <div class="form-group" style="margin-top:5%;">
                               From Date:
                               </div></div>-->
                           <div class="col-md-3">
                              <div class="form-group">                    
                                 <input class="form-control" type="text" name="dateFrom" id="datefrom" value="<?php echo $_REQUEST['dateFrom'];?>" placeholder="From Date" required>
                                 <span id="msgstate_id" style="color:red;"></span>
                              </div>
                           </div>
                           <!--<div class="clearfix"></div>
                              <div class="col-md-2">
                              <div class="form-group" style="margin-top:5%;">
                              To Date:
                              </div></div>-->
                           <div class="col-md-3">
                              <div class="form-group">
                                 <input class="form-control" type="text" name="dateTo" id="dateto" value="<?php echo $_REQUEST['dateTo'];?>" placeholder="To Date" required>
                                 <span id="msgcity_name" style="color:red;"></span>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">                    
                                 <input  class="btn btn-primary butoon_brow" type="submit" name="search" id="btnSelected" value="Search" />
                              </div>
                           </div>
                           <input type="hidden" name="control" value="expense_detail"/>
                           <input type="hidden" name="task" value="show"/>        
                        </div>
                     </div>
                  </form>
                  <table id="datatab" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15">
                              <div align="left">S.No</div>
                           </th>
                           <th>
                              <div align="left">Date</div>
                           </th>
                           <th>
                              <div align="left">Remark</div>
                           </th>
                           <th>
                              <div align="left">Transaction</div>
                           </th>
                           <th>
                              <div align="left">Debit Amount</div>
                           </th>
                           <th>
                              <div align="left">Credit Amount</div>
                           </th>
                           <?php if(($_SESSION['utype']=='Report') || ($_SESSION['utype']=='Administrator' && $_SESSION['post_id']=='top')) { ?> 
                           <!--<th><div align="left">Available Balance</div></th>-->
                           <?php } ?>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                           $countno = ($page-1)*$tpages;
                           $i=0;
                           foreach($results as $result){ 
                           $i++;
                           $countno++;
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           ?>
                        <tr>
                           <td class="<?php echo $class; ?>">
                              <div align="left"><?php echo $countno; ?></div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left">
                                 <?php echo $e_date = date('d-m-Y',strtotime($result['expense_date'])); ?>
                              </div>
                           </td>
                           <?php                                
                              $queryExpense = "SELECT * FROM expense where id='".$result['expense_id']."'";
                              $myqueryExpense =  mysql_query($queryExpense);
                              $resExpense =  mysql_fetch_array($myqueryExpense); ?>
                           <td class="<?php echo $class; ?>">
                              <div align="left">
                                 <?php echo $result['remark'];?> 
                              </div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left">
                                 <?php if($result['status']==2)
                                    {
                                    echo $result['user_type'].'<br/>'."( Cheque Bounce"."/".$result['cheque_dd_no']." )";
                                    }   
                                    elseif(($result['status']!=2) && ($result['user_type']!='Administrator') && ($result['bank_type']!='')) 
                                    { 
                                    echo $result['user_type'].'<br/>'."(".$result['bank_type']." - ".$result['cheque_dd_no'].")"; 
                                    }
                                    elseif($result['status']!=2 && $result['user_type']!='Administrator' && $result['bank_type']=='' && $result['transaction_type']=='Cash')
                                    { 
                                    echo $result['user_type'].'<br/>'."(Cash)"; 
                                    } 
                                    else 
                                    { 
                                    echo $resExpense['expense_name']; 
                                    } ?>
                              </div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left" style="color:red;"><b>
                                 <?php    
                                    if($result['account_type']=='Dr') {  if($result['status']==2) { 
                                     $debitAmont = $result['chk_bounce_amount']!=''?$result['chk_bounce_amount']:''; 
                                    echo $this->moneyFormatIndia($debitAmont);
                                    } else { $debitAmont = $result['amount']?$result['amount']:'';   
                                    echo $this->moneyFormatIndia($debitAmont);
                                    } 
                                    } ?></b>
                              </div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left" style="color:green;"><b> 
                                 <?php   if($result['account_type']=='Cr') { 
                                    if($result['status']==2) { 
                                    echo $this->moneyFormatIndia($creditAmount = (($result['chk_bounce_amount']!='')?$result['chk_bounce_amount']:'')); 
                                    } 
                                    else{ echo $this->moneyFormatIndia($creditAmount = $result['amount']?$result['amount']:''); 
                                    }  }  ?> </b>
                              </div>
                           </td>
                           <?php if(($_SESSION['utype']=='Report') || ($_SESSION['utype']=='Administrator' && $_SESSION['post_id']=='top')) { ?>                         
                           <!--<td class="<?php echo $class; ?>"><div align="left" style="color:blue;"><b>                             
                              <?php   if($result['account_type']=='Cr')
                                 { $creditAmountTotal += $creditAmount; }
                                                         if($result['account_type']=='Dr') { $debitAmontTotal += $debitAmont;  }
                                                         $avail_bal =  $search_Available_Balance+$creditAmountTotal-$debitAmontTotal;
                                 echo $this->moneyFormatIndia($avail_bal);  ?>
                                                    </b> </div></td>--> 
                           <?php } ?>
                        </tr>
                        <?php
                           if($result['account_type']=='Dr') { 
                           if($result['status']==2) {
                           $total_debit += $debitAmont ;
                           } 
                           else { 
                           $total_debit +=$debitAmont;
                           }
                           }
                           
                           if($result['account_type']=='Cr') { 
                           if($result['status']==2) {
                           $total_credit += $creditAmount; 
                           } 
                           else { 
                           $total_credit += $creditAmount;
                           }
                           }    
                           
                           }  }else{?>
                        <div>                 
                        </div>
                        <?php } ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <td class="<?php echo $class; ?>"></td>
                           <td class="<?php echo $class; ?>"></td>
                           <td class="<?php echo $class; ?>"></td>
                           <td class="<?php echo $class; ?>">
                              <div align="left"><strong>Total</strong></div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left" style="color:red;"><strong><?php echo $this->moneyFormatIndia($total_debit?$total_debit:"N/A"); ?></strong></div>
                           </td>
                           <td class="<?php echo $class; ?>">
                              <div align="left" style="color:green;"><strong><?php echo $this->moneyFormatIndia($total_credit?$total_credit:"N/A"); ?></strong></div>
                           </td>
                           <?php if(($_SESSION['utype']=='Report') || ($_SESSION['utype']=='Administrator' && $_SESSION['post_id']=='top')) { ?> 
                           <!--<td class="<?php echo $class; ?>"><div align="left" style="color:blue;"><strong><?php echo $total_credit?$search_Available_Balance+$total_credit-$total_debit:"N/A"; ?></strong></div></td>--> 
                           <?php }?>
                        </tr>
                     </tfoot>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   $('#datefrom').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   $('#dateto').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
</script>

