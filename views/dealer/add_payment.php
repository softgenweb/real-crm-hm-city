<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Dealer Payment</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=dealer&task=show"><i class="fa fa-list" aria-hidden="true"></i> Dealer List</a></li>
      <li><a href="index.php?control=dealer&task=payment_list&id=<?php echo $_REQUEST['id']; ?>"><i class="fa fa-list" aria-hidden="true"></i> Payment List</a></li>
      <?php// if($result!='') {?>           
      <!-- <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Payment</li> -->
      <?php// } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Payment</li>
      <?php// } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php   	unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control" readonly="">
                     <input type="hidden" name="dealer_id" value="<?php echo $result['id']; ?>">
                     <span class="msgValid" id="msgname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Mobile: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">								
                     <input type="text" maxlength="10" pattern="[6789][0-9]{9}" value="<?php echo $result['mobile']; ?>" id="mobile" name="mobile" class="form-control"  readonly="">
                     <span class="msgValid" id="msgmobile"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Commission Amount: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">								
                     <input type="text"  onkeypress="return isNumber(event)"  value="" id="commission_amount" name="commission_amount" class="form-control"  required="">
                     <span class="msgValid" id="msgcommision"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Payment Date: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">								
                     <input type="text"  value="" id="pay_date" placeholder="Select Date" name="payment_date" class="form-control" readonly="">
                     <span class="msgValid" id="msgcommision"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Payment Type: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="payment_type" class="pay_type form-control" required="">
                        <option value="">Select</option>
                        <option value="Cash">Cash</option>
                        <option value="Cheque">Cheque</option>
                        <option value="NEFT/RTGS">NEFT/RTGS</option>
                     </select>
                  </div>
               </div>
              <div class="clearfix"></div>
            <div class="col-md-3">
              <div class="form-group center_text">
                Remark: 
              </div>  
                            </div>
              <div class="col-md-6"><div class="form-group">                
                <!-- <input type="text"  value="<?php echo $result['remark']; ?>" id="remark" name="remark" class="form-control"> -->
                <textarea id="remark" name="remark" class="form-control"><?php echo $result['remark']; ?></textarea>
                <span class="msgValid" id="msgcommision"></span>
              </div>
            </div>
               <!--  <div class="clearfix"></div>
                  <div class="col-md-3">
                                       <div class="form-group center_text">
                  		Payment Status: 
                  	</div>	
                                        </div>
                  	<div class="col-md-6"><div class="form-group">								
                  	<select name="payment_status" class="pay_type form-control" required="">
                                        <option value="">Select</option>
                                        <option value="1">Paid</option>
                                        <option value="0">Pending</option>
                                        </select> 
                  	</div>
                  </div> -->
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Submit"></center>
               </div>
               <input type="hidden" name="control" value="dealer"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save_payment"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<script type="text/javascript">
   function validation()
   
   {   
   	var chk=1;
   	
   		if(document.getElementById('project_id').value == '') { 
   			document.getElementById('msgproject_id').innerHTML = "*Required field.";
   			chk=0;
   		}else {
   			document.getElementById('msgproject_id').innerHTML = "";
   		}	
   
   		if(document.getElementById('block_id').value == '') { 
   			document.getElementById('msgblock_id').innerHTML = "*Required field.";
   			chk=0;
   		}else {
   			document.getElementById('msgblock_id').innerHTML = "";
   		}	
   		if(document.getElementById('plot_id').value == '') { 
   			document.getElementById('msgplot_id').innerHTML = "*Required field.";
   			chk=0;
   		}else {
   			document.getElementById('msgplot_id').innerHTML = "";
   		}	
   		if(document.getElementById('area_id').value == '') { 
   			document.getElementById('msgarea_id').innerHTML = "*Required field.";
   			chk=0;
   		}else {
   			document.getElementById('msgarea_id').innerHTML = "";
   		}	
   		
   
   
   			if(chk)
   			 {			
   				return true;		
   			}		
   			else 
   			{		
   				return false;		
   			}	
   		
   		
   		}
   
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   $('#pay_date').datetimepicker({
       yearOffset:0,
       lang:'ch',
       timepicker:false,
       format:'d-m-Y',
       formatDate:'Y/m/d',
       // formatDate: new Date(),
       //maxDate:'+1970/01/01',
   
   
   });   
</script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
   	$(this).alert('close');
   });

      function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

