<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Dealer</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=dealer&task=show"><i class="fa fa-list" aria-hidden="true"></i> Dealer List</a></li>
            <?php if($result!='') {?>           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Dealer</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Dealer</li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
            
			              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">

					
						<div class="col-md-3">
                           <div class="form-group center_text">
								Name:
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<input type="text" value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control"  required="">
								<span class="msgValid" id="msgname"></span>
							</div>
						</div>
						 <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Mobile: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text" maxlength="10" pattern="[6789][0-9]{9}" value="<?php echo $result['mobile']; ?>" id="mobile" name="mobile" class="form-control"  required="">
								<span class="msgValid" id="msgmobile"></span>
							</div>
						</div>
						 <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Email-Id: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="email"  value="<?php echo $result['email_id']; ?>" id="email_id" name="email_id" class="form-control" >
								<span class="msgValid" id="msgemail_id"></span>
							</div>
						</div>
                         <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Commission(%): 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text"  value="<?php echo $result['commission']; ?>" id="commission" name="commission" class="form-control"  required="">
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>
					      <div class="clearfix"></div>
					      <div class="col-md-3">
                    <div class="form-group center_text">
                    Country Name:
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    <select name="country_id" id="country_id" class="form-control" onchange='state_change(this.value);' >
                    <option value="">Select Country</option>
                    <?php $sel=mysql_query("select * from country where 1");
                    while($select=mysql_fetch_array($sel))
                    {
                    if($select['status'] == '1') { ?>
                    <option value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
                    <?php } else { ?>  
                    <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>     
                    <?php } } ?>
                    </select>
                    <span id="msgcountry_id" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-3">
                    <div class="form-group center_text">
                    State Name:
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    
                    <select name="state_id" id="state_id" class="form-control" onchange='city_change(this.value);'>
                    <option value="">Select State</option>
                    <?php $sel=mysql_query("select * from state where 1");
                    while($select=mysql_fetch_array($sel))
                  if($result)  {
                    if($select['status'] == '1') { ?>
                    <option value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
                    <?php } else { ?>  
                    <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>     
                    <?php } } ?>
                    </select>
                    <span id="msgstate_id" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                     <div class="col-md-3">
                    <div class="form-group center_text">
                    City Name:
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    
                    <select name="city_id" id="city_id" class="form-control" >
                    <option value="">Select City</option>
                    <?php $sel=mysql_query("select * from city where 1");
                    while($select=mysql_fetch_array($sel))
                  if($result)  {
                    if($select['status'] == '1') { ?>
                    <option value="<?php echo $select['id'];?>"<?php if($result['city_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['city_name'];?></option>
                    <?php } else { ?>  
                    <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['city_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['city_name'];?></option>     
                    <?php } } ?>
                    </select>
                    <span id="msgcity_id" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
       					<div class="col-md-3">
                           <div class="form-group center_text">
								Address:
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
							<textarea id="address" name="address" class="form-control" ><?php echo $result['address']; ?></textarea>	
								<span class="msgValid" id="msgaddress"></span>
							</div>
						</div>
					      <div class="clearfix"></div>
					<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>                   

					<input type="hidden" name="control" value="dealer"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('project_id').value == '') { 
			document.getElementById('msgproject_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgproject_id').innerHTML = "";
		}	

		if(document.getElementById('block_id').value == '') { 
			document.getElementById('msgblock_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgblock_id').innerHTML = "";
		}	
		if(document.getElementById('plot_id').value == '') { 
			document.getElementById('msgplot_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgplot_id').innerHTML = "";
		}	
		if(document.getElementById('area_id').value == '') { 
			document.getElementById('msgarea_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgarea_id').innerHTML = "";
		}	
		


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>
<script>
/*function goBack() {
    window.history.back();
}*/
</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   