<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">View Plot Booking</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> View Plot Booking</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Booking </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
            
  
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
			<div class="box-body">				
          <!--=======================Plot Details======================-->	  

            <fieldset>
				<a href="javascript:void(0);" class="show_hide1">
					<legend> View Plot Details</legend>
				</a>
				<div class="col-sm-10 col-md-offset-1 first_header">
						<div class="form-group oneline">
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Project Name</label>
								 
							</div>							
								<div class="col-md-4 col-sm-4 col-xs-12">									
										<input type="text" value="<?php echo $result['project_name']; ?>" id="project_id" name="project_id" class="form-control" readonly>
									<span class="msgValid" id="msgbranch_id"></span> 
								</div>

							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Block Name</label>
								 
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">									
										<input type="text" value="<?php echo $result['block_name']; ?>" id="block_id" name="block_id" class="form-control" readonly>
								<span class="msgValid" id="msgvehicle_no"></span>
								</div>
							</div>  
							<div class="clearfix"></div>
							<div class="form-group oneline">
								
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Plote Name</label>
								 
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input type="text" value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" readonly>
									<span class="msgValid" id="msgvehicle_no"></span>
								</div>
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Area Dimention</label>
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" value="<?php echo $result['area_dimension']; ?>" id="area_id" name="area_dimension" class="form-control" autocomplete="off" readonly>
								<!-- ============================================================== -->
								<span class="msgValid" id=""></span>
								</div>
							</div>
						<div class="clearfix"></div>
						<div class="form-group oneline">	
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Plote Rate</label>
								 
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input type="text" value="<?php echo $result['rate']; ?>" id="plot_name" name="plot_name" class="form-control" readonly>
									<span class="msgValid" id="msgvehicle_no"></span>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Plote Total Price</label>
								 
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input type="text" value="<?php echo $result['total_price']; ?>" id="plot_name" name="plot_name" class="form-control" readonly>
									<span class="msgValid" id="msgvehicle_no"></span>
								</div>
						</div>
					</div>
              <!--panel1 end here -->
            </fieldset>
            
            <!--========================Applicant Detail Start here ============-->
            <fieldset>
				<a href="javascript:void(0);" class="show_hide2">
					<legend>Applicant Detail</legend>
				</a>
				<div class="col-sm-10 col-md-offset-1 second_header ons" >
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Title</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['title']; ?>" id="title" name="title" class="form-control" readonly>
							<span class="msgValid" id="msgtitle"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Applicant Name</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['applicant_name']; ?>" id="applicant_name" name="applicant_name" class="form-control" readonly>
						<span class="msgValid" id="msgapplicant_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Realtion</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['relation']; ?>" id="relation" name="relation" class="form-control" readonly>
							<span class="msgValid" id="msgrelation"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Father/Husband Name</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['father_husband_name']; ?>" id="father_husband_name" name="father_husband_name" class="form-control" readonly>
						<span class="msgValid" id="msgfather_husband_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Date Of Birth</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['dob']; ?>" id="dob" name="dob" class="form-control" readonly>
							<span class="msgValid" id="msgdob"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Profession</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">							
							<input type="text"  value="<?php echo $result['profession']; ?>" id="profession" name="profession" class="form-control" readonly>
							<span class="msgValid" id="msgprofession"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Residential Status</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['residence_status']; ?>" id="residence_status" name="residence_status" class="form-control" readonly>
							<span class="msgValid" id="msgresidence_status"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Mobile Number</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['mobile']; ?>" maxlength="10" id="mobile" name="mobile" class="form-control" readonly>
						<span class="msgValid" id="msgmobile"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Email</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['email']; ?>"  id="email" name="email" class="form-control" readonly>
							<span class="msgValid" id="msgmobile"></span>
						</div>
						</div> 
						<div class="clearfix"></div><br>
						<label><strong>Applicant Residential Address :</strong></label>
						<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<!-- <input type="text" value="<?php echo $country['country_name']; ?>" id="country_name" name="country_name" class="form-control" readonly> -->
							<input type="text" value="<?php echo $this->country_name($result['res_country']);?>" id="country_name" name="country_name" class="form-control" readonly>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->state_name($result['res_state']);?>" id="state_name" name="state_name" class="form-control" readonly>
							<span class="msgValid" id="msgbranch_id"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" value="<?php echo $this->city_name($result['res_city']);?>" id="city_name" name="city_name" class="form-control" readonly>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="res_address" id="res_address" class="form-control" cols="3" readonly><?php echo $result['res_address']; ?></textarea>
							<span class="msgValid" id="msgres_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['res_pincode']; ?>" id="res_pincode" name="res_pincode" class="form-control" readonly>
						<span class="msgValid" id="msgres_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
						<label><strong>Applicant Office Address :</strong></label>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->country_name($result['office_country']);?>" id="office_country" name="office_country" class="form-control" readonly>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->state_name($result['office_state']);?>" id="office_state" name="office_state" class="form-control" readonly>
							<span class="msgValid" id="msgbranch_id"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" value="<?php echo $this->city_name($result['office_city']);?>" id="office_city" name="office_city" class="form-control" readonly>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="office_address" id="office_address" class="form-control" cols="3" readonly><?php echo $result['office_address']; ?></textarea>
							<span class="msgValid" id="msgoffice_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['office_pincode']; ?>" id="office_pincode" name="office_pincode" class="form-control" readonly>
						<span class="msgValid" id="msgoffice_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
				<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Nationality</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="text"  value="<?php echo $result['nationality']; ?>" id="nationality" name="nationality" class="form-control" readonly>
						<span class="msgValid" id="msgnationality"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Id Proof</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<a href="media/plot_booking/<?php echo $result['id_proof'];?>" target="_blank" ><img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['id_proof']) { ?>media/plot_booking/<?php echo $result['id_proof'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgid_proof"></span>
					</div>
				</div>
				<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Form Scan</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<a href="media/plot_booking/<?php echo $result['scan_form'];?>" target="_blank" ><img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['scan_form']) { ?>media/plot_booking/<?php echo $result['scan_form'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgscan_form"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Applicant Photo</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<a href="media/plot_booking/<?php echo $result['applicant_photo'];?>" target="_blank" ><img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['applicant_photo']) { ?>media/plot_booking/<?php echo $result['applicant_photo'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgapplicant_photo"></span>
					</div>
					
					</div>
					<div class="clearfix"></div>
				</div>
		   </fieldset>
            <!--=================Applicant Detail End here=====================--> 
            
            <!--==================Co-Applicant Details Start here =====================-->
            <fieldset>
              <a href="javascript:void(0);" class="show_hide3">
				<legend>Co-Applicant Details</legend>
              </a>
           
				<div class="col-sm-10 col-md-offset-1 third_header ons">
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Title</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['applicant_name']; ?>" id="applicant_name" name="applicant_name" class="form-control" readonly>
							<span class="msgValid" id="msgco_title"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Co-Applicant Name</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_applicant_name']; ?>" id="co_applicant_name" name="co_applicant_name" class="form-control" readonly>
						<span class="msgValid" id="msgapplicant_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Realtion</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['co_relation']; ?>" id="co_relation" name="co_relation" class="form-control" readonly>
							<span class="msgValid" id="msgrelation"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Father/Husband Name</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_father_husband_name']; ?>" id="co_father_husband_name" name="co_father_husband_name" class="form-control" readonly>
						<span class="msgValid" id="msgco_father_husband_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Date Of Birth</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_dob']; ?>" id="co_dob" name="co_dob" class="form-control" readonly>
							<span class="msgValid" id="msgco_dob"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Profession</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">							
							<input type="text"  value="<?php echo $result['co_profession']; ?>" id="co_profession" name="co_profession" class="form-control" readonly>
							<span class="msgValid" id="msgprofession"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Residential Status</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['co_residence_status']; ?>" id="co_residence_status" name="co_residence_status" class="form-control" readonly>
							<span class="msgValid" id="msgco_residence_status"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Mobile Number</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_mobile']; ?>" maxlength="10" id="co_mobile" name="co_mobile" class="form-control" readonly>
						<span class="msgValid" id="msgmobile"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Email</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_email']; ?>"  id="co_email" name="co_email" class="form-control" readonly>
							<span class="msgValid" id="msgmobile"></span>
						</div>
						</div> 
						<div class="clearfix"></div><br>
						<label><strong>Co-Applicant Residential Address :</strong></label>
						<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->country_name($result['co_res_country']);?>" id="co_res_city" name="co_res_city" class="form-control" readonly>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->state_name($result['co_res_state']);?>" id="co_res_state" name="co_res_state" class="form-control" readonly>
							<span class="msgValid" id="msgco_res_state"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" value="<?php echo $this->city_name($result['co_res_city']);?>" id="co_res_city" name="" class="form-control" readonly>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="co_res_address" id="co_res_address" class="form-control" cols="3" readonly><?php echo $result['co_res_address']; ?></textarea>
							<span class="msgValid" id="msgco_res_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['co_res_pincode']; ?>" id="co_res_pincode" name="co_res_pincode" class="form-control" readonly>
						<span class="msgValid" id="msgco_res_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
						<label><strong>Co-Applicant Office Address :</strong></label>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->country_name($result['co_office_country']);?>" id="co_office_country" name="co_office_country" class="form-control" readonly>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->state_name($result['co_office_state']);?>" id="co_office_state" name="co_office_state" class="form-control" readonly>
							<span class="msgValid" id="msgco_office_state"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" value="<?php echo $this->city_name($result['co_office_city']);?>" id="co_office_city" name="co_office_city" class="form-control" readonly>
						<span class="msgValid" id="msgco_office_city"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="co_office_address" id="co_office_address" class="form-control" cols="3" readonly><?php echo $result['co_office_address']; ?></textarea>
							<span class="msgValid" id="msgco_office_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['co_office_pincode']; ?>" id="co_office_pincode" name="co_office_pincode" class="form-control" readonly>
						<span class="msgValid" id="msgoffice_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Nationality</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="text"  value="<?php echo $result['co_nationality']; ?>" id="co_nationality" name="co_nationality" class="form-control" readonly>
						<span class="msgValid" id="msgnationality"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Id Proof</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
					
						<a href="media/plot_booking/<?php echo $result['co_id_proof'];?>" target="_blank" ><img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['co_id_proof']) { ?>media/plot_booking/<?php echo $result['co_id_proof'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgco_id_proof"></span>
					</div>
				</div>
				<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Form Scan</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						
						<a href="media/plot_booking/<?php echo $result['co_scan_form'];?>" target="_blank" ><img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['co_scan_form']) { ?>media/plot_booking/<?php echo $result['co_scan_form'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgco_scan_form"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Co-Applicant Photo</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >						
						<a href="media/plot_booking/<?php echo $result['co_applicant_photo'];?>" target="_blank" ><img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['co_applicant_photo']) { ?>media/plot_booking/<?php echo $result['co_applicant_photo'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgco_applicant_photo"></span>
					</div>
					
					</div>
					<div class="clearfix"></div>
				</div>
		   </fieldset>
            <!--================Co-Applicant Details End here ======================--> 
            
            <!--====================Nominee Details Start here ===================-->
            <fieldset>
              <a href="javascript:void(0);" class="show_hide4">
              <legend>Nominee Details</legend>
              </a>
              <div class="col-sm-10 col-md-offset-1 fourth_header ons" >
              	
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Title</label>
						<!--   -->
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['nom_title']; ?>" id="nom_title" name="nom_title" class="form-control" readonly>
							<span class="msgValid" id="msgtitle"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Nominee Name</label>
						<!--   -->
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['nom_name']; ?>" id="nom_name" name="nom_name" class="form-control" readonly>
						<span class="msgValid" id="msgapplicant_name"></span>
						</div>
					</div>
					
               
                <div class="clearfix"></div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Realation</label>
						<!--   -->
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $result['nom_realtion']; ?>" id="nom_realtion" name="nom_realtion" class="form-control" readonly>
							<span class="msgValid" id="msgtitle"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Father/Husband Name</label>
						<!--   -->
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['nom_father_husband']; ?>" id="nom_father_husband" name="nom_father_husband" class="form-control" readonly>
						<span class="msgValid" id="msgnom_father_husband"></span>
						</div>
					</div>
					
               
                <div class="clearfix"></div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Age</label>
						<!--   -->
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" maxlength="2" value="<?php echo $result['nom_age']; ?>" id="nom_age" name="nom_age" class="form-control" readonly>
							<span class="msgValid" id="msgnom_age"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Mobile Number</label>
						<!--   -->
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" maxlength="10" value="<?php echo $result['nom_mobile']; ?>" id="nom_mobile" name="nom_mobile" class="form-control" readonly>
						<span class="msgValid" id="msgnom_mobile"></span>
						</div>
					</div>
					
               
                <div class="clearfix"></div>

                <!-- --------== -->
                <br>
						<label><strong>Nominee Address :</strong></label>
						<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->country_name($result['nom_res_country']);?>" id="nom_res_country" name="nom_res_country" class="form-control" readonly>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" value="<?php echo $this->state_name($result['nom_res_state']);?>" id="nom_res_state" name="nom_res_state" class="form-control" readonly>
							<span class="msgValid" id="msgnom_res_state"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" value="<?php echo $this->city_name($result['nom_res_city']);?>" id="nom_res_city" name="nom_res_city" class="form-control" readonly>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="nom_res_address" id="nom_res_address" class="form-control" cols="3" readonly><?php echo $result['nom_res_address']; ?></textarea>
							<span class="msgValid" id="msgnom_res_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['nom_res_pincode']; ?>" id="nom_res_pincode" name="nom_res_pincode" class="form-control" readonly>
						<span class="msgValid" id="msgnom_res_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Id Proof</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<a href="media/plot_booking/<?php echo $result['nom_id_proof'];?>" target="_blank" >
							<img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['nom_id_proof']) { ?>media/plot_booking/<?php echo $result['nom_id_proof'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgscan_form"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Nominee Photo</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<a href="media/plot_booking/<?php echo $result['nom_photo'];?>" target="_blank" >
							<img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['nom_photo']) { ?>media/plot_booking/<?php echo $result['nom_photo'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>
						<span class="msgValid" id="msgapplicant_photo"></span>
					</div>
					
					</div>
					<div class="clearfix"></div>
				</div>
					<br>
              </div>
            </fieldset>
            <!--=================Nominee Details End here =====================-->          
      
      <!--account details ends here-->
       </div>
   		</form>
 	</div>
</div>

</section>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   