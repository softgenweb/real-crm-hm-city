<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-3{
		padding: unset !important;
	}
	.tabs .col-md-3.active legend{
		background: #102b53 none repeat scroll 0 0;
	}
</style>
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Plot Booking</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Plot Booking </li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Booking </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php 
        	unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']); 

			//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
	   }	   ?>
            
  
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
			<div class="box-body">				
          <!--=======================Plot Details======================-->	  
<?php
	$project = mysql_fetch_array(mysql_query("SELECT * FROM `project_plot` WHERE `id`='".$result['project_plot_id']."'"));
	$area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id` ='".$project['area_id']."'"));

 ?>
            <fieldset>
            	<div class="col-md-12 tabs">
            		<div class="col-md-3 active"><a href="index.php?control=plot_booking&task=step1"><legend>Plot Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step2"><legend>Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step3"><legend>Co-Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step4"><legend>Nominee Details</legend></a></div>
            	</div>
				
				<div class="col-sm-10 col-md-offset-1 first_header">
						<div class="form-group oneline">
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Project Name</label>
								<!-- <b style="color:red">*</b> --> 
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
									<select name="project_id" id="project" onchange="booking_block(this.value)" class="form-control" autocomplete="off"  required="">
										<option value="">Select</option>
										<?php $sql = mysql_query("SELECT * FROM `project` WHERE `id` in (SELECT `project_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
											while($proplot = mysql_fetch_array($sql)) {?>
											<option value="<?php echo $proplot['id']; ?>" <?php if($project['project_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['project_name']; ?></option>
										<?php }?>
									</select>
									
									<span class="msgValid" id="msgbranch_id"></span> 
								</div>

							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Block Name</label>
								<!-- <b style="color:red">*</b> --> 
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
									<select name="block_id" id="block_id" onchange="booking_plot(this.value)" class="form-control" autocomplete="off"  required="">
										<option value="">Select</option>
										
										<?php $sql = mysql_query("SELECT * FROM `block` WHERE `id` in (SELECT `block_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
											while($proplot = mysql_fetch_array($sql)) {?>
											<option value="<?php echo $proplot['id']; ?>" <?php if($project['block_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['block_name']; ?></option>
										<?php }?> 
									</select>
									<!-- <input type="text"  value="<?php echo $result['vehicle_no']; ?>" id="vehicle_no" name="vehicle_no" class="form-control" autocomplete="off" readonly> -->
								<span class="msgValid" id="msgvehicle_no"></span>
								</div>
							</div>  
							<div class="clearfix"></div>
							<div class="form-group oneline">
								
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Plote Name</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
								<div class="col-md-4 col-sm-4 col-xs-12">
									<select name="plot_id" id="plot_id" class="form-control" onchange="booking_area(this.value)" autocomplete="off"  required="">
										<option value="">Select</option>
										<?php $sql = mysql_query("SELECT * FROM `plot` WHERE `id` in (SELECT `plot_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
											while($proplot = mysql_fetch_array($sql)) {?>
											<option value="<?php echo $proplot['id']; ?>" <?php if($project['plot_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['plot_name']; ?></option>
										<?php }?> 
									</select>
									<!-- <input type="text"  value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" autocomplete="off" readonly> -->
									<span class="msgValid" id="msgvehicle_no"></span>
								</div>
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Area Dimention</label>
							</div>
							<?php  ?>
								<div class="col-md-4 col-sm-4 col-xs-12 "  id="plotarea">
								<input type="text" value="<?php echo $result['area_dimension']; ?>" id="area_id" name="area_dimension" class="form-control area_dimension" autocomplete="off" readonly>
								<!-- ============================================================== -->
								<input type="hidden"  value="<?php echo $area['square_feet']; ?>" id="area_in_ft" name="area_in_ft" class="form-control" autocomplete="off" readonly>
								<input type="hidden"  value="<?php echo $result['project_plot_id']; ?>" id="project_plot_id" name="project_plot_id" class="form-control" autocomplete="off" readonly>
								<input type="hidden"  value="<?php echo $result['project_name']; ?>" id="project_name" name="project_name" class="form-control" autocomplete="off" readonly>
								<input type="hidden"  value="<?php echo $result['block_name']; ?>" id="block_name" name="block_name" class="form-control" autocomplete="off" readonly>
								<input type="hidden"  value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" autocomplete="off" readonly>
								<span class="msgValid" id=""></span>
								</div>
							</div>
								  <div class="clearfix"></div>
						<div class="form-group oneline">
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Rate(per ft)</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" value="<?php echo $result['rate']; ?>" id="plot_rate" name="plot_rate" class="form-control" autocomplete="off" required>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Total Price</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text"  value="<?php echo $result['total_price']?$result['total_price']:'0'; ?>" id="plot_price" name="plot_price" class="form-control" autocomplete="off" readonly>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
						</div>

						<div class="form-group oneline">
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>GST/IGST %</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="gst" id="gst" class="form-control" autocomplete="off"  required="">
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `tax_master` WHERE `status`= 1"); 
											while($proplot = mysql_fetch_array($sql)) {?>
								<option value="<?php echo $proplot['id']; ?>" <?php if($project['project_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['gst']; ?></option>
										<?php }?>
							</select>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label> G. Total Price</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text"  value="<?php echo $result['total_price']?$result['total_price']:'0'; ?>" id="grand_total" name="grand_total" class="form-control" autocomplete="off" readonly>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
						</div>
					</div>


              <!--panel1 end here -->
            </fieldset>
            
         
          <div class="row" style="text-align: center;">
           
           
            	<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
        
            <a style="position:relative; right:34px;" href="javascript:;" class="btn btn-primary redu" onclick="reload();" >Cancel</a> 
			</div>
          <input type="hidden" name="control" value="plot_booking"/>
          <input type="hidden" name="step" value="step1"/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="task" value="save"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
         
      
      <!--account details ends here-->
       </div>
   		</form>
 	</div>
</div>



</section>

<script>
function goBack() {
    window.history.back();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#accident_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',

});
$('#dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',

});
$('#co_dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',

});
$('#registration_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',

});

</script> 




<script type="text/javascript">
	/*===============================Gaurav Js================*/
	function booking_block(str) { 
//alert("hii");
var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("block_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_block.php?id="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('project_name').value  = $('#project').find(':selected').text();
}



	function booking_plot(str) {
{ 
//alert("hii");
var pid = $('#project').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("plot_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_plot.php?pid="+pid+"&bid="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('block_name').value  = $('#block_id').find(':selected').text();
}
}


	function booking_area(str) 
{ 
//alert("hii");
var pid = $('#project').val();
var bid = $('#block_id').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
	var plot_area = xmlhttp.responseText; 
	var res = plot_area.split("#");

	document.getElementById("area_id").value = res[0]; 
	document.getElementById("area_in_ft").value = res[1]; 
// document.getElementById("plotarea").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_area.php?project="+pid+"&block="+bid+"&plot="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();

// document.getElementById('area_id').style.display = none;
document.getElementById('plot_name').value  = $('#plot_id').find(':selected').text();
}


 /*=========================================*/

$("#plot_id").change(function(){
	var str = $(this).val();
	var pid = $('#project').val();
	var bid = $('#block_id').val();
    $.ajax({url: "script/plot_booking/project_plot_id.php?project="+pid+"&block="+bid+"&plot="+str, success: function(result){
        $("#project_plot_id").val(result);
    }});
});
 /*=========================================*/



  /*=========================================*/

$('#plot_rate').keyup(function(){
	var rate = $(this).val();
	var sqft = $('#area_in_ft').val();
	document.getElementById('plot_price').value = rate*sqft;
});

$('#gst').change(function(){
	var tax = $(this).find(':selected').text();
	var total = $('#plot_price').val();
	document.getElementById('grand_total').value = parseInt(total)+parseInt((tax*total)/100); 
});

</script>
<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(1000).slideUp(300, function() {
		$(this).alert('close');
	});
</script>
   