<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-3{
		padding: unset !important;
	}
	.tabs .col-md-3.active legend{
		background: #102b53 none repeat scroll 0 0;
	}
</style>
<!-- <script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script> -->
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Plot Booking</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Plot Booking </li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Booking </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php 
        	unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']); 

			// header( "refresh:1;url=index.php?control=plot_booking&task=show" );
	   }	   ?>
            
  
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
			<div class="box-body">				
          <!--=======================Plot Details======================-->	  
<?php
	$project = mysql_fetch_array(mysql_query("SELECT * FROM `project_plot` WHERE `id`='".$result['project_plot_id']."'"));
	$area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id` ='".$project['area_id']."'"));

 ?>


            
            <!--==================Co-Applicant Details Start here =====================-->
            <fieldset>
            	<div class="col-md-12 tabs">
            		<div class="col-md-3"><a href="javascript:;"><legend>Plot Details</legend></a></div>
            		<div class="col-md-3"><a href="javascript:;"><legend>Applicant Details</legend></a></div>
            		<div class="col-md-3 active"><a href="javascript:;"><legend>Co-Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step4"><legend>Nominee Details</legend></a></div>
            	</div>
           
				<div class="col-sm-10 col-md-offset-1 third_header ons" >
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Title</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_title" id="co_title" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<option value="Mr" <?php if($result['co_title']=='Mr') {?> selected="selected" <?php } ?>>Mr</option>
								<option value="Ms" <?php if($result['co_title']=='Ms') {?> selected="selected" <?php } ?>>Ms</option>
								<option value="Mrs" <?php if($result['co_title']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option>
								
							</select>
							<span class="msgValid" id="msgco_title"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Co-Applicant Name</label> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_applicant_name']; ?>" id="co_applicant_name" name="co_applicant_name" class="form-control" autocomplete="off">
						<span class="msgValid" id="msgapplicant_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Realtion</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_relation" id="co_relation" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<option value="Son" <?php if($result['co_relation']=='Son') {?> selected="selected" <?php } ?>>Son</option>
								<option value="Daughter" <?php if($result['co_relation']=='Daughter') {?> selected="selected" <?php } ?>>Daughter </option>
								<option value="Wife" <?php if($result['co_relation']=='Wife') {?> selected="selected" <?php } ?>>Wife </option>
								<option value="Husband" <?php if($result['co_relation']=='Husband') {?> selected="selected" <?php } ?>>Husband </option>
								
							</select>
							<span class="msgValid" id="msgrelation"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Father/Husband Name</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_father_husband_name']; ?>" id="co_father_husband_name" name="co_father_husband_name" class="form-control" autocomplete="off">
						<span class="msgValid" id="msgco_father_husband_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Date Of Birth</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_dob']; ?>" id="co_dob" name="co_dob" class="form-control" autocomplete="off" readonly>
							<span class="msgValid" id="msgco_dob"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Profession</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<!-- <select name="co_profession" id="co_profession" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<option value="Son" <?php if($result['co_profession']=='Son') {?> selected="selected" <?php } ?>>Son</option>
								<option value="Daughter" <?php if($result['co_profession']=='Daughter') {?> selected="selected" <?php } ?>>Daughter </option>
								<option value="Wife" <?php if($result['co_profession']=='Wife') {?> selected="selected" <?php } ?>>Wife </option>
								<option value="Husband" <?php if($result['co_profession']=='Husband') {?> selected="selected" <?php } ?>>Husband </option>
								
							</select> -->
							<input type="text"  value="<?php echo $result['co_profession']; ?>" id="co_profession" name="co_profession" class="form-control" autocomplete="off">
							<span class="msgValid" id="msgprofession"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Residential Status</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_residence_status" id="co_residence_status" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<option value="Resident" <?php if($result['co_residence_status']=='Resident') {?> selected="selected" <?php } ?>>Resident</option>
								<option value="NonResident" <?php if($result['co_residence_status']=='NonResident') {?> selected="selected" <?php } ?>>Non-Resident </option>
								<option value="ForeignNOIO" <?php if($result['co_residence_status']=='ForeignNOIO') {?> selected="selected" <?php } ?>>Foreign National Of Indian Origin </option>
								<option value="Husband" <?php if($result['co_residence_status']=='Husband') {?> selected="selected" <?php } ?>>Husband </option>
								
							</select>
							<span class="msgValid" id="msgco_residence_status"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Mobile Number</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_mobile']; ?>" maxlength="10" id="co_mobile" name="co_mobile" class="form-control" autocomplete="off">
							<span class="msgValid" id="msgmobile"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Email</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['co_email']; ?>"  id="co_email" name="co_email" class="form-control" autocomplete="off">
							<span class="msgValid" id="msgmobile"></span>
						</div>
						</div> 
						<div class="clearfix"></div><br>
						<label><strong>Co-Applicant Residential Address :</strong></label>
						<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_res_country" id="co_res_country" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `country` WHERE 1"); 
									while($contry = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $contry['id']; ?>" <?php if($result['co_res_country']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
								<?php }?>
							</select>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_res_state" id="co_res_state" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['co_res_country']."'"); 
									while($state = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $state['id']; ?>" <?php if($result['co_res_state']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
								<?php }?>
							</select>
							<span class="msgValid" id="msgco_res_state"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<select name="co_res_city" id="co_res_city" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['co_res_state']."' AND `country_id`='".$result['co_res_country']."'"); 
									while($city = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $city['id']; ?>" <?php if($result['co_res_city']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
								<?php }?>
							</select>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="co_res_address" id="co_res_address" class="form-control" cols="3"><?php echo $result['co_res_address']; ?></textarea>
							<span class="msgValid" id="msgco_res_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['co_res_pincode']; ?>" id="co_res_pincode" name="co_res_pincode" class="form-control" autocomplete="off">
						<span class="msgValid" id="msgco_res_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
						<label><strong>Co-Applicant Office Address :</strong></label>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_office_country" id="co_office_country" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `country` WHERE 1"); 
									while($contry = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $contry['id']; ?>" <?php if($result['co_office_country']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
								<?php }?>
							</select>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="co_office_state" id="co_office_state" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['co_office_country']."'"); 
									while($state = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $state['id']; ?>" <?php if($result['co_office_state']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
								<?php }?>
							</select>
							<span class="msgValid" id="msgco_office_state"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<select name="co_office_city" id="co_office_city" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['co_office_state']."' AND `country_id`='".$result['co_office_country']."'"); 
									while($city = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $city['id']; ?>" <?php if($result['office_city']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
								<?php }?>
							</select>
						<span class="msgValid" id="msgco_office_city"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="co_office_address" id="co_office_address" class="form-control" cols="3"><?php echo $result['co_office_address']; ?></textarea>
							<span class="msgValid" id="msgco_office_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['co_office_pincode']; ?>" id="co_office_pincode" name="co_office_pincode" class="form-control" autocomplete="off">
						<span class="msgValid" id="msgoffice_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Nationality</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="text"  value="<?php echo $result['co_nationality']; ?>" id="co_nationality" name="co_nationality" class="form-control" autocomplete="off">
						<span class="msgValid" id="msgnationality"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Id Proof</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="file" class="form-control-file" value="<?php echo $result['co_id_proof']; ?>" id="co_id_proof" name="co_id_proof" class="form-control" autocomplete="off">
						<a target="_blank" href="media/plot_booking/<?php echo $result['co_id_proof']; ?>" id="co_id_proof_a"><img id="co_id_proof_img" src="media/plot_booking/<?php echo $result['co_id_proof']; ?>" alt="" height="50"  /></a>
						<span class="msgValid" id="msgco_id_proof"></span>
					</div>
				</div>
				<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Form Scan</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="file" class="form-control-file" value="<?php echo $result['co_scan_form']; ?>" id="co_scan_form" name="co_scan_form" class="form-control" autocomplete="off">
						<a target="_blank" href="media/plot_booking/<?php echo $result['co_scan_form']; ?>" id="co_scan_form_a"><img id="co_scan_form_img" src="media/plot_booking/<?php echo $result['co_scan_form']; ?>" alt="" height="50"  /></a>
						<span class="msgValid" id="msgco_scan_form"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Co-Applicant Photo</label>
						 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="file" class="form-control-file" value="<?php echo $result['co_applicant_photo']; ?>" id="co_applicant_photo" name="co_applicant_photo" class="form-control" autocomplete="off">
						<a target="_blank" href="media/plot_booking/<?php echo $result['co_applicant_photo']; ?>" id="co_applicant_photo_a"><img id="co_applicant_photo_img" src="media/plot_booking/<?php echo $result['co_applicant_photo']; ?>" alt="" height="50"  /></a>
						<span class="msgValid" id="msgco_applicant_photo"></span>
					</div>
					
					</div>
					<div class="clearfix"></div>

				</div>
		   </fieldset>
            <!--================Co-Applicant Details End here ======================--> 
    
                <div class="row" style="text-align: center;">
       
            	<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
        
            <!-- <a style="position:relative; right:34px;" href="javascript:;" class="btn btn-primary redu" onclick="reload();" >Cancel</a>  -->

            <a style="position:relative; right:34px;" href="index.php?control=plot_booking&task=step4&stepid=<?php echo $_REQUEST['stepid']; ?>" class="btn btn-primary redu" >Skip</a> 
			</div>
          <input type="hidden" name="control" value="plot_booking"/>
          <input type="hidden" name="step" value="step3"/>
          <input type="hidden" name="stepid" value="<?php echo $_REQUEST['stepid']; ?>"/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="task" value="save"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
      <!--account details ends here-->
       </div>
   		</form>
 	</div>
</div>



</section>

<script>
function goBack() {
    window.history.back();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#accident_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#co_dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#registration_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#dpa').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#driver_dpa').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#expect_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script> 


<script type="text/javascript">
$(document).ready(function(){
 $ = jQuery.noConflict();
   $(".show_hide1").click(function(){
        $(".first_header").toggle();
    });
	 $(".show_hide2").click(function(){
        $(".second_header").toggle();
    });
	 $(".show_hide3").click(function(){
        $(".third_header").toggle();
    });
	$(".show_hide4").click(function(){
        $(".fourth_header").toggle();
    });
	
 
}); 
</script>

<script type="text/javascript">
	/*===============================Gaurav Js================*/
	function booking_block(str) { 
//alert("hii");
var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("block_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_block.php?id="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('project_name').value  = $('#project').find(':selected').text();
}



	function booking_plot(str) {
{ 
//alert("hii");
var pid = $('#project').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("plot_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_plot.php?pid="+pid+"&bid="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('block_name').value  = $('#block_id').find(':selected').text();
}
}


	function booking_area(str) 
{ 
//alert("hii");
var pid = $('#project').val();
var bid = $('#block_id').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
	var plot_area = xmlhttp.responseText; 
	var res = plot_area.split("#");

	document.getElementById("area_id").value = res[0]; 
	document.getElementById("area_in_ft").value = res[1]; 
// document.getElementById("plotarea").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_area.php?project="+pid+"&block="+bid+"&plot="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();

// document.getElementById('area_id').style.display = none;
document.getElementById('plot_name').value  = $('#plot_id').find(':selected').text();
}


 /*=========================================*/

$("#plot_id").change(function(){
	var str = $(this).val();
	var pid = $('#project').val();
	var bid = $('#block_id').val();
    $.ajax({url: "script/plot_booking/project_plot_id.php?project="+pid+"&block="+bid+"&plot="+str, success: function(result){
        $("#project_plot_id").val(result);
    }});
});
 /*=========================================*/
$("#office_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
      $("#office_state").html(result);
      
    }});
});
  /*=========================================*/
$("#office_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#office_city").html(result);
    }});
});
 
 /*=========================================*/

$("#co_res_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
        $("#co_res_state").html(result);
    }});
});
 /*=========================================*/ 
$("#co_res_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#co_res_city").html(result);
    }});
});
 /*=========================================*/
$("#co_office_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
        $("#co_office_state").html(result);
    }});
});
 /*=========================================*/ 
 
$("#co_office_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#co_office_city").html(result);
    }});
});
  /*=========================================*/
 
$("#nom_res_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
        $("#nom_res_state").html(result);
    }});
});
  /*=========================================*/
 
$("#nom_res_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#nom_res_city").html(result);
    }});
});
 
  /*=========================================*/

$('#plot_rate').keyup(function(){
	var rate = $(this).val();
	var sqft = $('#area_in_ft').val();
	document.getElementById('plot_price').value = rate*sqft;
})

/*=================Show Image=================*/

/*===========For Applicant===============*/
function readid_proof(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#id_proof_img').attr('src', e.target.result);
            $('#id_proof_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#id_proof").change(function(){
	readid_proof(this);
});


function readscan_form(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#scan_form_img').attr('src', e.target.result);
            $('#scan_form_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#scan_form").change(function(){
    readscan_form(this);
});

function readapplicant_photo(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#applicant_photo_img').attr('src', e.target.result);
            $('#applicant_photo_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#applicant_photo").change(function(){
    readapplicant_photo(this);
});

/*===========For Co-Applicant===============*/
function readco_id_proof(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#co_id_proof_img').attr('src', e.target.result);
            $('#co_id_proof_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#co_id_proof").change(function(){
    readco_id_proof(this);
});

function readco_scan_form(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#co_scan_form_img').attr('src', e.target.result);
            $('#co_scan_form_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#co_scan_form").change(function(){
    readco_scan_form(this);
});

function readco_applicant_photo(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#co_applicant_photo_img').attr('src', e.target.result);
            $('#co_applicant_photo_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#co_applicant_photo").change(function(){
    readco_applicant_photo(this);
});

/*===========For Nominee===============*/
function readnom_id_proof(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#nom_id_proof_img').attr('src', e.target.result);
            $('#nom_id_proof_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#nom_id_proof").change(function(){
    readnom_id_proof(this);
});

function readnom_photo(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#nom_photo_img').attr('src', e.target.result);
            $('#nom_photo_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#nom_photo").change(function(){
    readnom_photo(this);
});
</script>
<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   