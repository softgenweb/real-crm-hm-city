<section class="content-header">
          <!--<h1>
            Staff Management
          </h1>-->
          <!--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Staff</li>
          </ol>-->
        </section>
<?php foreach($results as $result) { }  ?>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>



<section class="content">
       <div class="panel panel-default">
       <div class="box-header">
 			<h3 class="box-title">Lawyer Profile</h3>
       </div>
                        <!--<div class="panel-heading">
                         <a onclick="goBack()" ><i class="fa fa-chevron-circle-left" style="font-size:20px;" title="Back"></i></a>
                        </div>-->
        <div class="panel-body">
        <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

            <div class="row">

               <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-2">
                <div class="row">
                               
                  
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Lawyer Firm</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="frmSearch_lawyer">
                         <input type="text"  value="<?php echo $result['lawyer_firm']; ?>" id="lawyer_firm" name="lawyer_firm" class="form-control" autocomplete="off" > 
                          <div id="suggesstion_lawyer-box"></div>
                    	<span class="msgValid" id="msglawyer_firm"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Address</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['lawyer_address']; ?>" id="lawyer_address" name="lawyer_address" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msglawyer_address"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Name</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['lawyer_name']; ?>" id="lawyer_name" name="lawyer_name" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msglawyer_name"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Contact No.</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       
                    <input type="text" id="lawyer_contact" name="lawyer_contact" class="form-control" value="<?php echo $result['lawyer_contact']; ?>" />
                    <span class="msgValid" id="msglawyer_contact"></span>
                        </div>
                </div>
                
                <div class="form-group oneline" >
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Email</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       <input type="text"  value="<?php echo $result['lawyer_email']; ?>" id="lawyer_email" name="lawyer_email" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msglawyer_email"></span>
                        </div>
                </div>
                
                <div class="form-group oneline" >
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Remark</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['lawyer_position']; ?>" id="lawyer_position" name="lawyer_position" class="form-control" autocomplete="off" > 
                    	 <span class="msgValid" id="msglawyer_position"></span>
                        </div>
                </div>
                
                              
               
                
                <div class="form-group oneline">    
        		 <div class="col-md-8 col-sm-8 col-xs-12 pull-right">
                 <br />
            
            <?php  
            
            if($_REQUEST['id'] !='')
            { ?>
            <center><input type="submit" name="update" class="btn btn-primary butoon_brow" value="Update"></center>
            <?php } else { ?> 
            <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Submit"></center>
            <?php } ?>
            	</div>
               </div>
            	
<input type="hidden" name="control" value="profile"/>
<input type="hidden" name="edit" value="1"/>
<input type="hidden" name="task" value="save_lawyer"/>
<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
<input type="hidden" name="student_id" id="student_id" value="<?php echo $_SESSION['student_id']; ?>"  />

               </div>
                    
               </div>
               
                   <!--account details ends here-->
          		</div>
            </form>
             </div>
                        </div>
                     
        </section>




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('name').value == '') { 
			document.getElementById('msgname').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!isletter(document.getElementById('name').value)) { 
			document.getElementById('msgname').innerHTML = "*Enter Valid Name.";
			chk=0;
		}
		else {
			document.getElementById('msgname').innerHTML = "";
		}
		
		if(document.getElementById('mobile').value == '') { 
			document.getElementById('msgmobile').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!numericWithoutPoint(document.getElementById('mobile').value)) { 
			document.getElementById('msgmobile').innerHTML = "*Enter Valid Number.";
			chk=0;
		}
		else {
			document.getElementById('msgmobile').innerHTML = "";
		}
		
		
		if(document.getElementById('address').value == '') { 
			document.getElementById('msgaddress').innerHTML = "*Required field.";
		chk=0;
		}
		
		else {
			document.getElementById('msgaddress').innerHTML = "";
		}


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>
<style>
.frmSearch_lawyer {border: 1px solid #a8d4b1;background-color: #c6f7d0;margin: 2px 0px;padding:40px;border-radius:4px;}
#lawyer_list{float:left;list-style:none;padding:0;width:100%;position: relative;z-index:999999;}
#lawyer_list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#lawyer_list li:hover{background:#6a7a91;cursor: pointer; color:#fff;}
#lawyer_firm{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
</style>

<script>
$(document).ready(function(){	 
	
	$("#lawyer_firm").keyup(function(){
		$.ajax({
		type: "POST",
		url: "views/profile/lawyer_firm.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#lawyer_firm").css("background","url(LoaderIcon.gif) right 5px bottom 5px no-repeat rgb(255, 255, 255)");
		},
		success: function(data){
			$("#suggesstion_lawyer-box").show();
			$("#suggesstion_lawyer-box").html(data);
			$("#lawyer_firm").css("background","#FFF");
		}
		});
	});
})
function selectlawyer(val,objid) {
$("#lawyer_firm").val(val);
$("#suggesstion_lawyer-box").hide();
full_detail(objid)
}
function full_detail(str)
{ //alert(str);
	
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		 
			//var data = $.parseJSON(xmlhttp.responseText);
			document.getElementById("lawyer_address").value = xmlhttp.responseText;
			
		 //alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/lawyer_company.php?id="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}



function goBack() {
    window.history.back();
}
</script>

