<section class="content-header">
          <!--<h1>
            Staff Management
          </h1>-->
          <!--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Staff</li>
          </ol>-->
        </section>
<?php foreach($results as $result) { }  ?>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>



<section class="content">
       <div class="panel panel-default">
       <div class="box-header">
 			<h3 class="box-title">Supplier Profile</h3>
       </div>
                        <!--<div class="panel-heading">
                         <a onclick="goBack()" ><i class="fa fa-chevron-circle-left" style="font-size:20px;" title="Back"></i></a>
                        </div>-->
        <div class="panel-body">
        <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

            <div class="row">

               <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-2">
                <div class="row">
                               
                  
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Supplier</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['supplier_firm']; ?>" id="supplier_firm" name="supplier_firm" class="form-control" autocomplete="off" > 
                    	<span class="msgValid" id="msglawyer_firm"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Name</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['supplier_name']; ?>" id="supplier_name" name="supplier_name" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msgsupplier_name"></span>
                        </div>
                </div>
                
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Contact No.</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       
                    <input type="text" id="supplier_contact" name="supplier_contact" class="form-control" value="<?php echo $result['supplier_contact']; ?>" />
                    <span class="msgValid" id="msgsupplier_contact"></span>
                        </div>
                </div>
                
                <div class="form-group oneline" >
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Email</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       <input type="text"  value="<?php echo $result['supplier_email']; ?>" id="supplier_email" name="supplier_email" class="form-control" autocomplete="off" > 
                    
                    <span class="msgValid" id="msgsupplier_email"></span>
                        </div>
                </div>
                
                <div class="form-group oneline" >
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Position</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                         <input type="text"  value="<?php echo $result['supplier_position']; ?>" id="supplier_position" name="supplier_position" class="form-control" autocomplete="off" > 
                    	 <span class="msgValid" id="msgsupplier_position"></span>
                        </div>
                </div>
                
                              
               
                
                <div class="form-group oneline">    
        		 <div class="col-md-8 col-sm-8 col-xs-12 pull-right">
                 <br />
            
            <?php  
            
            if($_REQUEST['id'] !='')
            { ?>
            <center><input type="submit" name="update" class="btn btn-primary butoon_brow" value="Update"></center>
            <?php } else { ?> 
            <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Submit"></center>
            <?php } ?>
            	</div>
               </div>
            	
<input type="hidden" name="control" value="profile"/>
<input type="hidden" name="edit" value="1"/>
<input type="hidden" name="task" value="save_supplier"/>
<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
<input type="hidden" name="student_id" id="student_id" value="<?php echo $_SESSION['student_id']; ?>"  />

               </div>
                    
               </div>
               
                   <!--account details ends here-->
          		</div>
            </form>
             </div>
                        </div>
                     
        </section>




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('name').value == '') { 
			document.getElementById('msgname').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!isletter(document.getElementById('name').value)) { 
			document.getElementById('msgname').innerHTML = "*Enter Valid Name.";
			chk=0;
		}
		else {
			document.getElementById('msgname').innerHTML = "";
		}
		
		if(document.getElementById('mobile').value == '') { 
			document.getElementById('msgmobile').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!numericWithoutPoint(document.getElementById('mobile').value)) { 
			document.getElementById('msgmobile').innerHTML = "*Enter Valid Number.";
			chk=0;
		}
		else {
			document.getElementById('msgmobile').innerHTML = "";
		}
		
		
		if(document.getElementById('address').value == '') { 
			document.getElementById('msgaddress').innerHTML = "*Required field.";
		chk=0;
		}
		
		else {
			document.getElementById('msgaddress').innerHTML = "";
		}


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>

<script>
function goBack() {
    window.history.back();
}
</script>

