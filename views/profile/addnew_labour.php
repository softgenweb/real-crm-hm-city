<section class="content-header">
          <!--<h1>
            Staff Management
          </h1>-->
          <!--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Staff</li>
          </ol>-->
        </section>
<?php foreach($results as $result) { }  ?>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>



<section class="content">
       <div class="panel panel-default">
       <div class="box-header">
 			<h3 class="box-title">Labour Profile</h3>
       </div>
                        <!--<div class="panel-heading">
                         <a onclick="goBack()" ><i class="fa fa-chevron-circle-left" style="font-size:20px;" title="Back"></i></a>
                        </div>-->
        <div class="panel-body">
        <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

            <div class="row">

               <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-2">
                <div class="row">
                             
               
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Labour Description</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                       
                       <textarea id="labour_description" name="labour_description" class="form-control"><?php echo $result['labour_description']; ?></textarea>
                       <span class="msgValid" id="msglabour_description"></span>
                   
                        </div>
                </div>
                <div class="form-group oneline">
                        <div class="col-md-4 col-sm-4 col-xs-12" id="br">
                        <label>Cost</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                        
                            <input type="text"  value="<?php echo $result['cost']; ?>" id="cost" name="cost" class="form-control" autocomplete="off" > 
                             <span class="msgValid" id="msgcost"></span>
                        </div>
                </div>
                
               
                <div class="form-group oneline">    
        		 <div class="col-md-8 col-sm-8 col-xs-12 pull-right">
                 <br />
            
            <?php  
            
            if($_REQUEST['id'] !='')
            { ?>
            <center><input type="submit" name="update" class="btn btn-primary butoon_brow" value="Update"></center>
            <?php } else { ?> 
            <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Submit"></center>
            <?php } ?>
            	</div>
               </div>
            	
<input type="hidden" name="control" value="profile"/>
<input type="hidden" name="edit" value="1"/>
<input type="hidden" name="task" value="save_labour"/>
<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
<input type="hidden" name="student_id" id="student_id" value="<?php echo $_SESSION['student_id']; ?>"  />

               </div>
                    
               </div>
               
                   <!--account details ends here-->
          		</div>
            </form>
             </div>
                        </div>
                     
        </section>




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('name').value == '') { 
			document.getElementById('msgname').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!isletter(document.getElementById('name').value)) { 
			document.getElementById('msgname').innerHTML = "*Enter Valid Name.";
			chk=0;
		}
		else {
			document.getElementById('msgname').innerHTML = "";
		}
		
		if(document.getElementById('mobile').value == '') { 
			document.getElementById('msgmobile').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!numericWithoutPoint(document.getElementById('mobile').value)) { 
			document.getElementById('msgmobile').innerHTML = "*Enter Valid Number.";
			chk=0;
		}
		else {
			document.getElementById('msgmobile').innerHTML = "";
		}
		
		
		if(document.getElementById('address').value == '') { 
			document.getElementById('msgaddress').innerHTML = "*Required field.";
		chk=0;
		}
		
		else {
			document.getElementById('msgaddress').innerHTML = "";
		}


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>

<script>
function goBack() {
    window.history.back();
}
</script>


<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
$('#car_in_time').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#car_out_time').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});


</script>

<script src="jquery/useravailability/jquery-1.8.1.min.js" type="text/javascript"></script>
<script src="jquery/useravailability/livevalidation.js" type="text/javascript"></script>
<script type="text/useravailability/javascript" src="jquery/jquery-1.9.0.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
$ = jQuery.noConflict();
$(document).ready(function($) { 
$("#userid").on('change',function (e) { 

	//removes spaces from username
	$(this).val($(this).val().replace(/\s/g, ''));
	
	var user_id = $(this).val();
	if(user_id.length < 2){$("#erruserid").html('');return;}
	
	if(user_id.length >= 2){
		$("#erruserid").html('<img src="media/ajax-loader.gif" />');
		$.post('script/check_userid.php', {'userid':user_id}, function(data) {
		  $("#erruserid").html(data);
		});
	}
});    
});
</script>
<!--<script type="text/javascript">
$ = jQuery.noConflict();
$(document).ready(function($) { 
$("#userid").blur(function (e) { 

	//removes spaces from username
	$(this).val($(this).val().replace(/\s/g, ''));
	
	var user_id = $(this).val();
	if(user_id.length < 2){$("#erruserid").html('');return;}
	
	if(user_id.length >= 2){
		$("#erruserid").html('<img src="media/ajax-loader.gif" />');
		$.post('script/check_userid.php', {'userid':user_id}, function(data) {
		  $("#erruserid").html(data);
		});
	}
});    
});
</script>-->
<script>
function branch() {
    var acs = document.getElementById("access_right").value;
	if(acs=='Administrator')
	{
		document.getElementById("branch_id").style.display = "none";
		document.getElementById("br").style.display = "none";
	}
	else {
				document.getElementById("branch_id").style.display = "block";

		document.getElementById("br").style.display = "block";
	}
}
function customer(str)
{
	if(str == 'Old')
	{
	document.getElementById("cu_name").style.display = "none";
	document.getElementById("cu_old_name").style.display = "block";
	}
	else 
	{
	document.getElementById("cu_name").style.display = "block";
	document.getElementById("cu_old_name").style.display = "none";	
	}
}
function full_detail(str)
{
	cu_mobile();
	cu_nric();
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		document.getElementById("cust_address").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/cust_address.php?id="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}
function cu_mobile()

{    var id = document.getElementById("cust_old_name").value; 
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		document.getElementById("cust_mobile").value=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/cust_mobile.php?id="+id, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}
function cu_nric()

{    var id = document.getElementById("cust_old_name").value; 
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		document.getElementById("cust_nric").value=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/cust_nric.php?id="+id, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}
</script>
