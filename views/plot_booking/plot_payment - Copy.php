<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
  .tabs, .tabs .col-md-3{
    padding: unset !important;
  }
  .tabs .col-md-3.active legend{
    background: #102b53 none repeat scroll 0 0;
  }
</style>
  <div class="panel panel-default" >
    <div class="box-header">        
    <h3 class="box-title">Plot EMI Payment</h3>
  </div> 
  <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Plot Booking </li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Plot EMI Payment </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php 
          unset($_SESSION['alertmessage']);
          unset($_SESSION['errorclass']); 

      //header( "refresh:1;url=index.php?control=plot_booking&task=show" );
     }    ?>

  <div class="panel-body">
       <?php
  $project = mysql_fetch_array(mysql_query("SELECT * FROM `project_plot` WHERE `id`='".$result['project_plot_id']."'"));
  $area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id` ='".$project['area_id']."'"));

  $emi = mysql_fetch_assoc(mysql_query("SELECT * FROM `plot_emi` WHERE `plot_booking_id`='".$result['id']."' AND `emi_status` = 0"));

  $emi_paid = mysql_fetch_assoc(mysql_query("SELECT * FROM `plot_emi` WHERE `plot_booking_id`='".$result['id']."' AND `emi_status` = 1"));
 
 
         $emi_no = $emi['emi_no']?$emi['emi_no']:$emi_paid['emi_no'];
      
// if($emi_paid['balance_amount']>='0'){
 // $emi_no = $emi['emi_no'];
// }

 ?>
    <form name="form" id="payment" method="post" enctype="multipart/form-data" onsubmit="return validation();">
      <div class="box-body">        
          <!--=======================Plot Details======================-->    
            
            <!--====================================-->
            <fieldset>
        <div class="col-sm-10 col-md-offset-1 second_header ons" >
          <div class="form-group oneline">
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>EMI</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
               <input type="text"  value="<?php echo $emi_no; ?>" id="emi_no" name="emi_no" class="form-control" autocomplete="off" readonly>
            </div>

           
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Project <label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="<?php echo $result['project_name']; ?>" id="project_name" name="project_name" class="form-control" autocomplete="off" readonly>

            </div>
             </div>
          <div class="form-group oneline">
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Block</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
      <input type="text"  value="<?php echo $result['block_name']; ?>" id="block_name" name="block_name" class="form-control" autocomplete="off" readonly>
           
            </div>
       
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Plot</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" autocomplete="off" readonly>
          
            </div>

           </div>
      <div class="form-group oneline">

          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Area</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="<?php echo $result['area_dimension']; ?>" id="area_dimension" name="area_dimension" class="form-control" autocomplete="off" readonly>
            </div>

          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Total Plot Cost</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="<?php echo $result['grand_total']; ?>" id="grand_total" name="grand_total" class="form-control" autocomplete="off" readonly>
            </div>
        </div>

          <div class="form-group oneline">
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Total Paid </label>
             
          </div>
          
            <div class="col-md-4 col-sm-4 col-xs-12">

              <input type="text"  value="<?php echo $emi_paid['total_paid_amt']?$emi_paid['total_paid_amt']:$result['booking_amount']; ?>"  id="total_paid" name="total_paid" class="form-control" autocomplete="off" readonly>

            </div>
         <div class="col-md-2 col-sm-2 col-xs-12">
            <label>To be Paid (EMI)</label>
             
          </div>
          
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="<?php echo $emi['emi_installment']?$emi['emi_installment']:$emi_paid['balance_amount']; ?>"  id="emi_installment" name="emi_installment" class="form-control" autocomplete="off" readonly>
            </div>
          </div> 
           

            <div class="form-group oneline">
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Total To be Paid</label>
             
          </div>
          
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="<?php echo $emi_paid['balance_amount']+$emi['emi_installment']; ?>"  id="total_emi_installment" name="total_emi_installment" class="form-control" autocomplete="off" readonly>
            </div>
          
          <div class="col-md-2 col-sm-2 col-xs-12">
        <label>Total Balance</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">             
            <input type="text"  value="0"  id="total_balance_amount" name="total_balance_amount" class="form-control" autocomplete="off" readonly="">
            </div>

          </div>
          <div class="form-group oneline">

          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Paid Amount</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value=""  id="paid_amount" name="paid_amount" class="form-control" autocomplete="off" >

            </div>

          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Balance Amount</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value="0"  id="balance_amount" name="balance_amount" class="form-control" autocomplete="off" readonly="">
            </div>
          </div>

           <div class="form-group oneline">        
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Payment Type</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">             
           <select name="payment_type" id="payment_type" class="form-control" required="">
                <option value="">Select</option>
                <option value="cash" >Cash</option>
                <option value="cheque" >Cheque</option>
                <option value="neft" >NEFT/RTGS</option>
                <option value="dd" >DD</option>
            </select>
            </div>
            <div id="cheque_date" style="display: block;">
            <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Cheque Date</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value=""  id="cheque" placeholder="DD/MM/YYYY"  name="cheque_date" class="form-control" autocomplete="off" readonly="">
            </div>
          </div>
          </div>

          <div class="form-group oneline" id="bank_field" style="display: block;">
                  
        
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Bank Name</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">             
             <input type="text"  value=""  id="bank_name" name="bank_name" class="form-control" autocomplete="off" >
            </div>

          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Chq/DD/Txn No.</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value=""  id="txn_no" name="txn_no" class="form-control" autocomplete="off" >
            </div>
         
          </div>

          <div class="form-group oneline">

        
          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Deposit Date</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">             
             <input type="text"  value=""  id="deposit_date" name="deposit_date" placeholder="DD/MM/YYYY" class="form-control" autocomplete="off" readonly="">
            </div>

          <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Receipt Date</label>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <input type="text"  value=""  id="receipt_date" name="receipt_date" placeholder="DD/MM/YYYY" class="form-control" autocomplete="off" readonly="">
            </div>
         
        
     <!--      <div class="col-md-2 col-sm-2 col-xs-12">
            <label>Address</label>
             
          </div>
            <div class="col-md-4 col-sm-4 col-xs-12">             
             <input type="text"  value="<?php echo $result['booking_amount']; ?>"  id="booking_amount" name="booking_amount" class="form-control" autocomplete="off" >
            </div>
          </div>
 -->
        

           

       </fieldset>
    
            <!--=================Applicant Detail End here=====================--> 
            
<br>
<br>
          
       <?php $project_p = mysql_fetch_array(mysql_query("SELECT * FROM `plot_booking_tmp` WHERE `id`='".$_REQUEST['stepid']."'")); ?>
         
          <div class="row" style="text-align: center;">
       
              <input type="submit" name="save" value="Submit" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
         <a style="position:relative; right:34px;" href="javascript:;" class="btn btn-primary redu" onclick="reload();" >Reset</a>
           
      </div>
          <input type="hidden" name="control" value="plot_booking"/>
          <input type="hidden" name="task" value="pay_emi"/>
          <input type="hidden" name="customer_id" value="<?php echo $result['customer_id']; ?>"/>
          <input type="hidden" name="plot_booking_id" value="<?php echo $result['id']; ?>"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
      
      <!--account details ends here-->
       </div>
      </form>
       
  </div>

</div>



</section>

<script>
function goBack() {
    window.history.back();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#deposit_date').datetimepicker({
  yearOffset:0,
  lang:'ch',
  timepicker:false,
  format:'d/m/Y',
  formatDate:'Y/m/d',
});
$('#receipt_date').datetimepicker({
  yearOffset:0,
  lang:'ch',
  timepicker:false,
  format:'d/m/Y',
  formatDate:'Y/m/d',
});
$('#cheque').datetimepicker({
  yearOffset:0,
  lang:'ch',
  timepicker:false,
  format:'d/m/Y',
  formatDate:'Y/m/d',
});
</script> 


<script type="text/javascript">
  /*===============================Gaurav Js================*/

$('#plot_rate').keyup(function(){
  var rate = $(this).val();
  var sqft = $('#area_in_ft').val();
  document.getElementById('plot_price').value = rate*sqft;
});


  /*============Auto hide alert box================*/
  $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
  });


  $('#payment_type').change(function(){
    if($('#payment_type').val() != 'cash'){
        $("#bank_field").css("display", "block");
        $("#cheque_date").css("display", "block");
    }else{
        $("#bank_field").css("display", "none");
        $("#cheque_date").css("display", "none");
    }

  });

function reload(){
  location.reload();
  }

$(document).ready(function(){
  var g_total = parseInt($('#grand_total').val());
  var total_paid = parseInt($('#total_paid').val());
    $('#total_balance_amount').val(g_total-total_paid);
});

$('#paid_amount').keyup(function(){
  var paid_amount = parseInt(($(this).val())?($(this).val()):'0');
  var total_emi_installment = parseInt($('#total_emi_installment').val());
    $('#balance_amount').val(total_emi_installment-paid_amount);
});

</script>
