<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<?php 


 function convertRsToWord($str)
 {
 $number = $str;
   $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
      if($points!='')
      {
  return $result . "Rupees  " . $points . " Paise";
      }
      else
      {
        return $result . "Rupees  Only."; 
      }
 }
 function GetFlatNumber($id)
  { 
//echo  $a="select * from flat_no_mgmt where  id='".$id."'";
  $Query = mysql_query("select * from flat_no_mgmt where  id='".$id."'");
  $Runquery = mysql_fetch_array($Query);
  return $Runquery['flat_number'];
  }
?>
<style type="text/css">
   button.btn.btn-primary a{
    color: white;
   }
  td a{
    margin: 2px;
   }
   .alert.alert-info{
      background-color: #247ab5 !important;
      border-color: #247ab5 !important;
   }
   table tr td{
    border-top: unset !important;
    padding: unset !important;
    font-family: "Verdana";
    /*font-size: 16px;*/
   }
</style>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">             
                <div class="box-header">
<h3 class="box-title">Plot Payment Receipt</h3>
<?php foreach($uresults as $result) { }  ?>

          
                </div><!-- /.box-header -->
                  <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Booked Plot List</a></li>
            <li class="active"><a href="index.php?control=plot_booking&task=receipt_list&id=<?php echo $_REQUEST['cid'] ?>"><i class="fa fa-list" aria-hidden="true"></i> Payment Receipt List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Payment Receipt</li>
          </ol>

          
          
                <div class="box-body">
                 <div>
                 <div class="divoverflow col-md-10 col-md-offset-1">
          <?php 

          $plot = mysql_fetch_array(mysql_query("SELECT * FROM `plot_booking` WHERE `customer_id`='".$result['customer_id']."' AND `id`='".$result['project_plot_id']."'"));

           ?>    
   
<table class="table no_pad ">
  <tr>
    <td colspan="4"><img src="media/hmcity.png" width="150"></td>
  </tr>
  <tr>
    <td rowspan="7" width="400"><strong>HM Green City </strong><br> 8/883/2,<br>Raheem Nagar Road, <br>Khurram Nagar, <br>Opp Fatimi Masjid,<br>Lucknow - 226022 </td>
    
  </tr>
  <tr>
    <td><strong>Receipt No:</strong></td>
    <td><?php echo $result['id']; ?></td>
    
  </tr>
  <tr>
    <td><strong>Customer ID:</strong></td>
    <td><?php echo $result['customer_id']; ?></td>
    
  </tr>
  <tr>
    <td><strong>Project/Block:</strong></td>
    <td><?php echo $plot['project_name'].' ('.$plot['block_name'].')'; ?></td>
    
  </tr>
  <tr>
    <td><strong>Plot/Area:</strong></td>
    <td><?php echo $plot['plot_name'].' ('.$plot['area_dimension'].')'; ?></td>
    
  </tr>
  <tr>
    <td><strong>Booking Date:</strong></td>
    <td><?php $book_date = date_create($plot['booking_date']); echo   date_format($book_date,'jS M y'); ?></td>
  </tr>

  <tr>
    <td><strong>Receipt Date:</strong></td>
    <td><?php  $receipt_date = date_create($result['creation_date']); echo date_format($receipt_date,'jS M y'); ?></td>
  </tr>

  <tr>
    <td colspan="4"><hr></td>
  </tr>
  <tr>
    <td>Received with thanks from <?php echo "<strong>".$plot['title']."</strong> ".$plot['applicant_name']. ($plot['co_applicant_name']?(" / ".$plot['co_applicant_name']):''); ?><br/><strong> by <?php echo ($result['paiment_type']=="cheque") ? (ucfirst($result['paiment_type'].' No. : </strong>'.$result['txn_no'])):('<strong>'.ucfirst($result['paiment_type']).'</strong>') ; ?></strong></td>
    <td><strong>Cheque Date:</strong><br>
     <?php if($result['paiment_type']!='cash'){ ?> <strong>Bank Name:</strong><?php } ?></td>
    <td><?php $receipt_date = date_create($result['creation_date']); echo date_format($receipt_date,'jS M y'); ?><br/><?php echo strtoupper($result['bank']); ?>
    </td>
  </tr>
  <tr>
    <td>&#8377; <mark><?php echo $result['amount'].'/-'; ?></mark><br>
      In Words: <mark><?php echo ucwords(convertRsToWord($result['amount'])); ?></mark>
    </td>
    <td></td>
  </tr>
<tr style="text-align: center;">
  <td colspan="5"><hr><em style="color: red;">*This is Computer Generated Receipt, Doesn't require signature. </em></td>
</tr>
</table>
<div class="pull-right">
  <a target="_blank" href="script/plot_booking/print_receipt.php?cid=<?php echo $result['customer_id']; ?>&id=<?php echo $result['id']; ?>" class="btn btn-success">Print</a>
</div>
                  </div>
                </div><!-- table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            
            <!--================ Second Table ================-->
        
 
          </div><!-- /.row -->
  
        
   

   
