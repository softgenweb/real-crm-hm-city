<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-3{
		padding: unset !important;
	}
	.tabs .col-md-3.active legend{
		background: #102b53 none repeat scroll 0 0;
	}
	  .tabs .col-md-6.active legend{
    background: #102b53 none repeat scroll 0 0;
  }
</style>
<!-- <script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script> -->
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Plot Booking</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Plot Booking </li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Booking </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php 
        	unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']); 

			//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
	   }	if($_REQUEST['id']==''){   ?>
            
     <div class="col-md-10 col-md-offset-1"> <em style="color: red;">** Step-1 and Step-2 is Mandatory, Please Complete form till Step-4.</em>
             </div> <?php } ?>
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
			<div class="box-body">				
          <!--=======================Plot Details======================-->	  
<?php
	$project = mysql_fetch_array(mysql_query("SELECT * FROM `project_plot` WHERE `id`='".$result['project_plot_id']."'"));
	$area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id` ='".$project['area_id']."'"));

 ?>
            
            <!--========================Applicant Detail Start here ============-->
            <fieldset>
            	<!-- <div class="col-md-12 tabs">
            		<div class="col-md-3"><a href="javascript:;"><legend>Plot Details</legend></a></div>

            		<div class="col-md-3 active"><a href="javascript:;"><legend>Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="javascript:;"><legend>Co-Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="javascript:;"><legend>Nominee Details</legend></a></div>
            	</div> -->
            	<div class="col-md-12 tabs">
	                <div class="col-md-6 "><a href="index.php?control=plot_booking&task=step1&id=<?php echo $_REQUEST['id'];?>"><legend>Plot Details</legend></a></div>
	                <div class="col-md-6 active"><a href="index.php?control=plot_booking&task=step2&id=<?php echo $_REQUEST['id'];?>"><legend>Applicant Details</legend></a></div>
	               <!--  <div class="col-md-3"><a href="index.php?control=plot_booking&task=step3&id=<?php echo $_REQUEST['id'];?>"><legend>Co-Applicant Details</legend></a></div>
	                <div class="col-md-3"><a href="index.php?control=plot_booking&task=step4&id=<?php echo $_REQUEST['id'];?>"><legend>Nominee Details</legend></a></div> -->
              </div>
				<div class="col-sm-10 col-md-offset-1 second_header ons" >
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Title</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="title" id="title" class="form-control" autocomplete="off"  required="">
								<option value="">Select</option>
								<option value="Mr" <?php if($result['title']=='Mr') {?> selected="selected" <?php } ?>>Mr</option>
								<option value="Ms" <?php if($result['title']=='Ms') {?> selected="selected" <?php } ?>>Ms</option>
								<option value="Mrs" <?php if($result['title']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option>
								
							</select>
							<span class="msgValid" id="msgtitle"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Applicant Name</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['applicant_name']; ?>" id="applicant_name" name="applicant_name" class="form-control" autocomplete="off"  required="">
						<span class="msgValid" id="msgapplicant_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Realtion</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="relation" id="relation" class="form-control" autocomplete="off"   required="">
								<option value="">Select</option>
								<option value="Son" <?php if($result['relation']=='Son') {?> selected="selected" <?php } ?>>Son</option>
								<option value="Daughter" <?php if($result['relation']=='Daughter') {?> selected="selected" <?php } ?>>Daughter </option>
								<option value="Wife" <?php if($result['relation']=='Wife') {?> selected="selected" <?php } ?>>Wife </option>
								<option value="Husband" <?php if($result['relation']=='Husband') {?> selected="selected" <?php } ?>>Husband </option>
								
							</select>
							<span class="msgValid" id="msgrelation"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Father/Husband Name</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['father_husband_name']; ?>" id="father_husband_name" name="father_husband_name" class="form-control" autocomplete="off"  required="">
						<span class="msgValid" id="msgfather_husband_name"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Date Of Birth</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text" placeholder="DD/MM/YYYY" value="<?php echo $result['dob']; ?>" id="dob" name="dob" class="form-control" autocomplete="off" readonly>
							<span class="msgValid" id="msgdob"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Profession</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<!-- <select name="profession" id="profession" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<option value="Son" <?php if($result['profession']=='Son') {?> selected="selected" <?php } ?>>Son</option>
								<option value="Daughter" <?php if($result['profession']=='Daughter') {?> selected="selected" <?php } ?>>Daughter </option>
								<option value="Wife" <?php if($result['profession']=='Wife') {?> selected="selected" <?php } ?>>Wife </option>
								<option value="Husband" <?php if($result['profession']=='Husband') {?> selected="selected" <?php } ?>>Husband </option>
							</select> -->
							<input type="text"  value="<?php echo $result['profession']; ?>" id="profession" name="profession" class="form-control" autocomplete="off"  required="">
							<span class="msgValid" id="msgprofession"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Residential Status</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="residence_status" id="residence_status" class="form-control" autocomplete="off"   required="">
								<option value="">Select</option>
								<option value="Resident" <?php if($result['residence_status']=='Resident') {?> selected="selected" <?php } ?>>Resident</option>
								<option value="NonResident" <?php if($result['residence_status']=='NonResident') {?> selected="selected" <?php } ?>>Non-Resident </option>
								<option value="ForeignNOIO" <?php if($result['residence_status']=='ForeignNOIO') {?> selected="selected" <?php } ?>>Foreign National Of Indian Origin </option>
								
							</select>
							<span class="msgValid" id="msgresidence_status"></span> 
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Mobile Number</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['mobile']; ?>" maxlength="10" id="mobile" name="mobile" pattern="[0-9]+" class="form-control" autocomplete="off"  required="">
						<span class="msgValid" id="msgmobile"></span>
						</div>
					</div> 
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Email</label>
						<!-- <b style="color:red">*</b>  -->
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['email']; ?>"  id="email" name="email" class="form-control" autocomplete="off"  required="">
							<span class="msgValid" id="msgmobile"></span>
						</div>
                        
                        <div class="col-md-2 col-sm-2 col-xs-12">
						<label>PAN Card</label>
						<!-- <b style="color:red">*</b>  -->
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['pan_card']; ?>"  id="pan_card" name="pan_card" pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" class="form-control" autocomplete="off"  required="">
							<span class="msgValid" id="msgmobile"></span>
						</div>
						</div>  
						<div class="clearfix"></div><br>
						<label><strong>Applicant Residential Address :</strong></label>
						<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="res_country" id="res_country" onchange="state_change(this.value);" class="form-control" autocomplete="off"  required="" >
								<!-- <option value="">Select</option> -->
								<?php $sql = mysql_query("SELECT * FROM `country` WHERE 1"); 
									while($contry = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $contry['id']; ?>" <?php if($result['res_country']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
								<?php }?>
							</select>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="res_state" id="state_id" onchange="city_change(this.value);" class="form-control" autocomplete="off"  required="" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['res_country']."'"); 
									while($state = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $state['id']; ?>" <?php if($result['res_state']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
								<?php }?>
							</select>
							<span class="msgValid" id="msgbranch_id"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<select name="res_city" id="city_id" class="form-control" autocomplete="off"  required="" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['res_state']."' AND `country_id`='".$result['res_country']."'"); 
									while($city = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $city['id']; ?>" <?php if($result['res_city']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
								<?php }?>
							</select>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="res_address" id="res_address" class="form-control" cols="3" style="height: 70px;" required=""><?php echo $result['res_address']; ?></textarea>
							<span class="msgValid" id="msgres_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['res_pincode']; ?>" id="res_pincode" name="res_pincode" class="form-control" autocomplete="off" required="" >
						<span class="msgValid" id="msgres_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
						<label><strong>Applicant Office Address :</strong></label>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Country</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="office_country" id="office_country"  class="form-control" autocomplete="off" >
								<!-- <option value="">Select</option> -->
								<?php $sql = mysql_query("SELECT * FROM `country` WHERE 1"); 
									while($contry = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $contry['id']; ?>" <?php if($result['office_country']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
								<?php }?>
							</select>
						</div>
					
					
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>State</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="office_state" id="office_state" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['office_country']."'"); 
									while($state = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $state['id']; ?>" <?php if($result['office_state']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
								<?php }?>
							</select>
							<span class="msgValid" id="msgbranch_id"></span> 
						</div>
					</div>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>City</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12">
						<select name="office_city" id="office_city" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['office_state']."' AND `country_id`='".$result['office_country']."'"); 
									while($city = mysql_fetch_array($sql)) {?>
									<option value="<?php echo $city['id']; ?>" <?php if($result['office_city']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
								<?php }?>
							</select>
						<span class="msgValid" id="msgvehicle_no"></span>
						</div>
				 
				
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Address</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="office_address" id="office_address" class="form-control" cols="3" style="height: 70px;"><?php echo $result['office_address']; ?></textarea>
							<span class="msgValid" id="msgoffice_address"></span> 
						</div>
					
					<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
						<label>Pin Code</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['office_pincode']; ?>" id="office_pincode" name="office_pincode" class="form-control" autocomplete="off">
						<span class="msgValid" id="msgoffice_pincode"></span>
						</div>
					</div>
					<div class="clearfix"></div><br>
					<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Nationality</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="text"  value="<?php echo $result['nationality']= $result['nationality']?$result['nationality']:'INDIAN'; ?>" id="nationality" name="nationality" class="form-control" autocomplete="off" >
						<span class="msgValid" id="msgnationality"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Id Proof</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="file" class="form-control-file" value="<?php echo $result['id_proof']; ?>" id="id_proof" name="id_proof" class="form-control" autocomplete="off" >
						 <a target="_blank" href="media/plot_booking/<?php echo $result['id_proof']; ?>" id="id_proof_a"><img id="id_proof_img" src="media/plot_booking/<?php echo $result['id_proof']; ?>" alt="" height="50"  /></a>
						<span class="msgValid" id="msgid_proof"></span>
					</div>
				</div>
				<div class="form-group oneline">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Form Scan</label>
						<!-- <b style="color:red">*</b>  -->
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="file" class="form-control-file" value="<?php echo $result['scan_form']; ?>" id="scan_form" name="scan_form" class="form-control" >
						 <a target="_blank" href="media/plot_booking/<?php echo $result['scan_form']; ?>" id="scan_form_a"><img id="scan_form_img" src="media/plot_booking/<?php echo $result['scan_form']; ?>" alt="" height="50"  /></a>
						<span class="msgValid" id="msgscan_form"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Applicant Photo</label>
						<!-- <b style="color:red">*</b> --> 
					</div>
					
					<div class="col-md-4 col-sm-4 col-xs-12" >
						<input type="file" class="form-control-file" value="<?php echo $result['applicant_photo']; ?>" id="applicant_photo" name="applicant_photo" class="form-control"  >
						<a target="_blank" href="media/plot_booking/<?php echo $result['applicant_photo']; ?>" id="applicant_photo_a"><img id="applicant_photo_img" src="media/plot_booking/<?php echo $result['applicant_photo']; ?>" alt="" height="50"  /></a>
						<span class="msgValid" id="msgapplicant_photo"></span>
					</div>
					
					</div>
					<div class="clearfix"></div>
				</div>
		   </fieldset>
            <!--=================Session Value=====================--> 

            <!-- ====================================================== -->
            
<br>
<br>
          
       <?php $project_p = mysql_fetch_array(mysql_query("SELECT * FROM `plot_booking_tmp` WHERE `id`='".$_REQUEST['stepid']."'")); ?>
         
          <div class="row" style="text-align: center;">
       
            	<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
         <a style="position:relative; right:34px;" href="<?php if($result['id']!=''){ echo 'index.php?control=plot_booking&task=step3&id='.$result["id"]; }else{ echo 'javascript:;';} ?>" class="btn btn-primary redu" onclick="reload();" ><?php if($result['id']!=''){ echo 'Next'; }else{ echo 'Cancel';} ?></a>
            <!-- <a style="position:relative; right:34px;" href="javascript:;" class="btn btn-primary redu" onclick="reload();" >Cancel</a>  -->
			</div>
          <input type="hidden" name="control" value="plot_booking"/>
          <input type="hidden" name="step" value="step2"/>
          <input type="hidden" name="stepid" value="<?php echo $_REQUEST['stepid']; ?>"/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="project_plot_id" value="<?php echo $project_p['project_plot_id'];?>"/>
          <input type="hidden" name="task" value="<?php if($_REQUEST['id']!=''){echo 'save';}else{echo 'save';} ?>"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
      
      <!--account details ends here-->
       </div>
   		</form>
 	</div>
</div>



</section>

<script>
function goBack() {
    window.history.back();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#accident_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false,
	//minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/01' // and tommorow is maximum date calendar
});
$('#co_dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#registration_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#dpa').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#driver_dpa').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#expect_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script> 


<script type="text/javascript">
$(document).ready(function(){
 $ = jQuery.noConflict();
   $(".show_hide1").click(function(){
        $(".first_header").toggle();
    });
	 $(".show_hide2").click(function(){
        $(".second_header").toggle();
    });
	 $(".show_hide3").click(function(){
        $(".third_header").toggle();
    });
	$(".show_hide4").click(function(){
        $(".fourth_header").toggle();
    });
	
 
}); 
</script>

<script type="text/javascript">
	/*===============================Gaurav Js================*/
	function booking_block(str) { 
//alert("hii");
var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("block_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_block.php?id="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('project_name').value  = $('#project').find(':selected').text();
}



	function booking_plot(str) {
{ 
//alert("hii");
var pid = $('#project').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("plot_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_plot.php?pid="+pid+"&bid="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('block_name').value  = $('#block_id').find(':selected').text();
}
}


	function booking_area(str) 
{ 
//alert("hii");
var pid = $('#project').val();
var bid = $('#block_id').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
	var plot_area = xmlhttp.responseText; 
	var res = plot_area.split("#");

	document.getElementById("area_id").value = res[0]; 
	document.getElementById("area_in_ft").value = res[1]; 
// document.getElementById("plotarea").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_area.php?project="+pid+"&block="+bid+"&plot="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();

// document.getElementById('area_id').style.display = none;
document.getElementById('plot_name').value  = $('#plot_id').find(':selected').text();
}


 /*=========================================*/

$("#plot_id").change(function(){
	var str = $(this).val();
	var pid = $('#project').val();
	var bid = $('#block_id').val();
    $.ajax({url: "script/plot_booking/project_plot_id.php?project="+pid+"&block="+bid+"&plot="+str, success: function(result){
        $("#project_plot_id").val(result);
    }});
});

  /*=========================================*/

$('#plot_rate').keyup(function(){
	var rate = $(this).val();
	var sqft = $('#area_in_ft').val();
	document.getElementById('plot_price').value = rate*sqft;
});


 /*=========================================*/
$("#office_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
      $("#office_state").html(result);
      
    }});
});
  /*=========================================*/
$("#office_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#office_city").html(result);
    }});
});
 
 /*=========================================*/

$("#co_res_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
        $("#co_res_state").html(result);
    }});
});
 /*=========================================*/ 
$("#co_res_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#co_res_city").html(result);
    }});
});
 /*=========================================*/
$("#co_office_country").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
        $("#co_office_state").html(result);
    }});
});
 /*=========================================*/ 
 
$("#co_office_state").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#co_office_city").html(result);
    }});
});
  /*=========================================*/
/*=================Show Image=================*/

/*===========For Applicant===============*/
function readid_proof(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log($(input).val());
        reader.onload = function (e) {
        	if(e.target.result.split(":")[1].split(';')[0] == 'application/pdf'){
 				$('#id_proof_img').attr('src', "images/pdf-icon.png");
        	    $('#id_proof_a').attr('href', "images/pdf-icon.png");
        	}else if(e.target.result.split(":")[1].split('/')[0] == 'image'){
        	    $('#id_proof_img').attr('src', e.target.result);
        	    $('#id_proof_a').attr('href', e.target.result);}
            console.log(e.target.result.split(":")[1].split('/')[0]);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#id_proof").change(function(){
	readid_proof(this);

});


function readscan_form(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#scan_form_img').attr('src', e.target.result);
            $('#scan_form_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#scan_form").change(function(){
    readscan_form(this);
});

function readapplicant_photo(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#applicant_photo_img').attr('src', e.target.result);
            $('#applicant_photo_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#applicant_photo").change(function(){
    readapplicant_photo(this);
});


/*==========Preventing Leaving page=====*/
/*$(window).bind('beforeunload', function(){
  return 'Are you sure you want to leave?';
});*/
/*=========Default Open Country India===========*/
$(document).ready(function(){
	state_change($('#res_country').val());
	var str = $('#office_country').val();
	 $.ajax({url: "script/state.php?id="+str, success: function(result){
      $("#office_state").html(result);
      
    }});
});
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   