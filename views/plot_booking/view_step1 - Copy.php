<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-3{
		padding: unset !important;
	}
	.tabs .col-md-3.active legend{
		background: #102b53 none repeat scroll 0 0;
	}
</style>
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Plot Booking</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> View Plot Booking Step 1 </li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Booking </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php 
        	unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']); 

			//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
	   }	   ?>
            
  
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off">
			<div class="box-body">				
          <!--=======================Plot Details======================-->	  
<?php
	$project = mysql_fetch_array(mysql_query("SELECT * FROM `project_plot` WHERE `id`='".$result['project_plot_id']."'"));
	$area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id` ='".$project['area_id']."'"));

 ?>
            <fieldset>
            	<div class="col-md-12 tabs">
            		<div class="col-md-3 active"><a href="index.php?control=plot_booking&task=view_step1&id=<?php echo $_REQUEST['id'];?>"><legend>Plot Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=view_step2&id=<?php echo $_REQUEST['id'];?>"><legend>Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=view_step3&id=<?php echo $_REQUEST['id'];?>"><legend>Co-Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=view_step4&id=<?php echo $_REQUEST['id'];?>"><legend>Nominee Details</legend></a></div>
            	</div>
				
				<div class="col-sm-10 col-md-offset-1 first_header">

						<div class="form-group oneline">
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Project Name</label>
								<!-- <b style="color:red">*</b> --> 
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
									<select name="project_id" id="project" onchange="booking_block(this.value)" class="form-control" disabled="disabled">
										<option value="">Select</option>
										<?php $sql = mysql_query("SELECT * FROM `project` WHERE `id` in (SELECT `project_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
											while($proplot = mysql_fetch_array($sql)) {?>
											<option value="<?php echo $proplot['id']; ?>" <?php if($project['project_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['project_name']; ?></option>
										<?php }?>
									</select>
									
									<span class="msgValid" id="msgbranch_id"></span> 
								</div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Area Dimention</label>
                        </div>
                        
                            <div class="col-md-4 col-sm-4 col-xs-12 "  id="plotarea">
                            <input type="text" value="<?php echo $result['area_dimension']; ?>" id="area_id" name="area_dimension" class="form-control area_dimension" readonly>
                           
                            <span class="msgValid" id=""></span>
                            </div>
                            
							</div>  
                            
						<div class="clearfix"></div>
                            
							<div class="form-group oneline">
                            
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <label>Block Name</label>
                                    <!-- <b style="color:red">*</b> --> 
                                </div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                  <select name="block_id" id="block_id" onchange="booking_plot(this.value)" class="form-control" disabled="disabled">
                                                                        <option value="">Select</option>
                                                                        
                                                                        <?php $sql = mysql_query("SELECT * FROM `block` WHERE `id` in (SELECT `block_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
                                                                            while($proplot = mysql_fetch_array($sql)) {?>
                                                                            <option value="<?php echo $proplot['id']; ?>" <?php if($project['block_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['block_name']; ?></option>
                                                                        <?php }?> 
                                                                    </select>
                                                                    <!-- <input type="text"  value="<?php echo $result['vehicle_no']; ?>" id="vehicle_no" name="vehicle_no" class="form-control" autocomplete="off" readonly> -->
                                                                <span class="msgValid" id="msgvehicle_no"></span>
                                                                </div>
                                                                
                                     
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <label>Total Amount</label>
                                    <!-- <b style="color:red">*</b>  -->
                                </div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text"  value="<?php echo $result['total_price']?$result['total_price']:'0'; ?>" id="plot_price" name="plot_price" class="form-control" readonly>
                                <span class="msgValid" id="msgvehicle_no"></span>
                                </div>                               
                                    
							</div>
					<div class="clearfix"></div>
						<div class="form-group oneline">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Plot Name</label>
                            <!-- <b style="color:red">*</b>  -->
                            </div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            <select name="plot_id" id="plot_id" class="form-control" onchange="booking_area(this.value)" disabled="disabled">
                            <option value="">Select</option>
                            <?php $sql = mysql_query("SELECT * FROM `plot` WHERE `id` in (SELECT `plot_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
                            while($proplot = mysql_fetch_array($sql)) {?>
                            <option value="<?php echo $proplot['id']; ?>" <?php if($project['plot_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['plot_name']; ?></option>
                            <?php }?> 
                            </select>
                            <!-- <input type="text"  value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" autocomplete="off" readonly> -->
                            <span class="msgValid" id="msgvehicle_no"></span>
                            </div>
                                
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> GST/IGST Amount</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text"  value="<?php echo $result['gst_amount']?$result['gst_amount']:'0'; ?>" id="gst_amount" name="gst_amount" class="form-control"  readonly>
                            <span class="msgValid" id="msgvehicle_no"></span>
                        </div>
                        
							
                        
						</div>
	<div class="clearfix"></div>
						<div class="form-group oneline">
                        
                      		 <div class="col-md-2 col-sm-2 col-xs-12">
								<label>Rate(per sqft)</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" value="<?php echo $result['rate']; ?>" onkeyup="rate();" id="plot_rate" name="plot_rate" class="form-control" disabled="disabled">
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
                            
							<div class="col-md-2 col-sm-2 col-xs-12">
                            <label> GST + Total Amount</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <input type="text"  value="<?php echo $result['grand_total']?$result['grand_total']:'0'; ?>" id="grand_total" name="grand_total" class="form-control" readonly>                         
                            <span class="msgValid" id="msgemi_option"></span>
                            </div>
                            
                            
                       </div>
                        
                        <div class="form-group oneline">
                        
                       
                        
                       		<div class="col-md-2 col-sm-2 col-xs-12">
								<label>GST/IGST %</label>							
							</div>	
                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="gst" id="gst" class="form-control" autocomplete="off"  disabled="disabled">
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `tax_master` WHERE `status`= 1"); 
                                while($proplot = mysql_fetch_array($sql)) {?>
                                <option value="<?php echo $proplot['id']; ?>" <?php if($result['gst']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['gst']; ?></option>
                                <?php }?>
							</select>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
                        
							<div class="col-md-2 col-sm-2 col-xs-12">                           
								<label>Left Amount</label>							
							</div>							
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text"  value="<?php echo $result['rest_amount']?$result['rest_amount']:'0'; ?>" id="rest_booking_amount" name="rest_booking_amount" class="form-control" readonly="readonly">
                               <div id="words"></div>
							<span class="msgValid" id="msgbooking_amount"></span>
							</div>
                            
                            
                       </div>
                        <div class="form-group oneline">
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">                           
                            <label>Booking Amount</label>							
                            </div>	                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text" onkeyup="word.innerHTML=convertNumberToWords(this.value)"  value="<?php echo $result['booking_amount']?$result['booking_amount']:'0'; ?>" id="booking_amount" name="booking_amount" class="form-control" disabled="disabled">
                              <div id="word"></div>
							<span class="msgValid" id="msgbooking_amount"></span>
							</div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">                           
                            <label>Discount</label>							
                            </div>	                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text"  value="<?php echo $result['discount']?$result['discount']:'0'; ?>" id="discount" name="discount" class="form-control" readonly="readonly">
							<span class="msgValid" id="msgbooking_amount"></span>
							</div>
                         </div>
                           <div class="form-group oneline">
                              <div class="col-md-2 col-sm-2 col-xs-12">                           
                            <label>Booking Date</label>							
                            </div>	                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text" placeholder="DD/MM/YYYY" value="<?php echo $result['booking_date']; ?>" id="booking_date" name="booking_date" class="form-control" readonly>
                            
							<span class="msgValid" id="msgbooking_amount"></span>
							</div> 
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> EMI</label>                        
                            </div>                                                    
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select class="form-control" name="emi_option" id="emi_option" onchange="emi_value();" disabled="disabled">
                                <option value="">Select</option>
                                <option value="yes" <?php if($result['emi_option']=='yes'){echo 'selected';} ?>>Yes</option>
                                <option value="no" <?php if($result['emi_option']=='no'){echo 'selected';} ?>>NO</option>
                                </select>                          
                            <span class="msgValid" id="msgemi_option"></span>
                            </div>
                         
                            
                       </div>
                            
                        <div class="form-group oneline" id="emi_field" style="display:none">
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Number of EMI</label>                            
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">
                             <input type="text"  value="<?php echo $result['emi']?$result['emi']:'0'; ?>" id="emi" name="emi" class="form-control" onkeyup="cal_EMI();" readonly="readonly">
                            <span class="msgValid" id="msgemi"></span>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> Interest</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                           <select name="interest" id="interest" class="form-control" onchange="cal_EMI();" disabled="disabled">
                                        <option value="">Select</option>
                                        <?php $sql = mysql_query("SELECT * FROM `interest_master` WHERE `id` != '' AND `status` = 1 "); 
                                            while($interest = mysql_fetch_array($sql)) {?>
                                            <option value="<?php echo $interest['id']; ?>" <?php if($result['interest']==$interest['id']) {?> selected="selected" <?php } ?>><?php echo $interest['name']; ?></option>
                                        <?php }?> 
                                    </select>
                            <span class="msgValid" id="msginterest"></span>
                            </div>
                            
                       </div>
                            
                        <div class="form-group oneline" id="emi_field" >
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Type of Payment</label>                          
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <select name="payment_type" id="payment_type" class="form-control" disabled="disabled">
                                    <option value="">Select</option>
                                    <option value="cash" <?php if($result['payment_type']=='cash'){ echo "selected";} ?>>Cash</option>
                                    <option value="cheque" <?php if($result['payment_type']=='cheque'){ echo "selected";} ?>>Cheque</option>
                                    <option value="neft" <?php if($result['payment_type']=='neft'){ echo "selected";} ?>>NEFT/RTGS</option>
                                    <option value="dd" <?php if($result['payment_type']=='dd'){ echo "selected";} ?>>DD</option>
                                   
                                </select>
                            <span class="msgValid" id="msgpayment_type"></span>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> Payment Date</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  placeholder="DD/MM/YYYY" value="<?php echo $result['payment_date']; ?>" id="payment_date" name="payment_date" class="form-control" readonly>
                            <span class="msgValid" id="msgpayment_date"></span>
                            </div>
                            
                       </div>
                            
                        <div class="form-group oneline" id="payment_field"  style="display: block;">
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Cheque date</label>                          
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  placeholder="DD/MM/YYYY" value="<?php echo $result['cheque_date']; ?>" id="cheque_date" name="cheque_date" class="form-control"  readonly>
                               
                            <span class="msgValid" id="msgchq_date"></span>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Chq/DD/Txn No.</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  value="<?php echo $result['txn_no']?$result['txn_no']:'0'; ?>" id="txn_no" name="txn_no" class="form-control" disabled="disabled">
                            <span class="msgValid" id="msgtxn_no"></span>
                            </div>
                            
                       </div>
                            
                        <div class="form-group oneline" >
                        <div id="bank" style="display: block;">
                            
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label> Bank Name</label>                         
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  value="<?php echo $result['bank_name']; ?>" id="bank_name" name="bank_name" class="form-control" disabled="disabled">
                               
                            <span class="msgValid" id="msgbank_name"></span>
                            </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Receipt Date</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  placeholder="DD/MM/YYYY" value="<?php echo $result['receipt_date']; ?>" id="receipt_date" name="receipt_date" class="form-control" readonly>
                            <span class="msgValid" id="msgreceipt_date"></span>
                            </div>
                            
                       </div>
                        <div class="form-group oneline" id="emi_field" >
                        <div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label> Application Received By</label>                           
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  value="<?php echo $result['received_by']; ?>" id="received_by" name="received_by" class="form-control"  disabled="disabled">
                               
                            <span class="msgValid" id="msgreceived_by"></span>
                            </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Referred By</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select name="referred_by" id="referred_by" class="form-control" disabled="disabled">
                                <option value="">Select</option>
                                <option value="dealer" <?php if($result['referred_by']=='dealer'){ echo "selected";} ?>>Dealer</option>
                                <option value="employee" <?php if($result['referred_by']=='employee'){ echo "selected";} ?>>Employee</option>


                                </select>
                            <span class="msgValid" id="msgrefer_by"></span>
                            </div>
                            
                       </div>
                        <div class="form-group oneline">
                              <div id="dealer_name" style="display: none;">

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label> Dealer Name</label>                         
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                 <select name="dealer_id" id="dealer_id" class="form-control" disabled="disabled">
                                <option value="">Select</option>
                
                                <?php $sql = mysql_query("SELECT * FROM `dealer` WHERE `status`= 1 "); 
                                    while($dealer = mysql_fetch_array($sql)) {?>
                                    <option value="<?php echo $dealer['id']; ?>" <?php if($result['referred_user_id']==$dealer['id'] && $result['referred_by']=='dealer') {?> selected="selected" <?php } ?>><?php echo $dealer['name'].' ('.$dealer['commission'].'%)'; ?></option>
                                <?php } ?> 
                                    </select>
                            <span class="msgValid" id="msgdealer_id"></span>
                            </div>
                            </div>
                            <div id="emp_name" style="display: none;">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> Employee Name</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         <select name="employee_id" id="employee_id" class="form-control" disabled="disabled">
                        <option value="">Select</option>
            
                        <?php $sql = mysql_query("SELECT * FROM `users` WHERE `utype`='Employee' AND `status`= 1"); 
                            while($employee = mysql_fetch_array($sql)) {?>
                            <option value="<?php echo $employee['id']; ?>" <?php if($result['referred_user_id']==$employee['id'] && $result['referred_by']=='employee') {?> selected="selected" <?php } ?>><?php echo $employee['name']; ?></option>
                        <?php }  ?> 
                        </select>
                            <span class="msgValid" id="msgemployee_id"></span>
                            </div>
                        </div>
                        <div id="com_percent" style="display: none;">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Commission (%)</label>                           
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  onkeyup="get_commission();" value="<?php echo $result['commission']?$result['commission']:'0'; ?>" id="commission" name="commission" class="form-control" readonly="readonly">
                               
                            <span class="msgValid" id="msgcommission"></span>
                            </div>
                        </div>
                      
                            
                       </div>
						<div class="form-group oneline" id="">
                        <div id="commission_field" style="display: none;">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Amount</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  value="<?php echo $result['commission_amount']?$result['commission_amount']:'0'; ?>" id="commission_amount" name="commission_amount" class="form-control" readonly="readonly">
                            <span class="msgValid" id="msgcom_amount"></span>
                            </div>

                        </div>
                           <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Form No</label>
                                <!-- <b style="color:red">*</b>  -->
                            </div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" value="<?php echo $result['form_no']; ?>" id="form_no" name="form_no" class="form-control" readonly="readonly">
                            <span class="msgValid" id="msgform_no"></span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">                     
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            </div>  
                            
                       </div>

                        <div class="form-group oneline">
                        
                                                    
                       </div>
                       <div id="div_cal_EMI">
                       </div>
					</div>


              <!--panel1 end here -->
            </fieldset>
            
         
          <div class="row" style="text-align: center;">
           
           
            	<!--<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">-->
              
        
            <a style="position:relative; right:34px;" href="<?php if($result['id']!=''){ echo 'index.php?control=plot_booking&task=view_step2&id='.$result["id"]; }else{ echo 'javascript:;';} ?>" class="btn btn-primary redu" onclick="reload();" ><?php if($result['id']!=''){ echo 'Next'; }else{ echo 'Cancel';} ?></a> 
			</div>
         <!-- <input type="hidden" name="control" value="plot_booking"/>
          <input type="hidden" name="step" value="step1"/>
          <input type="hidden" name="amount_with_emi" id="amount_with_emi" value=""/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="task" value="<?php if($_REQUEST['id']!=''){echo 'save';}else{echo 'save';} ?>"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />-->
         
      
      <!--account details ends here-->
       </div>
   		</form>
 	</div>
</div>



</section>

<script>

function goBack() {
    window.history.back();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 


<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(1000).slideUp(300, function() {
		$(this).alert('close');
	});

function emi_value()
{
	if($('#emi_option').val() =='yes')

	{
		document.getElementById("emi_field").style.display="block";
	}else
	{
		document.getElementById("emi_field").style.display="none";
	}
}
function cal_EMI(){
	var emino = $('#emi').val();
	if(isNaN(emino)){
		emino = 0;
	}
	var interest = $("#interest option:selected").html();
	if(isNaN(interest) || interest == "Select"){
		interest = 0;
	}

    // var balance_amt = $('#grand_total').val();
	var balance_amt = $('#rest_booking_amount').val();
	if(isNaN(balance_amt)){
		balance_amt = 0;
	}
 
	var installment = (parseFloat(balance_amt) / parseFloat(emino));
	var total = ((parseFloat(installment) * parseFloat(interest))/100);
	var emi_installment = (parseFloat(installment) + parseFloat(total));
	 
	var div_data = "";
	for(var i = 1; i <= parseInt(emino); i++){
	var div_inside = "<div class='form-group oneline' style='border:1px solid black'><div class='col-md-2 col-sm-2 col-xs-12'><label> EMI Amount -" + i +"</label></div><div class='col-md-2 col-sm-2 col-xs-12'><input name='installment["+i+"]' class='form-control' id='installment' value='" + installment.toFixed(2) + "' readonly><span class='msgValid' id='msgemi_option'></span></div><div class='col-md-2 col-sm-2 col-xs-12'><label>Interest Amount</label></div><div class='col-md-2 col-sm-2 col-xs-12'><input name='interest_amount["+i+"]' class='form-control' id='interest_amount' value='" + total + "' readonly><span class='msgValid' id='msgemi_option'></span></div><div class='col-md-2 col-sm-2 col-xs-12'><label>Total Amount</label></div><div class='col-md-2 col-sm-2 col-xs-12'><input name='emi_installment["+i+"]' class='form-control' id='emi_installment' value='" + emi_installment.toFixed(2) + "' readonly><span class='msgValid' id='msgemi_option'></span></div></div>";
	div_data = div_data=="" ? div_inside : div_data+div_inside;
	} 
	$('#div_cal_EMI').empty();
	$('#div_cal_EMI').append(div_data);

} 


$('#payment_type').change(function(){
     if($('#payment_type').val() != 'cash'){
        $("#payment_field").css("display", "block");
        $("#bank").css("display", "block");
    }else{
        $("#payment_field").css("display", "none");
        $("#bank").css("display", "none");
    }
});

$('#referred_by').change(function(){
    if($(this).val() != ''){
        $("#commission_field").css("display", "block");
        $("#com_percent").css("display", "block");

    if($(this).val() == 'dealer'){
        $("#dealer_name").css("display", "block");
        $("#emp_name").css("display", "none");
    }else if($(this).val() == 'employee'){
        $("#emp_name").css("display", "block");
        $("#dealer_name").css("display", "none");
    }
    }else{
        $("#commission_field").css("display", "none");   
        $("#dealer_name").css("display", "none");   
        $("#emp_name").css("display", "none"); 
        $("#com_percent").css("display", "none");  
    }
});


/*================During Update==========*/
$(document).ready(function(){
    emi_value();
    cal_EMI();
  //  alert();
 if($('#payment_type').val() != 'cash'){
        $("#payment_field").css("display", "block");
        $("#bank").css("display", "block");
    }else{
        $("#payment_field").css("display", "none");
        $("#bank").css("display", "none");
    }

    if($('#referred_by').val() != ''){
        $("#commission_field").css("display", "block");
        $("#com_percent").css("display", "block");

    if($('#referred_by').val() == 'dealer'){
        $("#dealer_name").css("display", "block");
        $("#emp_name").css("display", "none");
    }else if($('#referred_by').val() == 'employee'){
        $("#emp_name").css("display", "block");
        $("#dealer_name").css("display", "none");
    }
    }else{
        $("#commission_field").css("display", "none");   
        $("#dealer_name").css("display", "none");   
        $("#emp_name").css("display", "none");   
    }

});





</script>
   