<?php 

 foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-3{
		padding: unset !important;
	}
	.tabs .col-md-3.active legend{
		background: #102b53 none repeat scroll 0 0;
	}
  .tabs .col-md-6.active legend{
    background: #102b53 none repeat scroll 0 0;
  }
</style>
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Plot Booking</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Plot Booking </li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Booking </li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php 
        	unset($_SESSION['alertmessage']);
          unset($_SESSION['errorclass']); 

			//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
	   }	   ?>
            
   <div class="col-md-10 col-md-offset-1"> <em style="color: red;">** Step-1 and Step-2 is Mandatory, Please Complete form till Step-4.</em>
             </div>
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off">

			<div class="box-body">				
          <!--=======================Plot Details======================-->	  
<?php
	$project = mysql_fetch_array(mysql_query("SELECT * FROM `project_plot` WHERE `id`='".$result['project_plot_id']."'"));
	$area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id` ='".$project['area_id']."'"));

 ?>

            <fieldset>

            	<!-- <div class="col-md-12 tabs">
            		<div class="col-md-3 active"><a href="index.php?control=plot_booking&task=step1"><legend>Plot Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step2"><legend>Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step3"><legend>Co-Applicant Details</legend></a></div>
            		<div class="col-md-3"><a href="index.php?control=plot_booking&task=step4"><legend>Nominee Details</legend></a></div>
            	</div> -->
              <div class="col-md-12 tabs">
                <div class="col-md-6 active"><a href="index.php?control=plot_booking&task=step1&id=<?php echo $_REQUEST['id'];?>"><legend>Plot Details</legend></a></div>
                <div class="col-md-6"><a href="index.php?control=plot_booking&task=step2&id=<?php echo $_REQUEST['id'];?>"><legend>Applicant Details</legend></a></div>
               <!--  <div class="col-md-3"><a href="index.php?control=plot_booking&task=step3&id=<?php echo $_REQUEST['id'];?>"><legend>Co-Applicant Details</legend></a></div>
                <div class="col-md-3"><a href="index.php?control=plot_booking&task=step4&id=<?php echo $_REQUEST['id'];?>"><legend>Nominee Details</legend></a></div> -->
              </div>
				
				<div class="col-sm-10 col-md-offset-1 first_header">

						<div class="form-group oneline">
							<div class="col-md-2 col-sm-2 col-xs-12">
								<label>Project Name</label>
								<!-- <b style="color:red">*</b> --> 
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
									<select name="project_id" id="project" onchange="booking_block(this.value)" class="form-control" autocomplete="off"  required="">
										<option value="">Select</option>
										<?php $sql = mysql_query("SELECT * FROM `project` WHERE `id` in (SELECT `project_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
											while($proplot = mysql_fetch_array($sql)) {?>
											<option value="<?php echo $proplot['id']; ?>" <?php if($project['project_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['project_name']; ?></option>
										<?php }?>
									</select>
									
									<span class="msgValid" id="msgbranch_id"></span> 
								</div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Area Dimention</label>
                        </div>
                        
                            <div class="col-md-4 col-sm-4 col-xs-12 "  id="plotarea">
                            <input type="text" placeholder="Area Dimention" value="<?php echo $result['area_dimension']; ?>" id="area_id" name="area_dimension" class="form-control area_dimension" autocomplete="off" readonly>
                            <!-- ============================================================== -->
                            <input type="hidden"  value="<?php echo $area['square_feet']; ?>" id="area_in_ft" name="area_in_ft" class="form-control" autocomplete="off" readonly>
                            <input type="hidden"  value="<?php echo $result['project_plot_id']; ?>" id="project_plot_id" name="project_plot_id" class="form-control" autocomplete="off" readonly>
                            <input type="hidden"  value="<?php echo $result['project_name']; ?>" id="project_name" name="project_name" class="form-control" autocomplete="off" readonly>
                            <input type="hidden"  value="<?php echo $result['block_name']; ?>" id="block_name" name="block_name" class="form-control" autocomplete="off" readonly>
                            <input type="hidden"  value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" autocomplete="off" readonly>
                            <span class="msgValid" id=""></span>
                            </div>
                            
							</div>  
                            
						<div class="clearfix"></div>
                            
							<div class="form-group oneline">
                            
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <label>Block Name</label>
                                    <!-- <b style="color:red">*</b> --> 
                                </div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select name="block_id" id="block_id" onchange="booking_plot(this.value)" class="form-control" autocomplete="off"  required="">
                                            <option value="">Select</option>
                                                                        
                                            <?php 
                                            //$sql = mysql_query("SELECT * FROM `block` WHERE `id` in (SELECT `block_id` FROM `project_plot` WHERE 1) AND `status` = 1 "); 
                                            $sql = mysql_query("SELECT * FROM `block` WHERE `id` in (SELECT `block_id` FROM `project_plot` WHERE `project_id`='".$project['project_id']."') AND `status` = 1 "); 
                                                 while($proplot = mysql_fetch_array($sql)) {?>
                                            <option value="<?php echo $proplot['id']; ?>" <?php if($project['block_id']==$proplot['id']) {?> selected="selected" <?php } ?>><?php echo $proplot['block_name']; ?></option>
                                         <?php }?> 
                                    </select>
                          <!-- <input type="text"  value="<?php echo $result['vehicle_no']; ?>" id="vehicle_no" name="vehicle_no" class="form-control" autocomplete="off" readonly> -->
                               <span class="msgValid" id="msgvehicle_no"></span>
                                    </div>
                                                                
                                     
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <label>Total Amount</label>
                                    <!-- <b style="color:red">*</b>  -->
                                </div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text"  value="<?php echo $result['total_price']?$result['total_price']:'0'; ?>" id="plot_price" name="plot_price" class="form-control" autocomplete="off" readonly>
                                <span class="msgValid" id="msgvehicle_no"></span>
                                </div>                               
                                    
							</div>
					<div class="clearfix"></div>
						<div class="form-group oneline">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Plot Name</label>
                            <!-- <b style="color:red">*</b>  -->
                            </div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            <select name="plot_id" id="plot_id" class="form-control" onchange="booking_area(this.value)" autocomplete="off"  required="">
                            <option value="">Select</option>
                            <?php

                    
                            $sql = mysql_query("SELECT * FROM `project_plot` WHERE `project_id`='".$project['project_id']."' AND `block_id`='".$project['block_id']."' AND `status`=1 OR `customer_id`='".$_REQUEST['cid']."' ORDER BY `plot_id` ASC");
                            // $sql = mysql_query("SELECT * FROM `project_plot` WHERE `project_id`='".$project['project_id']."' AND `block_id`='".$project['block_id']."'  ORDER BY `plot_id` ASC");
                          while($proplot = mysql_fetch_array($sql)) {

                         // if($proplot['status']=='2'){  $val =' ->(Sold)';} 

                              ?>
                          <option value="<?php echo $proplot['plot_id']; ?>" <?php echo $project['plot_id']==$proplot['plot_id']?'selected':''; ?>><?php $pid = explode(',', $proplot['plot_id']);
                          for($j=0;$j<count($pid);$j++){ echo ($j>=1?', ':'').$this->plot_name($pid[$j]).(($proplot['status']=='2')?' ->(Sold)':'');} ?></option>


                            <?php }?> 
                            </select>
                            <!-- <input type="text"  value="<?php echo $result['plot_name']; ?>" id="plot_name" name="plot_name" class="form-control" autocomplete="off" readonly> -->
                            <span class="msgValid" id="msgvehicle_no"></span>
                            </div>
                                
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> GST/IGST Amount</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text"  value="<?php echo $result['gst_amount']?$result['gst_amount']:'0'; ?>" id="gst_amount" name="gst_amount" class="form-control" autocomplete="off" readonly>
                            <span class="msgValid" id="msgvehicle_no"></span>
                        </div>
                        
							
                        
						</div>
	<div class="clearfix"></div>
						<div class="form-group oneline">
                        
                      		 <div class="col-md-2 col-sm-2 col-xs-12">
								<label>Rate(per sqft)</label>
								<!-- <b style="color:red">*</b>  -->
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input type="text" placeholder="Plot Rate" value="<?php echo $result['rate']; ?>" onkeypress="return isNumber(event)" onkeyup="rate();" id="plot_rate" name="plot_rate" class="form-control" autocomplete="off" required>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
                            
							<div class="col-md-2 col-sm-2 col-xs-12">
                            <label> GST + Total Amount</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <input type="text"  value="<?php echo $result['grand_total']?$result['grand_total']:'0'; ?>" id="grand_total" name="grand_total" class="form-control" autocomplete="off" readonly>                         
                            <span class="msgValid" id="msgemi_option"></span>
                            </div>
                            
                            
                       </div>
                        
                        <div class="form-group oneline">
                        
                       
                        
                       		<div class="col-md-2 col-sm-2 col-xs-12">
								<label>GST/IGST %</label>							
							</div>	
                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="gst" id="gst" class="form-control" autocomplete="off"  required="">
								<option value="">Select</option>
								<?php $sql = mysql_query("SELECT * FROM `tax_master` WHERE `status`= 1"); 
                                while($proplot = mysql_fetch_array($sql)) {?>
                                <option value="<?php echo $proplot['id']; ?>" <?php if($result['gst']==$proplot['id']) {?> selected <?php } ?>><?php echo $proplot['gst']; ?></option>
                                <?php }?>
							</select>
							<span class="msgValid" id="msgvehicle_no"></span>
							</div>
                        
							<div class="col-md-2 col-sm-2 col-xs-12">                           
								<label>Left Amount</label>							
							</div>							
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text"  value="<?php echo $result['rest_amount']?$result['rest_amount']:'0'; ?>" id="rest_booking_amount" name="rest_booking_amount" class="form-control" readonly="readonly">
                               <div id="words"></div>
							<span class="msgValid" id="msgbooking_amount"></span>
							</div>
                            
                            
                       </div>
                        <div class="form-group oneline">
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">                           
                            <label>Booking Amount</label>							
                            </div>	                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text" onkeyup="word.innerHTML=convertNumberToWords(this.value)" onkeypress="return isNumber(event)" value="<?php echo $result['booking_amount']; ?>" id="booking_amount" name="booking_amount" class="form-control"  required="">
                              <div id="word"></div>
							<span class="msgValid" id="msgbooking_amount"></span>
							</div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">                           
                            <label>Discount</label>							
                            </div>	                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text"  value="<?php echo $result['discount']?$result['discount']:'0'; ?>" onkeypress="return isNumber(event)" id="discount" name="discount" class="form-control" >
							<span class="msgValid" id="msgbooking_amount"></span>
							</div>
                         </div>
                           <div class="form-group oneline">
                              <div class="col-md-2 col-sm-2 col-xs-12">                           
                            <label>Booking Date</label>							
                            </div>	                            						
							<div class="col-md-4 col-sm-4 col-xs-12">
							 <input type="text" placeholder="DD/MM/YYYY" value="<?php echo $result['booking_date']?$result['booking_date']:''; ?>" id="booking_date" name="booking_date" class="form-control" readonly>
                            
							<span class="msgValid" id="msgbooking_amount"></span>
							</div> 
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> EMI</label>                        
                            </div>                                                    
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select class="form-control" name="emi_option" id="emi_option" onchange="emi_value();" required="">
                                <option value="">Select</option>
                                <option value="yes" <?php if($result['emi_option']=='yes'){echo 'selected';} ?>>Yes</option>
                                <option value="no" <?php if($result['emi_option']=='no'){echo 'selected';} ?>>NO</option>
                                </select>                          
                            <span class="msgValid" id="msgemi_option"></span>
                            </div>
                         
                            
                       </div>
                            
                        <div class="form-group oneline" id="emi_field" style="display:none">
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Number of EMI</label>                            
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">
                             <input type="text" onkeypress="return isNumber(event)" value="<?php echo $result['emi']?$result['emi']:'0'; ?>" id="emi" name="emi" class="form-control" onkeyup="cal_EMI();" >
                              <!-- <select class="form-control" name="emi" id="emi" onchange="cal_EMI();" required="">
                                <option value="">Select</option>
                                <?php $sql = mysql_query("SELECT * FROM `emi_master` WHERE `status`=1"); 
                                while($emi = mysql_fetch_array($sql)){
                                ?>
                                <option value="<?php echo $emi['name']; ?>" <?php if($result['emi']==$emi['name']){echo 'selected';} ?>><?php echo $emi['name']; ?></option>
                               <?php } ?>
                                </select> -->
                            <span class="msgValid" id="msgemi"></span>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> Interest (%)</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                           <select name="interest" id="interest" class="form-control" onchange="cal_EMI();"  required="">
                                <option value="">Select</option>
                                <?php $sql = mysql_query("SELECT * FROM `interest_master` WHERE `id` != '' AND `status` = 1 "); 
                                    while($interest = mysql_fetch_array($sql)) {?>
                                    <option value="<?php echo $interest['name']; ?>" <?php if($result['interest']==$interest['name']) {?>  <?php } ?>><?php echo $interest['name'].'%'; ?></option>
                                        <?php }?> 
                                    </select>
                            <span class="msgValid" id="msginterest"></span>
                            </div>
                            
                       </div>
                            
                        <div class="form-group oneline" id="emi_field" >
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Type of Payment</label>                          
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <select name="payment_type" id="payment_type" class="form-control" required="">
                                    <option value="">Select</option>
                                    <option value="cash" <?php if($result['payment_type']=='cash'){ echo "selected";} ?>>Cash</option>
                                    <option value="cheque" <?php if($result['payment_type']=='cheque'){ echo "selected";} ?>>Cheque</option>
                                    <option value="neft" <?php if($result['payment_type']=='neft'){ echo "selected";} ?>>NEFT/RTGS</option>
                                    <option value="dd" <?php if($result['payment_type']=='dd'){ echo "selected";} ?>>DD</option>
                                   
                                </select>
                            <span class="msgValid" id="msgpayment_type"></span>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> Payment Date</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  placeholder="DD/MM/YYYY" value="<?php echo $result['payment_date']?$result['payment_date']:''; ?>" id="payment_date" name="payment_date" class="form-control" readonly>
                            <span class="msgValid" id="msgpayment_date"></span>
                            </div>
                            
                       </div>
                            
                        <div class="form-group oneline" id="payment_field"  style="display: block;">
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Cheque date</label>                          
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  placeholder="DD/MM/YYYY" value="<?php echo $result['cheque_date']?$result['cheque_date']:''; ?>" id="cheque_date" name="cheque_date" class="form-control"  readonly>
                               
                            <span class="msgValid" id="msgchq_date"></span>
                            </div>
                            
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Chq/DD/Txn No.</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  value="<?php echo $result['txn_no']?$result['txn_no']:'0'; ?>" id="txn_no" name="txn_no" class="form-control">
                            <span class="msgValid" id="msgtxn_no"></span>
                            </div>
                            
                       </div>
                            
                        <div class="form-group oneline" >
                        <div id="bank" style="display: block;">
                            
                        
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label> Bank Name</label>                         
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  value="<?php echo $result['bank_name']; ?>" id="bank_name" name="bank_name" class="form-control" >
                               
                            <span class="msgValid" id="msgbank_name"></span>
                            </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Receipt Date</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  placeholder="DD/MM/YYYY" value="<?php echo $result['receipt_date']?$result['receipt_date']:''; ?>" id="receipt_date" name="receipt_date" class="form-control" readonly>
                            <span class="msgValid" id="msgreceipt_date"></span>
                            </div>
                            
                       </div>
                        <div class="form-group oneline" id="emi_field" >
                        <div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label> Application Received By</label>                           
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  value="<?php echo $result['received_by']; ?>" id="received_by" name="received_by" class="form-control"  >
                               
                            <span class="msgValid" id="msgreceived_by"></span>
                            </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Referred By</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select name="referred_by" id="referred_by" class="form-control">
                                <option value="">Select</option>
                                <option value="dealer" <?php if($result['referred_by']=='dealer'){ echo "selected";} ?>>Dealer</option>
                                <option value="employee" <?php if($result['referred_by']=='employee'){ echo "selected";} ?>>Employee</option>


                                </select>
                            <span class="msgValid" id="msgrefer_by"></span>
                            </div>
                            
                       </div>
                        <div class="form-group oneline">
                              <div id="dealer_name" style="display: none;">

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label> Dealer Name</label>                         
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                 <select name="dealer_id" id="dealer_id" class="form-control">
                                <option value="">Select</option>
                
                                <?php $sql = mysql_query("SELECT * FROM `dealer` WHERE `status`= 1 "); 
                                    while($dealer = mysql_fetch_array($sql)) {?>
                                    <option value="<?php echo $dealer['id']; ?>" <?php if($result['referred_user_id']==$dealer['id'] && $result['referred_by']=='dealer') {?> selected="selected" <?php } ?>><?php echo $dealer['name'].' ('.$dealer['commission'].'%)'; ?></option>
                                <?php } ?> 
                                    </select>
                            <span class="msgValid" id="msgdealer_id"></span>
                            </div>
                            </div>
                            <div id="emp_name" style="display: none;">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label> Employee Name</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         <select name="employee_id" id="employee_id" class="form-control" >
                        <option value="">Select</option>
            
                        <?php $sql = mysql_query("SELECT * FROM `users` WHERE `utype`='Employee' AND `status`= 1"); 
                            while($employee = mysql_fetch_array($sql)) {?>
                            <option value="<?php echo $employee['id']; ?>" <?php if($result['referred_user_id']==$employee['id'] && $result['referred_by']=='employee') {?> selected="selected" <?php } ?>><?php echo $employee['name']; ?></option>
                        <?php }  ?> 
                        </select>
                            <span class="msgValid" id="msgemployee_id"></span>
                            </div>
                        </div>
                        <div id="com_percent" style="display: none;">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Commission (%)</label>                           
                            </div>                          
                            <div class="col-md-4 col-sm-4 col-xs-12">

                             <input type="text"  onkeyup="get_commission();" value="<?php echo $result['commission']?$result['commission']:'0'; ?>" id="commission" name="commission" class="form-control" >
                               
                            <span class="msgValid" id="msgcommission"></span>
                            </div>
                        </div>
                      
                            
                       </div>
						<div class="form-group oneline" id="">
                        <div id="commission_field" style="display: none;">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <label>Amount</label>                        
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                         
                             <input type="text"  value="<?php echo $result['commission_amount']?$result['commission_amount']:'0'; ?>" id="commission_amount" name="commission_amount" class="form-control" >
                            <span class="msgValid" id="msgcom_amount"></span>
                            </div>

                        </div>
                           <div class="col-md-2 col-sm-2 col-xs-12">
                                <label>Form No</label>
                                <!-- <b style="color:red">*</b>  -->
                            </div>
                            
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" value="<?php echo $result['form_no']; ?>" id="form_no" name="form_no" class="form-control" required>
                            <span class="msgValid" id="msgform_no"></span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">                     
                            </div>                        
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            </div>  
                            
                       </div>

                        <div class="form-group oneline">
                        
                                                    
                       </div>
                       <div id="div_cal_EMI">
                       </div>
					</div>


              <!--panel1 end here -->
            </fieldset>
            
         
          <div class="row" style="text-align: center;">
           
           
            	<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
              
        
            <a style="position:relative; right:34px;" href="<?php if($result['id']!=''){ echo 'index.php?control=plot_booking&task=step2&id='.$result["id"]; }else{ echo 'javascript:;';} ?>" class="btn btn-primary redu" onclick="reload();" ><?php if($result['id']!=''){ echo 'Next'; }else{ echo 'Cancel';} ?></a> 
			</div>
          <input type="hidden" name="control" value="plot_booking"/>
          <input type="hidden" name="step" value="step1"/>
          <input type="hidden" name="amount_with_emi" id="amount_with_emi" value=""/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="customer_id" value="<?php echo $_REQUEST['cid']; ?>"/>
          <input type="hidden" name="task" value="<?php if($_REQUEST['id']!=''){echo 'save';}else{echo 'save';} ?>"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
         
      
      <!--account details ends here-->
       </div>
   		</form>
 	</div>
</div>



</section>

<script>

function goBack() {
    window.history.back();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#booking_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    // formatDate: new Date(),
    maxDate:'+1970/01/01',
    scrollMonth : false


});


$('#payment_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    maxDate:'+1970/01/01',
    scrollMonth : false
});
$('#receipt_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    maxDate:'+1970/01/01',
    scrollMonth : false
});

$('#cheque_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d-m-Y',
	formatDate:'Y/m/d',
  scrollMonth : false
    // minDate:'+1970/01/01'
});


</script> 




<script type="text/javascript">
	/*===============================Gaurav Js================*/
function booking_block(str) { 
//alert("hii");

clear_data();


var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("block_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_block.php?id="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('project_name').value  = $('#project').find(':selected').text();
}

function booking_plot(str) {
{ 

clear_data();
//alert("hii");
var pid = $('#project').val();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
document.getElementById("plot_id").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_plot.php?pid="+pid+"&bid="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();


document.getElementById('block_name').value  = $('#block_id').find(':selected').text();
}
}

function booking_area(str) 
{ 
// alert(pid);
var pid = $('#project').val();
var bid = $('#block_id').val();

clear_data();

var xmlhttp; 
if (window.XMLHttpRequest)
{ // code for IE7+, Firefox, Chrome, Opera, Safari 
xmlhttp=new XMLHttpRequest();
}
else
{ // code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{ 
if (xmlhttp.readyState==4)
{
	var plot_area = xmlhttp.responseText; 
	var res = plot_area.split("#");

	document.getElementById("area_id").value = res[0]; 
	document.getElementById("area_in_ft").value = res[1]; 
// document.getElementById("plotarea").innerHTML = xmlhttp.responseText; 
//alert(xmlhttp.responseText);
}
}
xmlhttp.open("GET", "script/plot_booking/booking_area.php?project="+pid+"&block="+bid+"&plot="+str, true);
//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
xmlhttp.send();

// document.getElementById('area_id').style.display = none;
document.getElementById('plot_name').value  = $('#plot_id').find(':selected').text().replace(' ->(Sold)','');
}

 /*=========================================*/

$("#plot_id").change(function(){
    var str = $(this).val();
    var pid = $('#project').val();
    var bid = $('#block_id').val();
    $.ajax({url: "script/plot_booking/project_plot_id.php?project="+pid+"&block="+bid+"&plot="+str, success: function(result){
        $("#project_plot_id").val(result);
    }});
    $.ajax({url: "script/plot_booking/plot_rate.php?project="+pid+"&block="+bid+"&plot="+str, success: function(result){
        $("#plot_rate").val(result);
        
        rate();
    }});
});
$("#dealer_id").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/plot_booking/dealer_commission.php?id="+str, success: function(result){
        $("#commission").val(result);
        get_commission();
    }});

});
 /*=========================================*/

$('#interest').change(function(){
    var booking_amount = $('#booking_amount').val();
    var rest_booking_amount = $('#rest_booking_amount').val();
    var interest = $(this).val();
    var final_amt = parseInt(booking_amount)+parseInt(rest_booking_amount)+parseInt((interest*rest_booking_amount)/100); 
    $('#amount_with_emi').val(final_amt)
})

  /*=========================================*/
function rate(){
$('#plot_rate').ready(function(){
    var rate = $('#plot_rate').val();
	var sqft = $('#area_in_ft').val();
    $('#plot_price').val(rate*sqft);
});
}
$('#gst').change(function(){
    var tax = $(this).find(':selected').text();
    var total = $('#plot_price').val();
    $('#gst_amount').val(parseInt((tax*total)/100));
    $('#grand_total').val( parseInt(total)+parseInt((tax*total)/100) );
    $('#rest_booking_amount').val( parseInt(total)+parseInt((tax*total)/100));
});

/*========Commission Amount=============*/
function get_commission(){
// $('#commission').keyup(function(){
  var com = $('#commission').val();
	var g_total = $('#grand_total').val();
	document.getElementById('commission_amount').value = parseInt((com*g_total)/100); 

// });
}

$('#booking_amount').keyup(function(){	
		
	if($("#booking_amount").val() == '')
	{
		var amount = '0';
	}
	else
	{
		var amount = $(this).val();
	}
	
	var g_total = $('#grand_total').val();
	document.getElementById('rest_booking_amount').value = parseInt(g_total)-parseInt(amount); 
	
	/*if($("#booking_amount").val() > $("#grand_total").val())
	{
		  alert("Booking Amount Not Exceed to the Total Amount.")
		document.getElementById('booking_amount').value = '0';
		document.getElementById('rest_booking_amount').value = '0';
	}*/
	
});

$('#discount').keyup(function(){	
		
    var discount = ($(this).val())?($(this).val()):'0';
	var total = $('#grand_total').val();
	var booking = ($('#booking_amount').val())?($('#booking_amount').val()):'0';
    if(parseInt(discount) > parseInt($('#rest_booking_amount').val())){
       alert("Discount Can't be greater than Total Amount");
       $(this).val('0');
       $('#rest_booking_amount').val(parseInt(total)-parseInt(booking));
    }else{
	   $('#rest_booking_amount').val(parseInt(total)-parseInt(booking)-parseInt(discount));
        words.innerHTML= convertNumberToWords( $('#rest_booking_amount').val());
      }

});

</script>
<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(1000).slideUp(500, function() {
		$(this).alert('close');
	});

function emi_value()
{
	if($('#emi_option').val() =='yes')
	{
		document.getElementById("emi_field").style.display="block";

    $("interest").prop('required',true);
	}else
	{
		document.getElementById("emi_field").style.display="none";
	}
}
function cal_EMI(){
	var emino = $('#emi').val();
	if(isNaN(emino)){
		emino = 0;
	}
	// var interest = $("#interest option:selected").html();
  var interest = $('#interest').val();
	if(isNaN(interest) || interest == ""){
		interest = 0;
	}

    // var balance_amt = $('#grand_total').val();
	var balance_amt = $('#rest_booking_amount').val();
	if(isNaN(balance_amt)){
		balance_amt = 0;
	}
 
	var installment = (parseFloat(balance_amt) / parseFloat(emino));
	var total = ((parseFloat(installment) * parseFloat(interest))/100);
	var emi_installment = (parseFloat(installment) + parseFloat(total));
	 if(emino>0){
	var div_data = "<div class='form-group oneline' style='border:1px solid black;text-align: center;'><div class='col-md-1'><label>EMI</label></div><div class='col-md-2'><label>EMI Date</label></div><div class='col-md-3'><label>EMI Amount</label></div><div class='col-md-3'><label>Interest Amount</label></div><div class='col-md-3'><label>Total Amount</label></div></div>";}

	for(var i = 1; i <= parseInt(emino); i++){

	var div_inside = "<div class='form-group oneline' style='border:1px solid black;margin-bottom:0px'><div class='col-md-1'><label>" + i +"</label></div><div class='col-md-2'><input name='installment_date["+i+"]' class='form-control installment_date' id='installment_date"+i+"' value='" + addDays(30.5*i) + "' readonly></div><div class='col-md-3'><input name='installment["+i+"]' class='form-control' id='installment' value='" + installment.toFixed(2) + "' readonly></div><div class='col-md-3'><input name='interest_amount["+i+"]' class='form-control' id='interest_amount' value='" + total + "' readonly></div><div class='col-md-3'><input name='emi_installment["+i+"]' class='form-control' id='emi_installment' value='" + emi_installment.toFixed(2) + "' readonly></div></div>";
	div_data = div_data=="" ? div_inside : div_data+div_inside;


	} 
	$('#div_cal_EMI').empty();
	$('#div_cal_EMI').append(div_data);

    $('.installment_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    scrollMonth : false
});

} 

function addDays(n){
    var t = new Date();
    t.setDate(t.getDate() + n); 
    var month = "0"+(t.getMonth()+1);
    var date = "0"+t.getDate();
    month = month.slice(-2);
    date = date.slice(-2);
     var date = date +"-"+month +"-"+t.getFullYear();
    return (date);
}


$('#payment_type').change(function(){
     if($('#payment_type').val() != 'cash'){
        $("#payment_field").css("display", "block");
        $("#bank").css("display", "block");
    }else{
        $("#payment_field").css("display", "none");
        $("#bank").css("display", "none");
    }
});

$('#referred_by').change(function(){
    if($(this).val() != ''){
        $("#commission_field").css("display", "block");
        $("#com_percent").css("display", "block");

    if($(this).val() == 'dealer'){
        $("#dealer_name").css("display", "block");
        $("#emp_name").css("display", "none");
    }else if($(this).val() == 'employee'){
        $("#emp_name").css("display", "block");
        $("#dealer_name").css("display", "none");
    }
    }else{
        $("#commission_field").css("display", "none");   
        $("#dealer_name").css("display", "none");   
        $("#emp_name").css("display", "none"); 
        $("#com_percent").css("display", "none");  
    }
});


/*================During Update==========*/
$(document).ready(function(){
    emi_value();
    cal_EMI();
  //  alert();
 if($('#payment_type').val() != 'cash'){
        $("#payment_field").css("display", "block");
        $("#bank").css("display", "block");
    }else{
        $("#payment_field").css("display", "none");
        $("#bank").css("display", "none");
    }

    if($('#referred_by').val() != ''){
        $("#commission_field").css("display", "block");
        $("#com_percent").css("display", "block");

    if($('#referred_by').val() == 'dealer'){
        $("#dealer_name").css("display", "block");
        $("#emp_name").css("display", "none");
    }else if($('#referred_by').val() == 'employee'){
        $("#emp_name").css("display", "block");
        $("#dealer_name").css("display", "none");
    }
    }else{
        $("#commission_field").css("display", "none");   
        $("#dealer_name").css("display", "none");   
        $("#emp_name").css("display", "none");   
    }

});

function convertNumberToWords(amount) {
    var words = new Array();
    words['-'] = '';
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}


  



function clear_data(){
  $('input[type="text"]').val('');
  $('#gst').val('');
  $('#emi_option').val('');
  $('#interest').val('');
  $('#payment_type').val('');
  $('#referred_by').val('');
  $('#dealer_id').val('');
  $('#employee_id').val('');
}

$('#booking_amount').keyup(function(){
    var rest_amount = $('#rest_booking_amount').val();
    if(isNaN(rest_amount)){
  alert("Please Enter Valid Amount");
    $(this).val('');
    var amount = $('#grand_total').val();
    $('#rest_booking_amount').val(amount);
    word.innerHTML = '';
  return 0;
 }
   if(parseInt($(this).val()) > parseInt($('#grand_total').val())){
    alert("Booking Amount Can't be greater than Total Amount");
    $(this).val('');
    var amount = $('#grand_total').val();
    $('#rest_booking_amount').val(amount);
    word.innerHTML = '';
    return 0
   }else{
       words.innerHTML = convertNumberToWords(rest_amount);
   }
});

/*==========Preventing Leaving page=====*/
/*$(window).bind('beforeunload', function(){
  return 'Are you sure you want to leave?';
});
*/
<?php if($_REQUEST['id']){ ?>
/*$('#project').on('mousedown', function(e) {
   e.preventDefault();
   this.blur();
   window.focus();
});
$('#block_id').on('mousedown', function(e) {
   e.preventDefault();
   this.blur();
   window.focus();
});
$('#plot_id').on('mousedown', function(e) {
   e.preventDefault();
   this.blur();
   window.focus();
});
*/
<?php } ?>

      function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>



   <?php /*$time = strtotime(date("Y/m/d"));
echo $final = date("Y-m-d", strtotime("+1 month", $time)); ?>*/