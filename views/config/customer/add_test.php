
<div class="detail_right right">
    <div class="detail_container ">
      <div class="head_cont">
        <h2 class="main_head">
          <table width="99%">
            <tr>
              <td width="85%" class="main_txt"><?php if($results[0]['id']=='') { echo "Add New No. Of Test"; } else {echo "Edit No. Of Test";} ?></td>
              <td><!--<a href="#" class="button"><img src="images/add_new.png" alt="add new" /></a>--></td>
            </tr>
          </table>
        </h2>
      </div>
      <div class="grid_container">
        <div class="main_collaps">
          <form name="formLanguage" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return languageValidate();" >
            <div style="width:100%; height:auto;"> 
              <!--TOP TABING START HERE-->
              <div class='tab-container'>
                
                  <div id="lang0_1" class="pans">
                    <table width="98%" cellpadding="0" cellspacing="2" class="tab_regis">
				
            <tr>
               <td align="right" width="30%" valign="top"><strong>First Name :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="fname" id="fname" value="<?php echo $results[0]['fname']; ?>"/>
                <div id="msgfname"></div> 
                
                </td>                    
                
            
           </tr>
           <tr>
               <td align="right" width="30%" valign="top"><strong>Last Name :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="lname" id="lname" value="<?php echo $results[0]['lname']; ?>"/>
                <div id="msglname"></div>
              </td>  
                   
           </tr>
             <tr>
               <td align="right" width="30%" valign="top"><strong>Username :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="username" readonly="readonly" id="username" value="<?php echo $results[0]['username']; ?>"/>
                <div id="msgusername"></div>
              </td>         
           </tr>
            <tr>
               <td align="right" width="30%" valign="top"><strong>Email-Id :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="email" id="email" readonly="readonly" value="<?php echo $results[0]['email']; ?>"/>
                 <div id="msgemail"></div>
              </td>         
           </tr>
           
            
           <tr>
               <td align="right" valign="top"><strong>Number Of Test :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="no_of_test" id="no_of_test" maxlength="3" value="<?php echo $results[0]['no_of_test']; ?>"/>
                 <div id="msgmobile"></div>
                </td> 
           </tr>
            <tr>
               <td align="right" valign="top"><strong>Join Date :</strong></td>
                <td colspan="2">
                <?php $date= explode(" ",$results[0]['date_time']); ?>
                <input type="text" class="regis_area" name="date_time" id="date_time" readonly="readonly" value="<?php echo $date[0]; ?>"/>
                </td> 
           </tr>
            
           <tr>
               <td align="right" valign="top"><strong>Expire Date :</strong></td>
                <td colspan="2">
                <input type="text" class="regis_area" name="test_exp_date_time" id="test_exp_date_time" placeholder="YYYY-MM-DD" value="<?php echo $results[0]['test_exp_date_time']; ?>"/>
                </td> 
           </tr>
      
           
           
           
           
                                    
                  </table>
                  </div>
              
              </div>
              <!--TOP TABING END HERE--> 
            </div>
            <table width="100%" cellpadding="0" cellspacing="0" class="tab_button">
              <tr>
                <td width="70%">&nbsp;</td>
                <td align="left" ><button class="lang_button"><strong>Submit</strong></button>
                  <button class="lang_button_re" type="reset"  onclick="resetLanguageValidate()"><strong>Reset</strong></button></td>
              </tr>
            </table><br />

            <input type="hidden" name="control" value="customer"/>
            <input type="hidden" name="edit" value="1"/>
            <input type="hidden" name="task" value="add_test_save"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
