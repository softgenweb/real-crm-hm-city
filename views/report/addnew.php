<link href="multi/jquery.multiselect.css" rel="stylesheet" type="text/css">
<style>

ul,li { margin:0; padding:0; list-style:none;}
.label { color:#000; font-size:16px;}
.ms-options-wrap > .ms-options > ul label input{
margin-left:8px;
width:30px;

}

.ms-options-wrap > .ms-options > ul label{
text-align:left !important;
font-weight: unset !important;
}
.ms-options-wrap > button:focus, .ms-options-wrap button{
	color:#555 !important;
	border-radius: 5px !important;
	padding: 6px 16px !important;
	font-size: 14px !important;

}
</style> 
<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Project Plot</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=project_plot&task=show"><i class="fa fa-list" aria-hidden="true"></i> Project Plot List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Project Plot</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Project Plot</li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
            
			              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">

					
						<div class="col-md-3">
                           <div class="form-group center_text">
								<label>Project Name</label>
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<select name="project_id" id="project_id" onchange="block_change(this.value)" class="form-control"  required="">
								<option value="">Select Project</option>
								<?php $sel=mysql_query("SELECT * FROM `project` WHERE `status`= 1");
								while($select=mysql_fetch_array($sel)){
								if($select['status'] == '1') { ?>
								<option value="<?php echo $select['id'];?>"<?php if($result['project_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['project_name'];?></option>
								<?php } else { ?>  
								<option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['project_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['project_name'];?></option>     
								<?php } } ?>
								</select>
								<span class="msgValid" id="msgproject_id"></span>
							</div>
						</div>
						 <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								<label>Block Name</label>
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<select name="block_id" id="block_id" onchange="plot_change(this.value);" class="form-control"  required="">
								<option value="">Select Block</option>
								<?php $sel=mysql_query("SELECT * FROM `block` WHERE `project_id`='".$result['project_id']."' AND `status`= 1");
								while($select=mysql_fetch_array($sel)){
								if($select['status'] == '1') { ?>
								<option value="<?php echo $select['id'];?>"<?php if($result['block_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['block_name'];?></option>
								<?php } else { ?>  
								<option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['block_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['block_name'];?></option>     
								<?php } } ?>
								</select>
								<span class="msgValid" id="msgblock_id"></span>
							</div>
						</div>
						 <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								<label>Plot Name</label>
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<?php if($_GET['id']==''){ ?>
                                <div id="plot_ids">
								<select name="plot_id[]" id="plot_id" class="form-control plot_id"  required="">
								
								<option value="">Select Plot</option>
								</select></div>
							<?php }else{ ?>
								<select name="plot_id" id="plot_id_edit" class="form-control"  required="">
								<option value="">Select Plot</option>
								<?php $sel=mysql_query("SELECT * FROM `plot` WHERE 1");
								while($select=mysql_fetch_array($sel)){
								if($select['status'] == '1') { ?>
								<option value="<?php echo $select['id'];?>"<?php if($result['plot_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['plot_name'];?></option>
								<?php } else { ?>  
								<option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['plot_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['plot_name'];?></option>     
								<?php } } ?>
								</select>
							<?php } ?>
								<span class="msgValid" id="msgplot_id"></span>
							</div>
						</div>
					      <div class="clearfix"></div>
					      <div class="col-md-3">
                           <div class="form-group center_text">
								<label>Area Size</label>
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<select name="area_id" id="area_id" class="form-control"  required="">
								<option value="">Select Area Size</option>
								<?php $sel=mysql_query("SELECT * FROM `area_measurement` WHERE `status`= 1");
								while($select=mysql_fetch_array($sel)){
								if($select['status'] == '1') { ?>
								<option value="<?php echo $select['id'];?>"<?php if($result['area_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['dimenstion'].' ('.$select['square_feet'].' sqft)';?></option>
								<?php } else { ?>  
								<option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['area_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['dimenstion'].' ('.$select['square_feet'].' sqft)';?></option>     
								<?php } } ?>
								</select>
								<!-- <input type="text" onkeyup="area_change(this.value);" id="area_id" name="area_id" placeholder="Enter Dimension Or Area feet" class="form-control" > -->
								<span id="area_dim"></span>
							</div>
						</div>
					      <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								<label>Rate (per sqft)</label>
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<input type="text" placeholder="Enter Plot Rate" value="<?php echo $result['rate']; ?>" id="rate" name="rate" class="form-control" required="" >
								<span class="msgValid" id="msgrate"></span>
							</div>
						</div>
					
                    <div class="clearfix"></div>
       
					<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>                   

					<input type="hidden" name="control" value="project_plot"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('project_id').value == '') { 
			document.getElementById('msgproject_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgproject_id').innerHTML = "";
		}	

		if(document.getElementById('block_id').value == '') { 
			document.getElementById('msgblock_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgblock_id').innerHTML = "";
		}	
		if(document.getElementById('plot_id').value == '') { 
			document.getElementById('msgplot_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgplot_id').innerHTML = "";
		}	
		if(document.getElementById('area_id').value == '') { 
			document.getElementById('msgarea_id').innerHTML = "*Required field.";
			chk=0;
		}else {
			document.getElementById('msgarea_id').innerHTML = "";
		}	
		


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>
<script src="multi/jquery.min.js"></script>
<script src="multi/jquery.multiselect.js"></script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   