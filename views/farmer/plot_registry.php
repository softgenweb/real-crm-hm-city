<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Farmer Plot Registry</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=farmer&task=show_plot_registry"><i class="fa fa-list" aria-hidden="true"></i> Plot Registry List</a></li>
      <?php if($result!='') {?>           
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Plot Registry</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Plot Registry</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);   
      }
      
      $plot = mysql_fetch_array(mysql_query("SELECT * FROM `plot_booking` WHERE `customer_id`='".$result['customer_id']."' AND `id`='".$result['plot_booking_id']."'"));
      
      $farmer_plot = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$result['farmer_id']."' AND `status`= 1")); ?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-3">
                  <div class="form-group center_text">Plot Name:</div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="plot_booking" id="plot_booking" class="form-control"  required="">
                        <option value="">Select</option>
                        <?php 
                           $where = $_REQUEST['id']?" `id` ='".$result['plot_booking_id']."'":"`id` NOT IN (SELECT `plot_booking_id` FROM `plot_registry` WHERE `status`= 1)";
                           $sql = mysql_query("SELECT * FROM `plot_booking` WHERE `status`= 1 AND $where"); 
                             while($row=mysql_fetch_array($sql)){   ?>
                        <option value="<?php echo $row['id']; ?>" <?php echo $result['plot_booking_id']==$row['id']?'selected':''; ?>><?php echo $row['project_name'].'/'.$row['block_name'].'('.$row['plot_name'].')'; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Customer Name: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group" id="customer">                
                     <input type="text" id="customer" name="customer" value="<?php echo $plot['applicant_name']; ?>" class="form-control" readonly>
                     <input type="hidden" id="customer_id" name="customer_id" value="<?php echo $plot['customer_id']; ?>" class="form-control" >
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">Registry Through: </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="registry_by" id="registry_by" class="form-control" required="">
                        <option value="">Select</option>
                        <option value="Company" <?php echo $result['registry_by']=='Company'?'selected':''; ?>>Company</option>
                        <option value="Farmer" <?php echo $result['registry_by']=='Farmer'?'selected':''; ?>>Farmer</option>
                     </select>
                     <span class="msgValid" id="msgemail_id"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">Farmer name: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="farmer_id" id="farmer_id" class="form-control" required="">
                        <option value="">Select</option>
                        <?php $sql = mysql_query("SELECT * FROM `farmer` WHERE `status`= 1");
                           while($row=mysql_fetch_array($sql)){ ?>
                        <option value="<?php echo $row['id']; ?>" <?php echo $result['farmer_id']==$row['id']?'selected':''; ?>><?php echo $row['name']; ?></option>
                        <?php } ?>
                     </select>
                     <span class="msgValid" id="msggata_no"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Gata/Khasara no.:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group" id="ghata_no">
                     <input type="text" id="ghata_no" name="ghata_no" value="<?php echo $result['gata_no']; ?>" class="form-control" readonly>
                     <input type="hidden" id="sqft" name="sqft" value="<?php echo $farmer_plot['sqft']; ?>" class="form-control" >
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Date of Registry:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['registry_date']; ?>" placeholder="DD-MM-YYYY" id="registry_date" name="registry_date"  class="form-control" required>
                     <span class="msgValid" id="msgaddress"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Plot Size: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group" id="plot_area">
                     <input type="text" id="plot_area" name="plot_area" value="<?php echo $plot['area_dimension']; ?>" class="form-control" readonly>
                     <input type="hidden" id="plot_size" name="plot_size" value="<?php $area = explode('(', $plot['area_dimension']); echo $res = substr($area[1],0,-9); ?>" class="form-control">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Remaining Area : 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group" id="plot_area">                              
                     <input type="text" value="<?php echo $result['remaining_area']; ?>" id="remain_area" name="remain_area" class="form-control" readonly>
                     <span class="msgValid" id="msgpincode"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="farmer"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="farmer_name" id="farmer_name" value=""/>
               <input type="hidden" name="task" value="save_registry"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   $('#registry_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
     mask:new Date(),
    //minDate:'-1970/01/02', // yesterday is minimum date
    maxDate:0 // and tommorow is maximum date calendar
   });
   
   
   /*=============Gsingh Jquery==============*/
   
   function isNumber(evt) {
       evt = (evt) ? evt : window.event;
           var charCode = (evt.which) ? evt.which : event.keyCode;
       if (charCode != 46 && charCode > 31
       && (charCode < 48 || charCode > 57))
           return false;
   
       return true;
   }
   
   <?php if($_REQUEST['id']){ ?>
     $(document).ready(function(){
      var fname = $('#farmer_id').find(":selected").text();
       $('#farmer_name').val(fname);
       $('#plot_booking').prop('disabled', true);
       $('#customer').prop('disabled', true);
       $('#registry_by').prop('disabled', true);
       $('#farmer_id').prop('disabled', true);


     });
   <?php } ?>
   /*======Get Booked plot detail==========*/
   $("#plot_booking").change(function(){
   
       var str = $(this).val();
       $.ajax({url: "script/customer_name.php?id="+str, success: function(result){
           $("#customer").html(result);
   
       }});
       $.ajax({url: "script/plot_area.php?id="+str, success: function(result){
           $("#plot_area").html(result);
   
       }});
   
   });
   
   
   $("#farmer_id").change(function(){
       var str = $(this).val();
       var fname = $(this).find(":selected").text();
       $('#farmer_name').val(fname);
       $.ajax({url: "script/farmer_plot.php?id="+str, success: function(result){
           var res = $("#ghata_no").html(result);
           remain_area();
       }});
     
   });
   
   $(document).ready(function(){
     if($('#farmer_id').val()!=''){
       remain_area();
     }
   });
   
   function remain_area(){
     var sqft = parseFloat($('#sqft').val()).toFixed(2);
     var size = parseFloat($('#plot_size').val()).toFixed(2);
     $('#remain_area').val((sqft-size)+' SQFT');
   }
   
    /*============Auto hide alert box================*/
    $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
    });
</script>

