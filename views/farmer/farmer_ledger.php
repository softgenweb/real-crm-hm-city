<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style type="text/css">
   button.btn.btn-primary a{
   color: white;
   }
   td a{
   margin: 2px;
   }
   .form-control.pay_date{
   max-width: 100px !important;
   }
   @media print {

   body{
   visibility: hidden;
   margin-top: -100px;
   }
   .link_hide, #cheque_status{
   display: none;
   visibility: visible;
   }
   #example1-2, .show_detail{
   visibility: visible;
   margin:0;
   padding: 0;
   }
  /* #example1-2{
       width:80em;
   }*/
   #example1-2 td, #example1-2 {
   border: 1px solid #727272 !important;
   }
   #example1-2.dataTable thead > tr > th {
   vertical-align: top;
   padding: unset;
   border: 1px solid #727272 !important; 
   }
   #example1-2.dataTable thead .sorting, #example1-2.dataTable thead .sorting_asc{
   position: unset;
   }
   }
</style>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Farmer Payment Ledger</h3>
            <?php foreach($uresults as $result) { }  ?>
            <?php if($_SESSION['utype']!="Report"){ ?>  
            <a href="index.php?control=farmer&task=add_payment&id=<?php echo $_REQUEST['id']; ?>" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Payment Detail</a> <?php } ?>
            <a href="javascript:window.print();" class="btn btn-primary bulu pull-right" >Print</a>
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=farmer&task=show"><i class="fa fa-list" aria-hidden="true"></i> Farmer List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Payment Ledger</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php     unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }   
            $farmer = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$_REQUEST['id']."'"));
            ?>
         <div class="row show_detail">
            <div class="col-sm-12 text-center">
               <div class="col-sm-3">
                  <label>Farmer Name : </label> <?php echo $farmer['name'];?>
                  <!-- <label>Farmer Name : </label> <a class="link_hide" href="index.php?control=farmer&task=show_detail&id=<?php echo $farmer['id']; ?>" title="View"><span class="show_detail"><?php echo $farmer['name'];?></span></a>  -->
               </div>
               <div class="col-sm-3">
                  <label>Mobile no. : </label>  <?php echo $farmer['mobile']; ?>
               </div>
               <div class="col-sm-3"> 
                  <label>Total Area : </label> <?php echo $farmer['sqft'].' SQFT'; ?>
               </div>
               <div class="col-sm-3"> 
                  <label>Gata No. : </label> <?php echo $farmer['gata_no']; ?>
               </div>
            </div>
         </div>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-2" class="table table-bordered table-striped">
                     <thead>
                        <tr class="hide_tr">
                           <th width="15"><div align="left">S.No</div></th>
                           <th><div align="left">Receipt No.</div></th>
                           <th><div align="left">Generated Amount</div></th>
                           <th><div align="left">Paid Amount</div></th>
                           <th><div align="left">Balance Amount</div></th>
                           <th><div align="left">Payment Date</div></th>
                           <th><div align="left">Payment Type</div></th>
                           <th><div align="left">Cheque No.</div></th>
                           <th><div align="left">Bank Name</div></th>
                           <th><div align="left">Remark</div></th>
                           <th><div align="left">Cheque Status</div></th>
                           <?php if($_SESSION['utype']!="Report"){ ?> 
                           <th class="link_hide"><div align="left">Action</div></th>
                           <?php } ?>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           // $receipt = mysql_fetch_array(mysql_query("SELECT * FROM `farmer_payment_receipt` WHERE `id`='".$result['receipt_no']."'"));
                            if($result['cheque_status']=='1'){

                              $pay_date = $result['cheque_status_date']?$result['cheque_status_date']:($result['cheque_date']?$result['cheque_date']:$result['payment_date']);

                               $newDate = date("d-m-Y", strtotime($pay_date));

                              
                           ?>
                        <tr>
                           <td align="left"><?php echo $countno; ?></td>
                           <td align="left"><?php echo $result['receipt_no'];?></td>
                           <td align="left"><?php echo $result['amount'];?></td>
                           <td align="left"><?php echo $result['paid_amount'];?></td>
                           <td align="left"><?php echo $result['remain_amount'];?></td>
                           <!-- <td align="left"><?php echo $result['amount']-$result['paid_amount'];?></td> -->
                           <td align="left"><?php echo $result['cheque_status_date']?$newDate." (Clear Date)":$newDate; ?></td>
                           <td align="left"><?php echo ucfirst($result['payment_type']);?></td>
                           <td align="left"><?php echo $result['cheque_no'];?></td>
                           <td align="left"><?php echo ucfirst($result['bank_name']);?></td>
                           <td align="left"><?php echo $result['remark']?'<strong style="color:red">'.$result['remark'].'</strong>':'N/A';?></td>
                           <form name="myform" method="post" id="cheque_status_form" onsubmit="return validate();">
                              <td align="left">
                                 <?php if($result['cheque_status']=='1'){ echo "<strong style='color: green;'>Cleared</strong>";  }elseif($_SESSION['utype']!="Report"){
                                    ?>
                                 <select id="cheque_status" name="cheque_status" class="form-control" required="">
                                    <option value="">Clearing</option>
                                    <option value="1" <?php if($result['cheque_status']=='1'){echo "selected";} ?>>Cleared</option>
                                    <option value="2" <?php if($result['cheque_status']=='2'){echo "selected";} ?>>Bounce</option>
                                 </select>
                                 <?php }else{
                                    echo "<strong style='color: black;'>Clearing</strong>" ;
                                    } ?>
                              </td>

                              <td align="left" class="link_hide">
                                 <?php if($result['cheque_status']!='1' && $_SESSION['utype']!="Report"){ 
                                    $change_amount = $result['amount']-$result['paid_amount'];  ?>
                                 <input class="btn btn-primary bulu" type="submit" name="submit" value="Submit" >
                                 <input type="hidden" name="control" value="farmer">
                                 <input type="hidden" name="task" value="change_chq_status">
                                 <input type="hidden" name="farmer_id" value="<?php echo $result['farmer_id']; ?>">
                                 <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
                                 <input type="hidden" name="balance_amt" value="<?php echo $change_amount; ?>">
                                 <?php } if($_SESSION['utype']!="Report"){ ?>
                                 <a href="index.php?control=farmer&task=edit_payment&editid=<?php echo $result['id']; ?>&id=<?php echo $result['farmer_id']; ?>"><strong>Edit</strong></a>
                                 <?php }else{ echo "<strong>---</strong>"; } ?>
                              </td>
                           </form>
                           <!--  <?php if($_SESSION['utype']!="Report"){ ?> 
                              <td  class="link_hide" align="left"><a href="index.php?control=farmer&task=add_payment&editid=<?php echo $result['id']; ?>"><strong>Edit</strong></a></td><?php } ?> -->
                        </tr>
                        <?php $total_paid += $result['paid_amount']; } } ?>  
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <td></td>
                           <td align="left"><strong>Total Paid :</strong></td>
                           <td align="left"><strong><?php echo $total_paid; ?></strong></td>
                           <td colspan="7"></td>
                           <td></td>
                           <!-- <td></td>
                              <td></td>
                              <td></td>  -->    
                        </tr>
                     </tfoot>
                     <?php } ?>                  
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
         <!-- /.box-body -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- =============================Table 2 Pending Cheque========================== -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <div class="box-header">
            <h3 class="box-title">Pending Cheques</h3>
         </div>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-3" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="left">S.No</div></th>
                           <!-- <th><div align="left">Receipt No.</div></th> -->
                           <!-- <th><div align="left">Generated Amount</div></th> -->
                           <th><div align="left">Issue Date</div></th>
                           <th><div align="left">Cheque Amount</div></th>
                           <!-- <th><div align="left">Balance Amount</div></th> -->
                           
                           <!-- <th><div align="left">Payment Type</div></th> -->
                           <th><div align="left">Bank Name</div></th>
                           <th><div align="left">Chq/Txn No.</div></th>  
                           <th><div align="left">Remark</div></th>
                           <th><div align="left">Cheque Status</div></th>
                           <th id="chq_date" style="display:none"><div align="left">Cheque Status Date</div></th>
                           <th><div align="left">Action</div></th>
                           <!--<th><div align="left">Delete</div></th>-->
                         
                        </tr>
                     </thead>
                     <tbody>
                         <?php
                           // if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               //foreach($results as $result){ 
                               
                               $sql=mysql_query("SELECT * FROM `farmer_payment_receipt` WHERE `farmer_id`='".$_REQUEST['id']."' AND `payment_type`='Cheque' AND `cheque_status`='0'  AND `status`=1");
                               while($res = mysql_fetch_array($sql)){
                               $i++;
                               $countno++;
                           
                          // ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           $pay_date = $res['creation_date']?$res['creation_date']:$res['payment_date'];
                           $newDate = date("d-m-Y", strtotime($pay_date));  
                          
                            // if($result['cheque_status']=='0'){
                           ?>
                       <tr>
                           <td align="left"><?php echo $countno; ?></td>
                           <td align="left"><?php echo $newDate;?></td>
                           <td align="left"><?php echo $res['amount'];?></td>
                           
                           <td align="left"><?php echo $res['txn_no'];?></td>
                           <td align="left"><?php echo ucfirst($res['bank']);?></td>
                           <td align="left"><?php echo $res['remark']?$res['remark']:'N/A';?></td>
                           <form name="myform" method="post" id="cheque_status_form" onsubmit="return validate();">
                              <td align="left">
                                 <?php if($res['cheque_status']=='1'){ echo "<strong style='color: green;'>Cleared</strong>";  }elseif($_SESSION['utype']!="Report"){
                                    ?>
                                 <select id="cheque_status"   onchange="change_date(<?php echo $countno; ?>,this.value)" name="cheque_status" class="form-control" required="">
                                    <option value="0">Clearing</option>
                                    <option value="1" <?php if($res['cheque_status']=='1'){echo "selected";} ?>>Cleared</option>
                                    <option value="2" <?php if($res['cheque_status']=='2'){echo "selected";} ?>>Bounce</option>
                                 </select>
                                 <?php }else{
                                    echo "<strong style='color: black;'>Clearing</strong>" ;
                                    } ?>
                              </td>
                              <td align="left" id="chq_date1<?php echo $countno; ?>" style="display:none"><input type="text" width="100px" class="form-control cheque_status_date" name="cheque_status_date" id="cheque_status_date" value="<?php echo date('d-m-Y');?>" /></td>
                              <td align="left">
                                 <?php if($res['cheque_status']!='1' && $_SESSION['utype']!="Report"){ 
                                   // $change_amount = $res['amount']-$res['paid_amount'];  ?>
                                 <input class="btn btn-primary bulu" type="submit" name="submit" value="Submit" >
                                 <input type="hidden" name="control" value="farmer">
                                 <input type="hidden" name="task" value="change_chq_status">
                                 <input type="hidden" name="farmer_id" value="<?php echo $res['farmer_id']; ?>">
                                 <input type="hidden" name="id" value="<?php echo $res['id']; ?>">
                                 <input type="hidden" name="balance_amt" value="<?php echo $change_amount; ?>">
                                 <?php } if($_SESSION['utype']!="Report"){ ?>
                                 <!-- <a href="index.php?control=farmer&task=add_payment&editid=<?php echo $res['id']; ?>"><strong>Edit</strong></a> -->
                                 <?php }else{ echo "<strong>---</strong>"; } ?>
                              </td>
                           </form>
                             <!--<td align="left">
                          <?php  if($res['status']==1){  ?>
                              <a href="index.php?control=farmer&task=del_status&id=<?php echo $res['id']; ?>&fid=<?php echo $_REQUEST['id']; ?>" onclick="return confirm('Are you sure you want to Delete ?')" style="cursor:pointer;" title="Delete"><b style="color:red;cursor:pointer;" >Delete</b></a>
                              <?php } ?></td>-->

                        </tr>
                        <?php $pending_total += $res['amount']; } //} ?>  
                     </tbody>
                     <tfoot>
                        <tr>
                           <!-- <td></td> -->
                           <td></td>
                           <td align="left"><strong> Total </strong></td>
                           <td align="left"><strong><?php echo $pending_total?$pending_total:'0'; ?></strong></td>
                           <!-- <td></td> -->
                           <td></td>
                           <td></td>
                           <td></td>
                           
                        </tr>
                     </tfoot>
                      <?php //} ?>                  
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- =============================Table 2 Bounced Cheque========================== -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <div class="box-header">
            <h3 class="box-title">Bounced Cheques</h3>
         </div>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-4" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="left">S.No</div></th>
                           <th><div align="left">Receipt No.</div></th>
                           <!-- <th><div align="left">Generated Amount</div></th> -->
                           <th><div align="left">Cheque Amount</div></th>
                           <!-- <th><div align="left">Balance Amount</div></th> -->
                           <th><div align="left">Cheque Date</div></th>
                           <!-- <th><div align="left">Payment Type</div></th> -->
                           <th><div align="left">Bank Name</div></th>
                           <th><div align="left">Chq/Txn No.</div></th>
                           <th><div align="left">Cheque Status</div></th>
                           <th><div align="left">Remark</div></th>
                           <!-- <th><div align="left">Action</div></th> -->
                        </tr>
                     </thead>
                     <tbody>
                         <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                            $pay_date = $result['cheque_date']?$result['cheque_date']:$result['payment_date'];
                           $newDate = date("d-m-Y", strtotime($pay_date));  
                          
                             if($result['cheque_status']=='2'){
                           ?>
                       <tr>
                           <td align="left"><?php echo $countno; ?></td>
                           <td align="left"><?php echo $result['receipt_no'];?></td>
                           <!-- <td align="left"><?php echo $result['amount'];?></td> -->
                           <td align="left"><?php echo $result['paid_amount'];?></td>
                           <!-- <td align="left"><?php echo $result['remain_amount'];?></td> -->
                           <!-- <td align="left"><?php echo $result['amount']-$result['paid_amount'];?></td> -->
                           <td align="left"><?php echo $newDate;?></td>
                           <!-- <td align="left"><?php echo ucfirst($result['payment_type']);?></td> -->
                           <td align="left"><?php echo ucfirst($result['bank_name']);?></td>
                           <td align="left"><?php echo $result['cheque_no'];?></td>
                           <td align="left"><strong style='color: red;'>Bounce</strong></td>
                           <td align="left"><?php echo $result['remark']?$result['remark']:'N/A';?></td>
                         
                        </tr>
                        <?php $total += $result['paid_amount']; } } ?>  
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <!-- <td></td> -->
                           <td align="left"><strong> Total </strong></td>
                           <td align="left"><strong><?php echo $total?$total:'0'; ?></strong></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           
                        </tr>
                     </tfoot>
                     <?php } ?>                  
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(3500).slideUp(200, function() {
     $(this).alert('close');
   });
   
   
   function change_date(id,str)
{ 
  if(str!='0')
  {
    $('#chq_date1'+id).show();
    $('#chq_date').show();
  //  document.getElementById("chq_date").style.display="block;
  }else{
   $('#chq_date1'+id).hide();

   $('#chq_date').hide();
  }
}
  $('#cheque_status_date').datetimepicker({
  yearOffset:0,
  lang:'ch',
  timepicker:false,
  format:'d-m-Y',
  formatDate:'Y/m/d',
    // minDate:'+1970/01/01'
});
   
</script>

