<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Link Farmer Plot</h3>
	</div>
   <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i>  Link Farmer Plot</li>
           <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_farmer_plot.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->
          </ol>

            <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>             
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
                    <div class="row col-md-12">
                   <div class="col-md-12 col-sm-9 col-xs-12">        
                    
                    <div class="col-md-2" align="right">
                    <div class="form-group center_text">
                    Project Name:
                    </div></div>
                    <div class="col-md-2"><div class="form-group">                    
                      <select name="project_id" id="project_id" onchange="block_change(this.value)" class="form-control" required="">
                      <option value="">Select Project</option>
                      <?php $sel=mysql_query("SELECT * FROM `project` WHERE `status`= 1");
                      while($select=mysql_fetch_array($sel)){
                      if($select['status'] == '1') { ?>
                      <option value="<?php echo $select['id'];?>"<?php if($_REQUEST['project_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['project_name'];?></option>
                      <?php } else { ?>  
                      <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($_REQUEST['project_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['project_name'];?></option>     
                      <?php } } ?>
                      </select>
                    <span id="msgcountry_id" style="color:red;"></span>
                    </div></div>
                    
                    <div class="col-md-2" align="right">
                    <div class="form-group center_text">
                    Block Name:
                    </div></div>
                    <div class="col-md-2"><div class="form-group">                    
                        <select name="block_id" id="block_id" class="form-control" >
                          <option value="">Select Block</option>
                          <?php $sel=mysql_query("SELECT * FROM `block` WHERE `project_id`='".$_REQUEST['project_id']."' AND `status`= 1");
                          while($select=mysql_fetch_array($sel)){
                          if($select['status'] == '1') { ?>
                          <option value="<?php echo $select['id'];?>"<?php if($_REQUEST['block_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['block_name'];?></option>
                          <?php } else { ?>  
                          <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($_REQUEST['block_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['block_name'];?></option>     
                          <?php } } ?>
                        </select>
                    <span id="msgstate_id" style="color:red;"></span>
                    </div></div>
                
                 <div class="col-md-2" align="right">
                    <div class="form-group center_text">
                    Farmer Name:
                    </div></div>
                    <div class="col-md-2"><div class="form-group">                    
                        <select name="farmer_id" id="farmer_id" class="form-control" >
                          <option value="">Select Farmer</option>
                          <?php $sel=mysql_query("SELECT * FROM `farmer` WHERE `status`= 1");
                          while($select=mysql_fetch_array($sel)){
                          if($select['status'] == '1') { ?>
                          <option value="<?php echo $select['id'];?>"<?php if($_REQUEST['farmer_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['name'];?></option>
                          <?php } else { ?>  
                          <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($_REQUEST['farmer_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['name'];?></option>     
                          <?php } } ?>
                        </select>
                    <span id="msgfarmer_id" style="color:red;"></span>
                    </div></div>
                                
                 
       <div class="col-md-4 col-sm-8 col-xs-12">                      
            <input  class="btn btn-primary butoon_brow" type="submit" name="search" id="btnSelected" value="Search" />

          </div>
                   

          <input type="hidden" name="control" value="farmer"/>
          <input type="hidden" name="edit" value="1"/>
          <input type="hidden" name="task" value="link_plot"/>
          <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
       

        </div>
      </div>
    </form>
	 <form name="link_feature" action="index.php" method="post" >
                  <table id="example1-1"  class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th><div align="left">S.No</div></th>
                         <th><div align="left">Project Name</div></th>
                        <th><div align="left">Block Name</div></th>
                        <th><div align="left">Plot Name</div></th>
                        <th><div align="left">Area Dimension(ft)</div></th>
                        <th><div align="left">Area (in sqft)</div></th>
                        <th><div align="left">Area (in sqmt)</div></th>
                        <th><div align="left">Action(Check All) </div></th>
                      </tr>
                    </thead>
                    <tfoot style="display: none;">
                      <tr>
                        <th><div align="left">S.No</div></th>
                         <th><div align="left">Project Name</div></th>
                        <th><div align="left">Block Name</div></th>
                        <th><div align="left">Plot Name</div></th>
                        <th><div align="left">Area Dimension(ft)</div></th>
                        <th><div align="left">Area (in sqft)</div></th>
                        <th><div align="left">Area (in sqmt)</div></th>
                        <th><div align="left">Action(Check All) </div></th>
                      </tr>
                    </tfoot>
                    <tbody>

					<?php
                   
                        if($results) {
                            $countno = ($page-1)*$tpages;
                            $i=0;
                            foreach($results as $result){ 
							
                            $i++;
                            $countno++;
								
								($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
								
					

               	$project = mysql_fetch_array(mysql_query("SELECT * FROM `project` WHERE `id`='".$result['project_id']."'"));

                $block = mysql_fetch_array(mysql_query("SELECT * FROM `block` WHERE `id`='".$result['block_id']."'"));

                $plot = mysql_fetch_array(mysql_query("SELECT * FROM `plot` WHERE `id`='".$result['plot_id']."'"));

                $area = mysql_fetch_array(mysql_query("SELECT * FROM `area_measurement` WHERE `id`='".$result['area_id']."'"));
		             
		 $linked_plot = mysql_fetch_array(mysql_query("select *,fp.project_plot_id from farmer_plot as fp join project_plot as pp where fp.`project_plot_id` = pp.id "));		 
			 
			$farmer = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$result['farmer_id']."'"));
		    ?>
                   
                  
                        <tr>
                         <td align="left"><?php echo $countno; ?></td>
                            <td align="left"><?php echo $project['project_name'];?></td> 
                            <td align="left"><?php echo $block['block_name'];?></td> 
                            <td align="left"><?php echo $plot['plot_name'];?></td> 
                            <td align="left"><?php echo $area['dimenstion'];?></td> 
                            <td align="left"><?php echo $area['square_feet'];?></td> 
                            <td align="left"><?php echo $area['square_meter'];?></td>   
                         
                         <td align="left">
                         
						<?php if($result['farmer_id']!='') { ?>
                       <input type="checkbox"  class="btn btn-primary butoon_brow" value="<?php echo $result['id']; ?>" name="link[]<?php echo $i; ?>" id="link" <?php  if($_SESSION['farmer_id']== $result['farmer_id']) { ?>  checked="checked"  <?php } ?> />   
                                               
                           <?php } else { ?>
                         <input type="checkbox"  class="btn btn-primary butoon_brow" value="<?php echo $result['id'];?>" name="link[]<?php echo $i; ?>" id="link" />    
                        <?php } ?>
                            
                                              
                 <input type="hidden" class="form-control" name="project_plot_id[]" id="project_plot_id" value="<?php echo $result['id']; ?>" />
                <input type="hidden" name="project_id[]" id="project_id" value="<?php echo $result['project_id']; ?>" />
                <input type="hidden" name="block_id[]" id="block_id" value="<?php echo $result['block_id']; ?>" />
                <input type="hidden" name="plot_id[]" id="plot_id" value="<?php echo $result['plot_id']; ?>" />
                <input type="hidden" name="area_id[]" id="area_id" value="<?php echo $result['area_id']; ?>" /> 
                <input type="hidden" name="farmer_id[]" id="farmer_id" value="<?php echo $_SESSION['farmer_id']; ?>" /> 
                <input type="hidden" name="linked_plot_id[]" id="linked_plot_id" value="<?php  if($linked_plot != "") { echo $linked_plot['id'];  } else { echo "0"; }?>" />
                
                            <input type="hidden" name="control" value="farmer"/>
                            <input type="hidden" name="view" value="save_link_plot"/>
                            <input type="hidden" name="task" value="save_link_plot"/>                        
                        </td>
                      
                     </tr>
                       
						<?php }  }else{?>
                        <?php } ?>
                    </tbody>
                    
                  </table>
                  <input  class="btn btn-primary bulu" type="submit" name="search" id="btnSelected" value="Submit" />
                   </form>
                  <?php //} ?>
                  <div class="clearfix"></div> 
                  </div>
                </div><!-- table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            
            <!-------------------------------------- Second Table ------------------------------------------------>
        
          </div><!-- /.row -->
        </section> 
     




<script type="text/javascript">
function validation()

{   
	var chk=1;
	
		if(document.getElementById('category_id').value == '') { 
			document.getElementById('msgcategory_id').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgcategory_id').innerHTML = "";
		}
		
		if(document.getElementById('sub_category_id').value == '') { 
			document.getElementById('msgsub_category_id').innerHTML = "*Required field.";
			chk=0;
		}
			else {
			document.getElementById('msgsub_category_id').innerHTML = "";
		}
		
		
				if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}




</script>






<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});

   $(document).ready(function() {
            const table = $('#example1-1').DataTable({
                stateSave: true
            });

            // Setup - add a text input to each footer cell
            $('#example1-1 tfoot th').each( function (index,value) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" value="'+table.column(index).search()+'"/>' );
            } );


            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            });
        });
 

</script>
   