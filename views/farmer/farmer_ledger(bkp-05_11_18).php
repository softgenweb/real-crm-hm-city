<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style type="text/css">
   button.btn.btn-primary a{
   color: white;
   }
   td a{
   margin: 2px;
   }
   .form-control.pay_date{
   max-width: 100px !important;
   }
   @media print {

   body{
   visibility: hidden;
   margin-top: -100px;
   }
   .link_hide, #cheque_status{
   display: none;
   visibility: visible;
   }
   #example1-2, .show_detail{
   visibility: visible;
   margin:0;
   padding: 0;
   }
  /* #example1-2{
       width:80em;
   }*/
   #example1-2 td, #example1-2 {
   border: 1px solid #727272 !important;
   }
   #example1-2.dataTable thead > tr > th {
   vertical-align: top;
   padding: unset;
   border: 1px solid #727272 !important; 
   }
   #example1-2.dataTable thead .sorting, #example1-2.dataTable thead .sorting_asc{
   position: unset;
   }
   }
</style>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Farmer Payment Ledger</h3>
            <?php foreach($uresults as $result) { }  ?>
            <?php if($_SESSION['utype']!="Report"){ ?>  
            <a href="index.php?control=farmer&task=add_payment&id=<?php echo $_REQUEST['id']; ?>" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Payment Detail</a> <?php } ?>
            <a href="javascript:window.print();" class="btn btn-primary bulu pull-right" >Print</a>
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=farmer&task=show"><i class="fa fa-list" aria-hidden="true"></i> Farmer List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Payment Ledger</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php     unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }   
            $farmer = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$_REQUEST['id']."'"));
            ?>
         <div class="row show_detail">
            <div class="col-sm-10 col-sm-offset-1">
               <div class="col-sm-4">
                  <label>Farmer Name : </label> <?php echo $farmer['name'];?>
                  <!-- <label>Farmer Name : </label> <a class="link_hide" href="index.php?control=farmer&task=show_detail&id=<?php echo $farmer['id']; ?>" title="View"><span class="show_detail"><?php echo $farmer['name'];?></span></a>  -->
               </div>
               <div class="col-sm-4">
                  <label>Mobile no. : </label>  <?php echo $farmer['mobile']; ?>
               </div>
               <div class="col-sm-4"> 
                  <label>Total Area : </label> <?php echo $farmer['sqft'].' SQFT'; ?>
               </div>
            </div>
         </div>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-2" class="table table-bordered table-striped">
                     <thead>
                        <tr class="hide_tr">
                           <th width="15"><div align="left">S.No</div></th>
                           <th><div align="left">Receipt No.</div></th>
                           <th><div align="left">Generated Amount</div></th>
                           <th><div align="left">Paid Amount</div></th>
                           <th><div align="left">Balance Amount</div></th>
                           <th><div align="left">Payment Date</div></th>
                           <th><div align="left">Payment Type</div></th>
                           <th><div align="left">Cheque No.</div></th>
                           <th><div align="left">Bank Name</div></th>
                           <th><div align="left">Remark</div></th>
                           <th><div align="left">Cheque Status</div></th>
                           <?php if($_SESSION['utype']!="Report"){ ?> 
                           <th class="link_hide"><div align="left">Action</div></th>
                           <?php } ?>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                            if($result['cheque_status']=='1'){
                           ?>
                        <tr>
                           <td align="left"><?php echo $countno; ?></td>
                           <td align="left"><?php echo $result['receipt_no'];?></td>
                           <td align="left"><?php echo $result['amount'];?></td>
                           <td align="left"><?php echo $result['paid_amount'];?></td>
                           <td align="left"><?php echo $result['remain_amount'];?></td>
                           <!-- <td align="left"><?php echo $result['amount']-$result['paid_amount'];?></td> -->
                           <td align="left"><?php echo $result['cheque_date']?$result['cheque_date']:$result['payment_date'];?></td>
                           <td align="left"><?php echo ucfirst($result['payment_type']);?></td>
                           <td align="left"><?php echo $result['cheque_no'];?></td>
                           <td align="left"><?php echo ucfirst($result['bank_name']);?></td>
                           <td align="left"><?php echo $result['remark']?$result['remark']:'N/A';?></td>
                           <form name="myform" method="post" id="cheque_status_form" onsubmit="result validate();">
                              <td align="left">
                                 <?php if($result['cheque_status']=='1'){ echo "<strong style='color: green;'>Cleared</strong>";  }elseif($_SESSION['utype']!="Report"){
                                    ?>
                                 <select id="cheque_status" name="cheque_status" class="form-control" required="">
                                    <option value="">Clearing</option>
                                    <option value="1" <?php if($result['cheque_status']=='1'){echo "selected";} ?>>Cleared</option>
                                    <option value="2" <?php if($result['cheque_status']=='2'){echo "selected";} ?>>Bounce</option>
                                 </select>
                                 <?php }else{
                                    echo "<strong style='color: black;'>Clearing</strong>" ;
                                    } ?>
                              </td>
                              <td align="left">
                                 <?php if($result['cheque_status']!='1' && $_SESSION['utype']!="Report"){ 
                                    $change_amount = $result['amount']-$result['paid_amount'];  ?>
                                 <input class="btn btn-primary bulu" type="submit" name="submit" value="Submit" >
                                 <input type="hidden" name="control" value="farmer">
                                 <input type="hidden" name="task" value="change_chq_status">
                                 <input type="hidden" name="farmer_id" value="<?php echo $result['farmer_id']; ?>">
                                 <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
                                 <input type="hidden" name="balance_amt" value="<?php echo $change_amount; ?>">
                                 <?php } if($_SESSION['utype']!="Report"){ ?>
                                 <a href="index.php?control=farmer&task=add_payment&editid=<?php echo $result['id']; ?>"><strong>Edit</strong></a>
                                 <?php }else{ echo "<strong>---</strong>"; } ?>
                              </td>
                           </form>
                           <!--  <?php if($_SESSION['utype']!="Report"){ ?> 
                              <td  class="link_hide" align="left"><a href="index.php?control=farmer&task=add_payment&editid=<?php echo $result['id']; ?>"><strong>Edit</strong></a></td><?php } ?> -->
                        </tr>
                        <?php $total_paid += $result['paid_amount']; } } ?>  
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <td></td>
                           <td align="left"><strong>Total Paid :</strong></td>
                           <td align="left"><strong><?php echo $total_paid; ?></strong></td>
                           <td colspan="7"></td>
                           <td></td>
                           <!-- <td></td>
                              <td></td>
                              <td></td>  -->    
                        </tr>
                     </tfoot>
                     <?php } ?>                  
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
         <!-- /.box-body -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- =============================Table 2 Pending Cheque========================== -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <div class="box-header">
            <h3 class="box-title">Pending Cheques</h3>
         </div>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-3" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="left">S.No</div></th>
                           <th><div align="left">Receipt No.</div></th>
                           <!-- <th><div align="left">Generated Amount</div></th> -->
                           <th><div align="left">Cheque Amount</div></th>
                           <!-- <th><div align="left">Balance Amount</div></th> -->
                           <th><div align="left">Deposit Date</div></th>
                           <!-- <th><div align="left">Payment Type</div></th> -->
                           <th><div align="left">Bank Name</div></th>
                           <th><div align="left">Chq/Txn No.</div></th>  
                           <th><div align="left">Remark</div></th>
                           <th><div align="left">Cheque Status</div></th>
                           <th><div align="left">Action</div></th>
                         
                        </tr>
                     </thead>
                     <tbody>
                         <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           
                          
                             if($result['cheque_status']=='0'){
                           ?>
                       <tr>
                           <td align="left"><?php echo $countno; ?></td>
                           <td align="left"><?php echo $result['receipt_no'];?></td>
                           <!-- <td align="left"><?php echo $result['amount'];?></td> -->
                           <td align="left"><?php echo $result['paid_amount'];?></td>
                           <!-- <td align="left"><?php echo $result['remain_amount'];?></td> -->
                           <!-- <td align="left"><?php echo $result['amount']-$result['paid_amount'];?></td> -->
                           <td align="left"><?php echo $result['cheque_date']?$result['cheque_date']:$result['payment_date'];?></td>
                           <!-- <td align="left"><?php echo ucfirst($result['payment_type']);?></td> -->
                           <td align="left"><?php echo $result['cheque_no'];?></td>
                           <td align="left"><?php echo ucfirst($result['bank_name']);?></td>
                           <td align="left"><?php echo $result['remark']?$result['remark']:'N/A';?></td>
                           <form name="myform" method="post" id="cheque_status_form" onsubmit="result validate();">
                              <td align="left">
                                 <?php if($result['cheque_status']=='1'){ echo "<strong style='color: green;'>Cleared</strong>";  }elseif($_SESSION['utype']!="Report"){
                                    ?>
                                 <select id="cheque_status" name="cheque_status" class="form-control" required="">
                                    <option value="">Clearing</option>
                                    <option value="1" <?php if($result['cheque_status']=='1'){echo "selected";} ?>>Cleared</option>
                                    <option value="2" <?php if($result['cheque_status']=='2'){echo "selected";} ?>>Bounce</option>
                                 </select>
                                 <?php }else{
                                    echo "<strong style='color: black;'>Clearing</strong>" ;
                                    } ?>
                              </td>
                              <td align="left">
                                 <?php if($result['cheque_status']!='1' && $_SESSION['utype']!="Report"){ 
                                    $change_amount = $result['amount']-$result['paid_amount'];  ?>
                                 <input class="btn btn-primary bulu" type="submit" name="submit" value="Submit" >
                                 <input type="hidden" name="control" value="farmer">
                                 <input type="hidden" name="task" value="change_chq_status">
                                 <input type="hidden" name="farmer_id" value="<?php echo $result['farmer_id']; ?>">
                                 <input type="hidden" name="id" value="<?php echo $result['id']; ?>">
                                 <input type="hidden" name="balance_amt" value="<?php echo $change_amount; ?>">
                                 <?php } if($_SESSION['utype']!="Report"){ ?>
                                 <!-- <a href="index.php?control=farmer&task=add_payment&editid=<?php echo $result['id']; ?>"><strong>Edit</strong></a> -->
                                 <?php }else{ echo "<strong>---</strong>"; } ?>
                              </td>
                           </form>
                           

                        </tr>
                        <?php $total += $result['paid_amount']; } } ?>  
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <!-- <td></td> -->
                           <td align="left"><strong> Total </strong></td>
                           <td align="left"><strong><?php echo $total?$total:'0'; ?></strong></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           
                        </tr>
                     </tfoot>
                     <?php } ?>                  
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <!-- =============================Table 2 Bounced Cheque========================== -->
         <!-- ============================================================================= -->
         <!-- ============================================================================= -->
         <div class="box-header">
            <h3 class="box-title">Bounced Cheques</h3>
         </div>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-3" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="left">S.No</div></th>
                           <th><div align="left">Receipt No.</div></th>
                           <!-- <th><div align="left">Generated Amount</div></th> -->
                           <th><div align="left">Cheque Amount</div></th>
                           <!-- <th><div align="left">Balance Amount</div></th> -->
                           <th><div align="left">Deposit Date</div></th>
                           <!-- <th><div align="left">Payment Type</div></th> -->
                           <th><div align="left">Bank Name</div></th>
                           <th><div align="left">Chq/Txn No.</div></th>
                           <th><div align="left">Cheque Status</div></th>
                           <th><div align="left">Remark</div></th>
                        </tr>
                     </thead>
                     <tbody>
                         <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           
                          
                             if($result['cheque_status']=='2'){
                           ?>
                       <tr>
                           <td align="left"><?php echo $countno; ?></td>
                           <td align="left"><?php echo $result['receipt_no'];?></td>
                           <!-- <td align="left"><?php echo $result['amount'];?></td> -->
                           <td align="left"><?php echo $result['paid_amount'];?></td>
                           <!-- <td align="left"><?php echo $result['remain_amount'];?></td> -->
                           <!-- <td align="left"><?php echo $result['amount']-$result['paid_amount'];?></td> -->
                           <td align="left"><?php echo $result['cheque_date']?$result['cheque_date']:$result['payment_date'];?></td>
                           <!-- <td align="left"><?php echo ucfirst($result['payment_type']);?></td> -->
                           <td align="left"><?php echo $result['cheque_no'];?></td>
                           <td align="left"><?php echo ucfirst($result['bank_name']);?></td>
                           <td align="left"><strong style='color: red;'>Bounce</strong></td>
                           <td align="left"><?php echo $result['remark']?$result['remark']:'N/A';?></td>
                        </tr>
                        <?php $total += $result['paid_amount']; } } ?>  
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <!-- <td></td> -->
                           <td align="left"><strong> Total </strong></td>
                           <td align="left"><strong><?php echo $total?$total:'0'; ?></strong></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           
                        </tr>
                     </tfoot>
                     <?php } ?>                  
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(3500).slideUp(200, function() {
     $(this).alert('close');
   });
   
   
   
   
</script>

