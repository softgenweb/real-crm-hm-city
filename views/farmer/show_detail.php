<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Show Farmer Detail</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=farmer&task=show"><i class="fa fa-list" aria-hidden="true"></i> Farmer List</a></li>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Show Farmer Detail</li>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-2">
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Title:
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <select name="title" id="title" class="form-control" autocomplete="off" disabled="">
                        <option value="">Select</option>
                        <option value="Mr" <?php if($result['title']=='Mr') {?> selected="selected" <?php } ?>>Mr</option>
                        <option value="Ms" <?php if($result['title']=='Ms') {?> selected="selected" <?php } ?>>Ms</option>
                        <option value="Mrs" <?php if($result['title']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option>
                     </select>
                     <span class="msgValid" id="msgtitle"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Name:
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control" readonly>
                     <span class="msgValid" id="msgname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Mobile: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['mobile']; ?>" pattern="[6789][0-9]{9}" maxlength="10" id="mobile" name="mobile" class="form-control" readonly>
                     <span class="msgValid" id="msgmobile"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Email-Id: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['email_id']; ?>" id="email_id" name="email_id" class="form-control" readonly>
                     <span class="msgValid" id="msgemail_id"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Date of Birth: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['dob']; ?>" id="dob" name="dob"  class="form-control" readonly disabled>
                     <span class="msgValid" id="msgdob"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Area: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group" style="display: inline-flex;">                              
                     <input style="width: 70%" type="text"  value="<?php echo $result['hectare']; ?>" id="hectare" name="hectare" class="form-control" readonly><code style="width: 30%">Hectare</code> 
                  </div>
                  <div class="form-group"  style="display: inline-flex;">       
                     <input style="width: 70%" type="text"  value="<?php echo $result['acre']; ?>" id="acre" name="acre" class="form-control" readonly><code style="width: 30%">Acre</code>
                  </div>
                  <div class="form-group"  style="display: inline-flex;">                               
                     <input style="width: 70%" type="text"  value="<?php echo $result['sqft']; ?>" id="sqft" name="sqft" class="form-control" readonly><code style="width: 30%">SQFT.</code>
                     <span class="msgValid" id="msgdob"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Gata/Khasra No.: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['gata_no']; ?>" id="gata_no" name="gata_no"  class="form-control" readonly>
                     <span class="msgValid" id="msggata_no"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Country Name:
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <select name="country_id" id="country_id" class="form-control" onchange='state_change(this.value);' disabled="disabled">
                        <option value="">Select Country</option>
                        <?php $sel=mysql_query("select * from country where 1");
                           while($select=mysql_fetch_array($sel))
                           {
                           if($select['status'] == '1') { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
                        <?php } else { ?>  
                        <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
                        <?php } } ?>
                     </select>
                     <span id="msgcountry_id" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     State Name:
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <select name="state_id" id="state_id" class="form-control" onchange='city_change(this.value);' disabled="disabled">
                        <option value="">Select State</option>
                        <?php $sel=mysql_query("select * from state where 1");
                           while($select=mysql_fetch_array($sel))
                           if($result)  {
                           if($select['status'] == '1') { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
                        <?php } else { ?>  
                        <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
                        <?php } } ?>
                     </select>
                     <span id="msgstate_id" style="color:red;"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     City Name:
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <select name="city_id" id="city_id" class="form-control" disabled="">
                        <option value="">Select City</option>
                        <?php $sel=mysql_query("select * from city where 1");
                           while($select=mysql_fetch_array($sel))
                           if($result)  {
                           if($select['status'] == '1') { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['city_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['city_name'];?></option>
                        <?php } else { ?>  
                        <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['city_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['city_name'];?></option>
                        <?php } } ?>
                     </select>
                     <span id="msgcity_id" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Address:
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <textarea id="address" name="address" class="form-control" readonly><?php echo $result['address']; ?></textarea> 
                     <span class="msgValid" id="msgaddress"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Pincode: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['pincode']; ?>" id="pincode" name="pincode" class="form-control" readonly>
                     <span class="msgValid" id="msgpincode"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Amount: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['amount']; ?>" id="amount" name="amount" class="form-control" readonly>
                     <span class="msgValid" id="msgpincode"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Dealer: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <!-- <input type="text"  value="<?php echo $result['amount']; ?>" id="amount" name="amount" class="form-control" > -->
                     <select name="dealer" id="dealer" class="form-control" disabled="">
                        <option value="">Select</option>
                        <?php $sql= mysql_query("SELECT * FROM `dealer` WHERE `status`= 1");
                           while($row = mysql_fetch_array($sql)){  ?>
                        <option value="<?php echo $row['id']; ?>" <?php echo $result['dealer']==$row['id']?'selected':''; ?>><?php echo $row['name']; ?></option>
                        <?php } ?>
                     </select>
                     <span class="msgValid" id="msgpincode"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div id="dealer_comm" >
                  <div class="col-md-2">
                     <div class="form-group center_text">
                        Commission (%): 
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">                              
                        <input type="text"  value="<?php echo $result['comm_per']; ?>" id="comm_per" name="comm_per" class="form-control" readonly>
                        <span class="msgValid" id="msgpincode"></span>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group center_text">
                        Commission Amount: 
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">                              
                        <input type="text"  value="<?php echo $result['comm_amount']; ?>" id="comm_amount" name="comm_amount" class="form-control" readonly>
                        <span class="msgValid" id="msgpincode"></span>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     ID Proof: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">  
                     <img src="<?php if($result['id_proof']) { ?>media/farmer/<?php echo $result['id_proof'];  } else { echo "images/profile.jpg";}?>" width="80">
                     <span class="msgValid" id="msgid_proof"></span>
                  </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group center_text">
                     Photo: 
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">  
                     <img src="<?php if($result['photo']) { ?>media/farmer/<?php echo $result['photo'];  } else { echo "images/profile.jpg";}?>" width="80">
                     <span class="msgValid" id="msgphoto"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-12 col-sm-8 col-xs-12">
                  <?php if($_SESSION['utype']!="Report"){ ?>     
                  <center><a href="index.php?control=farmer&task=addnew&id=<?php echo $result['id']; ?>" class="btn btn-primary butoon_brow" >Edit</a> </center>
                  <?php } ?>
               </div>
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   $('#dob').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d/m/Y',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   
   
   /*=============Gsingh Jquery==============*/
   function id_proof(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#id_proof_img').attr('src', e.target.result);
               $('#id_proof_a').attr('href', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           }
       }
   $("#id_proof").change(function(){
       id_proof(this);
   });
   
   
   function photo(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#photo_img').attr('src', e.target.result);
               $('#photo_a').attr('href', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           }
       }
   $("#photo").change(function(){
       photo(this);
   });
   function isNumber(evt) {
       evt = (evt) ? evt : window.event;
           var charCode = (evt.which) ? evt.which : event.keyCode;
       if (charCode != 46 && charCode > 31
       && (charCode < 48 || charCode > 57))
           return false;
   
       return true;
   }
   
   $('#hectare').keyup(function(){
       var getacre = parseFloat(2.47107*$(this).val()).toFixed(2);
       $('#acre').val(getacre);
       var getsqft = parseFloat(107639*$(this).val()).toFixed(2);
       $('#sqft').val(getsqft);
   });
   $('#acre').keyup(function(){
      var gethectare = parseFloat(0.404686*$(this).val()).toFixed(2);
      $('#hectare').val(gethectare);
      var getsqft = parseFloat(43560*$(this).val()).toFixed(2);
      $('#sqft').val(getsqft);
   });
   $('#sqft').keyup(function(){
       var gethectare = parseFloat($(this).val()/107639).toFixed(2);
       $('#hectare').val(gethectare);
       var getacre = parseFloat($(this).val()/43560).toFixed(2);
       $('#acre').val(getacre);
   });
   
   
   /*==========Commission==============*/
   $("#dealer").change(function(){
   
       var str = $(this).val();
       $.ajax({url: "script/plot_booking/dealer_commission.php?id="+str, success: function(result){
           $("#comm_per").val(result);
           $('#dealer_comm').show();
           get_commission();
       }});
   
   });
   
   
   function get_commission(){
   
       var amount = $('#amount').val();
       var percent = $('#comm_per').val();
       var getcomm =  parseInt((amount*percent)/100);
   
       $('#comm_amount').val(getcomm);
   }
   $('#comm_per').keyup(function(){
       get_commission();
   });
   
    /*============Auto hide alert box================*/
    $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
    });
</script>

