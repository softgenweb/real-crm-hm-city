<?php foreach($results as $result) { } 

$edit =mysql_fetch_array(mysql_query("SELECT * FROM `farmer_payment` WHERE `status`= 1  AND `id` = '".$_REQUEST['editid']."' ORDER BY `id` DESC LIMIT 1"));

$farmer = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".($_REQUEST['id']?$_REQUEST['id']:$edit['farmer_id'])."' AND `status`= 1"));

$fid = $_REQUEST['id']?$_REQUEST['id']:$edit['farmer_id'];
 ?>

	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Add Payment</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=farmer&task=show"><i class="fa fa-list" aria-hidden="true"></i> Farmer List</a></li>
             <li><a href="index.php?control=farmer&task=farmer_ledger&id=<?php echo $fid; ?>"><i class="fa fa-list" aria-hidden="true"></i> Payment List</a></li>
            <?php// if($result!='') {?>           
             <!-- <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Payment</li> -->
         <?php// } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Payment</li>
          <?php// } ?>
          </ol>

          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
            
			              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">

					
						<div class="col-md-3">
                           <div class="form-group center_text">
								Name:
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">
								<input type="text" value="<?php echo $farmer['name']; ?>" id="name" name="name" class="form-control" readonly="">
								<input type="hidden" name="farmer_id" value="<?php echo $farmer['id']; ?>">
								<span class="msgValid" id="msgname"></span>
							</div>
						</div>
						 <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Mobile: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text" maxlength="10" pattern="[6789][0-9]{9}" value="<?php echo $farmer['mobile']; ?>" id="mobile" name="mobile" class="form-control"  readonly="">
								<span class="msgValid" id="msgmobile"></span>
							</div>
						</div>
                         <div class="clearfix"></div>
						<div class="col-md-3">
                           	<div class="form-group center_text">
								 Total Balance: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text"  value="<?php echo $result['remain_amount']!=''?$result['remain_amount']:$farmer['amount']; ?>" id="total_amount" name="total_amount" class="form-control"  readonly="">
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>
                         <div class="clearfix"></div>
						<div class="col-md-3">
                           	<div class="form-group center_text">
								Paid Amount: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text" onkeypress="return isNumber(event)"  value="<?php echo $edit['paid_amount']; ?>" id="paid_amount" name="paid_amount" class="form-control"  required="" <?php echo $_REQUEST['editid']?"readonly":""; ?>>
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>
                         <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Cash/ Cheque/ PDC Date: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text"  value="<?php echo $edit['payment_date']; ?>" id="pay_date" placeholder="Select Date" name="payment_date" class="form-control" readonly="">
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>
                         <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Payment Type: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
							<select name="payment_type" id="payment_type" class="pay_type form-control" required="">
                            <option value="">Select</option>
                            <option value="Cash" <?php echo $edit['payment_type']=='Cash'?'selected':''; ?>>Cash</option>
                            <option value="Cheque" <?php echo $edit['payment_type']=='Cheque'?'selected':''; ?>>Cheque</option>
                            <option value="NEFT/RTGS" <?php echo $edit['payment_type']=='NEFT/RTGS'?'selected':''; ?>>NEFT/RTGS</option>
                            </select> 
							</div>
						</div>

                         <div class="clearfix"></div>
                         <div id="chk_detail">
						<div class="col-md-3">
                           <div class="form-group center_text">
								Cheque No.: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text"  value="<?php echo $edit['cheque_no']; ?>" id="cheque_no" name="cheque_no" class="form-control">
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>

                        <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Bank Name: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<input type="text"  value="<?php echo $edit['bank_name']; ?>" id="bank_name" name="bank_name" class="form-control">
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>
					</div>
                         <div class="clearfix"></div>
						<div class="col-md-3">
                           <div class="form-group center_text">
								Remark: 
							</div>	
                            </div>
							<div class="col-md-6"><div class="form-group">								
								<!-- <input type="text"  value="<?php echo $result['remark']; ?>" id="remark" name="remark" class="form-control"> -->
								<textarea id="remark" name="remark" class="form-control"><?php echo $edit['remark']; ?></textarea>
								<span class="msgValid" id="msgcommision"></span>
							</div>
						</div>
					     
					      <div class="clearfix"></div>
					<div class="col-md-9 col-sm-8 col-xs-12">
					<?php if($_REQUEST['id']){ ?>    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Submit"></center>
					</div>                   

					<input type="hidden" name="control" value="farmer"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save_payment"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

					<?php }elseif ($_REQUEST['editid']) { ?>
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="Update"></center>
					</div>                   

					<input type="hidden" name="control" value="farmer"/>
					<!-- <input type="hidden" name="edit" value="1"/> -->
					<input type="hidden" name="task" value="edit_payment_detail"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $edit['id']; ?>"  />		
				<?php } ?>

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

$('#payment_type').change(function(){
	var pay_type = $(this).val();
/*	if(pay_type=='Cheque'){
		$('#chk_detail').show();
		$('#cheque_no').prop('required',true);

	}else{
		$('#chk_detail').hide();
		$('#cheque_no').prop('required',false);
		$('#chk_detail input[type="text"]').val('');
	}*/
	if(pay_type=='Cash'){
		$('#chk_detail').hide();
		$('#cheque_no').prop('required',false);
		$('#chk_detail input[type="text"]').val('');
	
	}else{
		$('#chk_detail').show();
		$('#cheque_no').prop('required',true);

	}
});
</script>
        
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#pay_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y/m/d',
    // formatDate: new Date(),
    // maxDate:'+1970/01/01',
});   
</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(3000).slideUp(200, function() {
		$(this).alert('close');
	});

	function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
   