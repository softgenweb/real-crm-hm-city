<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add Plot</h3>
	</div>
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=project&task=show_area_measurement"><i class="fa fa-list" aria-hidden="true"></i>  Area Statement List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Area Statement</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Area Statement</li>
          <?php } ?>
          </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
                    
                    
                    <div class="col-md-3">
                    <div class="form-group" style="margin-top:5%;">
                    Dimension (ft):
                    </div></div>
                    <?php $dimenstion =  explode('X', $result['dimenstion']); ?>
                    <div class="col-md-3"><div class="form-group">
                      <input type="text" class="form-control length" name="length" id="length" placeholder="Plot Length" value="<?php echo $dimenstion[0]; ?>" required=""/>
                      <span id="msglength" style="color:red;"></span>
                    </div></div> 
                    <div class="col-md-3"><div class="form-group">
                      <input type="text" class="form-control length" name="width" id="width" placeholder="Plot Width" value="<?php echo $dimenstion[1]; ?>" required=""/>
                      <span id="msgwidth" style="color:red;"></span>
                    </div></div>
               
                    <div class="clearfix"></div>                    
                    <div class="col-md-3">
                    <div class="form-group" style="margin-top:5%;">
                    Area (in sqft):
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                      <input type="text" pattern="[0-9\.]*" class="form-control" name="square_feet" id="square_feet" placeholder="Enter Plot" value="<?php echo $result['square_feet']; ?>" readonly="readonly" required=""/>
                      <span id="msgsquare_feet" style="color:red;"></span>
                    </div></div>
               
                    <div class="clearfix"></div>                    
                   	<div class="col-md-3">
                    <div class="form-group" style="margin-top:5%;">
                    Area (in sqmt):
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                      <input type="text" pattern="[0-9\.]*" class="form-control" name="square_meter" id="square_meter" placeholder="Enter Plot" value="<?php echo $result['square_meter']; ?>" readonly="readonly"/>
                      <span id="msgsquare_meter" style="color:red;"></span>
                    </div></div>
               
                    <div class="clearfix"></div>
       
					    
					<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>
					                   

					<input type="hidden" name="control" value="project"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save_area_measurement"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('length').value == '') { 
			document.getElementById('msglength').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msglocation_name').innerHTML = "";
		}
		if(document.getElementById('width').value == '') { 
			document.getElementById('msgwidth').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgwidth').innerHTML = "";
		}
		
		if(document.getElementById('square_feet').value == '') { 
			document.getElementById('msgsquare_feet').innerHTML = "*Required field.";
			chk=0;
		}
		else {
			document.getElementById('msgsquare_feet').innerHTML = "";
		}
		
		if(document.getElementById('square_meter').value == '') { 
			document.getElementById('msgsquare_meter').innerHTML = "*Required field.";
			chk=0;
		}
		else {
			document.getElementById('msgsquare_meter').innerHTML = "";
		}
		
		
		

			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

        /*==========dimenstion calculation=============*/
        // $(document).keyup(function(){


            // var in_meter = ;
            // var in_feet = ;

    $(".length").keyup(function(){
        var length = $('#length').val();
        var width = $('#width').val();      
        var area_sqft = parseFloat(length)*parseFloat(width);
       if(isNaN(area_sqft) == true){
        document.getElementById('square_feet').value = '';
        // document.getElementById('square_feet').value = (parseFloat(area_sqft)*0.09290333).toFixed(2);
        document.getElementById('square_meter').value = '';
            }else{
        document.getElementById('square_feet').value = area_sqft.toFixed(2);
        document.getElementById('square_meter').value = (parseFloat(area_sqft)*0.092903).toFixed(2);
            }
    });
    $('#square_feet').keyup(function(){

        var area_sqft = $(this).val();
        document.getElementById('square_meter').value = (parseFloat(area_sqft)*0.092903).toFixed(2);

    });  

    $('#square_meter').keyup(function(){

        var area_mt = $(this).val();
        document.getElementById('square_feet').value = (parseFloat(area_mt)*10.7639).toFixed(2);          

    });



</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   