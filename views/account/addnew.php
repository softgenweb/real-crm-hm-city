<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add Account</h3>
	</div>
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=account&task=show"><i class="fa fa-list" aria-hidden="true"></i> Account List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Account</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Account</li>
          <?php } ?>
          </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
                    
                    <div class="col-md-5">
                    <div class="form-group center_text">
                    Account Holder Name :
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    
                    <input class="form-control" type="text" name="acc_holder_name" id="acc_holder_name" value="<?php echo $result['acc_holder_name']; ?>" placeholder="Enter Account Holder Name">
                    <span id="msgacc_holder_name" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-5">
                    <div class="form-group center_text">
                    Account Number :
                    </div></div>
                    <div class="col-md-6"><div class="form-group">                    
                    <input class="form-control" type="text" name="account_no" id="account_no" value="<?php echo $result['account_no']; ?>" placeholder="Enter Account Number">
                    <span id="msgaccount_no" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-5">
                    <div class="form-group center_text">
                    IFSC Code :
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    <input class="form-control" type="text" name="ifsc_code" id="ifsc_code" value="<?php echo $result['ifsc_code']; ?>" placeholder="Enter IFSC Code ">
                    <span id="msgifsc_code" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                     
                    <div class="col-md-5">
                    <div class="form-group center_text">
                    Bank Name :
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    <input class="form-control" type="text" name="bank_name" id="bank_name" value="<?php echo $result['bank_name']; ?>" placeholder="Enter Bank Name">
                    <span id="msgbank_name" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                     
                    <div class="col-md-5">
                    <div class="form-group center_text">
                    Branch Name :
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    <input class="form-control" type="text" name="branch_name" id="branch_name" value="<?php echo $result['branch_name']; ?>" placeholder="Enter Branch Name ">
                    <span id="msgbranch_name" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                    
				<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>                  

					<input type="hidden" name="control" value="account"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('country_id').value == '') { 
			document.getElementById('msgcountry_id').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgcountry_id').innerHTML = "";
		}
		if(document.getElementById('state_id').value == '') { 
			document.getElementById('msgstate_id').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgstate_id').innerHTML = "";
		}
		
		if(document.getElementById('city_name').value == '') { 
			document.getElementById('msgcity_name').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!isletter(document.getElementById('city_name').value)) { 
			document.getElementById('msgcity_name').innerHTML = "*Enter Valid City Name.";
			chk=0;
		}
		else {
			document.getElementById('msgcity_name').innerHTML = "";
		}
		
		/*if(document.getElementById('mobile').value == '') { 
			document.getElementById('msgmobile').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!numericWithoutPoint(document.getElementById('mobile').value)) { 
			document.getElementById('msgmobile').innerHTML = "*Enter Valid Number.";
			chk=0;
		}
		else {
			document.getElementById('msgmobile').innerHTML = "";
		}
		
		
		if(document.getElementById('address').value == '') { 
			document.getElementById('msgaddress').innerHTML = "*Required field.";
		chk=0;
		}
		
		else {
			document.getElementById('msgaddress').innerHTML = "";
		}*/


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   







