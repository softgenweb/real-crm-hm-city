    <div class="col-md-12" style="margin-top:4%;">
    	<br>
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<br>
<li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
<li class="active"><a href="index.php?control=bill_account">Account Master</a></li>
<li class="active"><a href="index.php?control=bill_account&task=btob_transfer">Bank To Bank Transfer</a></li>
<li class="active">New B TO B Transfer</li>
</ol>
</div>
</div>
<!--/.row-->
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                    <u><h3>Bank To Bank Transfer</h3></u>
                    </div>
                    <br>
                    
                <form name="account" action="" method="post" enctype="multipart/form-data">
             
                
<div class="col-md-6 col-md-offset-3">


		<div class="col-md-4"><div class="form-group" align="left">
		From Bank :
		</div></div>
		<div class="col-md-8"><div class="form-group">
		<select class="form-control" name="from_bank" id="from_bank" required >
        <option value="">Select</option>              
           <?php foreach($results as $result) {  ?>           
           <option value="<?php echo $result['id'];?>"><?php echo $result['bank_name'];?></option>           
           <?php } ?>
        </select>
		</div></div>

    
		<div class="col-md-4"><div class="form-group" align="left">
		To Bank :
		</div></div>
		<div class="col-md-8"><div class="form-group" align="left">
		<select class="form-control" name="to_bank" id="to_bank"  required>
        <option value="">Select</option>              
           <?php foreach($results as $result) {  ?>           
           <option value="<?php echo $result['id'];?>"><?php echo $result['bank_name'];?></option>           
           <?php } ?>
        </select>
		</div></div>
        
        
		<div class="col-md-4"><div class="form-group" align="left">
		Transaction Type :
		</div></div>
		<div class="col-md-8"><div class="form-group" align="left">
		<select class="form-control" name="bank_type" id="bank_type"  required>
        <option value="">Select</option>              
            <option value="Cheque">Cheque</option>          
            <option value="NEFT">NEFT</option>              
            <option value="RTGS">RTGS</option>       
          </select>
		</div></div>

		<div class="col-md-4"><div class="form-group" align="left" >
		Amount  :
		</div></div>
		<div class="col-md-8"><div class="form-group">
		<input class="form-control" type="number" name="amount" id="amount" required>
		</div></div>


		<div class="col-md-4"><div class="form-group" align="left" >
		Remark :
		</div></div>
		<div class="col-md-8"><div class="form-group">
		<input class="form-control" type="text" name="remark" id="remark" required>
		</div></div>

  
		<div class="col-md-12" align="left">
		<br>

		
		<button type="submit" class="btn btn-primary" name="submit" id="submit" onclick="disableEnable();">Submit</button>
		
		 <input type="button" class="btn btn-default" value="Reset" id="btnReset" onclick="disableEnableRefresh();" >
        
        </div> 
        
        </div>
                                
		<input type="hidden" name="control" value="bill_account"/>
		<input type="hidden" name="edit" value="1"/>
		<input type="hidden" name="task" value="save_btob"/>
		<input type="hidden" name="id" id="idd" value=""  />

		<input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
		<input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                 
		</form>

		</div>
		</div>			
		</div>
    
    

