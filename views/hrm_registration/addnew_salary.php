<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Employee Salary</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=hrm_registration&task=show_salary"><i class="fa fa-list" aria-hidden="true"></i> Employee Salary List</a></li>
      <?php if($result!='') {?>           
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Salary</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Salary</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php     unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select class="form-control" name="user_id" id="user_id" required="">
                        <option value="">Select</option>
                        <?php $user = mysql_query("select * from users where status = '1' and utype='Employee'");
                           while($user_detail = mysql_fetch_array($user)){ ?>
                        <option value="<?php echo $user_detail['id'];?>" <?php if($user_detail['id']==$result['user_id']) { ?> selected="selected" <?php } ?>><?php echo $user_detail['name'];?></option>
                        <?php } ?>
                     </select>
                     <span class="msgValid" id="msgname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Salary:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['salary']; ?>" id="salary" name="salary" class="form-control" placeholder="Enter Monthly Salary here.." onkeypress="return isNumber(event)" required="">         
                     <span class="msgValid" id="msgname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Salary date:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['salary_date_time']; ?>" id="salary_date_time" name="salary_date_time" class="form-control" placeholder="Select Salary Date.." required="">                
                     <span class="msgValid" id="msgname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     Remark:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['remark']; ?>" id="remark" name="remark" class="form-control" placeholder="Enter Remark here.. ">                
                     <span class="msgValid" id="msgname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="hrm_registration"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save_salary"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   $('#salary_date_time').datetimepicker({
       yearOffset:0,
       lang:'ch',
       timepicker:false,
       format:'Y-m-d',
       formatDate:'Y/m/d',
       //minDate:'-1970/01/02', // yesterday is minimum date
       //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   
</script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });

    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

