<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:5;?>

    <div class="col-sm-12">			
		<div class="row">
			<ol class="breadcrumb">
            <br>
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">State Information</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
			  <h1></h1>
			</div>
		</div><!--/.row-->
				
            <div class="row" >
    <div class="col-md-12">
    <div class="panel panel-default">
		<div class="panel-body">
			<div class="col-md-3" style="top: -15px;">
			   <div class="panel-heading">
					<u>
					   <h3>State's List</h3>
					</u> 
			   </div>
			</div>
			<div class="col-md-9" style="margin-top:1%">
				<div class="col-md-9">
					<?php foreach($results as $result) { }  ?>
						<span class="col-md-6" style="top: 8px; color:<?php if($result[0]['account_id']!=''){ echo 'green';} else { echo 'red';}?>">
						   <?php  
								if($_REQUEST['search']!=''){
									if($result[0]['account_id']!=''){ 
										echo "Welcome! " .$result['name'];
									}
									else{ 
										echo "Sorry! This Account ID Does Not Exists.";
									}
								}				
							?>
						</span>
					<form class="col-md-6" name="customer_search" action="" method="post">
                    <div class="input-group">
						<!--<input type="text" id="btn-input" class="form-control input-md" name="search" value="<?php echo $_REQUEST['search']?$_REQUEST['search']:$_REQUEST['account_id'];?>" placeholder="Type your search here..." />
						<span class="input-group-btn">
							<button class="btn btn-primary btn-md" id="btn-chat">Search</button>
						</span>--->
						<input type="hidden" name="control" value="regional_hierarchy"/>
						<input type="hidden" name="view" value="show_state"/>
						<input type="hidden" name="task" value="show_state"/>
                        </div>
					</form>
				</div>
			
		
			<div class="col-md-2">				
				<a href="./mr_reporting/index.php?control=district&task=addnew" class="btn btn-primary btn-md" id="btn-chat">Add New District</a>
			</div>
			
                <div class="col-md-1">
                    <form action="./mr_reporting/index.php" method="post" name="filterForm" id="filterForm" style="width:77px;">
                        <!--<span style="font-size:12px;">Show entries :</span>-->
                        <select name="filter"  class="form-control"  id="filter" onchange="paging1(this.value)">
                            <option value="1000"<?php if($_REQUEST['tpages']=="1000") {?> selected="selected"<?php }?>>All</option>
                            <option value="2"<?php if($_REQUEST['tpages']=="2") {?> selected="selected"<?php }?> >2</option>
                            <option value="5"<?php if($_REQUEST['tpages']=="5") {?> selected="selected"<?php }?> >5</option>
                            <option value="10"<?php if($_REQUEST['tpages']=="10") {?> selected="selected"<?php }?>>10</option>
                            <option value="20"<?php if($_REQUEST['tpages']=="20") {?> selected="selected"<?php }?>>20</option>
                            <option value="50"<?php if($_REQUEST['tpages']=="50") {?> selected="selected"<?php }?>>50</option>
                            <option value="100"<?php if($_REQUEST['tpages']=="100") {?> selected="selected"<?php }?>>100</option>
                        </select>
                        <input type="hidden" name="control" value="<?php echo $_REQUEST['control'];?>"  />
                        <input type="hidden" name="view" value="<?php echo $_REQUEST['view'];?>"  />
                        <input type="hidden" name="task" id="task" value="<?php echo $_REQUEST['task'];?>"  />
                        <input type="hidden" name="tpages" id="tpages" value=""  />
                        <input type="hidden" name="adjacents" value="<?php echo $_REQUEST['adjacents'];?>"  />
                        <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                        <input type="hidden" name="tmpid" value="<?php echo $_REQUEST['tmpid'];?>"  />
                        <input type="hidden" name="id" id="id"  />
                        <input type="hidden" name="status" id="status"  />
                        <input type="hidden" name="defftask" id="defftask"  />
                        <input type="hidden" name="del" id="del"  />
                        <input type="hidden" name="edit" id="edit"  />
                    </form>        
                </div>
            </div>
            
            
			<div class="container">
				<div class="col-md-12"></div>
			</div>
			<br>
			<table style="width:100%" data-toggle="table" id="table-style"  data-row-style="rowStyle">
				<thead>
					<tr>
						  <th><div align="left">S.No.</div></th>
						 
						  <th><div align="left">State's Name</div></th>
						  
						  <th width="9%"><div align="left">Action</div></th>
					  </tr>
				</thead>
				<?php
					if($results) {
						$countno = ($page-1)*$tpages;
						$i=0;
						foreach($results as $result){ 
						$i++;
						$countno++;
						($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad"
					?>
	
						<tr>
							<td class="<?php echo $class; ?>"><div align="left"><?php echo $countno; ?></div></td>
							
							<td class="<?php echo $class; ?>"><?php echo $result['state_name'] ; ?></td>
							
							
							<td class="<?php echo $class; ?>"><div align="left"><!--<button class="panel-blue"id="btn-todo">Full Details</button>-->
								<a onclick="customer_edit(<?php echo $result['id']; ?>)" style="cursor:pointer;" title="Edit"><img src="images/edit.png" alt="edit" title="Edit" /></a>
								<?php if($result['status']){  ?>
								<a onclick="customer_status(<?php echo $result['id']; ?>,0)" style="cursor:pointer;" title="Click to Unpublish"><img src="images/backend/check.png" alt="check_de" /></a>
								<?php } else { ?>
								<a  onclick="customer_status(<?php echo $result['id']; ?>,1)" style="cursor:pointer;" title="Click to Publish"><img src="images/backend/check_de.png" alt="check_de" /></a>
								<?php } ?>
								<a onclick="customer_delete(<?php echo $result['id']; ?>)" title="Delete"  ><img src="images/backend/del.png" alt="Delete" title="Delete" /></a>
								</div>
							</td>
						</tr>
					<?php }  
					} else{ ?>
				<div>
			   <!-- <strong>Data not found.</strong>-->
				</div>
			   <!-- <tbody><tr class="no-records-found"><td colspan="11">No matching records found</td></tr></tbody>-->
				<?php } ?>
			
			</table>
		</div>
				<?php if(count($results)>0){ ?>
                <?php
                include("pagination.php");
                echo paginate_three($reload, $page, $tpages, $adjacents,$tdata); ?>
                <?php } ?>
			</div>
            </div>
            </div>
            </div>
			
			
			
<div id="myModal" class="reveal-modal">
    <div id="scheme_popup"></div>
	<a class="close-reveal-modal"><!--<img src="assets/popup/images/close.png" width="23" height="23" />--></a> 
</div>



<div id="myModalPayment" class="reveal-modal">
	<div id="customer_profile_popup"></div>
	<a class="close-reveal-modal"><!--<img src="assets/popup/images/close.png" width="23" height="23" />--></a>
</div>
              
              
<script type="text/javascript">
	function customer_edit(id)
	{
		//alert(id);
		document.getElementById("id").value = Number(id);

		document.getElementById("tpages").value = Number(document.getElementById("filter").value);

		document.getElementById("task").value = "addnew";

		document.getElementById("id").value = id;

		document.getElementById("status").value = status;

		document.filterForm.submit(); 

		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	
	function customer_status(id,status)
	{ 
		document.getElementById("id").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "status";
		document.getElementById("id").value = id;
		document.getElementById("status").value = status;
		document.filterForm.submit(); 
	}
	
	function customer_delete(id)
	{
		document.getElementById("del").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "delete";
		document.getElementById("id").value = id;
	
		if(confirm('Are you sure you want to delete ?')) {
			document.filterForm.submit(); 
		}
		else
		{
		//alert("No");
		}
	}
	
	function addnew_customer(id)
	{
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "addnew_customer";
		document.getElementById("id").value = id;
		//alert(id);
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	
	function add_test(id)
	{
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "add_test";
		document.getElementById("id").value = id;
		//alert(id);
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	  
	  
	  
function scheme_details(str) {
	{ 
	var xmlhttp; 
	if (window.XMLHttpRequest)
	  { // code for IE7+, Firefox, Chrome, Opera, Safari 
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  { // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  { 
	  if (xmlhttp.readyState==4)
		{
		document.getElementById("scheme_popup").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
	  }
	xmlhttp.open("GET", "./script/popup_scripts/scheme_details.php?acc_id="+str, true);
	//xmlhttp.open("GET","script/ajax.php?control=customer&task=scheme_details&acc_id="+str,true);
	xmlhttp.send();
	}
}

function customer_details(str) {
	{ 
	var xmlhttp; 
	if (window.XMLHttpRequest)
	  { // code for IE7+, Firefox, Chrome, Opera, Safari 
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  { // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  { 
	  if (xmlhttp.readyState==4)
		{
		document.getElementById("customer_profile_popup").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
	  }
	xmlhttp.open("GET", "./script/popup_scripts/customer_details.php?acc_id="+str, true);
	//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
	xmlhttp.send();
	}
}
</script> 
