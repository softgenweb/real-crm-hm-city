<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:5;?>



    <div class="col-sm-12">			
		
		
		<div class="row">
			<div class="col-lg-12">
			  <div class="row">
			<div class="col-lg-12">
			   <ol class="breadcrumb">
         
				<li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Employee Appointment Letter</li>
                
               
			</ol>
			</div>
		</div>
			</div>
		</div><!--/.row-->
				
            <div class="row" >
    <div class="col-md-12">
    <div class="panel panel-default">
		<div class="panel-body">
           <div class="col-md-10 col-md-offset-1">
		<?php foreach($results as $result) { }  ?>
			<div class="col-md-12" style="top: -15px;">
			   <div class="panel-heading">
					   <h3 style="font-weight:bold; color:#333;" align="left">OFFER LETTER</h3>
			   </div>
			</div>
			
            
            
			<div class="container">
				<div class="col-md-12"></div>
			</div>
			
			<!--<table style="width:100%" data-toggle="table" id="table-style"  data-row-style="rowStyle">
			
		<?php
		if($results) {
			$countno = ($page-1)*$tpages;
			$i=0;
			foreach($results as $result){ 
			$i++;
			$countno++;

			
 			$sel=mysql_fetch_array(mysql_query("select * from post where id ='".$result['post_name']."'"));
			$sel1=mysql_fetch_array(mysql_query("select * from state where id ='".$result['state_name']."'"));
			$sel2=mysql_fetch_array(mysql_query("select * from country where country_id ='".$result['zone_name']."'"));
			$sel3=mysql_fetch_array(mysql_query("select * from city where id ='".$result['city_name']."'"));
			$sel4=mysql_fetch_array(mysql_query("select * from hrm_registration as mrg join post as po where mrg.post_name=po.id and  mrg.id ='".$result['reporting']."'"));

			($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad"
		?>

			<tr>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $countno; ?></div></td>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $result['username'] ; ?></div></td>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $result['name'];?></div></td>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $sel['post_name'] ; ?></div></td>
                
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $result['email'] ; ?></div></td>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $result['mobile'] ; ?></div></td>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $sel4['name'].'--'.$sel4['post_name'] ; ?></div></td>
                <td class="<?php echo $class; ?>"><div align="left"><?php echo $sel['area'] ; ?></div></td>
				 <td class="<?php echo $class; ?>">
                 
                 </td>	
				
			</tr>
					<?php } } ?>
				
			
			</table>
-->            <div>
            <p>
             <p style="padding-bottom:10px;"><strong><u>REF: HR/SKMCF/16/0001</u></strong></p>
              <ul class="applicant_address_list">
              <li>To,</li>
              <li>&nbsp;</li>
              <li><strong>MS MAMTA MISHRA</strong></li>
              <li>E-3/122 SECTOR –H ALIGANJ</li>
              <li>LUCKNOW-226024</li>
            </ul>
            <br />
            <p>Dear <strong><?php echo $result['name'];?>,</strong></p>
           
 
<p>With reference to your application and subsequent interview; the management is pleased to make an offer to appoint you as HR HEAD at LUCKNOW on the following terms and conditions in the pay scale of 16000/Month The detailed offer is enclosed (Annexure-I).</p>
<ul>
<li>You will be on probation for a period of Six Months from the date of your joining. It can be extended further for a maximum period of six month at the sole discretion of the management if work and performance is not found satisfactory. Unless confirmed in writing you shall be deemed to be on probation even after the expiry of aforesaid initial period. During Probation period your services can be dispensed with at any time on either side by giving 07 days notice or salary in lieu of notice.</li>
<li>During your probation period you will be entitled for one casual leave per month and 08 days medical leave with prior approval (on completion of 03 months of service).<br />
Post confirmation you will be entitled for 15 days Medical Leave, 20 days Earned leave & 12 days Casual cum Sick leave per year alongwith 4 days Mandatory leave once a year. National Holidays & Festival Holidays as may be notified by the company from year to year</li>

<li>You will be entitled for statutory benefits namely provident fund, ESI, Bonus, Gratuity as per the existing laws of the land</li>
<li>You are liable to be transferred/assigned a job anywhere in India or abroad to any Division/Office/Establishment/Department/Company</li>
<li>You are also requested to bring the following documents at the time of joining for Personal records</li>
</ul>
<ol type="a">
<li>Two passport size photographs</li>
<li>Attested testimonials/certificates</li>
<li>Enclosed pre-joining documents</li>
<li>Previous job/employment documents/certificates</li>
</ol>
  <p>In case of acceptance please report at the following address for the joining on or 10-12-2016 at 10:00 AM failing which the said offer shall be deemed as cancelled.</p>
  <p>We hope that you will accept our job offer and that you will feel welcomed at our <strong>SKUMAR MICRO CREDIT FOUNDATION</strong> based at <strong>11, 1</strong><sup>ST</sup> <strong>FLOOR, OMAXE AVENUE, RAIBARELI ROAD, NEAR OMAXE CITY, LUCKNOW-226025.</strong></p><br />
  <p>HEAD OFFICE LUCKNOW</p>
  <p><span>HR DEPARTMENT</span><span class="pull-right">DATE : 20/15/2016</span></p><br />
  <p><i>Signature</i></p>
  <br /><br /><br />
  
   <table  style="width:80%; margin:0px auto;">
   <tr>
   <td><strong>Name :  MS. MAMTA MISHRA</strong></td>
   </tr>
   <tr>
   <td><strong>Post :  HR MANAGER</strong></td>
   </tr>
   </table>
  
  <br /><br />
      <table class="hr" style="width:80%; margin:0px auto;">
      <thead>
      <tr>
      <th>I(B)</th>
      <th>SALARY : (Per month)</th>
      <th>AMOUNT</th>
      </tr>
      </thead>
      <tbody>
      <tr>
      <td>1</td>
      <td>Basic</td>
      <td>6400</td>
      </tr>
      <tr>
      <td>2</td>
      <td>H.R.A.</td>
      <td>6400</td>
      </tr>
      <tr>
      <td>3</td>
      <td>CONV.ALLOW</td>
      <td>2560</td>
      </tr>
      <tr>
      <td>4</td>
      <td>SPL.ALLOW</td>
      <td>6040</td>
      </tr>
      <tr>
      <td>5</td>
      <td>INCENTIVE</td>
      <td>0</td>
      </tr>
      <tr>
      <td>6</td>
      <td>OTHERS</td>
      <td>0</td>
      </tr>
      <tr>
      <td colspan="2"><center>Total</center></td>
      <td>16000</td>
      </tr>
      </tbody>
      
      </table>
      
      
      <br /><br /><br />
      <table class="hr" style="width:80%; margin:0px auto;">
      <thead>
      <tr>
      <th>II(B)</th>
      <th>DEDUCTION : (Per Month)</th>
      <th>AMOUNT</th>
      </tr>
      </thead>
      <tbody>
      <tr>
      <td>1</td>
      <td>Provident Fund @ 12% of basic</td>
      <td>768</td>
      </tr>
      <tr>
      <td>2</td>
      <td>Income-Tax (If Any)</td>
      <td>0</td>
      </tr>
       <tr>
      <td colspan="2"><center>Total</center></td>
      <td>16000</td>
      </tr>
       <tr>
      <td colspan="2"><center><strong>Net Salary</strong></center></td>
      <td><strong>15232</strong> </td>
      </tr>
   	 <tr>
      <td colspan="3" style="border:none;">Subject to deduction under Income Tax Act 1961.</td>
      </tr>
      </tbody>
      
      </table>
            </p>
            </div>
            <div align="left">
                 <a class="btn btn-primary btn-md" href="index.php?control=hrm_registration&task=send_mail&id=<?php echo $result['id'];?>" >SEND MAIL</a>
                 </div>
                 
            </div>
		</div>
				
			</div>
            </div>
            </div>
            </div>
			
			
			
<div id="myModal" class="reveal-modal">
    <div id="scheme_popup"></div>
	<a class="close-reveal-modal"><!--<img src="assets/popup/images/close.png" width="23" height="23" />--></a> 
</div>



<div id="myModalPayment" class="reveal-modal">
	<div id="customer_profile_popup"></div>
	<a class="close-reveal-modal"><!--<img src="assets/popup/images/close.png" width="23" height="23" />--></a>
</div>
              
              
<script type="text/javascript">
	function employee_edit(id)
	{
		//alert(id);
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "addnew";
		document.getElementById("id").value = id;
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}
	
	function employee_status(id,status)
	{ 
		document.getElementById("id").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "status";
		document.getElementById("id").value = id;
		document.getElementById("status").value = status;
		document.filterForm.submit(); 
	}
	
	function employee_delete(id)
	{
		document.getElementById("del").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "delete";
		document.getElementById("id").value = id;
	
		if(confirm('Are you sure you want to delete ?')) {
			document.filterForm.submit(); 
		}
		else
		{
		//alert("No");
		}
	}

function customer_details(str) {
	{ 
	var xmlhttp; 
	if (window.XMLHttpRequest)
	  { // code for IE7+, Firefox, Chrome, Opera, Safari 
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  { // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  { 
	  if (xmlhttp.readyState==4)
		{
		document.getElementById("customer_profile_popup").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
	  }
	xmlhttp.open("GET", "script/popup_scripts/emp_details.php?acc_id="+str, true);
	//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
	xmlhttp.send();
	}
}
</script>

<script type="text/javascript">
/****************************************Form Validation********************************************/

function validation()
{	
var chk=1;

if(document.getElementById("search").value == '') {
chk = 0;
document.getElementById('search').focus();
document.getElementById('search').style.borderColor="red";
}
else {
document.getElementById('search').style.border="1px solid #ccc";
}
	
if(chk){
return true;
}else{
return false;	
}	
	
}

</script> 
