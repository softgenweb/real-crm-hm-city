<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<div class="col-sm-12">			
		
		
		<div class="row">
			<div class="col-lg-12">
			  <ol class="breadcrumb">
          
				<li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Employee / Employee Master</li>
			</ol>
			</div>
		</div><!--/.row-->
    






	<div class="panel panel-default">

		<div class="panel-body tabs">




<div class="col-md-0" align="right"><a href="javascript:history.go(-1)" class="btn btn-primary btn-md" id="btn-chat">Back</a></div>

		<form name="employee" autocomplete="off" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return validation();">

        

         <?php foreach($results as $result) { }  ?>
  <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
                <div class="panel-heading">
                    <u><h3>Employee Registration </h3></u><br> 
                </div>
                
              <div class="col-md-6 col-sm-6">
               <div class="col-md-4 col-sm-12">
                    <div class="form-group">Employee's Name :</div>
                </div>
                 <div class="col-md-8 col-sm-12">
                    <div class="form-group">
                    <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>" readonly="readonly">
                  </div>
                  </div>
                    <div class="clearfix"></div>

                <div class="col-md-4 col-sm-12">
               	 <div class="form-group">Email Address :</div>
                </div>

                <div class="col-md-8 col-sm-12">
                <div class="form-group">
              <input class="form-control" type="text" name="email" id="email" value="<?php echo $result['email']; ?>" readonly="readonly">
                 </div>
                 </div>
                 <div class="clearfix"></div>
                 <div class="col-md-4 col-sm-12">
                    <div class="form-group">Mobile No. :</div>
                </div>
                 <div class="col-md-8 col-sm-12">
                    <div class="form-group">
                    <input class="form-control" type="text" maxlength="10" name="mobile" id="mobile" value="<?php echo $result['mobile']; ?>" placeholder="Enter Mobile Number">
                  </div>
                  </div>
                    
              
              </div>
              
              <div class="col-md-6 col-sm-6">
                 

                <div class="col-md-4 col-sm-12">
               	 <div class="form-group">Marital Status :</div>
                </div>

                <div class="col-md-8 col-sm-12">
                <div class="form-group">
              <select class="form-control" name="marital" id="marital">
                    <option value="Married" <?php if($result['marital']=='marital') {echo "selected";} ?>>Married</option>
                    <option value="Unmarried" <?php if($result['marital']=='Unmarried') {echo "selected";} ?>>Unmarried</option>
                  </select>
                 </div>
                 </div>
                     <div class="clearfix"></div>
                     
                     <div class="col-md-4 col-sm-12">
               	 <div class="form-group">Hobbies :</div>
                </div>

                <div class="col-md-8 col-sm-12">
                <div class="form-group">
               <input class="form-control" type="text" name="hobbies" id="hobbies" value="<?php echo $result['hobbies']; ?>">
                 </div>
                 </div>
              
              </div>   
              
                    <div class="clearfix"></div>

                <div class="col-md-12">
									<div class="form-group"><label> Present Address :</label></div>
                                    
         <div class="col-md-6 col-sm-6">                           
							<div class="col-md-4 col-sm-12">   
                            <div class="form-group" >Present Address: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="present_address" id="present_address" value="<?php echo $result['present_address']; ?>" >
                            </div>
                            </div>
                            <div class="clearfix"> </div>
                           
                                                        
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" >Present State Name: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            
                            
                            
                            <select name="present_state" id="present_state" class="form-control" onchange="present_city1(this.value);">
                            
                            <option value="">Select State</option> 
                            <?php $sqlcpre = mysql_query("select * from state where status=1");	
                            while($res_PreUser = mysql_fetch_array($sqlcpre)){?>
                            <option value="<?php echo $res_PreUser['id'];?>" <?php if($res_PreUser['id']==$result['present_state']) {echo "selected";} ?>><?php echo $res_PreUser['state_name'];?></option> 
                            
                            <?php } ?>
                            </select>
                            
                            
                            
                            </div>
                            </div>
                            
        </div>
        <div class="col-md-6 col-sm-6"> 
                            
                            
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > Present PIN Code Number: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="present_pincode" id="present_pincode" value="<?php echo $result['present_pincode']; ?>">
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > Present City Name: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            
                            
                            <select class="form-control" id="present_city" name="present_city">
                            <option value="">--Select City--</option>
                            <?php
                            
                            $sqlPrecity =  mysql_query("SELECT * FROM  city where state_name='".$result['present_state']."' and status=1");
                            
                            while($resprecity=mysql_fetch_array($sqlPrecity))
                            {
                            ?>
                            <option value="<?php echo $resprecity['id']; ?>" <?php if($result['present_city']==$resprecity['id']) {?> selected="selected" <?php } ?>><?php echo $resprecity['city_name']; ?></option>
                            
                            <?php }	?>
                            </select>
                            
                            </div>
                            </div>
                            
           </div>                     
                                
                                
                       </div>
        </div>
  </div>

                    <div class="col-md-12" align="left" style="margin-bottom:15px;">

                            <br>

                      <button type="submit" class="btn btn-primary"><?php if($results[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>

                      <button type="reset" class="btn btn-default">Reset </button> </div>  

                        

                      <input type="hidden" name="control" value="hrm_registration"/>

                      <input type="hidden" name="edit" value="1"/>

                      <input type="hidden" name="task" value="save_emp"/>

                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />

                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />

                      <input class="form-control" type="hidden" name="utype" id="utype" value="Employee">

           </form>

					</div>

				</div><!--/.panel-->

		

        </div> 



<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
$('#dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#joining_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});


</script>


<script>

function area_level(str){

		reportingHead(str);

		var xmlhttp; 

		if (window.XMLHttpRequest)

		{ // code for IE7+, Firefox, Chrome, Opera, Safari 

		xmlhttp=new XMLHttpRequest();

		}

		else

		{ // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange=function()

		{ 

		if (xmlhttp.readyState==4)

		{

		document.getElementById("level").value=xmlhttp.responseText; 

		//alert(xmlhttp.responseText);

		}

		}

		xmlhttp.open("GET", "script/popup_scripts/level.php?id="+str, true);

		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);

		xmlhttp.send();

			

           }



function reportingHead(str){ //alert(str)

		

		var xmlhttp; 

		if (window.XMLHttpRequest)

		{ // code for IE7+, Firefox, Chrome, Opera, Safari 

		xmlhttp=new XMLHttpRequest();

		}

		else

		{ // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange=function()

		{ 

		if (xmlhttp.readyState==4)

		{

		document.getElementById("reporting").innerHTML=xmlhttp.responseText; 

		//alert(xmlhttp.responseText);

		}

		}

		xmlhttp.open("GET", "script/popup_scripts/reportingHead.php?id="+str, true);

		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);

		xmlhttp.send();

			

           }





</script> 



<!--****************************************Form Validation********************************************-->

<script type="text/javascript">

function sum(str)

{

//alert(str);	

  var basic_salary = document.getElementById('basic_salary').value;

	var hra = document.getElementById('hra').value;

	var special = document.getElementById('special').value;

	var education = document.getElementById('education').value;

	var ta = document.getElementById('ta').value;

        var fitness = document.getElementById('fitness').value;

	 var medical = document.getElementById('medical').value; 

	 

	 var total = Number(hra) + Number(special) + Number(education) + Number(ta) + Number(fitness) + Number(medical) + Number(basic_salary);

	  document.getElementById('total').value =total;

	/*  if( basic_salary > total)

	  {

		 // alert('Basic Salary Is GreaterThan Total Salary');

		  document.getElementById('total').value =''; 

	  }

	  elseif(basic_salary < total)

	  {

		  alert('Basic Salary Is LessThan Total Salary');

		  document.getElementById('total').value =''; 

	  }

	  */

}



function validation() {	

var chk=1;

if(document.getElementById("name").value == '') {

chk = 0;

document.getElementById('name').focus();

document.getElementById('name').style.borderColor="red";} 

else if (!isletter(document.getElementById("name").value)) {

chk = 0;

document.getElementById('name').focus();

document.getElementById('name').style.borderColor="red";}

else {

document.getElementById('name').style.border="1px solid #ccc";}



if(document.getElementById("password").value == '') {

chk = 0;

document.getElementById('password').focus();

document.getElementById('password').style.borderColor="red";

} else {

document.getElementById('password').style.border="1px solid #ccc";

}



if(document.getElementById("address").value == '') {

chk = 0;

document.getElementById('address').focus();

document.getElementById('address').style.borderColor="red";

}

else {

document.getElementById('address').style.border="1px solid #ccc";

}



if(document.getElementById("dob").value == '') {

chk = 0;

document.getElementById('dob').focus();

document.getElementById('dob').style.borderColor="red";

}

else {

document.getElementById('dob').style.border="1px solid #ccc";

}



if(document.getElementById("email").value == '') {

chk = 0;

document.getElementById('email').focus();

document.getElementById('email').style.borderColor="red";

} else if (!isEmail(document.getElementById("email").value)) {

chk = 0;

document.getElementById('email').focus();

document.getElementById('email').style.borderColor="red";

} else {

document.getElementById('email').style.border="1px solid #ccc";

}







	

if(chk){

	  // $('#form').submit();

	alert("Employee Registration Successful ..!!");

return true;}

else{

	return false;	}	

	}



</script>



<!--****************************************Form Validation********************************************-->

<script>

function clientAjax(srno) {



		//alert(srno);

		var str1 = document.getElementById('post_name').value;

		

		var city = document.getElementById('city_name').value;

		//alert(str1);

		var xmlhttp; 

		if (window.XMLHttpRequest)

		{ // code for IE7+, Firefox, Chrome, Opera, Safari 

		xmlhttp=new XMLHttpRequest();

		}

		else

		{ // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange=function()

		{ 

		if (xmlhttp.readyState==4)

		{//alert(str1);

		

		document.getElementById("daily_allownce").value=xmlhttp.responseText; 

		

		//alert(xmlhttp.responseText);

		}

		}

		xmlhttp.open("GET", "./script/popup_scripts/client.php?id="+str1+"&city="+city, true);

		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);

		xmlhttp.send();



}





function present_city1(str1) {

		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		document.getElementById("present_city").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/city.php?id="+str1, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}
</script>  
<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   