<?php foreach($results as $result) { }  ?>

    <div class="panel panel-default" >
        <div class="box-header">        
        <h3 class="box-title">Add Employee</h3>
    </div> 
    <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=hrm_registration&task=show"><i class="fa fa-list" aria-hidden="true"></i> Employee List</a></li>
            <?php if($result!='') {?>           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Employee</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Employee</li>
          <?php } ?>
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php     unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
       }?>
            
                          
    <div class="panel-body">
        <form name="employee" autocomplete="off" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return validation();">
            <?php foreach($results as $result) { }  ?>
                <div class="tab-content">

                    
                     <div class="col-md-6 col-sm-6">
                    <div class="col-md-12">
                    <div class="form-group">
                    <label>Employee Personal Details :</label>
                    </div>
                    </div>  
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-sm-12">
                    <div class="form-group" >
                    Employee's Name :
                    </div></div>
                    <div class="col-md-8 col-sm-12"><div class="form-group">
                    <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>">
                    </div></div>

                    <div class="clearfix"></div>

                     <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Password :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <?php if($_REQUEST['id']) { ?>

                    <input class="form-control" type="password" name="password" id="password"  value="<?php echo $result['password']; ?>" >

                    <?php } else { ?>

                     <input class="form-control" type="password" name="password" id="password" value="" >

                     <?php }?>

                    </div></div>

                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Date of Birth :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <input class="form-control" type="text"  name="dob" id="dob" value="<?php echo $result['dob']; ?>" readonly>

                    </div></div>


                    <div class="clearfix"></div>
                    
                    
                    <div class="col-md-4  col-sm-12">

                    <div class="form-group" style="margin-top:5%; margin-bottom: 9px !important;">

                    Gender :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group" style="margin-left:10px;">

                    <div class="radio">
                     <label>
                      <input type="radio" name="gender" id="gender" value="1" <?php if($result['gender']=='1' || $result['gender']=='') {?> checked="true" <?php } ?>>
                      Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <input type="radio" name="gender" id="gender" value="0" <?php if($result['gender']=='0') {?> checked="true" <?php } ?>>
                      Female </label>



                    </div>

                    </div></div>

                  <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Mobile Number :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <input class="form-control" type="text" maxlength="10" name="mobile" id="mobile" value="<?php echo $result['mobile']; ?>" onkeyup="chkNumeric(this.value,this.id)">

                    </div></div>

               <div class="clearfix"></div>

            

                <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Email Address :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="email" id="email" value="<?php echo $result['email']; ?>">

                </div></div>
                <div class="clearfix"></div>
                
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >
                Joining Date :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <input class="form-control" type="text" name="joining_date" id="joining_date" value="<?php echo $result['joining_date']; ?>" readonly>
                </div></div>  
                <div class="clearfix"></div>  

                  
                <!--  <div class="col-md-4 col-sm-12">

                 <div class="form-group" >
                Blood Group :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <input class="form-control" type="text" name="blood_group" id="blood_group" value="<?php echo $result['blood_group']; ?>">
                </div></div>   -->   
                    
              
                  <div class="clearfix"></div>
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >
                Pan Card Number :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <input class="form-control" type="text" name="pancard_no" id="pancard_no" value="<?php echo $result['pancard_no']; ?>">
                </div></div>  
                  <div class="clearfix"></div>
                  
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >
                Aadhar Card Number :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <input class="form-control" type="text" name="aadhar_no" id="aadhar_no" value="<?php echo $result['aadhar_no']; ?>" onkeyup="chkNumeric(this.value,this.id)" maxlength="12">
                </div></div>  
                <div class="clearfix"></div>          
       
                  <div class="col-md-4 col-sm-12">
                <div class="form-group" >
                Merital Status :
                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">
                <select class="form-control" name="marital" id="marital">
                    <option value="Married" <?php if($result['marital']=='marital') {echo "selected";} ?>>Married</option>
                    <option value="Unmarried" <?php if($result['marital']=='Unmarried') {echo "selected";} ?>>Unmarried</option>
                  </select>
                </div></div>  
         <div class="clearfix"></div>       
                                   <div class="col-md-4 col-sm-12">
                <div class="form-group" >
                Qualification :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
               <select class="form-control" name="qualification" id="qualification">
                    <option value="">Select</option>
                    <option value="Under Graduate" <?php if($result['qualification']=="Under Graduate") {echo "selected";} ?>>Under Graduate</option>
                    <option value="Graduate" <?php if($result['qualification']=="Graduate") {echo "selected";} ?>>Graduate</option>
                    <option value="Post Graduate" <?php if($result['qualification']=="Post Graduate") {echo "selected";} ?>>Post Graduate</option>
                    <option value="Others" <?php if($result['qualification']=="Others") {echo "selected";} ?>>Others</option>
                  </select>

                </div></div>  
                   
            <div class="clearfix"></div>
               <!--    <div class="col-md-4 col-sm-12">
                <div class="form-group" >
                Hobbies :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <input class="form-control" type="text" name="hobbies" id="hobbies" value="<?php echo $result['hobbies']; ?>">
                </div></div>  
                <div class="clearfix"></div>-->
                
               
            </div>

                    <div class="col-md-6 col-sm-6">   
            <div class="col-md-12">
            <div class="form-group">
            <label> Details of Employee:</label>
            </div>
            </div>  
            <input class="form-control" type="hidden" name="level" id="level" value="" >

            <div class="col-md-4 col-sm-12">
            <div class="form-group" >
              Country Name:
            </div></div>
            <div class="col-md-8 col-sm-12"><div class="form-group">
            <select name="country" id="country" onchange="state_change(this.value);" class="form-control" autocomplete="off"  >
                <option value="">Select</option>
                <?php $sql = mysql_query("SELECT * FROM `country` WHERE 1"); 
                    while($contry = mysql_fetch_array($sql)) {?>
                    <option value="<?php echo $contry['id']; ?>" <?php if($result['country']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
                <?php }?>
            </select>
            <span id="msgname" style="color:red;"></span>
            </div></div>
               
             <div class="clearfix"></div>  
               
            <div class="col-md-4 col-sm-12">
            <div class="form-group" >
            State Name:
            </div></div>
            <div class="col-md-8 col-sm-12"><div class="form-group">
            <!--  <input type="text" class="form-control" name="state_name" id="name" placeholder="Enter State Name" value="<?php echo $result['state_name']; ?>"/>-->
            <select name="state_name" id="state_id" onchange="city_change(this.value);" class="form-control" autocomplete="off"  >
                <option value="">Select</option>
                <?php $sql = mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['country']."'"); 
                    while($state = mysql_fetch_array($sql)) {?>
                    <option value="<?php echo $state['id']; ?>" <?php if($result['state_id']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
                <?php }?>
            </select>
           
            <span id="msgname" style="color:red;"></span>
            </div></div>
            <div class="clearfix"></div>

            <div class="col-md-4 col-sm-12">
            <div class="form-group" >
            City Name:
            </div></div>
            <div class="col-md-8 col-sm-12"><div class="form-group">

            <select name="city_name" id="city_id" class="form-control" autocomplete="off"  >
                    <option value="">Select</option>
                    <?php $sql = mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['state_id']."' AND `country_id`='".$result['country']."'"); 
                        while($city = mysql_fetch_array($sql)) {?>
                        <option value="<?php echo $city['id']; ?>" <?php if($result['city_id']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
                    <?php }?>
                </select>
            <span id="msgname" style="color:red;"></span>
            </div></div>


            <div class="clearfix"></div>

            <div class="col-md-4 col-sm-12">
            <div class="form-group" > PIN Code : </div>
            </div>
            <div class="col-md-8 col-sm-12">
            <div class="form-group">
            <input class="form-control" type="text" name="postalcode" id="postalcode" value="<?php echo $result['postalcode']; ?>" onkeyup="chkNumeric(this.value,this.id)">
            </div>
            </div>
        
           <!--  <div class="col-md-4 col-sm-12">
            <div class="form-group" >
            Location Name:
            </div></div>
            <div class="col-md-8 col-sm-12"><div class="form-group">
            <select name="location_name" id="location_name" class="form-control"  onchange="branch_code(this.value);">
            <option value="">---Select Location---</option>
            <?php $sel = mysql_query("select * from location where id='".$result['location_id']."'");
            while($select=mysql_fetch_array($sel))
            { ?>
            <option value="<?php echo $select['id'];?>"<?php if($result['location_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['name'];?></option>
            <?php } ?>
            </select>
            <span id="msglocation_name" style="color:red;"></span>
            </div></div>
                        
            <div class="clearfix"></div>                       
              
                <div class="col-md-4 col-sm-12">
                  <div class="form-group" >
                 Branch Name :
                  </div>
                </div>
                <div class="col-md-8 col-sm-12"><div class="form-group">     
                  <select name="branch_name" id="branch_name" class="form-control" >
                <option value="">---Select Branch---</option>
                <?php $selbranch = mysql_query("select * from branch where id='".$result['branch_id']."'");
                while($resBranch = mysql_fetch_array($selbranch))
                { ?>
                <option value="<?php echo $resBranch['id'];?>"<?php if($result['branch_id']==$resBranch['id']) { ?> selected="selected" <?php } ?>><?php echo $resBranch['branch_name']." - ".$resBranch['branch_code'];?></option>
                <?php } ?>
                </select>
                
                
                 <span id="msgbranch_name" class="msg"></span>
                </div></div>     --> 
             <div class="clearfix"></div>         
  <div class="col-md-4 col-sm-12">
            <div class="form-group" > Address : </div>
            </div>
            <div class="col-md-8 col-sm-12">
            <div class="form-group">
            <textarea name="address" id="address" class="form-control" cols="3" ><?php echo $result['address']; ?></textarea>
            </div>
            </div>
            <div class="col-md-4 col-sm-12">

            <div class="form-group" >

            Designation Name:

            </div></div>
            <div class="col-md-8 col-sm-12"><div class="form-group">
            <select class="form-control" id="post_name" name="post_name" onchange="clientAjax(this.value);">
            <option value="">--Select Designation--</option>
            <?php
            $projectquery11=mysql_query("SELECT * FROM `hrm_post` WHERE `status`=1");
            while($projectdata11=mysql_fetch_array($projectquery11))
            {
            ?>
            <option value="<?php echo $projectdata11['id']; ?>" <?php if($result['post_id']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['post_name']; ?></option>
            <?php }

            ?>

            </select>
            <span id="msgname" style="color:red;"></span>
            </div></div>
                
            <div class="clearfix"></div>                    

           <!--  <div class="col-md-4 col-sm-12">
            <div class="form-group" >
            Reporting Manager:
            </div></div>
            <div class="col-md-8 col-sm-12"><div class="form-group">
            <select class="form-control" id="reporting" name="reporting" >
            <option value="">--Select Manager--</option>
            <?php
                
            $sel4=mysql_query("select mrg.id,mrg.name,mrg.reporting,po.post_name from hrm_registration as mrg join post as po where mrg.post_name=po.id and mrg.status='1'");
            //$projectquery11=mysql_query("SELECT * FROM  hrm_registration where status='1'" );
            while($projectdata11=mysql_fetch_array($sel4))
            {
            ?>
            <option value="<?php echo $projectdata11['id']; ?>" <?php if($result['reporting']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['name'].'--'.$projectdata11['post_name']; ?></option>
            <?php }  ?>
            </select>
            <span id="msgname" style="color:red;"></span>
            </div></div> -->
        
                <div class="clearfix"></div>             
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    Employee's Photograph :
                </div>
            </div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
               
                    <input type="file" class="imgfile" name="image" id="image"   size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                    <a target="_blank" href="media/employee/<?php echo $result['image']?$result['image']:'javascript:;'; ?>" id="img_a"><img src="media/employee/<?php echo $result['image']?$result['image']:'javascript:;'; ?>" id="emp_pic" alt="" height="70" ></a>
                    <span class='label label-info' id="upload-file-info"></span>
                </div></div>
                    
            <div class="clearfix"></div>                     
                <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Pan Card Upload :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

             

                    <input type="file" class="imgfile" name="pancard" id="pancard" size="40"  onchange='$("#upload-file-info_pancard").html($(this).val());'>
                      <a target="_blank" href="media/employee/<?php echo $result['pancard']?$result['pancard']:'javascript:;'; ?>" id="pancard_a"><img src="media/employee/<?php echo $result['pancard']?$result['pancard']:'javascript:;'; ?>" id="pancard_img" alt="" height="70" ></a>
                    <span class='label label-info' id="upload-file-info_pancard"></span>

                </div></div>   
                    
         <div class="clearfix"></div>   
                     
<!--                 <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Other Documents :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <a class='btn btn-primary' href='javascript:;'>Other Documents
                    <input type="file" class="imgfile" name="aadhar" id="aadhar" size="40"  onchange='$("#upload-file-info_aadhar").html($(this).val());'></a>&nbsp;
                    <span class='label label-info' id="upload-file-info_aadhar"></span>
                </div></div>  
         <div class="clearfix"></div> -->
                  <div class="col-md-4 col-sm-12">
                <div class="form-group" >
                Nationality :
                </div></div>
                <div class="col-md-8 col-sm-12"><div class="form-group">
                <input class="form-control" type="text" name="nationality" id="nationality" value="<?php echo $result['nationality']?$result['nationality']:"Indian"; ?>">
                </div></div>  
                
            <div class="clearfix"></div>              
                

                    </div>


           
                       

                    </div>
                      
                     <div class="clearfix"></div>
                        
                      
                    <div class="clearfix"></div>
                   <!--  <div class="col-md-12">
                        <div class="form-group"><label> Permanent Address :</label></div>
                        <div class="clearfix"></div>
                         <div class="col-md-6 col-sm-6">
                                    
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > Address: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="permanent_address" id="permanent_address" value="<?php echo $result['permanent_address']; ?>" >
                            </div>
                            </div>
                            <div class="clearfix"></div>
                                                       
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" >  State : </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            
                            
                           <select name="res_state" id="state_id" onchange="city_change(this.value);" class="form-control" autocomplete="off"  >
                                <option value="">Select</option>
                                <?php $sql = mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['res_country']."'"); 
                                    while($state = mysql_fetch_array($sql)) {?>
                                    <option value="<?php echo $state['id']; ?>" <?php if($result['res_state']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
                                <?php }?>
                            </select>
                            
                            
                            
                            </div>
                            </div>
            </div>
            <div class="col-md-6 col-sm-6">
                            
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > PIN Code : </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="permanent_pincode" id="permanent_pincode" value="<?php echo $result['permanent_pincode']; ?>" onkeyup="chkNumeric(this.value,this.id)">
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > City : </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">                          
                            <select name="res_city" id="city_id" class="form-control" autocomplete="off"  >
                                <option value="">Select</option>
                                <?php $sql = mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['res_state']."' AND `country_id`='".$result['res_country']."'"); 
                                    while($city = mysql_fetch_array($sql)) {?>
                                    <option value="<?php echo $city['id']; ?>" <?php if($result['res_city']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
                                <?php }?>
                            </select>                           
                            </div>
                            </div>
                            
           </div>                     
                                
                                
                       </div> -->
                       
                       
                       <div class="col-md-12">                   
                       

                    <div class="col-md-12" align="left" style="margin-bottom:15px;">
   <br>

                      <button type="submit" class="btn btn-primary bulu"><?php if($results[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>

                
                     </div>  
                     <input type="hidden" name="total_month_deduction" id="total_month_deduction" value="<?php echo $result['total_month_deduction']; ?>"/>
                      <input type="hidden" name="control" value="hrm_registration"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                      <input class="form-control" type="hidden" name="utype" id="utype" value="Employee">

           </form>
    </div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     






<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
$('#dob').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d/m/Y',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#joining_date').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d/m/Y',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});



function id_proof(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#emp_pic').attr('src', e.target.result);
            $('#img_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#image").change(function(){
    id_proof(this);
});


function pancard(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#pancard_img').attr('src', e.target.result);
            $('#pancard_a').attr('href', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
$("#pancard").change(function(){
    pancard(this);
});
</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   