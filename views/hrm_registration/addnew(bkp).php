<div class="col-sm-12">			
		
			
		<div class="row">
			<div class="col-lg-12">
			  <ol class="breadcrumb">
         
				<li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=hrm_registration">HRM</a></li>
                <li class="active">Employee / Employee Registration</li>
			</ol>
			</div>
		</div><!--/.row-->	
        
	
	<div class="panel panel-default">

		<div class="panel-body">




<div class="col-md-0" align="right"><a href="javascript:history.go(-1)" class="btn btn-primary btn-md" id="btn-chat">Back</a></div>

		<form name="employee" autocomplete="off" action="index.php" method="post" enctype="multipart/form-data" onsubmit="return validation();">
        <?php foreach($results as $result) { }  ?>
                <div class="tab-content">

                     <div class="panel-heading"><u>

                    <h3>Employee Registration </h3></u> </div><br>
                     <div class="col-md-6 col-sm-6">

                    <div class="col-md-12">

                    <div class="form-group">

                    <label>Employee Personal Details :</label>

                    </div>

                    </div>	
                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Employee's Name :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <input class="form-control" type="text" name="name" id="name" value="<?php echo $result['name']; ?>">

                    </div></div>

                    <div class="clearfix"></div>

		   			 <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Password :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <?php if($_REQUEST['id']) { ?>

                    <input class="form-control" type="password" name="password" id="password"  value="<?php echo $result['password']; ?>" >

                    <?php } else { ?>

                     <input class="form-control" type="password" name="password" id="password" value="" >

                     <?php }?>

                    </div></div>

                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Date of Birth :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <input class="form-control" type="text"  name="dob" id="dob" value="<?php echo $result['dob']; ?>" readonly>

                    </div></div>


                    <div class="clearfix"></div>
                    
                    
                    <div class="col-md-4  col-sm-12">

                    <div class="form-group" style="margin-top:5%; margin-bottom: 9px !important;">

                    Gender :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group" style="margin-left:10px;">

                    <div class="radio">
                     <label>
                      <input type="radio" name="gender" id="gender" value="1" <?php if($result['gender']=='1' || $result['gender']=='') {?> checked="true" <?php } ?>>
                      Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <input type="radio" name="gender" id="gender" value="0" <?php if($result['gender']=='0') {?> checked="true" <?php } ?>>
                      Female </label>

                    <!--<label>
<input type="radio" name="gender" id="gender" value="Male" <?php if($result['gender']=='Male' || $result['gender']=='') {?> checked="true" <?php } ?>>Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <input type="radio" name="gender" id="gender" value="Female" <?php if($result['gender']=='Female') {?> checked="true" <?php } ?>>Female

                    </label>-->

                    </div>

                    </div></div>

                  <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-12">

                    <div class="form-group" >

                    Mobile Number :

                    </div></div>

                    <div class="col-md-8 col-sm-12"><div class="form-group">

                    <input class="form-control" type="text" maxlength="10" name="mobile" id="mobile" value="<?php echo $result['mobile']; ?>" onkeyup="chkNumeric(this.value,this.id)">

                    </div></div>

               <div class="clearfix"></div>

		    

                <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Email Address :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="email" id="email" value="<?php echo $result['email']; ?>">

                </div></div>
                <div class="clearfix"></div>
                
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Joining Date :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="joining_date" id="joining_date" value="<?php echo $result['joining_date']; ?>">

                </div></div>  
                <div class="clearfix"></div>  

                  
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Blood Group :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="blood_group" id="blood_group" value="<?php echo $result['blood_group']; ?>">

                </div></div>     
                    
              
                  <div class="clearfix"></div>
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Pan Card Number :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="pancard_no" id="pancard_no" value="<?php echo $result['pancard_no']; ?>">

                </div></div>  
                  <div class="clearfix"></div>
                      
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Aadhar Card Number :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="aadhar_no" id="aadhar_no" value="<?php echo $result['aadhar_no']; ?>" onkeyup="chkNumeric(this.value,this.id)" maxlength="12">

                </div></div>  
                <div class="clearfix"></div>          
       
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Marital Status :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <select class="form-control" name="marital" id="marital">
                    <option value="Married" <?php if($result['marital']=='marital') {echo "selected";} ?>>Married</option>
                    <option value="Unmarried" <?php if($result['marital']=='Unmarried') {echo "selected";} ?>>Unmarried</option>
                  </select>

                </div></div>  
         <div class="clearfix"></div>       
                     
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Hobbies :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="hobbies" id="hobbies" value="<?php echo $result['hobbies']; ?>">

                </div></div> 
                <div class="clearfix"></div>
                
               
            </div>

                    <div class="col-md-6 col-sm-6">   

            <div class="col-md-12">

            <div class="form-group">

            <label> Details of Employee:</label>

            </div>

            </div>  

            <input class="form-control" type="hidden" name="level" id="level" value="" >


                  

			<div class="col-md-4 col-sm-12">
			<div class="form-group" >
			  Zone Name:
			</div></div>
			<div class="col-md-8 col-sm-12"><div class="form-group">
			<select class="form-control" id="txtHint" name="zone_name" onchange="state_change(this.value);">
			<option value="">--Select Zone--</option>
			<?php

			$projectquery11 = mysql_query("SELECT * FROM  zone where status='1'");

			while($projectdata11=mysql_fetch_array($projectquery11))
			{
			?>
			<option value="<?php echo $projectdata11['id']; ?>" <?php if($result['zone']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['zone_name']; ?></option>

			<?php }
			?>
			</select>
			<span id="msgname" style="color:red;"></span>
			</div></div>
               
             <div class="clearfix"></div>  
               
			<div class="col-md-4 col-sm-12">
			<div class="form-group" >
			State Name:
			</div></div>
			<div class="col-md-8 col-sm-12"><div class="form-group">
			<!--  <input type="text" class="form-control" name="state_name" id="name" placeholder="Enter State Name" value="<?php echo $result['state_name']; ?>"/>-->
			<select name="state_name" id="state_name" class="form-control" onchange='city_change(this.value);'>
			<option value="">---Select State---</option>
			<?php $sel=  mysql_query("select * from state where zone_name='".$result['zone']."' and status=1");
			while($select=mysql_fetch_array($sel))
			{ ?>
			<option value="<?php echo $select['id'];?>"<?php if($result['state_name']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
			<?php } ?>
			</select>
           
			<span id="msgname" style="color:red;"></span>
			</div></div>
<div class="clearfix"></div>

			<div class="col-md-4 col-sm-12">
			<div class="form-group" >
			City Name:
			</div></div>
			<div class="col-md-8 col-sm-12"><div class="form-group">
			<select class="form-control" id="txtcity" name="city_name"  onchange='location_change(this.value);'>
			<option value="">--Select City--</option>
			<?php

			$projectquery11 =  mysql_query("SELECT * FROM  city where state_name='".$result['state_name']."' and status=1");

			while($projectdata11=mysql_fetch_array($projectquery11))
			{
			?>
			<option value="<?php echo $projectdata11['id']; ?>" <?php if($result['city_name']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['city_name']; ?></option>

			<?php }	?>
			</select>
			<span id="msgname" style="color:red;"></span>
			</div></div>


<div class="clearfix"></div>
 		
         				<div class="col-md-4 col-sm-12">
                        <div class="form-group" >
                        Location Name:
                        </div></div>
                        <div class="col-md-8 col-sm-12"><div class="form-group">
                        <select name="location_name" id="location_name" class="form-control"  onchange="branch_code(this.value);">
                        <option value="">---Select Location---</option>
                        <?php $sel = mysql_query("select * from location where id='".$result['location_id']."'");
                        while($select=mysql_fetch_array($sel))
                        { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['location_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['name'];?></option>
                        <?php } ?>
                        </select>
                        <span id="msglocation_name" style="color:red;"></span>
                        </div></div>
                        
  <div class="clearfix"></div>                      
              
                <div class="col-md-4 col-sm-12">
                  <div class="form-group" >
                 Branch Name :
                  </div>
                </div>
                <div class="col-md-8 col-sm-12"><div class="form-group">     
                  <select name="branch_name" id="branch_name" class="form-control" >
                <option value="">---Select Branch---</option>
                <?php $selbranch = mysql_query("select * from branch where id='".$result['branch_id']."'");
                while($resBranch = mysql_fetch_array($selbranch))
                { ?>
                <option value="<?php echo $resBranch['id'];?>"<?php if($result['branch_id']==$resBranch['id']) { ?> selected="selected" <?php } ?>><?php echo $resBranch['branch_name']." - ".$resBranch['branch_code'];?></option>
                <?php } ?>
                </select>
                
                
                 <span id="msgbranch_name" class="msg"></span>
                </div></div>
		
  <div class="clearfix"></div>          

            <div class="col-md-4 col-sm-12">

            <div class="form-group" >

            Designation Name:

            </div></div>

            <div class="col-md-8 col-sm-12"><div class="form-group">

            <select class="form-control" id="post_name" name="post_name" onchange="clientAjax(this.value);">

            <option value="">--Select Designation--</option>

            <?php

            

            $projectquery11=mysql_query("SELECT * FROM  post where status='1'");

            

            while($projectdata11=mysql_fetch_array($projectquery11))

            {

            ?>

            <option value="<?php echo $projectdata11['id']; ?>" <?php if($result['post_name']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['post_name']; ?></option>

            

            <?php }

            ?>

            </select>

            <span id="msgname" style="color:red;"></span>

            </div></div>

                

<div class="clearfix"></div>                

                

			<div class="col-md-4 col-sm-12">

			<div class="form-group" >

			Reporting Manager:

			</div></div>

			<div class="col-md-8 col-sm-12"><div class="form-group">

			<select class="form-control" id="reporting" name="reporting" ><!--onchange="area_level(this.value);"--->

			<option value="">--Select Manager--</option>

			<?php

				
$sel4=mysql_query("select mrg.id,mrg.name,mrg.reporting,po.post_name from hrm_registration as mrg join post as po where mrg.post_name=po.id and  mrg.status='1'");
			//$projectquery11=mysql_query("SELECT * FROM  hrm_registration where status='1'" );



			while($projectdata11=mysql_fetch_array($sel4))

			{

			?>

			<option value="<?php echo $projectdata11['id']; ?>" <?php if($result['reporting']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['name'].'--'.$projectdata11['post_name']; ?></option>



			<?php }

			?>

			</select>

			<span id="msgname" style="color:red;"></span>

			</div></div>
            
            
 <div class="clearfix"></div>           
                  
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    Employee's Photograph :
                </div>
            </div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <a class='btn btn-primary' href='javascript:;'>Upload Photograph

                    <input type="file" class="imgfile" name="image" id="image"   size="40"  onchange='$("#upload-file-info").html($(this).val());'></a>&nbsp;

                    <span class='label label-info' id="upload-file-info"></span>

                </div></div>
                    
<div class="clearfix"></div>                     
                <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Pan Card Upload :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <a class='btn btn-primary' href='javascript:;'>Pan Card Upload

                    <input type="file" class="imgfile" name="pancard" id="pancard" size="40"  onchange='$("#upload-file-info_pancard").html($(this).val());'></a>&nbsp;

                    <span class='label label-info' id="upload-file-info_pancard"></span>

                </div></div>   
                    
  <div class="clearfix"></div>   
                     
                <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Other Documents :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <a class='btn btn-primary' href='javascript:;'>Other Documents

                    <input type="file" class="imgfile" name="aadhar" id="aadhar" size="40"  onchange='$("#upload-file-info_aadhar").html($(this).val());'></a>&nbsp;

                    <span class='label label-info' id="upload-file-info_aadhar"></span>

                </div></div>  

<div class="clearfix"></div>
                  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Nationality :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

                <input class="form-control" type="text" name="nationality" id="nationality" value="<?php echo $result['nationality']?$result['nationality']:"Indian"; ?>">

                </div></div>  
                
  <div class="clearfix"></div>              
                
			  <div class="col-md-4 col-sm-12">

                <div class="form-group" >

                Qualification :

                </div></div>

                <div class="col-md-8 col-sm-12"><div class="form-group">

               <select class="form-control" name="qualification" id="qualification">
                    <option value="">Select</option>
                    <option value="Under Graduate" <?php if($result['qualification']=="Under Graduate") {echo "selected";} ?>>Under Graduate</option>
                    <option value="Graduate" <?php if($result['qualification']=="Graduate") {echo "selected";} ?>>Graduate</option>
                    <option value="Post Graduate" <?php if($result['qualification']=="Post Graduate") {echo "selected";} ?>>Post Graduate</option>
                    <option value="Others" <?php if($result['qualification']=="Others") {echo "selected";} ?>>Others</option>
                  </select>

                </div></div>  
                   
<div class="clearfix"></div>
                    </div>


           
                       

                    </div>
                      
                     <div class="clearfix"></div>
						<div class="col-md-12">
									<div class="form-group"><label> Present Address :</label></div>
                                    <div class="clearfix"></div>
                                    
         <div class="col-md-6 col-sm-6">                           
							<div class="col-md-4 col-sm-12">   
                            <div class="form-group" >Present Address: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="present_address" id="present_address" value="<?php echo $result['present_address']; ?>" >
                            </div>
                            </div>
                            <div class="clearfix"> </div>
                           
                                                        
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" >Present State Name: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            
                            
                            <select name="present_state" id="present_state" class="form-control" onchange="present_city1(this.value);">
                            
                            <option value="">Select State</option> 
                            <?php $sqlcpre = mysql_query("select * from state where status=1");	
                            while($res_PreUser = mysql_fetch_array($sqlcpre)){?>
                            <option value="<?php echo $res_PreUser['id'];?>" <?php if($res_PreUser['id']==$result['present_state']) {echo "selected";} ?>><?php echo $res_PreUser['state_name'];?></option> 
                            
                            <?php } ?>
                            </select>
                            
                            
                            
                            </div>
                            </div>
                            
        </div>
        <div class="col-md-6 col-sm-6"> 
                            
                            
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > Present PIN Code Number: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="present_pincode" id="present_pincode" value="<?php echo $result['present_pincode']; ?>" onkeyup="chkNumeric(this.value,this.id)">
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > Present City Name: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            
                            
                            <select class="form-control" id="present_city" name="present_city">
                            <option value="">--Select City--</option>
                            <?php
                            
                            $sqlPrecity =  mysql_query("SELECT * FROM  city where state_name='".$result['present_state']."' and status=1");
                            
                            while($resprecity=mysql_fetch_array($sqlPrecity))
                            {
                            ?>
                            <option value="<?php echo $resprecity['id']; ?>" <?php if($result['present_city']==$resprecity['id']) {?> selected="selected" <?php } ?>><?php echo $resprecity['city_name']; ?></option>
                            
                            <?php }	?>
                            </select>
                            
                            </div>
                            </div>
                            
           </div>                     
                                
                                
                       </div> 
                      
                    <div class="clearfix"></div>
                    <div class="col-md-12">
									<div class="form-group"><label> Permanent Address :</label></div>
                                    <div class="clearfix"></div>
	<div class="col-md-6 col-sm-6">
    								
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" >Permanent Address: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="permanent_address" id="permanent_address" value="<?php echo $result['permanent_address']; ?>" >
                            </div>
                            </div>
                            <div class="clearfix"></div>
                                                       
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" > Permanent State Name: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            
                            
                            <select name="permanent_state" id="permanent_state" class="form-control" onchange="permanent_state1(this.value);">
                            
                            <option value="">Select State</option> 
                            <?php $sqlcper = mysql_query("select * from state where status=1");	
                            while($resPerUser = mysql_fetch_array($sqlcper)){?>
                            <option value="<?php echo $resPerUser['id'];?>" <?php if($resPerUser['id']==$result['permanent_state']) {echo "selected";} ?>><?php echo $resPerUser['state_name'];?></option> 
                            
                            <?php } ?>
                            </select>
                            
                            
                            
                            </div>
                            </div>
            </div>
            <div class="col-md-6 col-sm-6">
                            
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" >Permanent PIN Code Number: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <input class="form-control" type="text" name="permanent_pincode" id="permanent_pincode" value="<?php echo $result['permanent_pincode']; ?>" onkeyup="chkNumeric(this.value,this.id)">
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group" >Permanent City Name: </div>
                            </div>
                            <div class="col-md-8 col-sm-12">
                            <div class="form-group">                          
                            <select class="form-control" id="permanent_city" name="permanent_city">
                            <option value="">--Select City--</option>
                            <?php  $sqlper_city =  mysql_query("SELECT * FROM  city where state_name='".$result['permanent_state']."' and status=1");
                             while($resper_city=mysql_fetch_array($sqlper_city)){
                            ?>
                            <option value="<?php echo $resper_city['id']; ?>" <?php if($result['permanent_city']==$resper_city['id']) {?> selected="selected" <?php } ?>><?php echo $resper_city['city_name']; ?></option>
                                   <?php }	?>
                            </select>                            
                            </div>
                            </div>
                            
           </div>                     
                                
                                
                       </div>
                       
                       
                       <div class="col-md-12">
		<div class="form-group"><label> Employee Salary Details :</label></div>  
   <div class="col-md-6 col-sm-6">
     <div class="col-md-4 col-sm-12">
     <div class="form-group" >Gross Salary : </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
          <input class="form-control" type="text" name="gross_salary" id="gross_salary" value="<?php echo $result['gross_salary']; ?>" onkeyup="createSalary(),chkNumeric(this.value,this.id);" >
        </div>
    </div>
    
    <div class="col-md-4 col-sm-12">
     <div class="form-group" >Paid Leave : </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
          <input class="form-control" type="text" name="paid_leave" id="paid_leave" value="<?php if($result) { echo $result['paid_leave'];} else { echo '0';} ?>">
        </div>
    </div>
                            
    <div class="col-md-4 col-sm-12">
     <div class="form-group" >Basic Salary : </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
          <input class="form-control" type="text" name="basic_salary" id="basic_salary" value="<?php echo $result['basic_salary']; ?>" readonly="readonly">
        </div>
    </div>                        
                            
     <div class="col-md-4 col-sm-12">
     <div class="form-group" >HRA : </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
         <input class="form-control" type="text" name="hra" id="hra" value="<?php echo $result['special']; ?>" readonly="readonly">
        </div>
    </div>                           
     </div>
     <div class="col-md-6 col-sm-6">                           
   <div class="col-md-4 col-sm-12">
     <div class="form-group" >Special Allowance :  </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
         <input class="form-control" type="text" name="special" id="special" value="<?php echo $result['special']; ?>"  onkeyup="createSalary();" >
        </div>
    </div> 

                        
       <div class="clearfix"></div>            
     <div class="col-md-4 col-sm-12">
     <div class="form-group" >Medical Reimbursement :  </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
        <input class="form-control" type="text" name="medical" id="medical" value="<?php echo $result['medical']; ?>"  onkeyup="createSalary();" >
        </div>
    </div> 
    <div class="clearfix"></div> 
    <!--                          
    <div class="col-md-2 col-sm-12">
     <div class="form-group" >Incentive :  </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
         <input class="form-control" type="text" name="incentive" id="incentive" value="<?php echo $result['incentive']; ?>"  onkeyup="createSalary();" >
        </div>
    </div> -->                 
       <div class="clearfix"></div>                
    <div class="col-md-4 col-sm-12">
     <div class="form-group" >Conveyance :  </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
          <input class="form-control" type="text"  name="ta" id="ta" value="<?php echo $result['ta']; ?>"  onkeyup="createSalary();" >
        </div>
    </div>
    
      </div>  
  <!--  <div class="col-md-2 col-sm-12">
     <div class="form-group" >Others : </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
        <input class="form-control" type="text" name="others" id="others" value="<?php echo $result['others']; ?>" onkeyup="createSalary();"  >
        </div>
    </div>  -->
         
      
      </div>   
      <div class="clearfix"></div>
        
                       <div class="col-md-12">
		<div class="form-group"><label> Deduction / Month :</label></div> 
             <div class="col-md-6 col-sm-6">   
     <div class="col-md-4 col-sm-12">
     <div class="form-group" >Provident Fund :  </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
        <input class="form-control" type="text" name="pf_detuction" id="pf_detuction" value="<?php echo $result['pf_detuction']; ?>"  onkeyup="createSalary();" >
        </div>
    </div> 
          
     <div class="col-md-4 col-sm-12" style="display:none;">
     <div class="form-group" >Professional Tax :  </div>
    </div>
    <div class="col-md-8 col-sm-12" style="display:none;">
        <div class="form-group">
        <input class="form-control" type="text" name="professional_tax" id="professional_tax" value="0<?php //echo $result['professional_tax']; ?>"  onkeyup="createSalary();" >
        </div>
    </div> 
           </div>             
                  <div class="col-md-6 col-sm-6">         
               <div class="col-md-4 col-sm-12">
                 <div class="form-group" >Net Salary Payable : </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="form-group">
                  <input class="form-control" type="text" name="month_salary_pay" id="month_salary_pay" value="<?php echo $result['month_salary_pay']; ?>">
                    </div>
                </div>                             
                 
    </div>
     
    
    
    
             </div>
                       
                       

                    <div class="col-md-12" align="left" style="margin-bottom:15px;">
   <br>

                      <button type="submit" class="btn btn-primary"><?php if($results[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>

                     <!-- <button type="reset" class="btn btn-default">Reset </button>--> 
                     </div>  
                     <input type="hidden" name="total_month_deduction" id="total_month_deduction" value="<?php echo $result['total_month_deduction']; ?>"/>
                      <input type="hidden" name="control" value="hrm_registration"/>
                      <input type="hidden" name="edit" value="1"/>

                      <input type="hidden" name="task" value="save"/>

                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />

                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />

                      <input class="form-control" type="hidden" name="utype" id="utype" value="Employee">

           </form>

					</div>

				</div><!--/.panel-->

		

        </div> 

<script type="text/javascript">
function createSalary(){
	
	var gross_salary = document.getElementById('gross_salary').value?document.getElementById('gross_salary').value:0;
  	
	
	  if(gross_salary!='' || gross_salary!=0){ 
	   var basic_salary  = Number((gross_salary*40)/100);
	  document.getElementById('basic_salary').value = basic_salary;
	  
	  var hra  = Number((basic_salary*40)/100);
	  document.getElementById('hra').value = hra;
	  
	  //var remain_amount = Number((gross_salary*37.5)/100);
	  var remain_amount = Number((gross_salary*20)/100);
	
	 //alert(remain_amount);
	  var medical  = 1250;
	  document.getElementById('medical').value = medical;
	  
	  var ta  = 1000;
	  document.getElementById('ta').value = ta;
	  
	   var pf = Math.round(Number((basic_salary*12)/100));	  
	  document.getElementById('pf_detuction').value = pf;
	  
	  var professional_tax = document.getElementById('professional_tax').value?Number(document.getElementById('professional_tax').value):Number(0);	  
	  
	  var totRemain = medical+ta;
	  document.getElementById('special').value = Math.round(remain_amount-totRemain-professional_tax);
	   
	  var total_month_deduction  = pf+professional_tax;
	 // alert(total_month_deduction);
	   document.getElementById('total_month_deduction').value = Math.round(pf+professional_tax);	 
	 //  pro_tax();
	 
	 document.getElementById('month_salary_pay').value = Math.round(gross_salary-pf-professional_tax);
	 
	  }
	  else{
		  
		document.getElementById('basic_salary').value=0;
		document.getElementById('hra').value=0;
		document.getElementById('special').value=0;
		document.getElementById('ta').value=0;
		document.getElementById('pf_detuction').value=0; 
		document.getElementById('medical').value=0;  
		document.getElementById('professional_tax').value=0; 
		document.getElementById('total_month_deduction').value=0; 
		document.getElementById('month_salary_pay').value=0;   
		  
	  }
	
}

function sum(){
	
}



function sum(str)

{

//alert(str);	

  var basic_salary = document.getElementById('basic_salary').value;

	var hra = document.getElementById('hra').value;

	var special = document.getElementById('special').value;

	var education = document.getElementById('education').value;

	var ta = document.getElementById('ta').value;

        var fitness = document.getElementById('fitness').value;

	 var medical = document.getElementById('medical').value; 

	 

	 var total = Number(hra) + Number(special) + Number(education) + Number(ta) + Number(fitness) + Number(medical) + Number(basic_salary);

	  document.getElementById('total').value =total;

	/*  if( basic_salary > total)

	  {

		 // alert('Basic Salary Is GreaterThan Total Salary');

		  document.getElementById('total').value =''; 

	  }

	  elseif(basic_salary < total)

	  {

		  alert('Basic Salary Is LessThan Total Salary');

		  document.getElementById('total').value =''; 

	  }

	  */

}



function validation() {	

var chk=1;

if(document.getElementById("name").value == '') {

chk = 0;

document.getElementById('name').focus();

document.getElementById('name').style.borderColor="red";} 

else if (!isletter(document.getElementById("name").value)) {

chk = 0;

document.getElementById('name').focus();

document.getElementById('name').style.borderColor="red";}

else {

document.getElementById('name').style.border="1px solid #ccc";}



if(document.getElementById("password").value == '') {

chk = 0;

document.getElementById('password').focus();

document.getElementById('password').style.borderColor="red";

} else {

document.getElementById('password').style.border="1px solid #ccc";

}



if(document.getElementById("address").value == '') {

chk = 0;

document.getElementById('address').focus();

document.getElementById('address').style.borderColor="red";

}

else {

document.getElementById('address').style.border="1px solid #ccc";

}



if(document.getElementById("dob").value == '') {

chk = 0;

document.getElementById('dob').focus();

document.getElementById('dob').style.borderColor="red";

}

else {

document.getElementById('dob').style.border="1px solid #ccc";

}



if(document.getElementById("email").value == '') {

chk = 0;

document.getElementById('email').focus();

document.getElementById('email').style.borderColor="red";

} else if (!isEmail(document.getElementById("email").value)) {

chk = 0;

document.getElementById('email').focus();

document.getElementById('email').style.borderColor="red";

} else {

document.getElementById('email').style.border="1px solid #ccc";

}







	

if(chk){

	  // $('#form').submit();

	alert("Employee Registration Successful ..!!");

return true;}

else{

	return false;	}	

	}



</script>


<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
$('#dob').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#joining_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});


</script>


<script>

function area_level(str){

		reportingHead(str);

		var xmlhttp; 

		if (window.XMLHttpRequest)

		{ // code for IE7+, Firefox, Chrome, Opera, Safari 

		xmlhttp=new XMLHttpRequest();

		}

		else

		{ // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange=function()

		{ 

		if (xmlhttp.readyState==4)

		{

		document.getElementById("level").value=xmlhttp.responseText; 

		//alert(xmlhttp.responseText);

		}

		}

		xmlhttp.open("GET", "script/popup_scripts/level.php?id="+str, true);

		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);

		xmlhttp.send();

			

           }



function reportingHead(str){ //alert(str)

		

		var xmlhttp; 

		if (window.XMLHttpRequest)

		{ // code for IE7+, Firefox, Chrome, Opera, Safari 

		xmlhttp=new XMLHttpRequest();

		}

		else

		{ // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange=function()

		{ 

		if (xmlhttp.readyState==4)

		{

		document.getElementById("reporting").innerHTML=xmlhttp.responseText; 

		//alert(xmlhttp.responseText);

		}

		}

		xmlhttp.open("GET", "script/popup_scripts/reportingHead.php?id="+str, true);

		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);

		xmlhttp.send();

			

           }





</script> 



<!--****************************************Form Validation********************************************-->





<!--****************************************Form Validation********************************************-->

<script>

function clientAjax(srno) {



		//alert(srno);

		var str1 = document.getElementById('post_name').value;

		

		var city = document.getElementById('city_name').value;

		//alert(str1);

		var xmlhttp; 

		if (window.XMLHttpRequest)

		{ // code for IE7+, Firefox, Chrome, Opera, Safari 

		xmlhttp=new XMLHttpRequest();

		}

		else

		{ // code for IE6, IE5

		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange=function()

		{ 

		if (xmlhttp.readyState==4)
		{//alert(str1);
		document.getElementById("daily_allownce").value=xmlhttp.responseText; 
	//alert(xmlhttp.responseText);

		}

		}
        xmlhttp.open("GET", "./script/popup_scripts/client.php?id="+str1+"&city="+city, true);

		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
}




function present_city1(str1) {

		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		document.getElementById("present_city").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/city.php?id="+str1, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}



function permanent_state1(str1) {

		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		document.getElementById("permanent_city").innerHTML=xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/city.php?id="+str1, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
		
}
</script>  