<?php

include('./configuration.php'); 

$project =   mysql_num_rows(mysql_query("SELECT * FROM `project` WHERE `status` = '1'"));

$state =  mysql_num_rows(mysql_query("SELECT * FROM `project_plot` where 1 ORDER BY `id` ASC "));

$city =  mysql_num_rows(mysql_query("SELECT * FROM `city` WHERE `status` = '1' ORDER BY `id` DESC "));

$query1 = "SELECT * FROM `category` WHERE `status` = '1' ORDER BY `id` DESC ";
$run1 = mysql_query($query1);
$provider1 = mysql_num_rows($run1);

$dt = date("Y-m-d");
$diffdate  = date( "Y-m-d", strtotime( "$dt +2 month" ) ); 

$query2 = "SELECT * FROM `sub_category` where `status` = '1' ORDER BY `id` DESC ";
$run2 = mysql_query($query2);
$provider2 = mysql_num_rows($run2);

$dealer=mysql_num_rows(mysql_query("select * from dealer where `status` = '1'"));

$farmer=mysql_num_rows(mysql_query("select * from farmer where `status` = '1'"));

$sold=mysql_num_rows(mysql_query("SELECT `customer_id` FROM `project_plot` WHERE status=2 "));
//echo "<pre>"; print_r($_SESSION);

$account=mysql_num_rows(mysql_query("select * from account where `status` = '1'"));
$day_book=mysql_num_rows(mysql_query("select * from day_book where 1"));
// print_r($_SESSION);
?>

<div class="panel panel-default">
	<div class="col-md-6" style="padding:0px;">

	</div>

	<?php if($_SESSION['utype']=='Administrator') { ?> 
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-blue">
						<div class="inner">
							<h3><?php echo $project = $project ? $project : 0; ?></h3>
							<p>Projects</p>
						</div>
						<div class="icon">
							<i class="fa fa-industry"></i>

						</div>
						<a href="index.php?control=project&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<h3><?php echo $state = $state ? $state : 0; ?></h3>
							<p>Plots</p>
						</div>
						<div class="icon">
							<i class="fa fa-pie-chart"></i>
						</div>
						<a href="index.php?control=project_plot&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->
				




				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-orange">
						<div class="inner">
							<h3><?php echo $city= $city ? $city : 0; ?></h3>
							<p>City Master</p>
						</div>
						<div class="icon">
							<i class="fa fa-user-plus"></i>

						</div>
						<a href="index.php?control=regional_hierarchy&task=show_city" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->



				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-red">
						<div class="inner">
							<h3><?php echo $dealer= $dealer ? $dealer : 0; ?></h3>
							<p>Dealer Master</p>
						</div>
						<div class="icon">
							<i class="fa fa-user-plus"></i>		
						</div>
						<a href="index.php?control=dealer&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->


				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-red">
						<div class="inner">
							<h3><?php echo $farmer = $farmer ? $farmer : 0; ?></h3>
							<p>Farmers</p>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>

						</div>
						<a href="index.php?control=farmer&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->


				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-blue">
						<div class="inner">
							<h3><?php echo $sold = $sold ? $sold : 0; ?></h3>
							<p>Plot Booked</p>
						</div>
						<div class="icon">
							<i class="fa fa-area-chart"></i>

						</div>
						<a href="index.php?control=plot_booking&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<h3><?php echo $account = $account ? $account : 0; ?></h3>
							<p>Account</p>
						</div>
						<div class="icon">
							<i class="fa fa-user-plus"></i>
						</div>
						<a href="index.php?control=account&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-orange">
						<div class="inner">
							<h3><?php echo $day_book= $day_book ? $day_book : 0; ?></h3>
							<p>Daybook</p>
						</div>
						<div class="icon">
							<i class="fa fa-pie-chart"></i>

						</div>
						<a href="index.php?control=daybook" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

			</div><!-- /.row -->

		</section>
	<?php } else{ ?>

		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-blue">
						<div class="inner">
							<h3><?php echo $project = $project ? $project : 0; ?></h3>
							<p>Projects</p>
						</div>
						<div class="icon">
							<i class="fa fa-industry"></i>

						</div>
						<a href="index.php?control=project&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>



<?php if($_SESSION['adminid']!='3') { ?>

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-red">
						<div class="inner">
							<h3><?php echo $dealer= $dealer ? $dealer : 0; ?></h3>
							<p>Dealer Master</p>
						</div>
						<div class="icon">
							<i class="fa fa-user-plus"></i>		
						</div>
						<a href="index.php?control=dealer&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

<?php } ?>


				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-blue">
						<div class="inner">
							<h3><?php echo $sold = $sold ? $sold : 0; ?></h3>
							<p>Plot Booked</p>
						</div>
						<div class="icon">
							<i class="fa fa-area-chart"></i>

						</div>
						<a href="index.php?control=plot_booking&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

			</div><!-- /.row -->

		</section>

	<?php	} ?>
</div>
