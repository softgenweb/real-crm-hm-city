<?php foreach($results as $result) { }  ?>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add City</h3>
	</div>
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=regional_hierarchy&task=show_city"><i class="fa fa-list" aria-hidden="true"></i> City List</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit City</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add City</li>
          <?php } ?>
          </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>              
	<div class="panel-body">
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >

			<div class="row col-md-12">

				<div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
                    
                    <div class="col-md-3">
                    <div class="form-group center_text">
                    Country Name:
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    
                    <select name="country_id" id="country_id" class="form-control" onchange='state_change(this.value);'  required="">
                    <option value="">Select Country</option>
                    <?php $sel=mysql_query("select * from country where 1");
                    while($select=mysql_fetch_array($sel))
                    {
                    if($select['status'] == '1') { ?>
                    <option value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
                    <?php } else { ?>  
                    <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>     
                    <?php } } ?>
                    </select>
                    <span id="msgcountry_id" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-3">
                    <div class="form-group center_text">
                    State Name:
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
                    
                    <select name="state_id" id="state_id" class="form-control"  required="">
                    <option value="">Select State</option>
                    <?php $sel=mysql_query("select * from state where 1");
                    while($select=mysql_fetch_array($sel))
                  if($result)  {
                    if($select['status'] == '1') { ?>
                    <option value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
                    <?php } else { ?>  
                    <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>     
                    <?php } } ?>
                    </select>
                    <span id="msgstate_id" style="color:red;"></span>
                    </div></div>
                    <div class="clearfix"></div>
                    
                   	<div class="col-md-3">
                    <div class="form-group center_text">
                    City Name:
                    </div></div>
                    <div class="col-md-6"><div class="form-group">
          <input type="text" class="form-control" name="city_name" id="city_name" placeholder="Enter City Name" value="<?php echo $result['city_name']; ?>"/>
          <span id="msgcity_name" style="color:red;"></span>
                    </div></div>
                     <div class="clearfix"></div>
       
				<div class="col-md-9 col-sm-8 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					</div>                  

					<input type="hidden" name="control" value="regional_hierarchy"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save_city"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

				</div>

			</div>

		<!--account details ends here-->

		</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     




<script type="text/javascript">

function validation()

{   
	var chk=1;
	
		if(document.getElementById('country_id').value == '') { 
			document.getElementById('msgcountry_id').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgcountry_id').innerHTML = "";
		}
		if(document.getElementById('state_id').value == '') { 
			document.getElementById('msgstate_id').innerHTML = "*Required field.";
			chk=0;
		}
		
		else {
			document.getElementById('msgstate_id').innerHTML = "";
		}
		
		if(document.getElementById('city_name').value == '') { 
			document.getElementById('msgcity_name').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!isletter(document.getElementById('city_name').value)) { 
			document.getElementById('msgcity_name').innerHTML = "*Enter Valid City Name.";
			chk=0;
		}
		else {
			document.getElementById('msgcity_name').innerHTML = "";
		}
		
		/*if(document.getElementById('mobile').value == '') { 
			document.getElementById('msgmobile').innerHTML = "*Required field.";
			chk=0;
		}
		else if(!numericWithoutPoint(document.getElementById('mobile').value)) { 
			document.getElementById('msgmobile').innerHTML = "*Enter Valid Number.";
			chk=0;
		}
		else {
			document.getElementById('msgmobile').innerHTML = "";
		}
		
		
		if(document.getElementById('address').value == '') { 
			document.getElementById('msgaddress').innerHTML = "*Required field.";
		chk=0;
		}
		
		else {
			document.getElementById('msgaddress').innerHTML = "";
		}*/


			if(chk)
			 {			
				return true;		
			}		
			else 
			{		
				return false;		
			}	
		
		
		}

</script>

<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   







