<?php foreach($results as $result) { }  ?>
<style>
.select2-choice {
    height: 33px !important;
    padding-top: 2px !important;
    background:#fff !important;
    border:1px solid #ccc !important;
}


.thWidthSr{
	width:4%;
}

.thWidthField{
	width:8%;
}

.selectWidthSr{
	width:60px;
	 margin-left: 30px;
}
/*.selectWidthField{
	width:10%;
}*/

.select-1 {
  margin: 0 12px 0 22px;
  width: 139px;
   height: 34px;
  padding: 6px 12px;
   border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}

.select-1 option{
padding:5px;	
}

.select-2 option{
padding:5px;	
}
.select-2 {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #eee;
  border-radius: 5px;
  height: 34px;
  margin: 0 10px 0 13px;
  padding: 6px 12px;
  width: 90px;
  border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	
}
.remark {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #eee;
  border-radius: 5px;
  height: 34px;
  margin: 0 10px 0 0;
  padding: 6px 12px;
  width: 56px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
 
}
.remark1 {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #eee;
  border-radius: 5px;
  height: 34px;
  margin: 0 10px 0 42px;
  padding: 6px 12px;
  width: 56px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
  position:relative;
	top:5px;
}

.my-form td {
  padding: 5px;
}

</style>
<!--<script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>-->
<script type="text/javascript" src="assets/javascript/add_button.js"></script>

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add-New Cr Expense</h3>
	</div>
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=daybook&task=cr_expense"><i class="fa fa-list" aria-hidden="true"></i> Day Book</a></li>
            <?php if($result!='') {?>
           
             <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit New Cr Expense</li>
         <?php } else { ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add New Cr Expense</li>
          <?php } ?>
          </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>              
	<div class="panel-body">
		<form name="form" method="post"  action="index.php" enctype="multipart/form-data" onsubmit="return validation();">
            <div class="my-form" style="margin-top:35px;">
               <div class="col-md-12">
                        <?php if($result['id']=='') { ?><div class="col-md-1" align="left"><b>Sr. No.</b></div><?php } ?>
                        <div class="col-md-1"  align="left"><b>Expense Type</b></div>
                       
                        <div class="col-md-2"  align="left"><b>Expense Name</b></div>
                    
                        <div class="col-md-2"  align="left" style="margin-left: 8px;"><b>Transaction Type</b></div>
                        <div id="hideType" style="display:none;">
                        <div class="col-md-1"  align="left"><b>Transfer Type</b></div>
                        <div class="col-md-1"  align="left"><b>Cheque No.</b></div>
                        </div>
                        <div class="col-md-1"  align="left"><b>Amount</b></div>
                        <div class="col-md-1"  align="left" style="margin-left: 34px;" ><b>Date</b></div>
                        <div class="col-md-1"  align="left"  style="margin-left: 50px;" ><b>Remarks</b></div>
                
                 </div>  
                     
            <div class="col-md-12">                
                <?php if($result['id']=='') { ?><div class="col-md-1" ><label for="box1"><span class="box-number">1</span></label></div><?php } ?> 
                
                <div class="col-md-1" >
                <select class="form-control search" name="expense_type" id="expense_type" style="width:200px !important;" onchange="show_value(this.value)" required>
                    <option value="">-Expense Type-</option>
                     <option value="farmer" <?php if($_REQUEST['expense_type']=='farmer') {?> selected="selected" <?php } ?>>Farmer</option>
                      <option value="customer" <?php if($_REQUEST['expense_type']=='customer') {?> selected="selected" <?php } ?>>Customer</option>
                       <option value="dealer" <?php if($_REQUEST['expense_type']=='dealer') {?> selected="selected" <?php } ?>>Dealer</option>
                    <option value="expense" <?php if($_REQUEST['expense_type']=='expense') {?> selected="selected" <?php } ?>>Expense</option>
                    
                    </select>
                </div>
                
                <div class="col-md-2" >
                <select class="form-control" name="expense_id1" id="expense_id1" required>
                <option value="">-Select Expense-</option>
                <?php 
                $sq="select * from expense where status=1 order by expense_name ASC";
                $que=  mysql_query($sq);
                while($fet=  mysql_fetch_array($que)){
                ?>
                <option value="<?php echo $fet['id']; ?>" <?php if($result['expense_id']==$fet['id']) {?> selected="selected" <?php } ?>><?php echo $fet['expense_name']; ?></option>
                <?php } ?>
                </select>
                </div>
                
                <div class="col-md-2" >
                <select class="form-control" name="transaction_type1" id="transaction_type1" required  onchange="chk_transaction(this.value,1);" style="margin-left: 8px;">
                <option value="">-Select-</option>
                
                <option value="Cash" <?php if($result['transaction_type']=="Cash") {echo 'selected'; } ?>>Cash</option>
                <!--<option value="2" <?php if($result['transaction_type']==2) {echo 'selected'; } ?>>Bank</option>-->
                <option disabled style="color:black !important;">--Bank--</option>
                <?php
                $sq1="select * from account where status=1";
                $qu1=mysql_query($sq1);
                while($fet11=mysql_fetch_array($qu1))
                { ?>
                <option value="<?php echo $fet11['id']; ?>"><?php echo $fet11['account_no']." (".strtoupper($fet11['bank_name']).")"; ?></option>
                <?php } ?>
                </select>
                </div>
                
                
             <div id="chkbank1"></div>
                
                <div class="col-md-1" style="width: 90px;">
                 <input class="form-control"  type="text" style="width: 90px;margin-left: 5px;"  name="amount1" id="amount1" value="<?php echo $result['amount']; ?>" required onkeyup="chkNumeric(this.value,this.id)">
                </div> &nbsp;
                
                <div class="col-md-1" >
                 <input class="form-control"  style="width: 90px;margin-left: 34px;" type="text" name="expense_date1" id="expense_date1" value="<?php echo $result['expense_date']?$result['expense_date']:date('Y-m-d'); ?>" required onclick="datetype();">
                </div> &nbsp;
                
                <div class="col-md-1" >
                    <textarea class="remark" style="margin-left: 90px;" type="text" name="remark1" id="remark1" rows="1"><?php echo $result['remark']; ?></textarea>
                </div>
                
                <div class="col-md-1" ></div>
                
             </div>             
       
		<p class="text-box"> </p>
		<div class="col-md-12" align="left">
      <center>
		<br>


		<?php if($result['id']=='') { ?>
		<input type="submit" class="btn btn-primary butoon_brow" name="submit" value="Submit" id="submit" onclick="disableEnable();" >
		<?php } else { ?>
        
		<input type="submit" class="btn btn-primary butoon_brow" name="submit" value="Update" id="submit" onclick="disableEnable();">
		<?php } ?>
		<input type="button" class="btn btn-default butoon_brow" value="Reset" id="btnReset" onclick="disableEnableRefresh();" >

		<?php if($result['id']=='') { ?>
		<img src="images/add-button.png" width="30" height="30" style="cursor:pointer;" class="add_box1" type="button" name="add_textbox" id="add_textbox" title="Add More">
		<?php } ?>
 
            <input type="hidden" name="control" value="daybook"/>
            <input type="hidden" name="edit" value="1"/>
            <input type="hidden" name="task" value="cr_expense_save"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['adminid']; ?>">
            <input type="hidden" id="number_count" name="number_count" value="1">
            

			</div>
    </center>
			</div>
			

            
			</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     





<script type="text/javascript">
jQuery(document).ready(function($){   
    $('.my-form .add_box1').click(function(){
        var n = $('.text-box').length + 1;
	$('#number_count').val(n);	
        if( 20 < n ) {
            alert('Maximum Textbox is Reached. Please Stop it!');
            return false;
        }
		  
		  
                        
                var box_html = $('<p class="text-box"><label for="box' + n + '" class="selectWidthSr"><span class="box-number">' + n + '</span></label><select class="form-control1 select-1"  name="expense_id'+n+'" id="expense_id'+n+'" required ><option value="">-Select-</option><?php $sq= mysql_query("select * from expense where status=1 order by expense_name ASC");while($fet=  mysql_fetch_array($sq)){?><option value="<?php echo $fet['id']; ?>" <?php if($result['expense_id']==$fet['id']) {?> selected="selected" <?php } ?>><?php echo $fet['expense_name']; ?></option><?php } ?></select><select class="form-control1 select-1"  name="transaction_type'+n+'" id="transaction_type'+n+'" required  onchange="chk_transaction(this.value,'+n+');"><option value="">-Select-</option><option value="Cash" <?php if($result['transaction_type']=="Cash") {echo 'selected'; } ?>>Cash</option><option disabled style="color:black !important;">--Bank--</option><?php $sq1=mysql_query("select * from account where status=1"); while($fet11=mysql_fetch_array($sq1)){ ?><option value="<?php echo $fet11['id']; ?>"><?php echo $fet11['account_no']." (".strtoupper($fet11['bank_name']).")"; ?></option><?php } ?></select><span id="chkbank'+n+'"></span><input class="form-control1  select-2" type="text" name="amount'+n+'" id="amount'+n+'" value="<?php echo $result['amount']; ?>" required onkeyup="chkNumeric(this.value,this.id)"><input class="form-control1  select-2" type="text" name="expense_date'+n+'" id="expense_date'+n+'" value="<?php  echo $result['expense_date']?$result['expense_date']:date('Y-m-d'); ?>" required onclick="datetype();"><textarea class="form-control1 remark1" type="text" name="remark'+n+'" id="remark'+n+'" rows="1"><?php echo $result['remark']; ?></textarea><img src="images/removes.jpeg" width="30" height="30" style="cursor:pointer;" class="remove-box1" type="button" name="add_textbox" id="add_textbox" title="Remove"></p>');
		
        box_html.hide();
        $('.my-form p.text-box:last').after(box_html);
        box_html.fadeIn('slow');
        return false;
    });
    $('.my-form').on('click', '.remove-box1', function(){
        //$(this).parent().css( 'background-color', '#FF6C6C' );
        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );//alert(text);
		$('#number_count').val(index+ 1);
            });
			
			/* $('.boxes'+n).each(function(index){ 
                $(this).text( index + 1 );
            });*/
        });
        return false;
    });

});
</script>


 
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<!--<script src="assets/date_picker/jquery.js"></script>-->
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
function datetype(){


for(i=0; i<20; i++) { 
 


$('#expense_date'+i).datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

}
}




function chk_transaction(type,srNo) { 
//alert(type);
   if(type=="Cash" || type==''){
	   document.getElementById("hideType").style.display = "none"; 
	   document.getElementById("bank_type"+srNo).value = ""
	   document.getElementById("cheque_dd_no"+srNo).value = ""
	  
   }else{
	   document.getElementById("hideType").style.display = "block";
	   //document.getElementById("chkbank"+srNo).style.display ="block;"; 
	 
   }
	  
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				
				//document.getElementById("taxtype"+srNo).innerHTML=xmlhttp.responseText;
				document.getElementById("chkbank"+srNo).innerHTML=xmlhttp.responseText;
				//alert(xmlhttp.responseText);
			}
		}
		//alert(unit_id);
		xmlhttp.open("GET","script/bank_expense.php?srNo="+srNo+"&type="+type,true);
		//xmlhttp.open("GET","script/ajaxUnitSize.php?unit_id="+unit_id,true);
		xmlhttp.send();
	
}



</script>



<script type="text/javascript">



/****************************************Form Validation********************************************/

function validation()
{	
var chk=1;

var tot=document.getElementById('number_count').value;
var x;





for(x=1;x<=tot;x++){

if(document.getElementById("expense_id"+x).value == '') {
chk = 0;
document.getElementById('expense_id'+x).focus();
document.getElementById('expense_id'+x).style.borderColor="red";
}
else {
document.getElementById('expense_id'+x).focus();
document.getElementById('expense_id'+x).style.border="1px solid #ccc";
}

if(document.getElementById("transaction_type"+x).value == '') {
chk = 0;
document.getElementById('transaction_type'+x).focus();
document.getElementById('transaction_type'+x).style.borderColor="red";
}
else {
document.getElementById('transaction_type'+x).focus();
document.getElementById('transaction_type'+x).style.border="1px solid #ccc";
}

if(document.getElementById("amount"+x).value == '') {
chk = 0;
document.getElementById('amount'+x).focus();
document.getElementById('amount'+x).style.borderColor="red";
}
else {
document.getElementById('amount'+x).focus();
document.getElementById('amount'+x).style.border="1px solid #ccc";
}

if(document.getElementById("transaction"+x).value == '') {

chk = 0;
document.getElementById('transaction'+x).focus();
document.getElementById('transaction'+x).style.borderColor="red";
}
else {
document.getElementById('transaction'+x).focus();
document.getElementById('transaction'+x).style.border="1px solid #ccc";
}

if(document.getElementById("remark"+x).value == '') {
chk = 0;
document.getElementById('remark'+x).focus();
document.getElementById('remark'+x).style.borderColor="red";
}
else {
document.getElementById('remark'+x).focus();
document.getElementById('remark'+x).style.border="1px solid #ccc";
}

}

	
if(chk){
  // $('#form').submit();
 //alert("ExPENSES ADDED SUCCESSFULLY");
return true;
}else{
return false;	
}	
	
}

function show_value(str)
{ // alert(str);
	var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
			document.getElementById("expanse_data").innerHTML = xmlhttp.responseText; 
			 
        //initMultiSelect();
			
		}
		}
		xmlhttp.open("GET", "script/show_expense_data.php?id="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
}
</script>


<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
</script>
   







