<?php foreach($results as $result) { }  ?>
<style>
.select2-choice {
    height: 33px !important;
    padding-top: 2px !important;
    background:#fff !important;
    border:1px solid #ccc !important;
}
      .select2-container .select2-selection--single{
      height: 34px !important;
      }

.thWidthSr{
  width:4%;
}

.thWidthField{
  width:8%;
}

.selectWidthSr{
  width:60px;
   margin-left: 30px;
}
/*.selectWidthField{
  width:10%;
}*/

.select-1 {
  margin: 0 12px 0 22px;
  width: 139px;
   height: 34px;
  padding: 6px 12px;
   border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}

.select-1 option{
padding:5px;  
}

.select-2 option{
padding:5px;  
}
.select-2 {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #eee;
  border-radius: 5px;
  height: 34px;
  margin: 0 10px 0 13px;
  padding: 6px 12px;
  width: 90px;
  border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
  
}
.remark {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #eee;
  border-radius: 5px;
  height: 34px;
  margin: 0 10px 0 0;
  padding: 6px 12px;
  width: 56px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
 
}
.remark1 {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #eee;
  border-radius: 5px;
  height: 34px;
  margin: 0 10px 0 42px;
  padding: 6px 12px;
  width: 56px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
  position:relative;
  top:5px;
}

.my-form td {
  padding: 5px;
}

</style>
<script type="text/javascript" src="assets/javascript/add_button.js"></script>
<link rel="stylesheet" href="template/realcrm/plugins/select2/select2.min.css">

	<div class="panel panel-default" >
		<div class="box-header">
		<h3 class="box-title">Add-New Dr Expense</h3>
		</div>
        <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
                <li><a href="index.php?control=daybook&task=dr_expense"><i class="fa fa-list" aria-hidden="true"></i> Day Book</a></li>
                <?php if($result!='') {?>           
                 <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit New Dr Expense</li>
                <?php } else { ?>
                <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add New Dr Expense</li>
              <?php } ?>
         </ol> 
              <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>              
	<div class="panel-body">
		<form name="form" method="post" autocomplete="off" action="index.php" enctype="multipart/form-data" onsubmit="return validation();">
           
            <div class="col-md-12 my-form" style="padding-left:0px">
               <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                        <th><div align="left">S.No.</div></th>
                        <th><div align="left">Expense Type</div></th>
                        <th><div align="left">Expense Name</div></th>
                        <th><div align="left">Transaction Type</div></th>
                        <th><div align="left">Transfer Type</div></th>
                        <th><div align="left">Cheque No.</div></th>
                        <th><div align="left">Amount</div></th>
                        <th><div align="left">Date</div></th>
                        <th><div align="left">Remarks</div></th>
                        <th><div align="left">Action</div></th>
                      </tr>
                    </thead>
               		<tbody id="add_items">
					  <tr>
                        <td><strong class="slno">1</strong></td>
                        <td><select class="form-control expense_type select2" style="width:130px" name="expense_type[]" id="expense_type1" onchange="show_value(this.value)" required>
                        <option value="">Expense Type</option>
                        <option value="farmer" <?php if($_REQUEST['expense_type']=='farmer') {?> selected="selected" <?php } ?>>Farmer</option>
                        <option value="customer" <?php if($_REQUEST['expense_type']=='customer') {?> selected="selected" <?php } ?>>Customer</option>
                        <option value="dealer" <?php if($_REQUEST['expense_type']=='dealer') {?> selected="selected" <?php } ?>>Dealer</option>
                        <option value="expense" <?php if($_REQUEST['expense_type']=='expense') {?> selected="selected" <?php } ?>>Expense</option>
                        </select></td>
                        <td><select class="form-control search select2 expense_id" style="width:135px" name="expense_id[]" id="expense_id1" required>
                        <option value="">Select Expense</option>
                        </select></td>
                        <td><select class="form-control transaction_type"  name="transaction_type[]" id="transaction_type1" required  onchange="chk_transaction(this.value,1);" >
                        <option value="">-Select-</option>
                        <option value="Cash" <?php if($result['transaction_type']=="Cash") {echo 'selected'; } ?>>Cash</option>
                        <option disabled style="color:black !important;">--Bank--</option>
                        <?php $sq1="select * from account where status=1";$qu1=mysql_query($sq1); 
                        while($fet11=mysql_fetch_array($qu1)){ ?>
                        <option value="<?php echo $fet11['id']; ?>"><?php echo $fet11['account_no']." (".strtoupper($fet11['bank_name']).")"; ?></option>
                        <?php } ?></select></td>
                        <td><select class="form-control bank_type" onchange="bank_type(this.value,1);"  style="width:125px" name="bank_type[]" id="bank_type1" required="">
                        <option value="">Select</option>                        
                        <option value="Cheque">Cheque</option>
                        <option value="RTGS">RTGS</option>
                        <option value="NEFT">NEFT</option>
                        <option value="Cash-Deposit">Cash Deposit</option> 
                        <option value="Cash-Withdrawal">Cash Withdrawal</option>
                        </select></td>
                        <td><input class="form-control chk_number cheque_dd_no"  type="text" name="cheque_dd_no[]" id="cheque_dd_no1" value="<?php echo $result['cheque_dd_no']; ?>" required ></td>
                        <td> <input class="form-control chk_number amount"  type="text" name="amount[]" id="amount1" value="<?php echo $result['amount']; ?>" required></td>
                        <td><input class="form-control expense_date" type="text" name="expense_date[]" style="width:90px" id="expense_date1" value="<?php echo $result['expense_date']?$result['expense_date']:date('Y-m-d'); ?>" required onclick="datetype(1);"></td>
                        <td><textarea class="form-control remark" type="text" name="remark[]" id="remark1" rows="1"><?php echo $result['remark']; ?></textarea></td>
                        <td></td>
                     </tr>
                    </tbody>
                    </table>
       

		<div class="col-md-12" align="left">
      <center>
		<br>


		<?php if($result['id']=='') { ?>
		<input type="submit" class="btn btn-primary butoon_brow" name="submit" value="Submit" id="submitBtn" onclick="disableEnable();" >
		<?php } else { ?>
        
		<input type="submit" class="btn btn-primary butoon_brow" name="submit" value="Update" id="submitBtn" onclick="disableEnable();">
		<?php } ?>
		<input type="button" class="btn btn-default butoon_brow" value="Reset" id="resetBtn" onclick="disableEnableRefresh();" >

		<?php if($result['id']=='') { ?>
		<img src="images/add-button.png" width="30" height="30" style="cursor:pointer;" class="add_box1" type="button" name="add_textbox" id="add_textbox" title="Add More">
		<?php } ?>
 
            <input type="hidden" name="control" value="daybook"/>
            <input type="hidden" name="edit" value="1"/>
            <input type="hidden" name="task" value="dr_expense_save"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            
            <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['adminid']; ?>">           
            <input type="hidden" name="total_item"  id="total_item" value="1">
            

			</div>
    </center>
			</div>
			

            
			</form>
	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     





<script type="text/javascript">
jQuery(document).ready(function($){   
    $('.my-form .add_box1').click(function(){
         n = $('tbody#add_items tr').length +1;
	$('#total_item').val(n);	
        if( 20 < n ) {
            alert('Maximum Textbox is Reached. Please Stop it!');
            return false;
        }
		  
		  
                        
                var box_html = $('<tr><td><strong class="slno">'+n+'</strong></td><td ><select class="form-control expense_type select2" style="width:130px" name="expense_type[]" id="expense_type'+n+'" onchange="show_value1(this.value,'+n+')" required><option value="">Expense Type</option><option value="farmer" <?php if($_REQUEST['expense_type']=='farmer') {?> selected="selected" <?php } ?>>Farmer</option><option value="customer" <?php if($_REQUEST['expense_type']=='customer') {?> selected="selected" <?php } ?>>Customer</option><option value="dealer" <?php if($_REQUEST['expense_type']=='dealer') {?> selected="selected" <?php } ?>>Dealer</option><option value="expense" <?php if($_REQUEST['expense_type']=='expense') {?> selected="selected" <?php } ?>>Expense</option></select></td><td ><select class="form-control search select2 expense_id" style="width:135px" name="expense_id[]" id="expense_id'+n+'" required><option value="">Select Expense</option></select></td><td ><select class="form-control transaction_type"  name="transaction_type[]" id="transaction_type'+n+'" required  onchange="chk_transaction(this.value,'+n+');" ><option value="">-Select-</option><option value="Cash" <?php if($result['transaction_type']=="Cash") {echo 'selected'; } ?>>Cash</option><option disabled style="color:black !important;">--Bank--</option><?php $sq1="select * from account where status=1";$qu1=mysql_query($sq1);while($fet11=mysql_fetch_array($qu1)){ ?><option value="<?php echo $fet11['id']; ?>"><?php echo $fet11['account_no']." (".strtoupper($fet11['bank_name']).")"; ?></option><?php } ?></select></td><td ><select class="form-control bank_type" style="width:125px" onchange="bank_type(this.value,'+n+');" name="bank_type[]" id="bank_type'+n+'" required=""><option value="">Select</option><option value="Cheque">Cheque</option><option value="RTGS">RTGS</option><option value="NEFT">NEFT</option><option value="Cash-Deposit">Cash Deposit</option><option value="Cash-Withdrawal">Cash Withdrawal</option></select></td><td ><input class="form-control chk_number cheque_dd_no"  type="text" name="cheque_dd_no[]" id="cheque_dd_no'+n+'" value="<?php echo $result['cheque_dd_no']; ?>" required></td><td ><input class="form-control chk_number amount"  type="text" name="amount[]" id="amount'+n+'" value="<?php echo $result['amount']; ?>" required></td><td ><input class="form-control expense_date" type="text" name="expense_date[]" style="width:90px" id="expense_date'+n+'" value="<?php echo $result['expense_date']?$result['expense_date']:date('Y-m-d'); ?>" required onclick="datetype('+n+');"></td><td ><textarea class="form-control  remark" type="text" name="remark[]" id="remark'+n+'" rows="1"><?php echo $result['remark']; ?></textarea></td><td ><a href="javascript:;" class="remove" id="remove'+n+'"><i class="fa fa-minus-circle fa-2x"></i></a></td></tr>');
		 $('#add_items').append(box_html);
   		$('#total_item').val(n);  
		
		   $(".select2").select2();
		   
		   chk_transaction('',n);
   $('.remove').click(function(){
		
   $(this).closest('tr').remove();
    $('#total_item').val(($('tbody#add_items tr').length));
	 $('td strong.slno').text(function (i) { return i + 1; });
	 
	    /*==========================================*/
   $('td select.expense_type').each(function(a){
    $(this).attr('id','expense_type'+parseInt(a+1));
    $(this).attr('onchange','show_value1(this.value,'+parseInt(a+1)+')');
   });
   
	    /*==========================================*/
   $('td select.expense_id').each(function(a){
    $(this).attr('id','expense_id'+parseInt(a+1));   
   });
       /*==========================================*/
   $('td select.transaction_type').each(function(a){
    $(this).attr('id','transaction_type'+parseInt(a+1));
    $(this).attr('onchange','chk_transaction(this.value,'+parseInt(a+1)+')');
   });
    /*==========================================*/
   $('td select.bank_type').each(function(a){
    $(this).attr('id','bank_type'+parseInt(a+1));
   });
      /*==========================================*/
   $('td input.cheque_dd_no').each(function(a){
    $(this).attr('id','cheque_dd_no'+parseInt(a+1));
      });
    /*==========================================*/
   $('td input.amount').each(function(a){
    $(this).attr('id','amount'+parseInt(a+1));
      });
    /*==========================================*/
   $('td input.expense_date').each(function(a){
    $(this).attr('id','expense_date'+parseInt(a+1));
	 $(this).attr('onclick','datetype(this.value,'+parseInt(a+1)+')');
      });
    /*==========================================*/
   $('td input.remark').each(function(a){
    $(this).attr('id','remark'+parseInt(a+1));
      });
   });
   
    });
	
    
});
</script>


 
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<!--<script src="assets/date_picker/jquery.js"></script>-->
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
function datetype(n){

$('#expense_date'+n).datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y/m/d',
  scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});


}

$(document).ready(function(){
	$('#bank_type1').prop('readonly',true).prop('required',false);
//	$('#bank_type1 option').prop('disabled',true);
		$('#bank_type1 option').hide();
	$('#cheque_dd_no1').prop('readonly',true).prop('required',false);
  });


function chk_transaction(type,srNo) { 

   if(type=="Cash" || type==''){
	   $('#bank_type'+srNo).prop('readonly',true).prop('required',false).val('');
	   $('#bank_type'+srNo+' option').hide();
	  $('#cheque_dd_no'+srNo).prop('readonly',true).prop('required',false).val('');
	  
   }else{
	  $('#bank_type'+srNo).prop('readonly',false).prop('required',true);
	  	$('#bank_type'+srNo+' option').show();
	  $('#cheque_dd_no'+srNo).prop('readonly',false).prop('required',true);
   }
	  
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				
				//document.getElementById("taxtype"+srNo).innerHTML=xmlhttp.responseText;
				document.getElementById("chkbank"+srNo).innerHTML=xmlhttp.responseText;
				//alert(xmlhttp.responseText);
			}
		}
		//alert(unit_id);
		xmlhttp.open("GET","script/bank_expense.php?srNo="+srNo+"&type="+type,true);
		//xmlhttp.open("GET","script/ajaxUnitSize.php?unit_id="+unit_id,true);
		xmlhttp.send();
	
}



</script>

<script src="template/realcrm/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript">
   $(".select2").select2();



/****************************************Form Validation********************************************/

function validation()
{	
var chk=1;

var tot=document.getElementById('number_count').value;
var x;





for(x=1;x<=tot;x++){

if(document.getElementById("expense_id"+x).value == '') {
chk = 0;
document.getElementById('expense_id'+x).focus();
document.getElementById('expense_id'+x).style.borderColor="red";
}
else {
document.getElementById('expense_id'+x).focus();
document.getElementById('expense_id'+x).style.border="1px solid #ccc";
}

if(document.getElementById("transaction_type"+x).value == '') {
chk = 0;
document.getElementById('transaction_type'+x).focus();
document.getElementById('transaction_type'+x).style.borderColor="red";
}
else {
document.getElementById('transaction_type'+x).focus();
document.getElementById('transaction_type'+x).style.border="1px solid #ccc";
}

if(document.getElementById("amount"+x).value == '') {
chk = 0;
document.getElementById('amount'+x).focus();
document.getElementById('amount'+x).style.borderColor="red";
}
else {
document.getElementById('amount'+x).focus();
document.getElementById('amount'+x).style.border="1px solid #ccc";
}

if(document.getElementById("transaction"+x).value == '') {
chk = 0;
document.getElementById('transaction'+x).focus();
document.getElementById('transaction'+x).style.borderColor="red";
}
else {
document.getElementById('transaction'+x).focus();
document.getElementById('transaction'+x).style.border="1px solid #ccc";
}

if(document.getElementById("remark"+x).value == '') {
chk = 0;
document.getElementById('remark'+x).focus();
document.getElementById('remark'+x).style.borderColor="red";
}
else {
document.getElementById('remark'+x).focus();
document.getElementById('remark'+x).style.border="1px solid #ccc";
}

}

	
if(chk){
  // $('#form').submit();
 //alert("ExPENSES ADDED SUCCESSFULLY");
return true;
}else{
return false;	
}	
	
}
function show_value(str)
{ // alert(str);
	var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
			document.getElementById("expense_id1").innerHTML = xmlhttp.responseText; 
			 
        //initMultiSelect();
			
		}
		}
		xmlhttp.open("GET", "script/show_expense_data.php?id="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
}

function show_value1(str,n)
{ // alert(str);
	var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
			document.getElementById("expense_id"+n).innerHTML = xmlhttp.responseText; 
			 
        //initMultiSelect();
			
		}
		}
		xmlhttp.open("GET", "script/show_expense_data1.php?id="+str+"&srNo="+n, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
}
</script>


<script>
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
	
$('.chk_number').keypress(function(evt){
evt = (evt) ? evt : window.event;
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode > 31
// if (charCode != 46 && charCode > 31
&& (charCode < 48 || charCode > 57))
return false;

return true;
});

$('#submitBtn').click(function(){
$(this).delay(600).hide(1);
});
$('#resetBtn').click(function(){
$('#submitBtn').show(1);
});

/*=========Gsingh==========*/
function bank_type(val,id){
  if(val=="Cash-Deposit" || val=="Cash-Withdrawal"){ 
    $("#cheque_dd_no"+id).prop("required", false).prop("readonly", true); 
  }
   else 
    { $("#cheque_dd_no"+id).prop("readonly", false).prop("required", true); }
}
/*===========================*/
</script>
   







