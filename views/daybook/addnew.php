<style>
   .select2-choice {
   height: 33px !important;
   padding-top: 2px !important;
   background:#fff !important;
   border:1px solid #ccc !important;
   }
   .thWidthSr{
   width:4%;
   }
   .thWidthField{
   width:8%;
   }
   .selectWidthSr{
   width:100px;
   margin-left: 30px;
   }
   /*.selectWidthField{
   width:10%;
   }*/
   .select-1 {
   margin: 0 12px 0 22px;
   width: 210px;
   height: 34px;
   padding: 6px 12px;
   border: 1px solid #ccc;
   border-radius: 4px;
   box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
   }
   .select-1 option{
   padding:5px; 
   }
   .select-2 option{
   padding:5px; 
   }
   .select-2 {
   background: #fff none repeat scroll 0 0;
   border: 1px solid #eee;
   border-radius: 5px;
   height: 34px;
   margin: 0 10px 0 13px;
   padding: 6px 12px;
   width: 100px;
   border: 1px solid #ccc;
   border-radius: 4px;
   box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
   }
   .remark {
   background: #fff none repeat scroll 0 0;
   border: 1px solid #eee;
   border-radius: 5px;
   height: 34px;
   margin: 0 10px 0 0;
   padding: 6px 12px;
   width: 270px;
   border: 1px solid #ccc;
   border-radius: 4px;
   box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
   }
   .remark1 {
   background: #fff none repeat scroll 0 0;
   border: 1px solid #eee;
   border-radius: 5px;
   height: 34px;
   margin: 0 10px 0 22px;
   padding: 6px 12px;
   width: 270px;
   border: 1px solid #ccc;
   border-radius: 4px;
   box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
   position:relative;
   top:5px;
   }
   .my-form td {
   padding: 5px;
   }
</style>
<!--<script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>-->
<script type="text/javascript" src="assets/javascript/add_button.js"></script>
<div class="col-sm-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=daybook">Day Book</a></li>
            <li class="active">Add New Expense</li>
         </ol>
      </div>
   </div>
   <?php foreach($results as $result) { }  ?> 
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="panel-heading">
            <u>
               <?php if($result['id']=='') { ?>
               <h3>Add New Expense</h3>
               <?php } else { ?>
               <h3>Edit Expense</h3>
               <?php } ?>
            </u>
         </div>
         <br>
         <form name="form" method="post"  action="index.php" enctype="multipart/form-data" onsubmit="return validation();">
            <div class="my-form" style="margin-top:35px;">
               <div class="col-md-12">
                  <?php if($result['id']=='') { ?>
                  <div class="col-md-1" align="left"><b>Sr. No.</b></div>
                  <?php } ?>
                  <div class="col-md-2"  align="left"><b>Expense</b></div>
                  <div class="col-md-2"  align="left"><b>Transaction Type</b></div>
                  <!--<div class="col-md-2"  align="left"><b>Payment Type</b></div>-->
                  <div class="col-md-1"  align="left"><b>Amount</b></div>
                  <div class="col-md-1"  align="left"><b>Date</b></div>
                  <div class="col-md-2"  align="left"><b>Remarks</b></div>
               </div>
               <div class="col-md-12">
                  <?php if($result['id']=='') { ?>
                  <div class="col-md-1" ><label for="box1"><span class="box-number">1</span></label></div>
                  <?php } ?> 
                  <div class="col-md-2" >
                     <select class="form-control" name="expense_id1" id="expense_id1" required>
                        <option value="">-Select Expense-</option>
                        <?php 
                           $sq="select * from bill_expense where status=1 order by expense_name ASC";
                           $que=  mysql_query($sq);
                           while($fet=  mysql_fetch_array($que)){
                           ?>
                        <option value="<?php echo $fet['id']; ?>" <?php if($result['expense_id']==$fet['id']) {?> selected="selected" <?php } ?>><?php echo $fet['expense_name']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
                  <div class="col-md-2" >
                     <select class="form-control" name="transaction_type1" id="transaction_type1" required>
                        <option value="">-Select-</option>
                        <option value="1" <?php if($result['transaction_type']==1) {echo 'selected'; } ?>>Cash</option>
                        <option value="2" <?php if($result['transaction_type']==2) {echo 'selected'; } ?>>Bank</option>
                     </select>
                  </div>
                  <!--<div class="col-md-2" >
                     <select class="form-control" name="transaction1" id="transaction1" required >
                                         <option value="">Select</option>
                                         <option value="Cr" <?php if($result['transaction']=='Cr') {echo 'selected'; } ?>>Cr</option>
                                         <option value="Dr" <?php if($result['transaction']=='Dr') {echo 'selected'; } ?>>Dr</option>
                                         </select>
                     </div>-->
                  <div class="col-md-1" >
                     <input class="form-control" type="text" name="amount1" id="amount1" value="<?php echo $result['amount']; ?>" required onkeyup="chkNumeric(this.value,this.id)">
                  </div>
                  <div class="col-md-1" >
                     <input class="form-control" type="text" name="expense_date1" id="expense_date1" value="<?php echo $result['amount']; ?>" required onclick="datetype();">
                  </div>
                  <div class="col-md-2" >
                     <textarea class="remark" type="text" name="remark1" id="remark1" rows="1"><?php echo $result['remark']; ?></textarea>
                  </div>
                  <div class="col-md-1" ></div>
               </div>
               <p class="text-box"> </p>
               <div class="col-md-12" align="left">
                  <br>
                  <?php if($result['id']=='') { ?>
                  <input type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit" onclick="disableEnable();" >
                  <?php } else { ?>
                  <input type="submit" class="btn btn-primary" name="submit" value="Update" id="submit" onclick="disableEnable();">
                  <?php } ?>
                  <input type="button" class="btn btn-default" value="Reset" id="btnReset" onclick="disableEnableRefresh();" >
                  <?php if($result['id']=='') { ?>
                  <img src="images/add-button.png" width="30" height="30" style="cursor:pointer;" class="add-box" type="button" name="add_textbox" id="add_textbox" title="Add More">
                  <?php } ?>
                  <input type="hidden" name="control" value="daybook"/>
                  <input type="hidden" name="edit" value="1"/>
                  <input type="hidden" name="task" value="save"/>
                  <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                  <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                  <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                  <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['adminid']; ?>">
                  <input type="hidden" id="number_count" name="number_count" value="1">
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<?php
   /*  for($i=1; $i<=20; $i++){
   $pac2 = new C_PhpAutocomplete('expense_id');
   $pac2->display('SELECT');
     }*/
   ?>
<script type="text/javascript">
   jQuery(document).ready(function($){   
       $('.my-form .add-box').click(function(){
           var n = $('.text-box').length + 1;
    $('#number_count').val(n);  
           if( 20 < n ) {
               alert('Maximum Textbox is Reached. Please Stop it!');
               return false;
           }
      
      
      
     
        
                           
                   var box_html = $('<p class="text-box"><label for="box' + n + '" class="selectWidthSr"><span class="box-number">' + n + '</span></label><select class="form-control1 select-1"  name="expense_id'+n+'" id="expense_id'+n+'" required ><option value="">-Select-</option><?php $sq= mysql_query("select * from bill_expense where status=1 order by expense_name ASC");while($fet=  mysql_fetch_array($sq)){?><option value="<?php echo $fet['id']; ?>" <?php if($result['expense_id']==$fet['id']) {?> selected="selected" <?php } ?>><?php echo $fet['expense_name']; ?></option><?php } ?></select><select class="form-control1 select-1"  name="transaction_type'+n+'" id="transaction_type'+n+'" required><option value="">-Select-</option><option value="1" <?php if($result['transaction_type']==1) {echo 'selected'; } ?>>Cash</option><option value="2" <?php if($result['transaction_type']==2) {echo 'selected'; } ?>>Bank</option></select><input class="form-control1  select-2" type="text" name="amount'+n+'" id="amount'+n+'" value="<?php echo $result['amount']; ?>" required onkeyup="chkNumeric(this.value,this.id)"><input class="form-control1  select-2" type="text" name="expense_date'+n+'" id="expense_date'+n+'" value="<?php echo $result['expense_date']; ?>" required onclick="datetype();"><textarea class="form-control1 remark1" type="text" name="remark'+n+'" id="remark'+n+'" rows="1"><?php echo $result['remark']; ?></textarea><img src="images/removes.jpeg" width="30" height="30" style="cursor:pointer;" class="remove-box" type="button" name="add_textbox" id="add_textbox" title="Remove"></p>');
      
           box_html.hide();
           $('.my-form p.text-box:last').after(box_html);
           box_html.fadeIn('slow');
           return false;
       });
       $('.my-form').on('click', '.remove-box', function(){
           //$(this).parent().css( 'background-color', '#FF6C6C' );
           $(this).parent().fadeOut("slow", function() {
               $(this).remove();
               $('.box-number').each(function(index){
                   $(this).text( index + 1 );//alert(text);
      $('#number_count').val(index+ 1);
               });
        
        /* $('.boxes'+n).each(function(index){ 
                   $(this).text( index + 1 );
               });*/
           });
           return false;
       });
   
   });
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<!--<script src="assets/date_picker/jquery.js"></script>-->
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   function datetype(){
   
   
   for(i=0; i<20; i++) { 
    
    
   
   $('#expense_date'+i).datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'Y-m-d',
    formatDate:'Y/m/d',
    
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   
   }
   }
   
</script>
<script type="text/javascript">
   /****************************************Form Validation********************************************/
   
   function validation()
   {  
   var chk=1;
   
   var tot=document.getElementById('number_count').value;
   var x;
   
   
   
   
   
   for(x=1;x<=tot;x++){
   
   if(document.getElementById("expense_id"+x).value == '') {
   chk = 0;
   document.getElementById('expense_id'+x).focus();
   document.getElementById('expense_id'+x).style.borderColor="red";
   }
   else {
   document.getElementById('expense_id'+x).focus();
   document.getElementById('expense_id'+x).style.border="1px solid #ccc";
   }
   
   if(document.getElementById("transaction_type"+x).value == '') {
   chk = 0;
   document.getElementById('transaction_type'+x).focus();
   document.getElementById('transaction_type'+x).style.borderColor="red";
   }
   else {
   document.getElementById('transaction_type'+x).focus();
   document.getElementById('transaction_type'+x).style.border="1px solid #ccc";
   }
   
   if(document.getElementById("amount"+x).value == '') {
   chk = 0;
   document.getElementById('amount'+x).focus();
   document.getElementById('amount'+x).style.borderColor="red";
   }
   else {
   document.getElementById('amount'+x).focus();
   document.getElementById('amount'+x).style.border="1px solid #ccc";
   }
   
   if(document.getElementById("transaction"+x).value == '') {
   chk = 0;
   document.getElementById('transaction'+x).focus();
   document.getElementById('transaction'+x).style.borderColor="red";
   }
   else {
   document.getElementById('transaction'+x).focus();
   document.getElementById('transaction'+x).style.border="1px solid #ccc";
   }
   
   if(document.getElementById("remark"+x).value == '') {
   chk = 0;
   document.getElementById('remark'+x).focus();
   document.getElementById('remark'+x).style.borderColor="red";
   }
   else {
   document.getElementById('remark'+x).focus();
   document.getElementById('remark'+x).style.border="1px solid #ccc";
   }
   
   }
   
    
   if(chk){
     // $('#form').submit();
    //alert("ExPENSES ADDED SUCCESSFULLY");
   return true;
   }else{
   return false;  
   }  
    
   }
   
</script>

