<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">

<title>Customer Relationship Management</title>
<link rel="icon" href="favicon.png" type="image/png">
<link rel="shortcut icon" href="img/CRM.jpg" type="img/x-icon">


<link href="textconfig/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="textconfig/css/style.css" rel="stylesheet" type="text/css">
<link href="textconfig/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="textconfig/css/responsive.css" rel="stylesheet" type="text/css">
<link href="textconfig/css/animate.css" rel="stylesheet" type="text/css">

<!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

<script type="text/javascript" src="textconfig/js/jquery.1.8.3.min.js"></script>
<script type="text/javascript" src="textconfig/js/bootstrap.js"></script>

<script type="text/javascript" src="textconfig/js/jquery.easing.1.3.js"></script>

<script type="text/javascript" src="textconfig/js/wow.js"></script>



<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->


</head>
<body>






<section class="main-section" id="service"><!--main-section-start-->
	<div class="container" >
    	<div align="center"><img src="textconfig/img/logo.png" alt="" ><br>
<h2 style="color:#e98b00;">Customer <span style="color:#6639b6;"> Relationship Management </span></h2></div>
<!--    	<h6>Accurate and up-to-date IT asset details</h6>download app -->
       
        <div class="row">
        	<div class="col-lg-4 col-sm-12 wow fadeInLeft delay-05s">
            	<div class="service-list">
                	<div class="service-list-col1">
               	    <img src="textconfig/img/1.jpg"  alt=""/> </div>
                	<div class="service-list-col2">
                        <h3>Booking Details</h3>
                        <p>Proin iaculis purus consequat sem digni. </p>
                  </div>
                </div>
                <div class="service-list">
                	<div class="service-list-col1">
                    	<img src="textconfig/img/2.jpg"  alt=""/>
                    </div>
                	<div class="service-list-col2">
                        <h3>Payment Laser</h3>
                        <p>Proin iaculis purus consequat sem digni. </p>
                  </div>
                </div>
                <div class="service-list">
                	<div class="service-list-col1">
                    <img src="textconfig/img/3.jpg"  alt=""/> </div>
                	<div class="service-list-col2">
                        <h3>Share App</h3>
                        <p>Proin iaculis purus consequat sem digni.</p>
                  </div>
                </div>
                <div class="service-list">
                	<div class="service-list-col1">
               	    <img src="textconfig/img/4.jpg"  alt=""/> </div>
                	<div class="service-list-col2">
                        <h3>Receipts</h3>
                        <p>Proin iaculis purus consequat sem digni. </p>
                  </div>
                </div>
                <div class="service-list">
                	<div class="service-list-col1">
               	    <img src="textconfig/img/5.jpg"  alt=""/> </div>
                	<div class="service-list-col2">
                        <h3>Profile</h3>
                        <p>Proin iaculis purus consequat sem digni. </p>
                  </div>
                </div>
                
                <div class="service-list">
                	<div class="service-list-col1">
               	    <img src="textconfig/img/6.jpg"  alt=""/> </div>
                	<div class="service-list-col2">
                        <h3>Rating</h3>
                        <p>Proin iaculis purus consequat sem digni. </p>
                  </div>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12  text-right wow fadeInUp delay-02s crmimg">
            	<img src="textconfig/img/softgen-landing-page-CRM.jpg" alt="" width="750" height="450">
            </div>
            
            
        
        </div>
        
        <div align="center"><a href="https://play.google.com/store/apps/details?id=soft.gen.sgtcrm" target="_blank"><img src="textconfig/img/playstore.jpg" alt="" class="crmbtn"></a> <a href="login_realcrm.php"><img src="textconfig/img/login.jpg" alt="" class="crmbtn"></a></div>
        
	</div>
</section><!--main-section-end-->



<script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100
      }
    );
    wow.init();
    document.getElementById('').onclick = function() {
      var section = document.createElement('section');
      section.className = 'wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };
  </script>


<script type="text/javascript">
	$(window).load(function(){
		
		$('.main-nav li a').bind('click',function(event){
			var $anchor = $(this);
			
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top - 102
			}, 1500,'easeInOutExpo');
			/*
			if you don't want to use the easing effects:
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1000);
			*/
			event.preventDefault();
		});
	})
</script>

<script type="text/javascript">

$(window).load(function(){
  
  
  var $container = $('.portfolioContainer'),
      $body = $('body'),
      colW = 375,
      columns = null;

  
  $container.isotope({
    // disable window resizing
    resizable: true,
    masonry: {
      columnWidth: colW
    }
  });
  
  $(window).smartresize(function(){
    // check if columns has changed
    var currentColumns = Math.floor( ( $body.width() -30 ) / colW );
    if ( currentColumns !== columns ) {
      // set new column count
      columns = currentColumns;
      // apply width to container manually, then trigger relayout
      $container.width( columns * colW )
        .isotope('reLayout');
    }
    
  }).smartresize(); // trigger resize to set container width
  $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
 
        var selector = $(this).attr('data-filter');
        $container.isotope({
			
            filter: selector,
         });
         return false;
    });
  
});

</script>
</body>
</html>