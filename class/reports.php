<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class reportClass extends DbAccess {
		public $view='';
		public $name='report';
		
		
		/***************************************************** Country START **********************************************************/
		
		function show(){

			if($_REQUEST['search']){							
				$project_id =   $_REQUEST['project_id']?" and project_id='".$_REQUEST['project_id']."'":'';
				$block_id =   $_REQUEST['block_id']?" and block_id = '".$_REQUEST['block_id']."'":'';
				// $city_id =   $_REQUEST['city_id']?" and city_id = '".$_REQUEST['city_id']."'":'';

			    $uquery ="SELECT * FROM `project_plot` WHERE 1 $project_id $block_id ORDER BY `project_id`, `block_id`, `plot_id` ASC";
			}
			else{
				$uquery ="SELECT * FROM `project_plot` WHERE 1 ORDER BY `project_id`, `block_id`, `plot_id` ASC";
			}
				$this->Query($uquery);
				$uresults = $this->fetchArray();
				$no_of_row=count($uresults);	
				$_SESSION['country'] = $uquery;
				$tdata=count($uresults);

				/* Paging start here */
					$page   = intval($_REQUEST['page']);
					$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
					$adjacents  = intval($_REQUEST['adjacents']);
					$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
					$tpages);//$_GET['tpages'];// 
					$tdata = floor($tdata);
					if($page<=0)  $page  = 1;
					if($adjacents<=0) $tdata?($adjacents = 4):0;
					$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
				/* Paging end here */	
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				$this->Query($query);
				$results = $this->fetchArray();		
				
				require_once("views/".$this->name."/".$this->task.".php"); 
			
				
		}
			
		function addnew(){
		
			$uquery ="SELECT * FROM `project_plot` WHERE 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
				
				if($_REQUEST['id']) {
					$query_com ="SELECT * FROM `project_plot` WHERE `id` = ".$_REQUEST['id'];
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			
		}
		
		function save(){
		
			$project_id = $_POST['project_id'];	
			$block_id = $_POST['block_id'];	
			$plot_id = $_POST['plot_id'];	
			$area_id = $_POST['area_id'];	
			$rate = $_POST['rate'];	
			
			$check = mysql_num_rows(mysql_query("SELECT * FROM `project_plot` WHERE `project_id`='".$project_id ."' AND `block_id`='".$block_id ."' AND `plot_id`='".$plot_id ."' AND `area_id`='".$area_id ."' AND `rate`='".$rate ."'"));
			
			if($check == '0') {
				
					if(!$_REQUEST['id']){


			foreach( $_POST['plot_id'] as $i=>$value) {

				
						$query = "INSERT INTO `project_plot`(`project_id`, `block_id`, `plot_id`, `area_id`, `rate`,`created_date`, `modified_date`) VALUES ('".$project_id."','".$block_id."','".$plot_id[$i]."','".$area_id."','".$rate."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
						$this->Query($query);
						$this->Execute();		
						} 
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project_plot&task=addnew");
					} 
					else {
								
						$update = "UPDATE `project_plot` SET `project_id`='".$project_id."',`block_id`='".$block_id."',`plot_id`='".$plot_id."',`area_id`='".$area_id."',`rate`='".$rate."',`modified_date`='".date('Y-m-d H:i:s')."' WHERE id='".$_REQUEST['id']."'";

						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project_plot&task=show");
					}
			}
			else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project_plot&task=addnew");
			}
			
		 header("location:index.php?control=project_plot&task=addnew");
		}
		
				
		
		function status(){
			$query="update project_plot set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=project_plot&task=show");
		}		
		
		function delete(){
			
			$query="DELETE FROM project_plot WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$this->task="show";
			$this->view ='show';			
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			header("location:index.php?control=project_plot&task=show");
		}
	
		
		/**************************************** Country END **************************************/
		
		/***************************************************** STATE START**************/	
		function activity(){
										
			
		
		    $uquery ="SELECT * FROM `activity_log` WHERE `status`= 1 ORDER BY `id` DESC";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['city'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : 200;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			 $query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
	
		/*============for delete==========*/
		function activity_status(){
			$query = "UPDATE `activity_log` SET `status`= 0 WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=report&task=activity");
		}
		
	


		function show_plot(){
										
			$plot_id = $_POST['plot_id']?" and plot_id = '".$_POST['plot_id']."'":'';
		
		    $uquery = "SELECT * FROM `plot` WHERE 1 $plot_id ";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['city'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_plot() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  plot WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_plot(){
			
			$plot_name   =  mysql_real_escape_string($_POST['plot_name']);
			$id = $_REQUEST['id'];
			
			$check_plot = mysql_num_rows(mysql_query("SELECT `plot_name` FROM `plot` WHERE `plot_name` like '%".$plot_name."%' "));
			if($check_plot=='0') {
						if(!$id){
					
					$query="INSERT INTO `plot`( `plot_name`, `datetime`) VALUES ('".strtoupper($plot_name)."','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=project&task=addnew_plot");
					} else	{
						echo $update="UPDATE `plot` SET `plot_name`='".strtoupper($plot_name)."' WHERE `id`='".$id."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    	$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=addnew_plot");
					}
			} else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    	$_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project&task=addnew_plot");
			}
		}
		
		function plot_status(){
			$query = "update plot set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=project&task=show_plot");
		}
		
		function plot_delete(){
		
			$query="DELETE FROM plot WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=project&task=show_plot");
		}

		
	
		function show_area_measurement(){
										
			// $project_id     =   $_POST['project_id']?" and project_id = '".$_POST['project_id']."'":'';
		
		    $uquery ="SELECT * FROM `area_measurement` WHERE 1";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['city'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_area_measurement() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `area_measurement` WHERE  id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
				
		function save_area_measurement(){
			$dimenstion  =  $_REQUEST['length'].'X'.$_REQUEST['width'];

			$square_feet = $_REQUEST['square_feet'];
			$square_meter = $_REQUEST['square_meter'];
			$id = $_REQUEST['id'];
			
			$check_area = mysql_num_rows(mysql_query("SELECT `dimenstion` FROM `area_measurement` WHERE `dimenstion` like '%".$dimenstion."%'"));
			if($check_area=='0') {
						if(!$id){
					
					$query="INSERT INTO `area_measurement`(`square_feet`, `square_meter`, `dimenstion`, `created_date`, `modified_date`) VALUES ('".$square_feet."','".$square_meter."','".$dimenstion."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=project&task=addnew_area_measurement");
					} else	{
					
						 $update="UPDATE `area_measurement` SET `square_feet`='".$square_feet."',`square_meter`='".$square_meter."',`dimenstion`='".$dimenstion."',`modified_date`='".date("Y-m-d H:i:s")."' WHERE `id`='".$id."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=addnew_area_measurement");
					}
			} else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project&task=addnew_area_measurement");
			}
		}
		
		function area_measurement_status(){
			$query = "UPDATE `area_measurement` SET `status`='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=project&task=show_area_measurement");
		}
		
		function area_measurement_delete(){
		
			$query="DELETE FROM area_measurement WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=project&task=show_area_measurement");
		}

	}
