<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class hrm_postClass extends DbAccess {
		public $view='';
		public $name='hrm_post';

		
		/***************************************************** POST START **********************************************************/
		
		function show(){	
		$uquery ="select * from hrm_post order by id ASC";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$no_of_row=count($uresults);
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		 $query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show.php"); 
		}
	
		function status(){
		$query="update hrm_post set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		if($this->Execute()) {	
			$_SESSION['error'] = STATUS;	
			$_SESSION['errorclass'] = ERRORCLASS;
		    }
		$this->show();	
		//header("location:index.php?control=hrm_post&task=show");
		}
		
		
			function save(){
		
			$post_name = mysql_real_escape_string($_POST['post_name']);	
			$status = '1';
			$id = $_REQUEST['id'];
		
			$check_post = mysql_num_rows(mysql_query("select * from hrm_post where post_name like '%".$post_name."%'"));
			
			if($check_post=='0') {
				
					if(!$id){
						$query = "INSERT INTO `hrm_post`(`post_name`,`status`,`datetime`) VALUES ('".$post_name."','1','".date('Y-m-d H:i:s')."')";
						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=hrm_post&task=addnew");
					} 
					else {
								
						$update="UPDATE `hrm_post` SET `post_name`='".$post_name."' where id='".$id."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=hrm_post&task=show");
					}
			}
			else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=hrm_post&task=addnew");
				}
			
		 header("location:index.php?control=hrm_post&task=addnew");
		}
		
		
		
		function delete(){
		
		$query="DELETE FROM hrm_post WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		if($this->Execute()) {	
			$_SESSION['error'] = DELETE;	
			$_SESSION['errorclass'] = ERRORCLASS;
		    }
		$this->show();
		//header("location:index.php?control=hrm_post&task=show");
		
		}
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  hrm_post WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		/***************************************************** POST END **********************************************************/

	}
