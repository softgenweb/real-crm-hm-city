<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class project_masterClass extends DbAccess {
		public $view='';
		public $name='project_master';
		
		
		/***************************************************** Project START **********************************************************/
		
		function show(){	
		$uquery ="select * from project where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row = count($uresults);	
		//$_SESSION['project'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
			
		}
			
		function addnew(){
		
		$uquery ="select * from project where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM project WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
			
		}
		
		function save(){
		
			$project = mysql_real_escape_string($_POST['project']);	
			 $status = '1';
		         $id = $_REQUEST['id'];
			$check_project = mysql_num_rows(mysql_query("select * from project where project_name like '%".$project."%'"));
			
			if($check_project == '0') {				
					if(!$id){
						$query = "INSERT INTO `project`(`project_name`,`status`,`datetime`) VALUES ('".$project."','1','".date('Y-m-d H:i:s')."')";
						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project_master&task=addnew");
					} 
					else {
								
						$update="UPDATE `project` SET `project_name`='".$project."' where id='".$id."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project_master&task=show");
					}
			}
			else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project_master&task=addnew");
			}
			
		 header("location:index.php?control=project_master&task=addnew");
		}
		
				
		
		function status(){
		$query="update project set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=project_master&task=show");
		}		
		
		function delete(){
		
		$query="DELETE FROM project WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';			
		$_SESSION['alertmessage'] = DELETE; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		
		header("location:index.php?control=project_master&task=show");
		}
	
		
		/***************************************************** Country END **********************************************************/
		
		
		
	
	}
