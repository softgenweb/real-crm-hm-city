<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		require_once('configuration.php');
	}
	
	class daybookClass extends DbAccess {
		public $view='';
		public $name='daybook';

		
		function show(){

		$dateFrom = $_REQUEST['dateFrom'];
		$dateTo =  $_REQUEST['dateTo'];
		$expense_type = ($_REQUEST['expense_type'])?(' and expense_type="'.$_REQUEST['expense_type'].'"'):"";
		$expense_id = ($_REQUEST['expense_id'])?(' and expense_id="'.$_REQUEST['expense_id'].'"'):"";

		if($dateFrom){
		$dateUse = ($dateFrom)?('expense_date="'.$dateFrom.'"'):"";
		}else{
		$dateUse = ($dateTo)?('expense_date="'.$dateTo.'"'):"";
		}

		$dateFromTo = ($dateFrom && $dateTo)?(' and expense_date between "'.$dateFrom.'" and "'.$dateTo.'"'):$dateUse;
		if($dateFrom || $dateTo || $expense_id){
		//$uquery ="select * from day_book where user_id='".$_SESSION['adminid']."' $expense_id $dateFromTo  order by id desc ";
		   $uquery ="SELECT * FROM `day_book` WHERE `status` !='2' $expense_type $expense_id $dateFromTo  ORDER BY `expense_date` DESC";
	
		}
		else
		{
		//$uquery ="select * from day_book where 1 and expense_date='".date('Y-m-d')."'";	
		//$uquery ="select * from day_book where user_id='".$_SESSION['adminid']."' and expense_date='".date('Y-m-d')."' order by id desc ";
		 $uquery ="SELECT * FROM `day_book` WHERE `status` !='2' AND `expense_date` LIKE '%".date('Y-m')."%' ORDER BY `expense_date` DESC";
		}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		 $query =$uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function cr_expense(){
		
				$dateFrom = $_REQUEST['dateFrom'];
				$dateTo =  $_REQUEST['dateTo'];
				
				$expense_type = ($_REQUEST['expense_type'])?(' and expense_type="'.$_REQUEST['expense_type'].'"'):"";
				
				$expense_id = ($_REQUEST['expense_id'])?(' and expense_id="'.$_REQUEST['expense_id'].'"'):"";
		
				if($dateFrom){
				$dateUse = ($dateFrom)?(' AND expense_date="'.$dateFrom.'"'):"";
				}else{
				$dateUse = ($dateTo)?(' AND expense_date="'.$dateTo.'"'):"";
				}
		
				$dateFromTo = ($dateFrom && $dateTo)?(' and expense_date between "'.$dateFrom.'" and "'.$dateTo.'"'):$dateUse;
				if($dateFrom || $dateTo || $expense_id || $expense_type){
		
				 $uquery ="SELECT *, CASE WHEN `cheque_status_date` <> '' THEN `cheque_status_date` ELSE `expense_date` END AS exdate  FROM `day_book` WHERE `status` !='2' AND  `account_type`='Cr' AND `cheque_status`='1' $expense_type $expense_id $dateFromTo  ORDER BY exdate DESC ";
			// echo	 $uquery ="select * from day_book where account_type='Cr' AND user_id='".$_SESSION['adminid']."' $expense_id $dateFromTo  order by id desc ";
			
				}
				else
				{
				//$uquery ="select * from day_book where account_type='Cr' and user_id='".$_SESSION['adminid']."' and expense_date='".date('Y-m-d')."' order by id desc ";
				$uquery ="SELECT *, CASE WHEN `cheque_status_date` <> '' THEN `cheque_status_date` ELSE `expense_date` END AS exdate  FROM `day_book` WHERE `status` !='2' AND `account_type`='Cr' AND `cheque_status`='1' ORDER BY exdate DESC ";
				}
				$this->Query($uquery);
				$uresults = $this->fetchArray();	
				$tdata=count($uresults);
				/* Paging start here */
					$page   = intval($_REQUEST['page']);
					$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
					$adjacents  = intval($_REQUEST['adjacents']);
					$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
					$tdata = floor($tdata);
					if($page<=0)  $page  = 1;
					if($adjacents<=0) $tdata?($adjacents = 4):0;
					$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
				/* Paging end here */	
				 $query =$uquery. "LIMIT ".(($page-1)*$tpages).",".$tpages;
				$this->Query($query);
				$results = $this->fetchArray();		
				
				require_once("views/".$this->name."/cr_expense.php"); 
				}		
		
		function cr_expense_addnew() {
		if($_REQUEST['id']) {
			$query_com ="SELECT * FROM  day_book WHERE id = ".$_REQUEST['id'];
			$this->Query($query_com);

			$results = $this->fetchArray();
		//	echo '<pre>';
		//	print_r($results);
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		else {
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
	}		
		
		function cr_expense_save() {
						
				
					if(!$_REQUEST['id']) {
					$countNo = $_REQUEST['total_item'];
					$expense_type = $_REQUEST['expense_type'];
				    $expense_id = $_REQUEST['expense_id'];
					$user_id= $_REQUEST['user_id'];
					       		     	
					$transaction_types = $_REQUEST['transaction_type'];
						if($transaction_types!="Cash")
						{ 
						$self_account_no = $transaction_types;
						$transaction_type ="Bank";
						}
						else
						{
						$self_account_no = ''; 
						$transaction_type = $transaction_types;
						}
					$bank_type = $_REQUEST['bank_type'];
					$cheque_dd_no = $_REQUEST['cheque_dd_no'];
					
					$amount = $_REQUEST['amount'];      			     $remark = $_REQUEST['remark'];
					$bill_account_id = $_REQUEST['bill_account_id'];  $account_type =  'Cr';//$_REQUEST['transaction'.$k];
					$payment_mode = $_REQUEST['payment_mode'];	     $expense_date = $_REQUEST['expense_date'];
					$date_time = date('d-m-Y H:i:s');                 $status = 1;
				
				for($k=0; $k<$_REQUEST['total_item']; $k++)
					{	$user_type  = ucfirst($expense_type[$k]).'-'."Expense";  
					if($bank_type[$k]=='Cheque')
					{
						$cheque_status = '0';
					}
					else
					{
						$cheque_status = '1';
					}
					
				 $queryOfficeExpense = "INSERT INTO day_book(expense_type,expense_id, user_id, user_type, transaction_type, amount, account_type,  self_account_no,bank_type, cheque_dd_no,remark, expense_date, date_time, status,cheque_status) values('".$expense_type[$k]."','".$expense_id[$k]."','".$user_id."','".$user_type."','".$transaction_type."','".$amount[$k]."', '".$account_type."', '".$self_account_no[$k]."', '".$bank_type[$k]."', '".$cheque_dd_no[$k]."','".$remark[$k]."','".$expense_date[$k]."','".$date_time."','".$status."','".$cheque_status."') ";
					
					$this->Query($queryOfficeExpense);
					$this->Execute();					
					$last_id = mysql_insert_id();				
					
					if($transaction_types!='Cash' || $transaction_types!=''){
						
				$query = "INSERT INTO `bank_transaction`(`office_expense_id`, `all_type_id`, `user_id`, `user_type`, `self_account_no`, `amount`, `account_type`, `transaction_type`, `bank_type`, `cheque_dd_no`, `date`, `date_time`, `remark`, `status`,cheque_status) values('".$last_id."','".$expense_id[$k]."','".$user_id."','".$user_type."','".$self_account_no[$k]."','".$amount[$k]."','".$account_type."','Bank','".$bank_type[$k]."','".$cheque_dd_no[$k]."','".$expense_date[$k]."','".$date_time."','".$remark[$k]."','1','".$cheque_status."')";
					
					$this->Query($query);
					$this->Execute();
					}
						if($expense_type[$k] =='farmer')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from farmer where id = '".$expense_id[$k]."'"));
						$name = $expense_name['name'];
						}
						elseif($expense_type[$k] =='customer')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from plot_booking where  id = '".$expense_id[$k]."'"));
						$name = $expense_name['applicant_name'];	
						}
						elseif($expense_type[$k] =='dealer')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from dealer where status  id = '".$expense_id[$k]."'"));
						$name = $expense_name['name'];	
						}
						elseif($expense_type[$k] =='expense')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from expense where  id = '".$expense_id[$k]."'"));
						$name = $expense_name['expense_name'];	
						}
						
				 
				 				
				$activity = "Add Cr expense for : ".$name."(".$transaction_type .'-'.$amount[$k].")";			
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
			
					$_SESSION['error'] = ADDNEWRECORD;	
					$_SESSION['errorclass'] = ERRORCLASS; 
					
					}
					
				header("location:index.php?control=daybook&task=cr_expense"); 	   
					
					}
					 
				}
				
		function change_chq_status() {
			
			$cheque_status  = $_REQUEST['cheque_status'];
			$row_id = $_REQUEST['id'];
			$cheque_status_date = date('Y-m-d',strtotime($_REQUEST['cheque_status_date']));
			if($_REQUEST['submit'])
			{
				
				$chq_update = mysql_query("update day_book set  cheque_status = '".$cheque_status."' , cheque_status_date = '".$cheque_status_date."' where id = '".$row_id."'");
				$chq_update = mysql_query("update bank_transaction set  cheque_status = '".$cheque_status."' , cheque_status_date = '".$cheque_status_date."' where id = '".$row_id."'");
				
				 $fetch_data = mysql_fetch_array(mysql_query("select * from bank_transaction where id ='".$row_id."'"));
				
			if($cheque_status=='1') { echo $change = 'Cleared';} elseif($cheque_status=='2') { echo $change = 'Bounce'; }elseif($cheque_status=='3') { echo $change = 'Cancel'; }
			
					$activity = "Change Status ".$change." Cr expense for : ".$fetch_data['user_type']."(ChqNo-".$fetch_data['cheque_dd_no'].'- Rs-'.$fetch_data['amount'].")";
								
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
			}
			header("location:index.php?control=daybook&task=cr_expense");
		}
		
		function change_chq_status_dr() {
			
			$cheque_status  = $_REQUEST['cheque_status'];
			$row_id = $_REQUEST['id'];
			$cheque_status_date = date('Y-m-d',strtotime($_REQUEST['cheque_status_date']));
			if($_REQUEST['submit'])
			{
				
				$chq_update = mysql_query("update day_book set  cheque_status = '".$cheque_status."' , cheque_status_date = '".$cheque_status_date."' where id = '".$row_id."'");
				$chq_update = mysql_query("update bank_transaction set  cheque_status = '".$cheque_status."' , cheque_status_date = '".$cheque_status_date."' where id = '".$row_id."'");
				
				if($cheque_status=='1') { echo $change = 'Cleared';} elseif($cheque_status=='2') { echo $change = 'Bounce'; }elseif($cheque_status=='3') { echo $change = 'Cancel'; }
				
			$activity = "Change Status ".$change." Dr expense for : ".$fetch_data['user_type']."(ChqNo-".$fetch_data['cheque_dd_no'].'- Rs-'.$fetch_data['amount'].")";
								
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");	
				
			}
			header("location:index.php?control=daybook&task=dr_expense");
		}
		
		
		function dr_expense(){

		$dateFrom = $_REQUEST['dateFrom'];
		$dateTo =  $_REQUEST['dateTo'];
		
		$expense_type = ($_REQUEST['expense_type'])?(' and expense_type="'.$_REQUEST['expense_type'].'"'):"";
		$expense_id = ($_REQUEST['expense_id'])?(' and expense_id="'.$_REQUEST['expense_id'].'"'):"";

		if($dateFrom){
		$dateUse = ($dateFrom)?('expense_date="'.$dateFrom.'"'):"";
		}else{
		$dateUse = ($dateTo)?('expense_date="'.$dateTo.'"'):"";
		}

		$dateFromTo = ($dateFrom && $dateTo)?(' and expense_date between "'.$dateFrom.'" and "'.$dateTo.'"'):$dateUse;
		if($dateFrom || $dateTo || $expense_id){

		$uquery ="select *, CASE WHEN `cheque_status_date` <> '' THEN `cheque_status_date` ELSE `expense_date` END AS exdate from day_book where status !='2' and account_type='Dr' and `cheque_status`='1'  $expense_type $expense_id $dateFromTo  order by exdate  desc ";
	
		}
		else
		{
		//$uquery ="select * from day_book where account_type='Dr' and user_id='".$_SESSION['adminid']."' and expense_date='".date('Y-m-d')."' order by id desc ";
		$uquery ="select *,CASE WHEN `cheque_status_date` <> '' THEN `cheque_status_date` ELSE `expense_date` END AS exdate from day_book where status !='2' and account_type='Dr'  and `cheque_status`='1' order by exdate  desc ";
		}



		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		 $query =$uquery. "LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/dr_expense.php"); 
		}		
		
		function dr_expense_addnew() {
		if($_REQUEST['id']) {
			$query_com ="SELECT * FROM  day_book WHERE id = ".$_REQUEST['id'];
			$this->Query($query_com);
			$results = $this->fetchArray();		
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		else {
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
	}		
		
		function dr_expense_save() {				
			
			if(!$_REQUEST['id']) {
			$countNo = $_REQUEST['total_item']; 
			$expense_type = $_REQUEST['expense_type'];
			$expense_id = $_REQUEST['expense_id'];			
			$expnc = $_REQUEST['expnc'];			
			$user_id= $_REQUEST['user_id'];
			$last_read = $_REQUEST['last'];			
			$current_read= $_REQUEST['current'];
			
			      		     	
			$transaction_types = $_REQUEST['transaction_type'];
            if($transaction_types!="Cash"){ 
			$self_account_no = $transaction_types;
			$transaction_type = "Bank"; 
			}else
			{ 
			$self_account_no = '';
			$transaction_type = $transaction_types;
			}
    		$bank_type = $_REQUEST['bank_type'];
			$cheque_dd_no = $_REQUEST['cheque_dd_no'];
			$amount = $_REQUEST['amount'];      			     $remark = $_REQUEST['remark'];
         	$bill_account_id = $_REQUEST['bill_account_id'];  $account_type =  'Dr';

			$payment_mode = $_REQUEST['payment_mode'];	     $expense_date = $_REQUEST['expense_date'];
			$date_time = date('d-m-Y H:i:s');                 $status = 1;
			
			  
				for($k=0; $k<$_REQUEST['total_item']; $k++)
					{	$user_type  = ucfirst($expense_type[$k]).'-'."Expense";  
					if($bank_type[$k]=='Cheque')
					{
						$cheque_status = '0';
					}
					else
					{
						$cheque_status = '1';
					}
					
				 $queryOfficeExpense = "INSERT INTO day_book(expense_type,expense_id, user_id, user_type, transaction_type, amount, account_type,  self_account_no,bank_type, cheque_dd_no,remark, expense_date, date_time, status,cheque_status) values('".$expense_type[$k]."','".$expense_id[$k]."','".$user_id."','".$user_type."','".$transaction_type."','".$amount[$k]."', '".$account_type."', '".$self_account_no[$k]."', '".$bank_type[$k]."', '".$cheque_dd_no[$k]."','".$remark[$k]."','".$expense_date[$k]."','".$date_time."','".$status."','".$cheque_status."') ";
					
					$this->Query($queryOfficeExpense);
					$this->Execute();					
					$last_id = mysql_insert_id();
										
					if($transaction_types!='Cash' || $transaction_types!=''){
						
				$query = "INSERT INTO `bank_transaction`(`office_expense_id`, `all_type_id`, `user_id`, `user_type`, `self_account_no`, `amount`, `account_type`, `transaction_type`, `bank_type`, `cheque_dd_no`, `date`, `date_time`, `remark`, `status`,cheque_status) values('".$last_id."','".$expense_id[$k]."','".$user_id."','".$user_type."','".$self_account_no[$k]."','".$amount[$k]."','".$account_type."','Bank','".$bank_type[$k]."','".$cheque_dd_no[$k]."','".$expense_date[$k]."','".$date_time."','".$remark[$k]."','1','".$cheque_status."')";
					
					$this->Query($query);
					$this->Execute();
					}

				if($expense_type[$k] =='farmer')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from farmer where id = '".$expense_id[$k]."'"));
						$name = $expense_name['name'];
						}
						elseif($expense_type[$k] =='customer')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from plot_booking where  id = '".$expense_id[$k]."'"));
						$name = $expense_name['applicant_name'];	
						}
						elseif($expense_type[$k] =='dealer')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from dealer where id = '".$expense_id[$k]."'"));
						$name = $expense_name['name'];	
						}
						elseif($expense_type[$k] =='expense')
						{
						$expense_name = mysql_fetch_array(mysql_query("select * from expense where  id = '".$expense_id[$k]."'"));
						$name = $expense_name['expense_name'];	
						}
						
				 
				 				
				$activity = "Add Dr expense for : ".$name."(".$transaction_type .'-'.$amount[$k].")";			
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
			 
						    
				 
			$_SESSION['error'] = ADDNEWRECORD;	
            $_SESSION['errorclass'] = ERRORCLASS; 
			
			}
		header("location:index.php?control=daybook&task=dr_expense"); 	   
			
			}
	
			 
			 
		}
	
	
	 function delete()
    {
       $query1 = mysql_query("update bank_transaction set status = '2' WHERE office_expense_id='".$_REQUEST['id']."'");
         $query2 = mysql_query("update day_book set status = '2' WHERE id='".$_REQUEST['id']."'");
		 
		 $fetch_data = mysql_fetch_array(mysql_query("select * from day_book where id ='".$_REQUEST['id']."'"));
		 
		 $activity = "Delete expense for : ".$fetch_data['account_type']."(".$fetch_data['user_type'].'-'.$fetch_data['amount'].")";	
		 		
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
         $_SESSION['msg'] = '1';
        header("location:index.php?control=daybook&task=show");
    }
	
}
