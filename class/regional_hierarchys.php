<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class regional_hierarchyClass extends DbAccess {
		public $view='';
		public $name='regional_hierarchy';
		
		
		/***************************************************** Country START **********************************************************/
		
		function show(){	
		if($_SESSION['utype']=='Administrator')
		{
		$uquery ="SELECT * FROM `country` WHERE 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$_SESSION['country'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		else
			{
				header('location:index.php?msg=1');	
			}	
		}
			
		function addnew(){
		if($_SESSION['utype']=='Administrator')
			{	
		$uquery ="SELECT * FROM `country` WHERE 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM country WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		else
			{
				header('location:index.php?msg=1');	
			}	
		}
		
		function save(){
		
			$country_name = strtoupper($_POST['country_name']);	
			$status = '1';
		
			$check_country = mysql_num_rows(mysql_query("SELECT * FROM `country` WHERE  country_name = '".$country_name."'"));
			
			if($check_country == '0') {
				
					if(!$_REQUEST['id']){
						$query = "INSERT INTO `country`(`country_name`,`status`,`datetime`) VALUES ('".$country_name."','1','".date('Y-m-d H:i:s')."')";
						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=regional_hierarchy&task=addnew");
					} 
					else {
								
						$update="UPDATE `country` SET `country_name`='".$country_name."' where id='".$_REQUEST['id']."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=regional_hierarchy&task=show");
					}
			}
			else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=regional_hierarchy&task=addnew");
			}
			
		 header("location:index.php?control=regional_hierarchy&task=addnew");
		}
		
				
		
		function status(){
		$query="update country set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=regional_hierarchy&task=show");
		}		
		
		function delete(){
		
		$query="DELETE FROM country WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';			
		$_SESSION['alertmessage'] = DELETE; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		
		header("location:index.php?control=regional_hierarchy&task=show");
		}
	
		
		/***************************************************** Country END **********************************************************/
		
		/***************************************************** STATE START **********************************************************/
		
		function show_state(){
			if($_REQUEST['search']){							
			$country_id       =   $_POST['country_id']?" and country_id='".$_POST['country_id']."'":'';
			/*$state_id       =   $_POST['state_id']?" and state_id = '".$_POST['state_id']."'":'';
			$city_id  =   $_POST['city_id']?" and city_id = '".$_POST['city_id']."'":'';*/
		     $uquery ="select * from state where 1 $country_id";
			}
			else{
			$uquery ="select * from state where 1";
			}
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row=count($uresults);		
			$tdata=count($uresults);
			$_SESSION['state'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_state() {
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  state WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);
				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_state(){
			$state_name=strtoupper($_POST['state_name']);
			$country_id=$_POST['country_id'];
			
			$check_state = mysql_num_rows(mysql_query("select * from state where state_name = '".$state_name."' and country_id = '".$country_id."'"));
			if($check_state == '0') {
						if(!$_REQUEST['id']){
					
					$query="insert into state(state_name,country_id,status,datetime) value('".$state_name."','".$country_id."','1','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=regional_hierarchy&task=addnew_state");
					}
					else
					{
						$update="update state set state_name='".$name."',country_id='".$country_id."' where id='".$_REQUEST['id']."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
				    $_SESSION['alertmessage'] = UPDATERECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=regional_hierarchy&task=addnew_state");
					}
			}
		else {
				         //$_SESSION['msg'] = '0';
				    $_SESSION['alertmessage'] = DUPLICATE; 
				    $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=regional_hierarchy&task=addnew_state");
			}
		
		}
		
		function state_delete(){
		
		$query="DELETE FROM state WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();
		$_SESSION['alertmessage'] = DELETE; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;	
		
		//$this->show();	
		header("location:index.php?control=regional_hierarchy&task=show_state");
		}
		
		function state_status(){
		$query = "update state set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$_SESSION['alertmessage'] = STATUS; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
		
		//$this->show();	
		header("location:index.php?control=regional_hierarchy&task=show_state");
		}
		
		/***************************************************** STATE END **********************************************************/
		
		/***************************************************** CITY START **********************************************************/
		
		function show_city(){
			if($_REQUEST['search']) 
			{							
			$country_id       =   $_POST['country_id']?" and country_id = '".$_POST['country_id']."'":'';
			$state_id       =   $_POST['state_id']?" and state_id = '".$_POST['state_id']."'":'';
			/*$city_id  =   $_POST['city_id']?" and city_id = '".$_POST['city_id']."'":'';*/
		     $uquery ="select * from city where 1 $country_id $state_id";
			}
			else
			{
			$uquery ="select * from city where 1";
			}
			
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row=count($uresults);	
		$_SESSION['city'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_city() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  city WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			} else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_city(){
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_name=strtoupper($_POST['city_name']);
			
			$check_city = mysql_num_rows(mysql_query("select * from city where city_name = '".$city_name."' and state_id = '".$state_id."' and country_id = '".$country_id."'"));
			if($check_city == '0') {
						if(!$_REQUEST['id']){
					
					 $query="insert into city(city_name,country_id,state_id,status,datetime) value('".$city_name."','".$country_id."','".$state_id."','1','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=regional_hierarchy&task=addnew_city");
					}
					else
					{
						$update="update city set city_name='".$city_name."',country_id='".$country_id."',state_id='".$state_id."' where id='".$_REQUEST['id']."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=regional_hierarchy&task=addnew_city");
					}
			}
		else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=regional_hierarchy&task=addnew_city");
			}
		}
		
		function city_status(){
			$query = "update city set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=regional_hierarchy&task=show_city");
			}
		
		function city_delete(){
		
			$query="DELETE FROM city WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=regional_hierarchy&task=show_city");
		}
		
		/******************************* CITY END *********************************************/
		
		
		
		
		
		/********************************* CITY START ****************************************/
		
		function show_location(){
										
			$country_id     =   $_POST['country_id']?" and country_id = '".$_POST['country_id']."'":'';
			$state_id       =   $_POST['state_id']?" and state_id = '".$_POST['state_id']."'":'';
			$city_id        =        $_POST['city_id']?" and city_id = '".$_POST['city_id']."'":'';
		    $uquery ="select * from location where 1 $country_id $state_id $city_id order by location_name";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['location'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_location() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  location WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_location(){
			$country_id  =  $_POST['country_id'];
			$state_id    =  $_POST['state_id'];
			$city_id     =  $_POST['city_id'];
			$location_name   =  strtoupper(mysql_real_escape_string($_POST['location_name']));
			$id = $_REQUEST['id'];
			
			$check_location = mysql_num_rows(mysql_query("select * from location where location_name = '".$location_name."' and city_id = '".$city_id."' and state_id = '".$state_id."' and country_id = '".$country_id."'"));
			if($check_location=='0') {
						if(!$id){
					
					 $query="insert into location(location_name,country_id,state_id,city_id,status,datetime) value('".$location_name."','".$country_id."','".$state_id."','".$city_id."','1','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=regional_hierarchy&task=addnew_location");
					} else	{
						$update="update location set location_name='".$location_name."', country_id='".$country_id."', state_id='".$state_id."', city_id='".$city_id."' where id='".$id."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=regional_hierarchy&task=addnew_location");
					}
			} else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=regional_hierarchy&task=addnew_location");
			}
		}
		
		function location_status(){
			$query = "update location set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=regional_hierarchy&task=show_location");
		}
		
		function location_delete(){
		
			$query="DELETE FROM location WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=regional_hierarchy&task=show_location");
		}
		
		/***************************************************** CITY END **********************************************************/
				
		/***************************************************** CITY START **********************************************************/
	
		/***************************************************** CITY END **********************************************************/
		
		
	
	}
