<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class dealerClass extends DbAccess {
		public $view='';
		public $name='dealer';
		
		
		/***************************************************** Country START **********************************************************/
		
		function show(){

			if($_REQUEST['search']){	
								
				$country_id  =  $_POST['country_id']?" and country_id = '".$_POST['country_id']."'":'';
				$state_id    =   $_POST['state_id']?" and state_id = '".$_POST['state_id']."'":'';
				$city_id     =   $_POST['city_id']?" and city_id = '".$_POST['city_id']."'":'';

			    $uquery ="SELECT * FROM `dealer` WHERE 1 $country_id $state_id $city_id";
			}
			else{
				$uquery ="SELECT * FROM `dealer` WHERE 1";
			}
				$this->Query($uquery);
				$uresults = $this->fetchArray();
				$no_of_row=count($uresults);	
				$_SESSION['dealer'] = $uquery;
				$tdata=count($uresults);

				/* Paging start here */
					$page   = intval($_REQUEST['page']);
					$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
					$adjacents  = intval($_REQUEST['adjacents']);
					$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
					$tpages);//$_GET['tpages'];// 
					$tdata = floor($tdata);
					if($page<=0)  $page  = 1;
					if($adjacents<=0) $tdata?($adjacents = 4):0;
					$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
				/* Paging end here */	
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				$this->Query($query);
				$results = $this->fetchArray();		
				
				require_once("views/".$this->name."/".$this->task.".php"); 
			
				
		}
			
		function addnew(){
		
			$uquery ="SELECT * FROM `dealer` WHERE 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
				
				if($_REQUEST['id']) {
					$query_com ="SELECT * FROM `dealer` WHERE `id` = ".$_REQUEST['id'];
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					    require_once("views/".$this->name."/".$this->task.".php"); 
				}
			
		}
		
		function save(){
		
				
					if(!$_REQUEST['id']){

						$query = "INSERT INTO `dealer`(`name`, `mobile`, `email_id`,`commission`, `country_id`, `state_id`, `city_id`, `address`, `created_date`, `modified_date`) VALUES ('".$_REQUEST['name']."','".$_REQUEST['mobile']."','".$_REQUEST['email_id']."','".$_REQUEST['commission']."','".$_REQUEST['country_id']."','".$_REQUEST['state_id']."','".$_REQUEST['city_id']."','".mysql_real_escape_string($_REQUEST['address'])."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";
					
						$this->Query($query);
						$this->Execute();				
					
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=dealer&task=addnew");
					} 
					else {
								
						$update = "UPDATE `dealer` SET `name`='".$_REQUEST['name']."',`mobile`='".$_REQUEST['mobile']."',`email_id`='".$_REQUEST['email_id']."',`commission`='".$_REQUEST['commission']."',`country_id`='".$_REQUEST['country_id']."',`state_id`='".$_REQUEST['state_id']."',`city_id`='".$_REQUEST['city_id']."',`address`='".mysql_real_escape_string($_REQUEST['address'])."',`modified_date`='".date('Y-m-d H:i:s')."' WHERE id='".$_REQUEST['id']."'";

						$this->Query($update);
						$this->Execute();
					
						$_SESSION['alertmessage'] = UPDATERECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=dealer&task=show");
					}
		
		 header("location:index.php?control=dealer&task=addnew");
		}
		
				
		
		function status(){
			$query="update dealer set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=dealer&task=show");
		}

		function del_status(){

			$dealer = $_REQUEST['dealer'];
			echo $query="update dealer_payment set status=0 WHERE dealer_id='".$dealer."' AND  id='".$_REQUEST['id']."'";	
			//exit;
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			// header("location:index.php?control=dealer&task=show");
			header("location:index.php?control=dealer&task=payment_list&id=1");
		}		
		
		function delete(){
			
			$query="DELETE FROM project_plot WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$this->task="show";
			$this->view ='show';			
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			header("location:index.php?control=dealer&task=show");
		}
	

		function payment_list(){
						
				if($_REQUEST['id']) {
					$query_com ="SELECT `id`,`referred_user_id`,`commission_amount`,`customer_id`,`user_type`,`booking_date`,`commission` FROM `plot_booking` WHERE `referred_by`='dealer' AND `referred_user_id`='".$_REQUEST['id']."' AND `status`='1' UNION SELECT `id`,`dealer` as `referred_user_id`,`comm_amount` as `commission_amount`,`name` as `customer_id`,`user_type`,`created_date`,`comm_per` FROM `farmer` WHERE `dealer`='".$_REQUEST['id']."' AND `status`=1";
					//$query_com ="SELECT * FROM `plot_booking` WHERE `referred_by`='dealer' AND `referred_user_id`='".$_REQUEST['id']."' AND `status`='1'  ORDER BY `id` ASC";
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					    require_once("views/".$this->name."/".$this->task.".php"); 
				}
			
		}

		function confirm_payment(){
			$dealer_id = $_REQUEST['dealer_id'];			$plot_booking_id = $_REQUEST['plot_booking_id'];
			$commission = $_REQUEST['commission'];			$commission_amount = $_REQUEST['commission_amount'];
			$payment_type = $_REQUEST['payment_type'];		$payment_date = $_REQUEST['payment_date'];
			$remark = $_REQUEST['remark'];		//$payment_date = $_REQUEST['payment_date'];
			$payment_status = '1';		

			if($payment_type!='' && $payment_date!=''){
			$sql = "INSERT INTO `dealer_payment`(`dealer_id`, `plot_booking_id`, `commission`, `commission_amount`, `payment_type`, `type`, `payment_date`, `payment_status`, `date_created`) VALUES ('".$dealer_id."','".$plot_booking_id."','".$commission."','".$commission_amount."','".$payment_type."','1','".$payment_date."','".$payment_status."','".date('Y-m-d H:i:s')."')";
// exit;
			mysql_query($sql);

			$id = mysql_insert_id();
			$rid = date('Y').'0'.$id;
			$update = mysql_query("UPDATE `dealer_payment` SET `receipt_no`='".$rid."' WHERE `id`='".$id."'");

			$customer_id = 'HMCRM'.$_SESSION['fyear'].$plot_booking_id;
								/*======Activity Log=========*/
			$activity = "Confirm Dealer Payment of plot commission Rs. ".$commission_amount." of customer_id:- ".$customer_id;

			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`, `dealer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$dealer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

			$_SESSION['alertmessage'] = ADDNEWRECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;

			header('location:index.php?control=dealer&task=payment_list&id='.$dealer_id);

			}else{
				$_SESSION['alertmessage'] = "Please Select Payment Date and Payment Type..."; 
				$_SESSION['errorclass'] = ERRORCLASS;

				header('location:index.php?control=dealer&task=payment_list&id='.$dealer_id);
			}
		}


		function add_payment(){
			if($_REQUEST['id']) {
					$query_com ="SELECT * FROM `dealer` WHERE `id`='".$_REQUEST['id']."' AND `status`='1'";
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					    require_once("views/".$this->name."/".$this->task.".php"); 
				}
		}

		function save_payment(){
			$dealer_id = $_REQUEST['dealer_id'];			//$plot_booking_id = $_REQUEST['plot_booking_id'];
			$commission = $_REQUEST['commission'];			$commission_amount = $_REQUEST['commission_amount'];
			$payment_type = $_REQUEST['payment_type'];		$payment_date = $_REQUEST['payment_date'];
			$payment_status = $_REQUEST['payment_status']?$_REQUEST['payment_status']:'1';	
			$remark = $_REQUEST['remark'];	


			$sql = "INSERT INTO `dealer_payment`(`dealer_id`, `plot_booking_id`, `commission`, `commission_amount`, `remark`, `payment_type`, `type`, `payment_date`, `payment_status`, `date_created`) VALUES ('".$dealer_id."','".$plot_booking_id."','".$commission."','".$commission_amount."','".$remark."','".$payment_type."','2','".$payment_date."','".$payment_status."','".date('Y-m-d H:i:s')."')";

			mysql_query($sql);

			$id = mysql_insert_id();
			$rid = date('Y').'0'.$id;
			$update = mysql_query("UPDATE `dealer_payment` SET `receipt_no`='".$rid."' WHERE `id`='".$id."'");


			$activity = $commission_amount." rupees is paid to dealer";

			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`, `dealer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$dealer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

			$_SESSION['alertmessage'] = ADDNEWRECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;

			header('location:index.php?control=dealer&task=payment_list&id='.$dealer_id);
		}
		
	}
