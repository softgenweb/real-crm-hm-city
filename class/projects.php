<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class projectClass extends DbAccess {
		public $view='';
		public $name='project';
		
		
		/***************************************************** Country START **********************************************************/
		
		function show(){	
			if($_SESSION['utype']=='Administrator')
			{
				$uquery ="SELECT * FROM `project` WHERE 1 ORDER BY `project_name` ASC";
				$this->Query($uquery);
				$uresults = $this->fetchArray();
				$no_of_row=count($uresults);	
				$_SESSION['country'] = $uquery;
				$tdata=count($uresults);

				/* Paging start here */
					$page   = intval($_REQUEST['page']);
					$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
					$adjacents  = intval($_REQUEST['adjacents']);
					$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
					$tpages);//$_GET['tpages'];// 
					$tdata = floor($tdata);
					if($page<=0)  $page  = 1;
					if($adjacents<=0) $tdata?($adjacents = 4):0;
					$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
				/* Paging end here */	
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				$this->Query($query);
				$results = $this->fetchArray();		
				
				require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else
				{
					header('location:index.php?msg=1');	
				}	
		}
			
		function addnew(){
		if($_SESSION['utype']=='Administrator')
			{	
			$uquery ="SELECT * FROM `project` WHERE 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
				
				if($_REQUEST['id']) {
					$query_com ="SELECT * FROM `project` WHERE `id` = ".$_REQUEST['id'];
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					    require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}
			else
				{
					header('location:index.php?msg=1');	
				}	
		}
		
		function save(){
		
			$project_name = strtoupper($_POST['project_name']);	
			$status = '1';
	
				$check_project = mysql_num_rows(mysql_query("select * from project where project_name like '%".$project_name."%'"));
			
			if($check_project == '0') {
				
					if(!$_REQUEST['id']){
						$query = "INSERT INTO `project`(`project_name`,`status`,`datetime`) VALUES ('".strtoupper($project_name)."','1','".date('Y-m-d H:i:s')."')";
						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=addnew");
					} 
					else {
								
						$update="UPDATE `project` SET `project_name`='".strtoupper($project_name)."' where id='".$_REQUEST['id']."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=show");
					}
			}
			else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project&task=addnew");
			}
			
		 header("location:index.php?control=project&task=addnew");
		}
		
				
		
		function status(){
			$query="update project set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=project&task=show");
		}		
		
		function delete(){
			
			$query="DELETE FROM project WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$this->task="show";
			$this->view ='show';			
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			header("location:index.php?control=project&task=show");
		}
	
		
		/***************************************************** Country END **********************************************************/
		
		/***************************************************** STATE START**************/	
		function show_block(){
										
			$project_id = $_POST['project_id']?" and project_id = '".$_POST['project_id']."'":'';
		
		    $uquery ="SELECT * FROM `block` WHERE 1 $project_id ORDER BY `block_name` ASC";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['city'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_block() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  block WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_block(){
			$project_id  =  $_POST['project_id'];
			
			$block_name   =  mysql_real_escape_string($_POST['block_name']);
			$id = $_REQUEST['id'];
	
			
			$check_block = mysql_num_rows(mysql_query("SELECT `block_name` FROM `block` WHERE `block_name` like '".$block_name."' and project_id = '".$project_id."'"));

			if($check_block=='0') {
						if(!$id){
					
					$query="INSERT INTO `block`(`project_id`, `block_name`, `datetime`) VALUES ('".$project_id."','".strtoupper($block_name)."','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=project&task=addnew_block");
					} else	{
						echo $update="UPDATE `block` SET `project_id`='".$project_id."',`block_name`='".strtoupper($block_name)."' WHERE `id`='".$id."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=addnew_block");
					}
			} else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project&task=addnew_block");
			}
		}
		
		function block_status(){
			$query = "update block set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=project&task=show_block");
		}
		
		function block_delete(){
		
			$query="DELETE FROM block WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=project&task=show_block");
		}


		function show_plot(){
										
			$plot_id     =   $_POST['plot_id']?" and plot_id = '".$_POST['plot_id']."'":'';
		
		    $uquery ="SELECT * FROM `plot` WHERE 1 $plot_id ";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['city'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : 10000;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_plot() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  plot WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_plot(){
			// $project_id  =  $_POST['project_id'];
			
			$plot_name   =  mysql_real_escape_string($_POST['plot_name']);
			$id = $_REQUEST['id'];
			
			$check_plot = mysql_num_rows(mysql_query("SELECT `plot_name` FROM `plot` WHERE `plot_name` = '".$plot_name."' "));
			if($check_plot=='0') {
						if(!$id){
					
					$query="INSERT INTO `plot`( `plot_name`, `datetime`) VALUES ('".strtoupper($plot_name)."','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=project&task=addnew_plot");
					} else	{
						echo $update="UPDATE `plot` SET `plot_name`='".strtoupper($plot_name)."' WHERE `id`='".$id."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    	$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=addnew_plot");
					}
			} else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    	$_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project&task=addnew_plot");
			}
		}
		
		function plot_status(){
			$query = "update plot set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=project&task=show_plot");
		}
		
		function plot_delete(){
		
			$query="DELETE FROM plot WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=project&task=show_plot");
		}

		
	
		function show_area_measurement(){
										
			// $project_id     =   $_POST['project_id']?" and project_id = '".$_POST['project_id']."'":'';
		
		    $uquery ="SELECT * FROM `area_measurement` WHERE 1  ORDER BY `dimenstion` ASC";
			
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			$no_of_row=count($uresults);	
			$_SESSION['city'] = $uquery;
			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */
			$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew_area_measurement() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `area_measurement` WHERE  id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
				
		function save_area_measurement(){
			$dimenstion  =  $_REQUEST['length'].'X'.$_REQUEST['width'];

			$square_feet = $_REQUEST['square_feet'];
			$square_meter = $_REQUEST['square_meter'];
			$id = $_REQUEST['id'];
			
			$check_area = mysql_num_rows(mysql_query("SELECT `dimenstion` FROM `area_measurement` WHERE `dimenstion` = '".$dimenstion."'"));
			if($check_area=='0') {
						if(!$id){
					
					$query="INSERT INTO `area_measurement`(`square_feet`, `square_meter`, `dimenstion`, `created_date`, `modified_date`) VALUES ('".$square_feet."','".$square_meter."','".$dimenstion."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";	
					$this->Query($query);	
					$this->Execute();
					//$_SESSION['msg'] = '1';
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=project&task=addnew_area_measurement");
					} else	{
					
						 $update="UPDATE `area_measurement` SET `square_feet`='".$square_feet."',`square_meter`='".$square_meter."',`dimenstion`='".$dimenstion."',`modified_date`='".date("Y-m-d H:i:s")."' WHERE `id`='".$id."'";
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
				    $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=project&task=addnew_area_measurement");
					}
			} else {
				        // $_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
				    $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=project&task=addnew_area_measurement");
			}
		}
		
		function area_measurement_status(){
			$query = "UPDATE `area_measurement` SET `status`='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			//$this->show();	
			header("location:index.php?control=project&task=show_area_measurement");
		}
		
		function area_measurement_delete(){
		
			$query="DELETE FROM area_measurement WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			//$this->show();	
			header("location:index.php?control=project&task=show_area_measurement");
		}

	}
