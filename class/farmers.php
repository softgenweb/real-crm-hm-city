<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class farmerClass extends DbAccess {
		public $view='';
		public $name='farmer';
		
		function show(){

			if($_REQUEST['search']){							
				$country_id  =  $_POST['country_id']?" and country_id = '".$_POST['country_id']."'":'';
				$state_id    =   $_POST['state_id']?" and state_id = '".$_POST['state_id']."'":'';
				$city_id     =   $_POST['city_id']?" and city_id = '".$_POST['city_id']."'":'';

			    $uquery ="SELECT * FROM `farmer` WHERE 1 $country_id $state_id $city_id";
			}
			else{
				$uquery ="SELECT * FROM `farmer` WHERE 1";
			}
				$this->Query($uquery);
				$uresults = $this->fetchArray();
				$no_of_row=count($uresults);	
				$_SESSION['farmer'] = $uquery;
				$tdata=count($uresults);

				/* Paging start here */
					$page   = intval($_REQUEST['page']);
					$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
					$adjacents  = intval($_REQUEST['adjacents']);
					$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
					$tpages);//$_GET['tpages'];// 
					$tdata = floor($tdata);
					if($page<=0)  $page  = 1;
					if($adjacents<=0) $tdata?($adjacents = 4):0;
					$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
				/* Paging end here */	
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				$this->Query($query);
				$results = $this->fetchArray();		
				
				require_once("views/".$this->name."/".$this->task.".php"); 
						
		}
		
		function show_farmer_plot(){

			if($_REQUEST['search']){	
									
				$project_id       =   $_POST['project_id']?" and project_id = '".$_POST['project_id']."'":'';
				$block_id       =   $_POST['block_id']?" and block_id ='".$_POST['block_id']."'":'';	

			    $uquery ="SELECT * FROM `project_plot` WHERE `status`= 1 AND farmer_id = '".$_REQUEST['id']."' $project_id $block_id ";
			}
			else{
				$uquery ="SELECT * FROM `project_plot` WHERE `status`= 1 AND farmer_id = '".$_REQUEST['id']."'";
			}
				$this->Query($uquery);
				$uresults = $this->fetchArray();
				$no_of_row=count($uresults);	
				$_SESSION['farmer_plot'] = $uquery;
				$tdata=count($uresults);

				/* Paging start here */
					$page   = intval($_REQUEST['page']);
					$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
					$adjacents  = intval($_REQUEST['adjacents']);
					$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
					$tpages);//$_GET['tpages'];// 
					$tdata = floor($tdata);
					if($page<=0)  $page  = 1;
					if($adjacents<=0) $tdata?($adjacents = 4):0;
					$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
				/* Paging end here */	
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				$this->Query($query);
				$results = $this->fetchArray();		
				
				require_once("views/".$this->name."/".$this->task.".php"); 
						
		}
			
		function addnew(){
		
			$uquery ="SELECT * FROM `farmer` WHERE `status`= 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
				
				if($_REQUEST['id']) {
					$query_com ="SELECT * FROM `farmer` WHERE `status`= 1 AND `id` = ".$_REQUEST['id'];
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					    require_once("views/".$this->name."/".$this->task.".php"); 
				}
			
		}
		function show_detail(){

			$query_com ="SELECT * FROM `farmer` WHERE `status`= 1 AND `id` = ".$_REQUEST['id'];
			$this->Query($query_com);

			$results = $this->fetchArray();
		    require_once("views/".$this->name."/".$this->task.".php"); 
				
		}
		function save(){
					
					if(!$_REQUEST['id'])
					{

					$query = "INSERT INTO `farmer`(`title`,`name`, `mobile`, `email_id`,`dob`, `hectare`, `acre`, `sqft`, `gata_no`,  `country_id`, `state_id`, `city_id`, `address`,`pincode`, `amount`, `dealer`, `comm_per`, `comm_amount`,`emp_id`,`created_date`,`modified_by`) VALUES ('".$_REQUEST['title']."','".$_REQUEST['name']."','".$_REQUEST['mobile']."','".$_REQUEST['email_id']."','".$_REQUEST['dob']."','".$_REQUEST['hectare']."','".$_REQUEST['acre']."','".$_REQUEST['sqft']."','".strtoupper($_REQUEST['gata_no'])."','".$_REQUEST['country_id']."','".$_REQUEST['state_id']."','".$_REQUEST['city_id']."','".mysql_real_escape_string($_REQUEST['address'])."','".$_REQUEST['pincode']."','".$_REQUEST['amount']."','".$_REQUEST['dealer']."','".$_REQUEST['comm_per']."','".$_REQUEST['comm_amount']."','".$_SESSION['adminid']."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')";
					
					
						$exe_query = mysql_query($query);
						$id = mysql_insert_id();
						$activity = "Add new farmer ".$_REQUEST['name'];
						$log =mysql_query("INSERT INTO `activity_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

						if (!is_dir('media/farmer/'.$id)) {
						// dir doesn't exist, make it
						mkdir('media/farmer/'.$id,0777,true);
						}
						$folder = "media/farmer/".$id.'/';

						$id_proof = $_FILES['id_proof']['tmp_name'];						
						$short_id_proof = $_FILES['id_proof']['name'];						
						move_uploaded_file($id_proof , $folder.$id.$short_id_proof);


						$photo = $_FILES['photo']['tmp_name'];						
						$short_photo = $_FILES['photo']['name'];						
						move_uploaded_file($photo , $folder.$id.$short_photo);



						$update_attechment = "update farmer set id_proof='".($short_id_proof?$id.'/'.$id.$short_id_proof:'')."',photo='".($short_photo?$id.'/'.$id.$short_photo:'')."' where id='".$id."'";
						$exe_attechment = mysql_query($update_attechment);

						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=farmer&task=addnew");
					} 
					else
					{
						$id = $_REQUEST['id'];	
						$old_file = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$id."'"));	
						
						$update = "UPDATE `farmer` SET `title`='".$_REQUEST['title']."',`name`='".$_REQUEST['name']."',`mobile`='".$_REQUEST['mobile']."',`email_id`='".$_REQUEST['email_id']."',`dob`='".$_REQUEST['dob']."',`hectare`='".$_REQUEST['hectare']."',`acre`='".$_REQUEST['acre']."',`sqft`='".$_REQUEST['sqft']."',`gata_no`='".strtoupper($_REQUEST['gata_no'])."',`country_id`='".$_REQUEST['country_id']."',`state_id`='".$_REQUEST['state_id']."',`city_id`='".$_REQUEST['city_id']."',`address`='".mysql_real_escape_string($_REQUEST['address'])."',`pincode`='".$_REQUEST['pincode']."',`amount`='".$_REQUEST['amount']."',`dealer`='".$_REQUEST['dealer']."',`comm_per`='".$_REQUEST['comm_per']."',`comm_amount`='".$_REQUEST['comm_amount']."', `modified_date`='".date('Y-m-d H:i:s')."',`modified_by`= '".$_SESSION['adminid']."' WHERE id='".$_REQUEST['id']."'";

						$sql_update = mysql_query($update);

						$activity = "Update farmer detail of ".$_REQUEST['name'];
						$log =mysql_query("INSERT INTO `activity_log`( `system_ip`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
						
						if (!is_dir('media/farmer/'.$id)) {
						// dir doesn't exist, make it
						mkdir('media/farmer/'.$id ,0777,true);
						}
						$folder = "media/farmer/".$id.'/';
						$unlink = "media/farmer/";

						$id_proof = $_FILES['id_proof']['tmp_name'];						
						$short_id_proof = $_FILES['id_proof']['name'];	
						if($short_id_proof){	
							unlink($unlink.$old_file['id_proof']);
							}
						move_uploaded_file($id_proof , $folder.$id.$short_id_proof);
						

						$photo = $_FILES['photo']['tmp_name'];						
						$short_photo = $_FILES['photo']['name'];
						if($short_photo){	
							unlink($unlink.$old_file['photo']);
							}							
						move_uploaded_file($photo , $folder.$id.$short_photo);

				 	$update_attechment = "update farmer set id_proof='".($short_id_proof?$id.'/'.$id.$short_id_proof:$old_file['id_proof'])."',photo='".($short_photo?$id.'/'.$id.$short_photo:$old_file['photo'])."' where id='".$id."'";
				
						$exe_attechment = mysql_query($update_attechment);

							 $_SESSION['alertmessage'] = UPDATERECORD; 
							 $_SESSION['errorclass'] = SUCCESSCLASS;
						
						header("location:index.php?control=farmer&task=show");
					}		
					
				}
		
		function link_plot(){
			
				if($_REQUEST['search']) {	
				$_SESSION['farmer_id'] = $_REQUEST['farmer_id'];		
				
				$project_id = $_POST['project_id']?" and project_id = '".$_POST['project_id']."'":'';
				$block_id   = $_POST['block_id']?" and block_id ='".$_POST['block_id']."'":'';	
				//$farmer_id   =   $_POST['farmer_id']?" and farmer_id ='".$_POST['farmer_id']."'":'';		
				
				$uquery ="select * from project_plot where status = 1 AND farmer_id IN ('".$_POST['farmer_id']."' ,'') $project_id $block_id ";

				$uqueryexcl ="select * from farmer_plot where  status = 1 AND farmer_id = '".$_POST['farmer_id']."' $project_id $block_id";

				$_SESSION['farmer_plot'] = $uqueryexcl;
		
		
				$this->Query($uquery);
				$results = $this->fetchArray();
				$no_of_row=count($results);	
				$tdata=count($results);
				
				require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else
				{
				 require_once("views/".$this->name."/".$this->task.".php"); 
				}
			
		}
		
		
		function save_link_plot(){
			
			$farmer_id=$_POST['farmer_id'];
			$project_id=$_POST['project_id'];
			$block_id=$_POST['block_id'];
			$project_id=$_POST['project_id'];
			$plot_id=$_POST['plot_id'];
			//print_r($plot_id);exit;
			$area_id=$_POST['area_id'];
			$project_plot_id=$_POST['project_plot_id'];
			$link=$_POST['link'];
			//echo count($link);exit;
			if(!$_REQUEST['id']){
								
				for($j=0;$j<count($link);$j++){
					
					$del_old_link = mysql_query("DELETE FROM `farmer_plot` WHERE `project_id`='".$project_id[$j]."' and `block_id`='".$block_id[$j]."' and `project_plot_id`='".$link[$j]."' and `farmer_id`='".$farmer_id[$j]."'");

					
					$del_farmer = mysql_query("update `project_plot` set farmer_id = '' where id = '".$link[$j]."'");
								
			   $query="insert into farmer_plot(farmer_id,project_plot_id,project_id,block_id,emp_id,created_date,status) value('".$farmer_id[$j]."','".$link[$j]."','".$project_id[$j]."','".$block_id[$j]."','".$_SESSION['adminid']."','".date('Y-m-d H:i:s')."','1')";
			 
				$exe_query = mysql_query($query);

				$activity = "Plot Linking to farmer";
				$log =mysql_query("INSERT INTO `activity_log`( `system_ip`, `customer_id`, `project_plot_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$farmer_id[$j]."','".$project_plot_id[$j]."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");					
					
					$update="update project_plot set farmer_id = '".$farmer_id[$j]."' where id = '".$link[$j]."'";
			 		 
					$this->Query($update);
					$this->Execute();
				
				}
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=farmer&task=link_plot");
			}
			
		
		}
			
	function show_plot_registry(){
		
			$id = $_REQUEST['id']?" AND `farmer_id`='".$_REQUEST['id']."' ":'';
			 $uquery ="SELECT * FROM `plot_registry` WHERE `status`= 1 $id";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
				
			require_once("views/".$this->name."/".$this->task.".php"); 
		
		}	
			
	function plot_registry(){
		
			$uquery ="SELECT * FROM `plot_registry` WHERE `status`=1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
				
				if($_REQUEST['id']) {
					$query_com ="SELECT * FROM `plot_registry` WHERE `status`= 1 AND `id` = ".$_REQUEST['id'];
					$this->Query($query_com);

					$results = $this->fetchArray();
				    require_once("views/".$this->name."/".$this->task.".php"); 
				}
				else {
					
					    require_once("views/".$this->name."/".$this->task.".php"); 
				}
			
		}	


	function save_registry(){

		$plot_booking = $_REQUEST['plot_booking'];  $customer_id = $_REQUEST['customer_id'];
		$registry_by = $_REQUEST['registry_by']; 	$farmer_id = $_REQUEST['farmer_id'];
		$ghata_no = $_REQUEST['ghata_no'];  		$registry_date = $_REQUEST['registry_date'];
		$plot_size = $_REQUEST['plot_size'];  		$total_area = $_REQUEST['sqft'];
		$remain_area = $total_area-$plot_size;  	$id = $_REQUEST['id'];
		$farmer = $_REQUEST['farmer_name'];

		if(!$id){
			if($remain_area >= 0){
		 	$query = "INSERT INTO `plot_registry`(`plot_booking_id`, `customer_id`, `registry_by`, `farmer_id`, `gata_no`, `registry_date`, `total_area`, `plot_size`, `remaining_area`, `created_by`, `date_created`) VALUES ('".$plot_booking."','".$customer_id."','".$registry_by."','".$farmer_id."','".$ghata_no."','".$registry_date."','".$total_area."','".$plot_size."','".$remain_area."','".$_SESSION['adminid']."','".date('Y-m-d H:i:s')."')";
		
			mysql_query($query);

			$update = mysql_query("UPDATE `farmer` SET `remain_area`='".$remain_area."' WHERE `id`='".$farmer_id."'");

			$activity = "plot registry of Farmer (".$farmer.") with Gata No. ".$ghata_no;
			$log =mysql_query("INSERT INTO `activity_log`( `system_ip`, `customer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$customer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");	

			$_SESSION['alertmessage'] = ADDNEWRECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			header("location:index.php?control=farmer&task=plot_registry");
		}else{
			$_SESSION['alertmessage'] = "You can not place registry more than plot size !!!"; 
			$_SESSION['errorclass'] = ERRORCLASS;
			header("location:index.php?control=farmer&task=plot_registry");
		}

		}else{

			$query = "UPDATE `plot_registry` SET `registry_date`='".$registry_date."' WHERE `id`='".$id."'";

			mysql_query($query);

			// $update = mysql_query("UPDATE `farmer` SET `remain_area`='".$remain_area."' WHERE `id`='".$farmer_id."'");

			$activity = "Update plot registry of Farmer (".$farmer.") with Gata No. ".$ghata_no;
			$log = mysql_query("INSERT INTO `activity_log`( `system_ip`, `customer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$customer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

			$_SESSION['alertmessage'] = UPDATERECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			header("location:index.php?control=farmer&task=plot_registry");
		}

	}	



	function delete_registry(){
		$fid = $_REQUEST['fid'];

		$farmer = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$fid."'"));

		$remove_query=mysql_query("UPDATE `plot_registry` SET `status`=0 WHERE `farmer_id`='".$fid."' AND `status`=1");
		$update_query=mysql_query("UPDATE `farmer` SET `remain_area`='' WHERE `id`='".$fid."' ");



			$activity = "Delete All plot registry of Farmer (".$farmer['name'].") with Gata No. ".$farmer['gata_no'];
			$log = mysql_query("INSERT INTO `activity_log`( `system_ip`, `customer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$fid."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");	
			
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=farmer&task=show_plot_registry&id=".$fid."");
		}	

	function status(){
			$query="update farmer set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=farmer&task=show");
		}		
		
		function delete(){
			
			$query="DELETE FROM farmer WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$this->task="show";
			$this->view ='show';			
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = ERRORCLASS;
			
			header("location:index.php?control=farmer&task=show");
		}


		function farmer_ledger(){
			
			$query_com ="SELECT * FROM `farmer_payment` WHERE `status`=1 AND  `farmer_id` = ".$_REQUEST['id'];
			$this->Query($query_com);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}

		function add_payment(){
			// if($_REQUEST['id']){
			$query_com ="SELECT * FROM `farmer_payment` WHERE `status`= 1  AND `farmer_id` = '".$_REQUEST['id']."' ORDER BY `id` DESC LIMIT 1";

		// }
			$this->Query($query_com);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php");
		}
		function edit_payment(){
			// if($_REQUEST['id']){
			$rid = $_REQUEST['editid'];
			 $query_com ="SELECT * FROM `farmer_payment` WHERE `status`= 1  AND `farmer_id` = '".$_REQUEST['id']."' AND 	`id`='".$rid."'";

		// }
			$this->Query($query_com);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php");
		}

		function save_payment(){
			$farmer_id = $_REQUEST['farmer_id'];			
			// $commission = $_REQUEST['commission'];			
			$amount = $_REQUEST['total_amount'];
			$paid_amount = $_REQUEST['paid_amount'];

			$payment_type = $_REQUEST['payment_type'];	

			if($payment_type!='Cheque'){
				$remain_amount = $amount-$paid_amount;
			}else{
				$remain_amount = $amount;
			}	
			$payment_date = $_REQUEST['payment_date'];
			$cheque_no = $_REQUEST['cheque_no'];
			$bank_name = $_REQUEST['bank_name'];
			$remark = $_REQUEST['remark'];

			$cheque_status = ($payment_type=='Cheque')?'0':'1';

			
			$chk = mysql_fetch_array(mysql_query("SELECT * FROM `farmer_payment` WHERE `farmer_id`='".$farmer_id."' AND `payment_type`='".$payment_type."' AND `payment_date`='".$payment_date."' AND `paid_amount`='".$paid_amount."' AND `cheque_no`='".$cheque_no."' AND `status`= 1 ORDER BY `id` DESC LIMIT 1"));
				
			if($chk>0){
				$_SESSION['alertmessage'] = STEPERRORMSG; 
				$_SESSION['errorclass'] = ERRORCLASS;
				}else{


			$sql = "INSERT INTO `farmer_payment`(`farmer_id`, `amount`, `paid_amount`, `remain_amount`, `payment_type`, `payment_date`, `cheque_no`, `cheque_status`, `bank_name`, `remark`, `date_created`) VALUES ('".$farmer_id."','".$amount."','".$paid_amount."','".$remain_amount."','".$payment_type."','".$payment_date."','".$cheque_no."','".$cheque_status."','".$bank_name."','".$remark."','".date('Y-m-d H:i:s')."')";
		if($payment_type!='Cheque'){
				
			mysql_query($sql);


			$id = mysql_insert_id();
			$rid = date('Ym').'0'.$id;

			$update = mysql_query("UPDATE `farmer_payment` SET `receipt_no`='".$rid."' WHERE `id`='".$id."'");
			}
			/*=======Payment Record========*/
			$sql1 = mysql_query("INSERT INTO `farmer_payment_receipt`(`farmer_id`, `payment_type`, `txn_no`, `amount`, `bank`, `remark`, `status`, `creation_date`, `date`) VALUES ('".$farmer_id."', '".$payment_type."', '".$cheque_no."', '".$paid_amount."', '".$bank_name."', '".$remark."', '1', '".$payment_date."', '".date('Y-m-d H:i:s')."')");
			/*============================*/

			

			$activity = $paid_amount." rupees is paid to farmer";
			
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`, `customer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$farmer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
			$_SESSION['alertmessage'] = ADDNEWRECORD; 	
			$_SESSION['errorclass'] = SUCCESSCLASS;
		}
			
		

			header('location:index.php?control=farmer&task=farmer_ledger&id='.$farmer_id);
		}

function edit_payment_detail(){

	$amount = $_REQUEST['total_amount'];
	$paid_amount = $_REQUEST['paid_amount'];
	$payment_type = $_REQUEST['payment_type'];
	$payment_date = $_REQUEST['payment_date'];
	$cheque_no = $_REQUEST['cheque_no'];
	$bank_name = $_REQUEST['bank_name'];
	$remark = $_REQUEST['remark'];
	$id = $_REQUEST['id'];
	$farmer_id = $_REQUEST['farmer_id'];


	$remain_amount = $amount-$paid_amount;


	$query = "UPDATE `farmer_payment` SET `amount`='".$amount."',`paid_amount`='".$paid_amount."',`remain_amount`='".$remain_amount."',`payment_type`='".$payment_type."',`payment_date`='".$payment_date."',`cheque_no`='".$cheque_no."',`bank_name`='".$bank_name."',`remark`='".$remark."' WHERE `id`='".$id."' AND `farmer_id`='".$farmer_id."'";
	 // $query = "UPDATE `farmer_payment` SET `payment_type`='".$payment_type."',`payment_date`='".$payment_date."',`cheque_no`='".$cheque_no."',`bank_name`='".$bank_name."',`remark`='".$remark."' WHERE `id`='".$id."' AND `farmer_id`='".$farmer_id."'";

		
	$this->Query($query);
	$this->Execute();	

	$activity = "Update farmer ledger amount(".$paid_amount."/-)";
			
	$log = mysql_query("INSERT INTO `activity_log`(`system_ip`, `customer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$farmer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

	$_SESSION['alertmessage'] = UPDATERECORD; 
	$_SESSION['errorclass'] = SUCCESSCLASS;

	header('location:index.php?control=farmer&task=farmer_ledger&id='.$farmer_id);

}



	function del_status(){

			$fid = $_REQUEST['fid'];
			$id = $_REQUEST['id'];
			$query="UPDATE `farmer_payment_receipt` SET `status`=0 WHERE `id`='".$_REQUEST['id']."' AND `farmer_id`='".$fid."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=farmer&task=farmer_ledger&id=".$fid);
		}		


/*============Cahnge Check Histroy===========*/

/*	function cheque_history(){


			$uquery ="SELECT * FROM `plot_ledger` WHERE `status`=1 AND `payment_type`='cheque' AND `customer_id`='".$_REQUEST['cid']."' ORDER BY `id` ASC";


			$this->Query($uquery);
			$uresults = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}*/



	/*function change_chq_status(){


		$id = $_REQUEST['id'];
		$farmer_id = $_REQUEST['farmer_id'];
		$balance_amt = $_REQUEST['balance_amt'];
		$cheque_status = $_REQUEST['cheque_status'];

	if($cheque_status=='1'){
		$query = mysql_query("UPDATE `farmer_payment` SET `remain_amount`='".$balance_amt."', `cheque_status`='".$cheque_status."' WHERE `farmer_id`='".$farmer_id."' AND `id`='".$id."'");
	}else{
		$query = mysql_query("UPDATE `farmer_payment` SET `cheque_status`='".$cheque_status."' WHERE `farmer_id`='".$farmer_id."' AND `id`='".$id."'");
	}

		$stat = ($cheque_status=='1'?'Clear':($cheque_status=='2'?'Bounce':'Cancel'));

		$activity = "Change status of cheque(".$cheque_no.") is:".$stat." of farmer: ".$farmer_id;

		$log = mysql_query("INSERT INTO `activity_log`( `system_ip`, `customer_id`, `activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$farmer_id."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");

		$_SESSION['alertmessage'] = UPDATERECORD; 
		$_SESSION['errorclass'] = SUCCESSCLASS;


		header("location:index.php?control=farmer&task=farmer_ledger&id=$farmer_id");

			}	
*/
/*===============New Method to clear farmer cheque===========*/
	function change_chq_status(){


		$id = $_REQUEST['id'];
		$farmer_id = $_REQUEST['farmer_id'];
		$balance_amt = $_REQUEST['balance_amt'];
		$cheque_status = $_REQUEST['cheque_status'];
		$cheque_status_date = $_REQUEST['cheque_status_date'];

		
		$gen_amt = mysql_fetch_array(mysql_query("SELECT * FROM `farmer_payment` WHERE `farmer_id`='".$farmer_id."' AND `status`=1 ORDER BY `id` DESC LIMIT 1"));

		$old_amt = mysql_fetch_array(mysql_query("SELECT * FROM `farmer` WHERE `id`='".$farmer_id."' AND `status`=1"));
		
		$chq = mysql_fetch_array(mysql_query("SELECT * FROM `farmer_payment_receipt` WHERE `farmer_id`='".$farmer_id."' AND `id`='".$id."' AND `payment_type`='Cheque'"));

		$gen_amt1 = $gen_amt['remain_amount']?$gen_amt['remain_amount']:$old_amt['amount'];
		
		$remain_amt  = $gen_amt1-$chq['amount'];

	if($cheque_status=='1'){
		$sql = "INSERT INTO `farmer_payment`(`farmer_id`, `amount`, `paid_amount`, `remain_amount`, `payment_type`, `payment_date`, `cheque_no`, `cheque_status`, `bank_name`, `remark`, `date_created`) VALUES ('".$chq['farmer_id']."','".$gen_amt1."','".$chq['amount']."','".$remain_amt."','".$chq['payment_type']."','".$chq['creation_date']."','".$chq['txn_no']."','1','".$chq['bank']."','".$chq['remark']."','".date('Y-m-d H:i:s')."')";
		
				
			mysql_query($sql);


			$ledid = mysql_insert_id();
			$rid = date('Ym').'0'.$ledid;

			$update = mysql_query("UPDATE `farmer_payment` SET `receipt_no`='".$rid."', `cheque_status_date`='".$cheque_status_date."' WHERE `id`='".$ledid."'");

			$change =  mysql_query("UPDATE `farmer_payment_receipt` SET `cheque_status`='1' WHERE `id`='".$id."'");
			}else{
				
			$sql = "INSERT INTO `farmer_payment`(`farmer_id`, `amount`, `paid_amount`, `remain_amount`, `payment_type`, `payment_date`, `cheque_no`, `cheque_status`, `cheque_status_date`, `bank_name`, `remark`, `date_created`) VALUES ('".$chq['farmer_id']."','".$gen_amt1."','".$chq['amount']."','".$gen_amt1."','".$chq['payment_type']."','".$chq['creation_date']."','".$chq['txn_no']."','2','".$chq['bank']."','".$cheque_status_date."','".$chq['remark']."','".date('Y-m-d H:i:s')."')";
		
				
			mysql_query($sql);	
			$change =  mysql_query("UPDATE `farmer_payment_receipt` SET `cheque_status`='2' WHERE `id`='".$id."'");
			}

		$_SESSION['alertmessage'] = UPDATERECORD; 
		$_SESSION['errorclass'] = SUCCESSCLASS;


		header("location:index.php?control=farmer&task=farmer_ledger&id=$farmer_id");

			}	







	}
