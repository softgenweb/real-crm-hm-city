<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class tax_masterClass extends DbAccess {
		public $view='';
		public $name='tax_master';
		
		
		/***************************************************** Project START **********************************************************/
		
		function show(){	
			$uquery ="select * from tax_master where 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row = count($uresults);	
			$_SESSION['tax'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
			
		}
		
		function show_interest(){	
			$uquery ="SELECT * FROM `interest_master` WHERE 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row = count($uresults);	
			$_SESSION['interest'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
			
		}
		
		function show_emi(){	
			$uquery ="select * from emi_master where 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row = count($uresults);	
			//$_SESSION['project'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
				
		}		
		function show_late_payment(){	
			$uquery ="SELECT * FROM `late_payment` WHERE 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row = count($uresults);	
			//$_SESSION['project'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
				
		}
			
		function addnew(){
		
			$uquery ="select * from tax_master where 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM tax_master WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
			
		}
					
		function addnew_interest(){
		
			$uquery ="select * from interest_master where 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM interest_master WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
			
		}
		

		function addnew_emi(){
		
			$uquery ="SELECT * FROM `emi_master` WHERE 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `emi_master` WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
			
		}		

		function addnew_late_payment(){
		
			$uquery ="SELECT * FROM `late_payment` WHERE `status`= 1";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `late_payment` WHERE `id` = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
			
		}
		
		function save(){
		
			$gst = $_REQUEST['gst'];			
			$sgst = $_REQUEST['sgst'];
			$cgst = $_REQUEST['cgst'];
			$tax = $_REQUEST['gst_type'];
			
		
		         $id = $_REQUEST['id'];
				{
					if(!$id){
						$query = "INSERT INTO `tax_master`(`tax`, `gst`, `sgst`, `cgst`,`date_time`) VALUES ('".$tax."','".$gst."','".$cgst."','".$sgst."','".date('Y-m-d H:i:s')."')";
						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						 $_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew");
					} 
					else {
								
						$update="UPDATE `tax_master` SET `tax`='".$tax."',`gst`='".$gst."',`cgst`='".$cgst."',`sgst`='".$sgst."' where id='".$id."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=show");
					}
			}
			
			
		 header("location:index.php?control=tax_master&task=addnew");
		}


		function save_interest(){
		
			$name = $_REQUEST['name'];			
	         $id = $_REQUEST['id'];
	         $check = mysql_num_rows(mysql_query("SELECT * FROM `interest_master` WHERE `name` ='".$name."'"));
				if($check == '0') 	{
					if(!$id){
						$query = "INSERT INTO `interest_master`(`name`, `date_time`) VALUES ('".$name."','".date('Y-m-d H:i:s')."')";

						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew_interest");
					} 
					else {
								
						$update="UPDATE `interest_master` SET `name`='".$name."' where id='".$id."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew_interest");
					}
			}else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=tax_master&task=addnew_interest");
			}
			
			
		 header("location:index.php?control=tax_master&task=addnew_interest");
		}
		

		function save_emi(){
		
			$name = $_REQUEST['name'];			

		    $id = $_REQUEST['id'];

		    $check = mysql_num_rows(mysql_query("SELECT * FROM `emi_master` WHERE `name` ='".$name."'"));
				if($check == '0') {

					if(!$id){


						$query = "INSERT INTO `emi_master`(`name`, `date_time`) VALUES ('".$name."','".date('Y-m-d H:i:s')."')";

						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew_emi");
					} 
					else {
								
						$update="UPDATE `emi_master` SET `name`='".$name."' where id='".$id."'";
						
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew_emi");
					}
			
			}else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=tax_master&task=addnew_emi");
			}
			
		 header("location:index.php?control=tax_master&task=addnew_emi");
		}
		
		function save_late_payment(){
		
			$month = $_REQUEST['month'];			
			$type = $_REQUEST['type'];			
			$amount = $_REQUEST['amount'];			

		    $id = $_REQUEST['id'];

		    $check = mysql_num_rows(mysql_query("SELECT * FROM `late_payment` WHERE `month` ='".$month."' AND (`type`='".$type."' || `amount`='".$amount."' ) "));
				if($check == '0') {

					if(!$id){


					echo	$query = "INSERT INTO `late_payment`(`month`, `type`, `amount`, `date_created`, `date_modify`) VALUES ('".$month."','".$type."','".$amount."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";


						$this->Query($query);
						$this->Execute();				
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = ADDNEWRECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew_late_payment");
					} 
					else {
								
					echo 	$update="UPDATE `late_payment` SET `month`='".$month."', `type`='".$type."', `amount`='".$amount."' where id='".$id."'";
						exit;
						$this->Query($update);
						$this->Execute();
						//$_SESSION['msg'] = '1';
						$_SESSION['alertmessage'] = UPDATERECORD; 
						$_SESSION['errorclass'] = SUCCESSCLASS;
						header("location:index.php?control=tax_master&task=addnew_late_payment");
					}
			
			}else {
				         //$_SESSION['msg'] = '0';
						 $_SESSION['alertmessage'] = DUPLICATE; 
						 $_SESSION['errorclass'] = ERRORCLASS;
						 header("location:index.php?control=tax_master&task=addnew_late_payment");
			}
			
		 header("location:index.php?control=tax_master&task=addnew_late_payment");
		}
		
				
		function status(){
			$query="update tax_master set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=tax_master&task=show");
		}		
				
		function interest_status(){
			$query="update interest_master set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=tax_master&task=show_interest");
		}		
				
		function emi_status(){
			$query="update emi_master set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			$_SESSION['alertmessage'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=tax_master&task=show_emi");
		}		
		
		function delete(){
		
			$query="DELETE FROM tax_master WHERE id in (".$_REQUEST['id'].")";	
			$this->Query($query);
			$this->Execute();	
			$this->task="show";
			$this->view ='show';			
			$_SESSION['alertmessage'] = DELETE; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			header("location:index.php?control=tax_master&task=show");
		}
			
		/********************************** Country END ***************************/
		
	}
