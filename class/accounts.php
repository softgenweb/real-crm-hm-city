<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class accountClass extends DbAccess {
		public $view='';
		public $name='account';

		
		function show(){	
		$uquery ="select * from account where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		 $query =$uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show.php"); 
		}
		

		function addnew() {
		if($_REQUEST['id']) {
			$query_com ="SELECT * FROM account WHERE id = ".$_REQUEST['id'];
			$this->Query($query_com);

			$results = $this->fetchArray();

			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		else {
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		     }
		}

		function save() {
		
		$acc_holder_name = $_REQUEST['acc_holder_name'];
		$account_no = $_REQUEST['account_no'];
		$ifsc_code = strtoupper($_REQUEST['ifsc_code']);
		$bank_name = $_REQUEST['bank_name'];
		$branch_name = $_REQUEST['branch_name'];
		$date_time = date('d-m-Y H:i:s');
		$status = 1;
		
		if(!$_REQUEST['id']) {
			
			 $query = "INSERT INTO account(acc_holder_name,account_no,ifsc_code,bank_name,branch_name,date_time,status) values('".$acc_holder_name."','".$account_no."','".$ifsc_code."','".$bank_name."','".$branch_name."','".$date_time."','".$status."') ";
			$this->Query($query);
			$this->Execute();
			
			$activity = "Add new account of : ".$bank_name."(".$account_no.")";			
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
			
			$_SESSION['error'] = ADDNEWRECORD; 
        $_SESSION['errorclass'] = ERRORCLASS;
		    	header("location:index.php?control=account");
		}
		else {
			 $query = "UPDATE account SET acc_holder_name = '".$acc_holder_name."', account_no = '".$account_no."', ifsc_code = '".$ifsc_code."', bank_name = '".$bank_name."', branch_name = '".$branch_name."', date_time = '".$date_time."' WHERE id='".$_REQUEST['id']."' ";
			$this->Query($query);
			$this->Execute();
			
			$activity = "Edit account of : ".$bank_name."(".$account_no.")";			
			$log = mysql_query("INSERT INTO `activity_log`(`system_ip`,`activity`, `activity_date`, `user_name`) VALUES ('".$_SESSION['sys_ip']."','".$activity."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."')");
			
			$_SESSION['error'] = UPDATERECORD; 
            $_SESSION['errorclass'] = ERRORCLASS;
			//$this->show();
			header("location:index.php?control=account");
		     }
					
		}

		function status(){
		$query="update account set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$_SESSION['error'] = STATUS; 
		$_SESSION['errorclass'] = ERRORCLASS;
		//$this->show();
		header("location:index.php?control=account");
		
		}
		
		
		function delete(){
		
		$query="DELETE FROM account WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		     $_SESSION['error'] = DELETE; 
            $_SESSION['errorclass'] = ERRORCLASS;
		//$this->show();
			header("location:index.php?control=account");
		
		}
		 
		
		
	}
