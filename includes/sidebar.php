<?php session_start(); 
   // print_r($_SESSION);
if($_SESSION['utype']=='Administrator' && $_SESSION['post_id']=='top'){
   ?>
   <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
      <div class="background_layer">
      </div>
      <section class="sidebar">
         <div class="user-panel">
            <div class="pull-left image">
               <img src="images/user_avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
               <p><?php echo $_SESSION['admin_name']; ?></p>
               <a href="index.php?control=user&task=changepassword"><i class="fa fa-circle text-success"></i><b>Change Password</b></a>
            </div>
         </div>
         <ul class="sidebar-menu">
            <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
               <a href="#">
                  <i class="fa fa-indent" aria-hidden="true"></i>
                  <span>Dashboard</span>
               </a>
            </li>

            <li class="treeview <?php if(($_REQUEST['control'] == 'regional_hierarchy') || ($_REQUEST['control'] == 'project') || ($_REQUEST['control'] == 'tax_master')){ ?>active<?php } ?>">
               <a href="javascript:;">  
                  <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Master</span>
               </a>
               <ul class="treeview-menu">
                  <!--<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew">Add Country</a></li>-->
                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show">View Country</a></li>
                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_state'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_state">View State</a></li>
                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_city'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_city">View City</a></li>
                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_location'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_location">View Location</a></li>
                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show">View Tax</a></li>
                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show_interest'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show_interest">View Interest</a></li>
                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show_emi'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show_emi">View EMI</a></li>
                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show_late_payment'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show_late_payment">View Late Payment</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show">View Project</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show_block'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show_block">View Block</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show_plot'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show_plot">View Plot</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show_area_measurement'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show_area_measurement">View Area </a></li>


               </ul>
            </li>

            <li class="treeview <?php if($_REQUEST['control']=='project_plot' ){ ?>active<?php } ?>">
               <a href="index.php?control=project_plot&task=show"> <i class="fa fa-area-chart" aria-hidden="true"></i><span>Project Plot Management</span></a>
            </li>

            <li class="treeview <?php if($_REQUEST['control']=='farmer' ){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-users" aria-hidden="true"></i><span>Farmer Management</span></a>
               <ul class="treeview-menu">
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=addnew">Add Farmer</a></li>
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show">View Farmer</a></li>
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show_plot_registry'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show_plot_registry">Farmer Plot Registry</a></li>
               </ul>
            </li>

            <li class="treeview <?php if(($_REQUEST['control']=='expense' && $_REQUEST['task']=='show') || ($_REQUEST['control']=='expense' && $_REQUEST['task']=='addnew') || ($_REQUEST['control']== 'expense_detail' && $_REQUEST['task']=='show') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='cr_expense') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='cr_expense_addnew')  || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='dr_expense') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='dr_expense_addnew' )|| ($_REQUEST['control']== 'account' && $_REQUEST['task']=='show') || ($_REQUEST['control']== 'account_detail' && $_REQUEST['task']=='show')){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Expense Management</span></a>
               <ul class="treeview-menu">
                 <li <?php if((($_REQUEST['control']== 'account') || ($_REQUEST['control']== 'account_detail')) && ($_REQUEST['task']=='show')){ ?>class="active"<?php } ?>><a href="index.php?control=account&task=show">View Account </a></li>
                 <li <?php if(($_REQUEST['control']== 'expense' || $_REQUEST['control']=='expense_detail') && ($_REQUEST['task']=='show' || $_REQUEST['task']=='addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=expense&task=show">View Expense </a></li>
                 <li <?php if($_REQUEST['control']=='daybook' && ($_REQUEST['task']=='cr_expense' || $_REQUEST['task']=='cr_expense_addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=cr_expense">Cr Expense</a></li>
                 <li <?php if($_REQUEST['control']=='daybook' && ($_REQUEST['task']=='dr_expense' || $_REQUEST['task']=='dr_expense_addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=dr_expense">Dr Expense</a></li>
              </ul>
           </li>

           <li class="treeview <?php if($_REQUEST['control']=='dealer' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Dealer Management</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=addnew">Add Dealer</a></li>
               <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=show">View Dealer</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='hrm_post' || $_REQUEST['control']=='hrm_registration'){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-plus" aria-hidden="true"></i><span>HRM</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='hrm_post' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_post&task=show">Employee Post</a></li>
               <li <?php if($_REQUEST['control']=='hrm_registration' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_registration&task=show">Employee Registration</a></li>
               <li <?php if($_REQUEST['control']=='hrm_registration' && $_REQUEST['task']=='show_salary'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_registration&task=show_salary">Employee Salary Management</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='plot_booking' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-th" aria-hidden="true"></i><span>Plot Booking</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='step1'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=step1">Add Booking</a></li>
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=show">View Booking</a></li>
               <!-- <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='cheque_detail'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=cheque_detail">Cheque Status</a></li> -->
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='ledger'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=ledger">Plot Payment Ledger</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if(($_REQUEST['control']=='report' || $_REQUEST['control']=='daybook') && ($_REQUEST['task']=='show')){ ?>active<?php } ?>">

            <a href="javascript:;"> <i class="fa fa-bookmark" aria-hidden="true"></i><span>Reports</span></a>
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='daybook'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=daybook">Day Book</a></li> -->
               <!-- <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">All Reports</a></li> -->
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity">Activity Log</a></li>
               <li <?php if($_REQUEST['control']=='daybook' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=show">Day Book</a></li>
            </ul>
         </li>
         
      </ul>
   </section>
</aside>
<?php }elseif($_SESSION['utype']=='Administrator'){
   ?>
   <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
      <div class="background_layer">
      </div>
      <section class="sidebar">
         <div class="user-panel">
            <div class="pull-left image">
               <img src="images/user_avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
               <p><?php echo $_SESSION['admin_name']; ?></p>
               <a href="index.php?control=user&task=changepassword"><i class="fa fa-circle text-success"></i><b>Change Password</b></a>
            </div>
         </div>
         <ul class="sidebar-menu">

            <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
               <a href="#">
                  <i class="fa fa-indent" aria-hidden="true"></i>
                  <span>Dashboard</span>
               </a>
            </li>

            <li class="treeview <?php if(($_REQUEST['control'] == 'regional_hierarchy') || ($_REQUEST['control'] == 'project') || ($_REQUEST['control'] == 'tax_master') ){ ?>active<?php } ?>">
               <a href="javascript:;">
                  <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Master</span>
               </a>
               <ul class="treeview-menu">

                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show">View Country</a></li>

                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_state'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_state">View State</a></li>

                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_city'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_city">View City</a></li>

                  <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_location'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_location">View Location</a></li>

                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show">View Tax</a></li>

                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show_interest'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show_interest">View Interest</a></li>

                  <!-- <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show_emi'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show_emi">View EMI</a></li> -->

                  <li <?php if($_REQUEST['control']=='tax_master' && $_REQUEST['task']=='show_late_payment'){ ?>class="active"<?php } ?>><a href="index.php?control=tax_master&task=show_late_payment">View Late Payment</a></li>

                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show">View Project</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show_block'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show_block">View Block</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show_plot'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show_plot">View Plot</a></li>
                  <li <?php if($_REQUEST['control']== 'project' && $_REQUEST['task']=='show_area_measurement'){ ?>class="active"<?php } ?>><a href="index.php?control=project&task=show_area_measurement">View Area </a></li>



               </ul>
            </li>

            <li class="treeview <?php if($_REQUEST['control']=='project_plot' ){ ?>active<?php } ?>">
               <a href="index.php?control=project_plot&task=show"> <i class="fa fa-area-chart" aria-hidden="true"></i><span>Project Plot Management</span></a>
            </li>

            <li class="treeview <?php if($_REQUEST['control']=='farmer' ){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-users" aria-hidden="true"></i><span>Farmer Management</span></a>
               <ul class="treeview-menu">
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=addnew">Add Farmer</a></li>
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show">View Farmer</a></li>
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show_plot_registry'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show_plot_registry">Farmer Plot Registry</a></li>
               </ul>
            </li>

            <li class="treeview <?php if(($_REQUEST['control']=='expense' && $_REQUEST['task']=='show') || ($_REQUEST['control']=='expense' && $_REQUEST['task']=='addnew') || ($_REQUEST['control']== 'expense_detail' && $_REQUEST['task']=='show') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='cr_expense') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='cr_expense_addnew')  || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='dr_expense') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='dr_expense_addnew' )|| ($_REQUEST['control']== 'account' && $_REQUEST['task']=='show') || ($_REQUEST['control']== 'account_detail' && $_REQUEST['task']=='show')){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Expense Management</span></a>
               <ul class="treeview-menu">
                 <li <?php if((($_REQUEST['control']== 'account') || ($_REQUEST['control']== 'account_detail')) && ($_REQUEST['task']=='show')){ ?>class="active"<?php } ?>><a href="index.php?control=account&task=show">View Account </a></li>
                 <li <?php if(($_REQUEST['control']== 'expense' || $_REQUEST['control']=='expense_detail') && ($_REQUEST['task']=='show' || $_REQUEST['task']=='addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=expense&task=show">View Expense </a></li>
                 <li <?php if($_REQUEST['control']=='daybook' && ($_REQUEST['task']=='cr_expense' || $_REQUEST['task']=='cr_expense_addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=cr_expense">Cr Expense</a></li>
                 <li <?php if($_REQUEST['control']=='daybook' && ($_REQUEST['task']=='dr_expense' || $_REQUEST['task']=='dr_expense_addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=dr_expense">Dr Expense</a></li>
              </ul>
           </li>

           <li class="treeview <?php if($_REQUEST['control']=='dealer' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Dealer Management</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=addnew">Add Dealer</a></li>
               <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=show">View Dealer</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='hrm_post' || $_REQUEST['control']=='hrm_registration'){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-plus" aria-hidden="true"></i><span>HRM</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='hrm_post' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_post&task=show">Employee Post</a></li>
               <li <?php if($_REQUEST['control']=='hrm_registration' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_registration&task=show">Employee Registration</a></li>
               <li <?php if($_REQUEST['control']=='hrm_registration' && $_REQUEST['task']=='show_salary'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_registration&task=show_salary">Employee Salary Management</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='plot_booking' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-th" aria-hidden="true"></i><span>Plot Booking</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='step1'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=step1">Add Booking</a></li>
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=show">View Booking</a></li>
               <!-- <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='cheque_detail'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=cheque_detail">Cheque Status</a></li> -->
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='ledger'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=ledger">Plot Payment Ledger</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='daybook' && $_REQUEST['task']=='show'){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-bookmark" aria-hidden="true"></i><span>Reports</span></a>
            <ul class="treeview-menu">
               <!--   <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='daybook'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=daybook">Day Book</a></li>
                  <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">All Reports</a></li>-->
                  <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity">Activity Log</a></li>
                  <li <?php if($_REQUEST['control']=='daybook' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=show">Day Book</a></li>
               </ul>
            </li>
         </ul>
      </section>
   </aside>
<?php }elseif($_SESSION['utype']=='Employee'){ ?>
   <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
      <div class="background_layer">
      </div>
      <section class="sidebar">
         <div class="user-panel">
            <div class="pull-left image">
               <img src="images/user_avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
               <p><?php echo $_SESSION['admin_name']; ?></p>
               <a href="index.php?control=user&task=changepassword"><i class="fa fa-circle text-success"></i><b>Change Password</b></a>
            </div>
         </div>
         <ul class="sidebar-menu">
            <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
               <a href="#">
                  <i class="fa fa-indent" aria-hidden="true"></i>
                  <span>Dashboard</span>
               </a>
            </li>
            <li class="treeview <?php if($_REQUEST['control']=='project_plot' ){ ?>active<?php } ?>">
               <a href="index.php?control=project_plot&task=show"> <i class="fa fa-area-chart" aria-hidden="true"></i><span>Project Plot Management</span></a>
            </li>

            <li class="treeview <?php if($_REQUEST['control']=='farmer' ){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-users" aria-hidden="true"></i><span>Farmer Management</span></a>
               <ul class="treeview-menu">
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=addnew">Add Farmer</a></li>
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show">View Farmer</a></li>
                  <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show_plot_registry'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show_plot_registry">Farmer Plot Registry</a></li>
               </ul>
            </li>
            <li class="treeview <?php if($_REQUEST['control']=='dealer' ){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Dealer Management</span></a>
               <ul class="treeview-menu">
                  <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=addnew">Add Dealer</a></li>
                  <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=show">View Dealer</a></li>
               </ul>
            </li>
            <li class="treeview <?php if($_REQUEST['control']=='plot_booking' ){ ?>active<?php } ?>">
               <a href="javascript:;"> <i class="fa fa-th" aria-hidden="true"></i><span>Plot Booking</span></a>
               <ul class="treeview-menu">
                  <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='step1'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=step1">Add Booking</a></li>
                  <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=show">View Booking</a></li>
                  <!-- <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='cheque_detail'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=cheque_detail">Cheque Status</a></li> -->
                  <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='ledger'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=ledger">Plot Payment Ledger</a></li>
               </ul>
            </li>

         </ul>
      </section>
   </aside>
<?php }elseif($_SESSION['utype']=='Report' && $_SESSION['adminid']=='3'){ ?>
   <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
      <div class="background_layer">
      </div>
      <section class="sidebar">
         <div class="user-panel">
            <div class="pull-left image">
               <img src="images/user_avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
               <p><?php echo $_SESSION['admin_name']; ?></p>
               <a href="index.php?control=user&task=changepassword"><i class="fa fa-circle text-success"></i><b>Change Password</b></a>
            </div>
         </div>
         <ul class="sidebar-menu">
            <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
               <a href="#">
                  <i class="fa fa-indent" aria-hidden="true"></i>
                  <span>Dashboard</span>
               </a>
            </li>
         <li class="treeview <?php if($_REQUEST['control']=='project_plot' ){ ?>active<?php } ?>">
            <a href="index.php?control=project_plot&task=show"> <i class="fa fa-area-chart" aria-hidden="true"></i><span>Project Plot Management</span></a>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='farmer' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-users" aria-hidden="true"></i><span>Farmer Management</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show">View Farmer</a></li>
               <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show_plot_registry'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show_plot_registry">Farmer Plot Registry</a></li>
            </ul>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='plot_booking' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-th" aria-hidden="true"></i><span>Plot Booking</span></a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=show">View Booking</a></li>
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='ledger'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=ledger">Plot Payment Ledger</a></li>
            </ul>
         </li>
   </ul>
</section>
</aside>
<?php }elseif($_SESSION['utype']=='Report'){ ?>
   <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
      <div class="background_layer">
      </div>
      <section class="sidebar">
         <div class="user-panel">
            <div class="pull-left image">
               <img src="images/user_avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
               <p><?php echo $_SESSION['admin_name']; ?></p>
               <a href="index.php?control=user&task=changepassword"><i class="fa fa-circle text-success"></i><b>Change Password</b></a>
            </div>
         </div>
         <ul class="sidebar-menu">
            <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
               <a href="#">
                  <i class="fa fa-indent" aria-hidden="true"></i>
                  <span>Dashboard</span>
               </a>
            </li>
         <!--<li class="treeview <?php if($_REQUEST['control'] == 'account_detail' || $_REQUEST['control']== 'account'){ ?>active<?php } ?>">
            <a href="javascript:;">  
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Master</span>
            </a>
            <ul class="treeview-menu">
    
              
               <li <?php if((($_REQUEST['control']== 'account') || ($_REQUEST['control']== 'account_detail')) && ($_REQUEST['task']=='show')){ ?>class="active"<?php } ?>><a href="index.php?control=account&task=show">View Account </a></li>
              
            </ul>
         </li>-->
         <li class="treeview <?php if($_REQUEST['control']=='project_plot' ){ ?>active<?php } ?>">
            <a href="index.php?control=project_plot&task=show"> <i class="fa fa-area-chart" aria-hidden="true"></i><span>Project Plot Management</span></a>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='farmer' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-users" aria-hidden="true"></i><span>Farmer Management</span></a>
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=addnew">Add Farmer</a></li> -->
               <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show">View Farmer</a></li>
               <li <?php if($_REQUEST['control']=='farmer' && $_REQUEST['task']=='show_plot_registry'){ ?>class="active"<?php } ?>><a href="index.php?control=farmer&task=show_plot_registry">Farmer Plot Registry</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='dealer' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Dealer Management</span></a>
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=addnew">Add Dealer</a></li> -->
               <li <?php if($_REQUEST['control']=='dealer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=dealer&task=show">View Dealer</a></li>
            </ul>
         </li>
         
         <!--  <li class="treeview <?php if($_REQUEST['control']=='hrm_post' || $_REQUEST['control']=='hrm_registration'){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-plus" aria-hidden="true"></i><span>HRM</span></a>
            
            <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='hrm_post' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_post&task=show">Employee Post</a></li>
            <li <?php if($_REQUEST['control']=='hrm_registration' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=hrm_registration&task=show">Employee Registration</a></li>
            </ul>
         </li> -->
         <li class="treeview <?php if($_REQUEST['control']=='plot_booking' ){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-th" aria-hidden="true"></i><span>Plot Booking</span></a>
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='step1'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=step1">Add Booking</a></li> -->
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=show">View Booking</a></li>
               <!-- <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='cheque_detail'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=cheque_detail">Cheque Status</a></li> -->
               <li <?php if($_REQUEST['control']=='plot_booking' && $_REQUEST['task']=='ledger'){ ?>class="active"<?php } ?>><a href="index.php?control=plot_booking&task=ledger">Plot Payment Ledger</a></li>
            </ul>
         </li>
         
         <li class="treeview <?php if(($_REQUEST['control']=='expense' && $_REQUEST['task']=='show') || ($_REQUEST['control']=='expense' && $_REQUEST['task']=='addnew') || ($_REQUEST['control']== 'expense_detail' && $_REQUEST['task']=='show') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='cr_expense') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='cr_expense_addnew')  || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='dr_expense') || ($_REQUEST['control']== 'daybook' && $_REQUEST['task']=='dr_expense_addnew' )|| ($_REQUEST['control']== 'account' && $_REQUEST['task']=='show') || ($_REQUEST['control']== 'account_detail' && $_REQUEST['task']=='show')){ ?>active<?php } ?>">
            <a href="javascript:;"> <i class="fa fa-user-secret" aria-hidden="true"></i><span>Expense Management</span></a>
            <ul class="treeview-menu">
              <li <?php if((($_REQUEST['control']== 'account') || ($_REQUEST['control']== 'account_detail')) && ($_REQUEST['task']=='show')){ ?>class="active"<?php } ?>><a href="index.php?control=account&task=show">View Account </a></li>
              <li <?php if(($_REQUEST['control']== 'expense' || $_REQUEST['control']=='expense_detail') && ($_REQUEST['task']=='show' || $_REQUEST['task']=='addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=expense&task=show">View Expense </a></li>
              <li <?php if($_REQUEST['control']=='daybook' && ($_REQUEST['task']=='cr_expense' || $_REQUEST['task']=='cr_expense_addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=cr_expense">Cr Expense</a></li>
              <li <?php if($_REQUEST['control']=='daybook' && ($_REQUEST['task']=='dr_expense' || $_REQUEST['task']=='dr_expense_addnew')){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=dr_expense">Dr Expense</a></li>
           </ul>
        </li>
        <li class="treeview <?php if(($_REQUEST['control']=='report' || $_REQUEST['control']=='daybook') && ($_REQUEST['task']=='show')){ ?>active<?php } ?>">
         <a href="javascript:;"> <i class="fa fa-bookmark" aria-hidden="true"></i><span>Reports</span></a>
         <ul class="treeview-menu">
            <!-- <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='daybook'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=daybook">Day Book</a></li> -->
            <!-- <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">All Reports</a></li> -->
            <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity">Activity Log</a></li>
            <li <?php if($_REQUEST['control']=='daybook'  && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=daybook&task=show">Day Book</a></li>
         </ul>
      </li>
   </ul>
</section>
</aside>
<?php } ?>

