<?php session_start();?>


  <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
  <div class="background_layer"></div>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <div class="user-panel">
            <div class="pull-left image">
              <img src="images/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>User name</p>
              <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
          </div>
    
    
      <ul class="sidebar-menu">
        
        <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
          <a href="#">
             <i class="fa fa-indent" aria-hidden="true"></i>
<span>Dashboard</span>
          </a>
        </li>
        
        
        
        <li class="treeview <?php if($_REQUEST['control']=='regional_hierarchy' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Country Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew">Add Country</a></li>
            <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show">View Country</a></li>
            
          </ul>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='regional_hierarchy' && ($_REQUEST['task']=='addnew_state' || $_REQUEST['task']=='show_state')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>State Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew_state'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew_state">Add State</a></li>
            <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_state'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_state">View State</a></li>
            
          </ul>
        </li>
		
        <li class="treeview <?php if($_REQUEST['control']=='regional_hierarchy' && ($_REQUEST['task']=='addnew_city' || $_REQUEST['task']=='show_city')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i><span>City Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew_city'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew_city">Add City</a></li>
            <li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_city'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_city">View City</a></li>
            
          </ul>
        </li>
     
       
        
        <li class="treeview <?php if($_REQUEST['control']=='category_master' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-users" aria-hidden="true"></i>
<span>Category Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=addnew">Add Category</a></li>
            <li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=show">View Category</a></li>
            
          </ul>
        </li>

		<li class="treeview <?php if($_REQUEST['control']=='category_master' && ($_REQUEST['task']=='addnew_sub_category' || $_REQUEST['task']=='show_sub_category')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-pie-chart" aria-hidden="true"></i>
<span>Sub Category Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='addnew_sub_category'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=addnew_sub_category">Add Sub Category</a></li>
            <li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='show_sub_category'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=show_sub_category">View Sub Category</a></li>
            
          </ul>
        </li>
		
		<li class="treeview <?php if($_REQUEST['control']=='feature_master' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-user" aria-hidden="true"></i>
<span>Feature Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='feature_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=feature_master&task=addnew">Add Feature</a></li>
            <li <?php if($_REQUEST['control']=='feature_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=feature_master&task=show">View Feature</a></li>
            
          </ul>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='feature_master' && ($_REQUEST['task']=='addnew_link_feature' || $_REQUEST['task']=='show_link_feature')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-link" aria-hidden="true"></i>
            <span>Link Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='feature_master' && $_REQUEST['task']=='addnew_link_feature'){ ?>class="active"<?php } ?>><a href="index.php?control=feature_master&task=addnew_link_feature">Link Feature</a></li>
           <!--  <li <?php if($_REQUEST['control']=='feature_master' && $_REQUEST['task']=='show_link_feature'){ ?>class="active"<?php } ?>><a href="#">View Link Feature</a></li> -->
            
          </ul>
        </li>
 			
		<li class="treeview <?php if($_REQUEST['control']=='seller_master'  && ($_REQUEST['task']=='show' || $_REQUEST['task']=='show_seller_product')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i>
<span>Seller Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='seller_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=seller_master&task=show">View Seller Profile</a></li>
        <!-- <li <?php if($_REQUEST['control']=='seller_master' && $_REQUEST['task']=='show_seller_product'){ ?>class="active"<?php } ?>><a href="index.php?control=seller_master&task=show_seller_product">View Seller Product</a></li>-->
            
          </ul>
        </li>

        <li class="treeview <?php if($_REQUEST['control']=='buyer_master'  && ($_REQUEST['task']=='show' || $_REQUEST['task']=='show_buyer_product')){ ?>active<?php } ?>">
                  <a href="javascript:;">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span>Buyer Master</span>
                  </a>
                  
                  <ul class="treeview-menu">
                    <li <?php if($_REQUEST['control']=='buyer_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=buyer_master&task=show">View Buyer Profile</a></li>
               <!--  <li <?php if($_REQUEST['control']=='buyer_master' && $_REQUEST['task']=='show_buyer_product'){ ?>class="active"<?php } ?>><a href="index.php?control=buyer_master&task=show_buyer_product">View Buyer Product</a></li>
                    -->
                  </ul>
                </li>

          <li class="treeview <?php if($_REQUEST['control']=='advertisement_master' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
          <i class="fa fa-users" aria-hidden="true"></i>
          <span>Advertisement Master</span>
          </a>

          <ul class="treeview-menu">
          <li <?php if($_REQUEST['control']=='advertisement_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=advertisement_master&task=addnew">Add Advertisement</a></li>
          <li <?php if($_REQUEST['control']=='advertisement_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=advertisement_master&task=show">View Advertisement</a></li>

          </ul>
          </li>

           <li class="treeview <?php if($_REQUEST['control']=='product_master' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i><span>Product Master</span>
          </a>
          
          <ul class="treeview-menu">
            <!-- <li <?php if($_REQUEST['control']=='product_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=product_master&task=addnew">Add Product</a></li> -->
            <li <?php if($_REQUEST['control']=='product_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=product_master&task=show">View Product</a></li>
            
          </ul>
        </li>
        <li class="treeview <?php if($_REQUEST['control']=='prioritize_management' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='addnew_prioritize_management')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Prioritize Management</span>
          </a>
          
          <ul class="treeview-menu">            
            <li <?php if($_REQUEST['control']=='prioritize_management' && $_REQUEST['task']=='addnew_prioritize_management'){ ?>class="active"<?php } ?>><a href="index.php?control=prioritize_management&task=addnew_prioritize_management">View Prioritize Management</a></li>
            
          </ul>
        </li>
        <li class="treeview <?php if($_REQUEST['control']=='you_like' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-pie-chart" aria-hidden="true"></i><span>You May Like Management</span>
          </a>
          
          <ul class="treeview-menu">            
            <li <?php if($_REQUEST['control']=='you_like' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=you_like&task=show">You May Like Management</a></li>
            
          </ul>
        </li>

        <li class="treeview <?php if($_REQUEST['control']=='reported_product' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-pie-chart" aria-hidden="true"></i><span>Reported Products</span>
          </a>
          
          <ul class="treeview-menu">            
            <li <?php if($_REQUEST['control']=='reported_product' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=reported_product&task=show">View Reported Product</a></li>
            
          </ul>
        </li>

</ul>
</section>
<!-- /.sidebar -->
</aside>



