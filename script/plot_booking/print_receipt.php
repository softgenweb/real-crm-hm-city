<?php 
include('../../configuration.php');
 $result=mysql_fetch_array(mysql_query("SELECT * FROM `receipt` WHERE `customer_id`='".$_REQUEST['cid']."' AND `project_plot_id`!='' AND `id`='".$_REQUEST['id']."'"));

 $plot = mysql_fetch_array(mysql_query("SELECT * FROM `plot_booking` WHERE `customer_id`='".$result['customer_id']."' AND `id`='".$result['project_plot_id']."'"));

 function convertRsToWord($str)
 {
 $number = $str;
   $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
      if($points!='')
      {
  return $result . "Rupees  " . $points . " Paise";
      }
      else
      {
        return $result . "Rupees  Only."; 
      }
 }
 function GetFlatNumber($id)
  { 
//echo  $a="select * from flat_no_mgmt where  id='".$id."'";
  $Query = mysql_query("select * from flat_no_mgmt where  id='".$id."'");
  $Runquery = mysql_fetch_array($Query);
  return $Runquery['flat_number'];
  }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Receipt</title>
	<meta charset="utf-8">
</head>
<body>

<script type="text/javascript">
	window.print();
</script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="../../template/realcrm/bootstrap/css/bootstrap.min.css">
<style type="text/css">
	.rate_box{
		width:80%;
		border:#CCCCCC solid 1px;
		border-radius: 5px; 
		padding:5px;
	}
   button.btn.btn-primary a{
    color: white;
   }
  td a{
    margin: 2px;
   }
   .alert.alert-info{
      background-color: #247ab5 !important;
      border-color: #247ab5 !important;
   }
   table tr td{
    border-top: unset !important;
    padding: unset !important;
    font-family: "Verdana";
    /*font-size: 16px;*/
   }
</style>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">             

          
          
                <div class="box-body">
                 <div>
                 <div class="divoverflow col-md-10 col-md-offset-1">
  
   
<table class="table no_pad "  style="margin-bottom: 30px;">
	<tr><td colspan="4" style="text-align: center;"><strong>Customer Copy</strong></td></tr>
  <tr>
    <td colspan="4"><img src="../../media/hmcity.png" width="150"></td>
  </tr>
  <tr>
    <td rowspan="7" width="350"><strong>HM Green City </strong><br> 8/883/2,<br>Raheem Nagar Road, <br>Khurram Nagar, <br>Opp Fatimi Masjid,<br>Lucknow - 226022 </td>
    
  </tr>
  <tr>
    <td><strong>Receipt No:</strong></td>
    <td><?php echo $result['id']; ?></td>
    
  </tr>
  <tr>
    <td><strong>Customer ID:</strong></td>
    <td><?php echo $result['customer_id']; ?></td>
    
  </tr>
  <tr>
    <td><strong>Project/Block:</strong></td>
    <td><?php echo $plot['project_name'].' ('.$plot['block_name'].')'; ?></td>
    
  </tr>
  <tr>
    <td><strong>Plot/Area:</strong></td>
    <td><?php echo $plot['plot_name'].' ('.$plot['area_dimension'].')'; ?></td>
    
  </tr>
  <tr>
    <td><strong>Booking Date:</strong></td>
    <td><?php $book_date = date_create($plot['booking_date']); echo   date_format($book_date,'jS M y'); ?></td>
  </tr>

  <tr>
    <td><strong>Receipt Date:</strong></td>
    <td><?php  $receipt_date = date_create($result['creation_date']); echo date_format($receipt_date,'jS M y'); ?></td>
  </tr>

  <tr>
    <td colspan="4"><br></td>
  </tr>
  <tr>
    <td>Received with thanks from <?php echo "<strong>".$plot['title']."</strong> ".$plot['applicant_name']. ($plot['co_applicant_name']?(" / ".$plot['co_applicant_name']):''); ?><br/><strong> by <?php echo ($result['paiment_type']=="cheque") ? (ucfirst($result['paiment_type'].' No. : </strong>'.$result['txn_no'])):('<strong>'.ucfirst($result['paiment_type']).'</strong>') ; ?></strong></td>
    <td><strong>Cheque Date:</strong><br>
     <?php if($result['paiment_type']!='cash'){ ?> <strong>Bank Name:</strong><?php } ?></td>
    <td><?php $receipt_date = date_create($result['creation_date']); echo date_format($receipt_date,'jS M y'); ?><br/><?php echo strtoupper($result['bank']); ?>
    </td>
  </tr>
  <tr>
    <td><div class="rate_box"> <strong>&#8377;</strong> <mark><?php echo $result['amount'].'/-'; ?></mark><br>
      In Words: <mark><?php echo ucwords(convertRsToWord($result['amount'])); ?></mark></div>
    </td>
    <td></td>
  </tr>
	<tr style="text-align: center;">
  		<td colspan="5"><hr><em style="color: red;">*This is Computer Generated Receipt, Doesn't require signature. </em></td>
	</tr>
</table> 
<div class="clearfix"><hr style="border-style: dotted;border-color: #000;"></div>  
<table class="table no_pad" style="margin-top: 30px;">
	<tr><td colspan="4" style="text-align: center;"><strong>Office Copy</strong></td></tr>
  <tr>
    <td colspan="4"><img src="../../media/hmcity.png" width="150"></td>
  </tr>
  <tr>
    <td rowspan="7" width="350"><strong>HM Green City </strong><br> 8/883/2,<br>Raheem Nagar Road, <br>Khurram Nagar, <br>Opp Fatimi Masjid,<br>Lucknow - 226022 </td>
    
  </tr>
  <tr>
    <td><strong>Receipt No:</strong></td>
    <td><?php echo $result['id']; ?></td>
    
  </tr>
  <tr>
    <td><strong>Customer ID:</strong></td>
    <td><?php echo $result['customer_id']; ?></td>
    
  </tr>
  <tr>
    <td><strong>Project/Block:</strong></td>
    <td><?php echo $plot['project_name'].' ('.$plot['block_name'].')'; ?></td>
    
  </tr>
  <tr>
    <td><strong>Plot/Area:</strong></td>
    <td><?php echo $plot['plot_name'].' ('.$plot['area_dimension'].')'; ?></td>
    
  </tr>
  <tr>
    <td><strong>Booking Date:</strong></td>
    <td><?php $book_date = date_create($plot['booking_date']); echo   date_format($book_date,'jS M y'); ?></td>
  </tr>

  <tr>
    <td><strong>Receipt Date:</strong></td>
    <td><?php  $receipt_date = date_create($result['creation_date']); echo date_format($receipt_date,'jS M y'); ?></td>
  </tr>

  <tr>
    <td colspan="4"><br></td>
  </tr>
  <tr>
    <td>Received with thanks from <?php echo "<strong>".$plot['title']."</strong> ".$plot['applicant_name']. ($plot['co_applicant_name']?(" / ".$plot['co_applicant_name']):''); ?><br/><strong> by <?php echo ($result['paiment_type']=="cheque") ? (ucfirst($result['paiment_type'].' No. : </strong>'.$result['txn_no'])):('<strong>'.ucfirst($result['paiment_type']).'</strong>') ; ?></strong></td>
    <td><strong>Cheque Date:</strong><br>
     <?php if($result['paiment_type']!='cash'){ ?> <strong>Bank Name:</strong><?php } ?></td>
    <td><?php $receipt_date = date_create($result['creation_date']); echo date_format($receipt_date,'jS M y'); ?><br/><?php echo strtoupper($result['bank']); ?>
    </td>
  </tr>
  <tr>
    <td> <div class="rate_box">  <strong>&#8377;</strong> <mark><?php echo $result['amount'].'/-'; ?></mark><br>
      In Words: <mark><?php echo ucwords(convertRsToWord($result['amount'])); ?></mark></div>
    </td>
    <td></td>
  </tr>
	<tr style="text-align: center;">
  		<td colspan="5"><hr><em style="color: red;">*This is Computer Generated Receipt, Doesn't require signature. </em></td>
	</tr>
</table>

                  </div>
                </div><!-- table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            
            <!--================ Second Table ================-->
        
 
          </div><!-- /.row -->
  
        
   

   

</body>
</html>